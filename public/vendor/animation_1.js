(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"animation_1_atlas_", frames: [[848,287,145,74],[0,0,477,366],[266,734,223,177],[0,588,264,155],[479,330,367,216],[743,0,281,285],[479,0,262,328],[276,548,311,184],[589,548,290,169],[589,719,260,153],[0,745,254,155],[0,368,274,218]]}
];


// symbols:



(lib.arm = function() {
	this.spriteSheet = ss["animation_1_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.cancel = function() {
	this.spriteSheet = ss["animation_1_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.car = function() {
	this.spriteSheet = ss["animation_1_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.contracttext = function() {
	this.spriteSheet = ss["animation_1_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.exporter = function() {
	this.spriteSheet = ss["animation_1_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.home = function() {
	this.spriteSheet = ss["animation_1_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.importer = function() {
	this.spriteSheet = ss["animation_1_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.lineredleft = function() {
	this.spriteSheet = ss["animation_1_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.lineyellowbottom = function() {
	this.spriteSheet = ss["animation_1_atlas_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.lineyellowtop = function() {
	this.spriteSheet = ss["animation_1_atlas_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.moneycenter = function() {
	this.spriteSheet = ss["animation_1_atlas_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.moneyleft = function() {
	this.spriteSheet = ss["animation_1_atlas_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Символ23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.moneyleft();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ23, new cjs.Rectangle(0,0,274,218), null);


(lib.Символ22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#31ADCE").ss(0.1,1,1).p("A3MrFMAwHgDSIrkYMMgmRAEjg");
	this.shape.setTransform(159.5,92);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#31ADCE").s().p("A3MrFMAwHgDSIrkYNMgmRAEig");
	this.shape_1.setTransform(159.5,92);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ22, new cjs.Rectangle(-1,-1,321,186), null);


(lib.Символ21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.exporter();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ21, new cjs.Rectangle(0,0,367,216), null);


(lib.Символ20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#31ADCE").ss(0.1,1,1).p("EgkjAAEIAAgJMBJHAAAIAAAL");
	this.shape.setTransform(244.5,0.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#31ADCE").s().p("EglpAWBMAAUg1uIBVAAMBJGAACIAkAAMgAUA/ag");
	this.shape_1.setTransform(241,204);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ20, new cjs.Rectangle(0,-1,482,408.1), null);


(lib.Символ18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.moneycenter();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ18, new cjs.Rectangle(0,0,254,155), null);


(lib.Символ16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.importer();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ16, new cjs.Rectangle(0,0,262,328), null);


(lib.Символ15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.car();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ15, new cjs.Rectangle(0,0,223,177), null);


(lib.Символ13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#31ADCE").ss(0.1,1,1).p("APqNnMgmMgYnIEiimMAojAYLg");
	this.shape.setTransform(144.3,87.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#31ADCE").s().p("A2irAIEiimMAojAYLIm5DCg");
	this.shape_1.setTransform(144.3,87.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ13, new cjs.Rectangle(-1,-1,290.6,176.2), null);


(lib.Символ11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.importer();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ11, new cjs.Rectangle(0,0,262,328), null);


(lib.Символ9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.exporter();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ9, new cjs.Rectangle(0,0,367,216), null);


(lib.Символ7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.arm();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ7, new cjs.Rectangle(0,0,145,74), null);


(lib.Символ2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.contracttext();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ2, new cjs.Rectangle(0,0,264,155), null);


(lib.Символ1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#31ADCE").ss(0.1,1,1).p("A16u1MAwuAZpIqkECMgrDgaFg");
	this.shape.setTransform(171.6,95);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#31ADCE").s().p("A6zrOIE5jnMAwuAZpIqjEBg");
	this.shape_1.setTransform(171.6,95);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ1, new cjs.Rectangle(-1,-1,345.2,191.9), null);


(lib.Анимация1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.lineyellowbottom();
	this.instance.parent = this;
	this.instance.setTransform(-145,-84.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-145,-84.5,290,169);


(lib.Символ19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ18();
	this.instance.parent = this;
	this.instance.setTransform(127,77.5,1,1,0,0,0,127,77.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ19, new cjs.Rectangle(0,0,254,155), null);


(lib.Символ17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ16();
	this.instance.parent = this;
	this.instance.setTransform(131,164,1,1,0,0,0,131,164);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ17, new cjs.Rectangle(0,0,262,328), null);


(lib.Символ14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ13();
	this.instance.parent = this;
	this.instance.setTransform(144.3,87,1,1,0,0,0,144.3,87);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ14, new cjs.Rectangle(0,0,288.7,174.3), null);


(lib.Символ12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ11();
	this.instance.parent = this;
	this.instance.setTransform(131,164,1,1,0,0,0,131,164);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ12, new cjs.Rectangle(0,0,262,328), null);


(lib.Символ10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ9();
	this.instance.parent = this;
	this.instance.setTransform(183.5,108,1,1,0,0,0,183.5,108);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ10, new cjs.Rectangle(0,0,367,216), null);


(lib.Символ8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ7();
	this.instance.parent = this;
	this.instance.setTransform(72.5,37,1,1,0,0,0,72.5,37);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ8, new cjs.Rectangle(0,0,145,74), null);


(lib.Символ6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Анимация1();
	this.instance.parent = this;
	this.instance.setTransform(145.8,104.7,1,1,0,0,0,0.8,20.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ6, new cjs.Rectangle(0,0,290,169), null);


(lib.Символ4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ2();
	this.instance.parent = this;
	this.instance.setTransform(132,77.5,1,1,0,0,0,132,77.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ4, new cjs.Rectangle(0,0,264,155), null);


// stage content:
(lib.animation_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// importer.png
	this.instance = new lib.importer();
	this.instance.parent = this;
	this.instance.setTransform(908,345);

	this.instance_1 = new lib.Символ12();
	this.instance_1.parent = this;
	this.instance_1.setTransform(1039,509,1,1,0,0,0,131,164);
	this.instance_1._off = true;

	this.instance_2 = new lib.Символ17();
	this.instance_2.parent = this;
	this.instance_2.setTransform(1039,509,1,1,0,0,0,131,164);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},68).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance_2}]},33).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_2}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(68).to({_off:false},0).wait(1).to({scaleX:1.02,scaleY:1.01},0).wait(1).to({scaleX:1.04,scaleY:1.03},0).wait(1).to({scaleX:1.06,scaleY:1.04},0).wait(1).to({scaleX:1.07,scaleY:1.06},0).wait(1).to({scaleX:1.09,scaleY:1.07},0).wait(1).to({scaleX:1.11,scaleY:1.08},0).wait(1).to({scaleX:1.13,scaleY:1.1},0).wait(1).to({scaleX:1.15,scaleY:1.11},0).wait(1).to({scaleX:1.17,scaleY:1.13},0).wait(1).to({scaleX:1.18,scaleY:1.14},0).wait(1).to({scaleX:1.17,scaleY:1.12},0).wait(1).to({scaleX:1.16,scaleY:1.11},0).wait(1).to({scaleX:1.15,scaleY:1.09},0).wait(1).to({scaleX:1.14,scaleY:1.07},0).wait(1).to({scaleX:1.14,scaleY:1.06},0).wait(1).to({scaleX:1.13,scaleY:1.04},0).wait(1).to({scaleX:1.12,scaleY:1.02},0).wait(1).to({scaleX:1.11,scaleY:1.01},0).wait(1).to({scaleX:1.1,scaleY:0.99},0).wait(1).to({scaleX:1.09,scaleY:0.97},0).wait(1).to({scaleX:1.08,scaleY:0.96},0).to({_off:true},1).wait(160));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(123).to({_off:false},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.03,scaleY:1.02},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.04,scaleY:1.03},0).wait(1).to({scaleX:1.05,scaleY:1.04},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.06,scaleY:1.05},0).wait(1).to({scaleX:1.07,scaleY:1.06},0).wait(1).to({scaleX:1.07,scaleY:1.06},0).wait(1).to({scaleX:1.08,scaleY:1.07},0).wait(1).to({scaleX:1.09,scaleY:1.07},0).wait(1).to({scaleX:1.09,scaleY:1.08},0).wait(1).to({scaleX:1.08,scaleY:1.07},0).wait(1).to({scaleX:1.08,scaleY:1.06},0).wait(1).to({scaleX:1.07,scaleY:1.05},0).wait(1).to({scaleX:1.06,scaleY:1.04},0).wait(1).to({scaleX:1.05,scaleY:1.03},0).wait(1).to({scaleX:1.04,scaleY:1.02},0).wait(1).to({scaleX:1.03,scaleY:1.02},0).wait(1).to({scaleX:1.02,scaleY:1.01},0).wait(1).to({scaleX:1.02,scaleY:1},0).wait(1).to({scaleX:1.01,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:0.98},0).wait(1).to({scaleX:0.99,scaleY:0.97},0).wait(101));

	// Слой 15
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#31ADCE").ss(0.1,1,1).p("A1Yu1MAvkAZqIqUEAMgqDgaEg");
	this.shape.setTransform(839.4,316.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#31ADCE").s().p("A6LrOIEzjnMAvkAZqIqUEAg");
	this.shape_1.setTransform(839.4,316.6);

	this.instance_3 = new lib.Символ1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(843.5,316.6,1,1,0,0,0,171.6,95);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.instance_3}]},34).to({state:[{t:this.instance_3}]},34).to({state:[{t:this.instance_3}]},136).wait(46));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(34).to({_off:false},0).to({x:1144.4,y:490.6},34).wait(182));

	// line-yellow-bottom.png
	this.instance_4 = new lib.Символ6();
	this.instance_4.parent = this;
	this.instance_4.setTransform(832,317.5,1,1,0,0,0,145,84.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(34).to({_off:false},0).wait(216));

	// contract-text.png
	this.instance_5 = new lib.Символ4();
	this.instance_5.parent = this;
	this.instance_5.setTransform(833,384.5,1,1,0,0,0,132,77.5);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(34).to({_off:false},0).wait(1).to({alpha:0.011},0).wait(1).to({alpha:0.022},0).wait(1).to({alpha:0.033},0).wait(1).to({alpha:0.044},0).wait(1).to({alpha:0.056},0).wait(1).to({alpha:0.067},0).wait(1).to({alpha:0.078},0).wait(1).to({alpha:0.089},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0.122},0).wait(1).to({alpha:0.133},0).wait(1).to({alpha:0.144},0).wait(1).to({alpha:0.156},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.178},0).wait(1).to({alpha:0.189},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.211},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.233},0).wait(1).to({alpha:0.244},0).wait(1).to({alpha:0.256},0).wait(1).to({alpha:0.267},0).wait(1).to({alpha:0.278},0).wait(1).to({alpha:0.289},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.311},0).wait(1).to({alpha:0.322},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.344},0).wait(1).to({alpha:0.356},0).wait(1).to({alpha:0.367},0).wait(1).to({alpha:0.378},0).wait(1).to({alpha:0.389},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.411},0).wait(1).to({alpha:0.422},0).wait(1).to({alpha:0.433},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.456},0).wait(1).to({alpha:0.467},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.489},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.511},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.533},0).wait(1).to({alpha:0.544},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.567},0).wait(1).to({alpha:0.578},0).wait(1).to({alpha:0.589},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.611},0).wait(1).to({alpha:0.622},0).wait(1).to({alpha:0.633},0).wait(1).to({alpha:0.644},0).wait(1).to({alpha:0.656},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.678},0).wait(1).to({alpha:0.689},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.711},0).wait(1).to({alpha:0.722},0).wait(1).to({alpha:0.733},0).wait(1).to({alpha:0.744},0).wait(1).to({alpha:0.756},0).wait(1).to({alpha:0.767},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.789},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.811},0).wait(1).to({alpha:0.822},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.844},0).wait(1).to({alpha:0.856},0).wait(1).to({alpha:0.867},0).wait(1).to({alpha:0.878},0).wait(1).to({alpha:0.889},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.911},0).wait(1).to({alpha:0.922},0).wait(1).to({alpha:0.933},0).wait(1).to({alpha:0.944},0).wait(1).to({alpha:0.956},0).wait(1).to({alpha:0.967},0).wait(1).to({alpha:0.978},0).wait(1).to({alpha:0.989},0).wait(1).to({alpha:1},0).wait(126));

	// arm.png
	this.instance_6 = new lib.Символ8();
	this.instance_6.parent = this;
	this.instance_6.setTransform(663.5,285,1,1,0,0,0,72.5,37);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(34).to({_off:false},0).wait(1).to({alpha:0.011},0).wait(1).to({alpha:0.022},0).wait(1).to({alpha:0.033},0).wait(1).to({alpha:0.044},0).wait(1).to({alpha:0.056},0).wait(1).to({alpha:0.067},0).wait(1).to({alpha:0.078},0).wait(1).to({alpha:0.089},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0.122},0).wait(1).to({alpha:0.133},0).wait(1).to({alpha:0.144},0).wait(1).to({alpha:0.156},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.178},0).wait(1).to({alpha:0.189},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.211},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.233},0).wait(1).to({alpha:0.244},0).wait(1).to({alpha:0.256},0).wait(1).to({alpha:0.267},0).wait(1).to({alpha:0.278},0).wait(1).to({alpha:0.289},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.311},0).wait(1).to({alpha:0.322},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.344},0).wait(1).to({alpha:0.356},0).wait(1).to({alpha:0.367},0).wait(1).to({alpha:0.378},0).wait(1).to({alpha:0.389},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.411},0).wait(1).to({alpha:0.422},0).wait(1).to({alpha:0.433},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.456},0).wait(1).to({alpha:0.467},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.489},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.511},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.533},0).wait(1).to({alpha:0.544},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.567},0).wait(1).to({alpha:0.578},0).wait(1).to({alpha:0.589},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.611},0).wait(1).to({alpha:0.622},0).wait(1).to({alpha:0.633},0).wait(1).to({alpha:0.644},0).wait(1).to({alpha:0.656},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.678},0).wait(1).to({alpha:0.689},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.711},0).wait(1).to({alpha:0.722},0).wait(1).to({alpha:0.733},0).wait(1).to({alpha:0.744},0).wait(1).to({alpha:0.756},0).wait(1).to({alpha:0.767},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.789},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.811},0).wait(1).to({alpha:0.822},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.844},0).wait(1).to({alpha:0.856},0).wait(1).to({alpha:0.867},0).wait(1).to({alpha:0.878},0).wait(1).to({alpha:0.889},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.911},0).wait(1).to({alpha:0.922},0).wait(1).to({alpha:0.933},0).wait(1).to({alpha:0.944},0).wait(1).to({alpha:0.956},0).wait(1).to({alpha:0.967},0).wait(1).to({alpha:0.978},0).wait(1).to({alpha:0.989},0).wait(1).to({alpha:1},0).wait(126));

	// exporter.png
	this.instance_7 = new lib.exporter();
	this.instance_7.parent = this;
	this.instance_7.setTransform(494,7);

	this.instance_8 = new lib.Символ10();
	this.instance_8.parent = this;
	this.instance_8.setTransform(677.5,115,1,1,0,0,0,183.4,108);
	this.instance_8._off = true;

	this.instance_9 = new lib.Символ21();
	this.instance_9.parent = this;
	this.instance_9.setTransform(677.5,115,1,1,0,0,0,183.5,108);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_7}]}).to({state:[{t:this.instance_8}]},68).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(68).to({_off:false},0).wait(1).to({regX:183.5,scaleX:1,scaleY:1.01,x:677.6},0).wait(1).to({scaleX:1,scaleY:1.01},0).wait(1).to({scaleX:1,scaleY:1.02},0).wait(1).to({scaleX:1,scaleY:1.03},0).wait(1).to({scaleY:1.03},0).wait(1).to({scaleX:1,scaleY:1.04},0).wait(1).to({scaleX:1.01,scaleY:1.05},0).wait(1).to({scaleX:1.01,scaleY:1.05},0).wait(1).to({scaleX:1.01,scaleY:1.06},0).wait(1).to({scaleX:1.01,scaleY:1.07},0).wait(1).to({scaleX:1,scaleY:1.06},0).wait(1).to({scaleX:1,scaleY:1.05},0).wait(1).to({scaleX:0.99,scaleY:1.05},0).wait(1).to({scaleX:0.99,scaleY:1.04},0).wait(1).to({scaleX:0.98,scaleY:1.04},0).wait(1).to({scaleX:0.98,scaleY:1.03},0).wait(1).to({scaleX:0.97,scaleY:1.02},0).wait(1).to({scaleX:0.96,scaleY:1.02},0).wait(1).to({scaleX:0.96,scaleY:1.01},0).wait(1).to({scaleX:0.95,scaleY:1.01},0).wait(1).to({scaleX:0.95,scaleY:1},0).wait(89).to({_off:true},1).wait(71));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(179).to({_off:false},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.03,scaleY:1.03,x:677.6},0).wait(1).to({scaleX:1.03,scaleY:1.03,y:114.9},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.06,scaleY:1.06},0).wait(1).to({scaleX:1.06,scaleY:1.06},0).wait(1).to({scaleX:1.07,scaleY:1.07},0).wait(1).to({scaleX:1.05,scaleY:1.05,x:677.7,y:114.8},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.93,scaleY:0.93,y:114.7},0).wait(1).to({scaleX:0.92,scaleY:0.92},0).wait(2).to({scaleX:0.92,scaleY:0.92,x:677.8},0).wait(2).to({scaleX:0.92,scaleY:0.92},0).wait(2).to({scaleX:0.92,scaleY:0.92},0).wait(2).to({scaleX:0.92,scaleY:0.92},0).wait(1).to({x:677.9,y:114.6},0).wait(1).to({scaleX:0.92,scaleY:0.92},0).wait(2).to({scaleX:0.92,scaleY:0.92},0).wait(2).to({scaleY:0.92},0).wait(4).to({x:678},0).wait(1).to({scaleY:0.92,y:114.5},0).wait(1).to({y:114.6},0).wait(1).to({y:114.5},0).wait(2).to({scaleY:0.92},0).wait(2).to({scaleX:0.94,scaleY:0.93,x:677.9,y:114.6},0).wait(1).to({scaleX:0.95,scaleY:0.94},0).wait(1).to({scaleX:0.96,scaleY:0.96,x:677.8},0).wait(1).to({scaleX:0.97,scaleY:0.97,y:114.7},0).wait(1).to({scaleX:0.98,scaleY:0.98,x:677.7},0).wait(1).to({scaleX:0.99,scaleY:0.99,y:114.8},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:677.6,y:114.9},0).wait(1).to({scaleX:1.03,scaleY:1.02},0).wait(1).to({scaleX:1.04,scaleY:1.04,x:677.5,y:115},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1));

	// home.png
	this.instance_10 = new lib.home();
	this.instance_10.parent = this;
	this.instance_10.setTransform(25,258);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(250));

	// Слой 16
	this.instance_11 = new lib.Символ14();
	this.instance_11.parent = this;
	this.instance_11.setTransform(885.8,285.8,1,1,0,0,0,144.3,87);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(102).to({_off:false},0).wait(1).to({regY:87.1,x:900.6,y:294.3},0).wait(1).to({x:915.4,y:302.7},0).wait(1).to({x:930.1,y:311.1},0).wait(1).to({x:944.9,y:319.4},0).wait(1).to({x:959.7,y:327.8},0).wait(1).to({x:974.5,y:336.2},0).wait(1).to({x:989.2,y:344.6},0).wait(1).to({x:1004,y:353},0).wait(1).to({x:1018.8,y:361.4},0).wait(1).to({x:1033.6,y:369.8},0).wait(1).to({x:1048.3,y:378.1},0).wait(1).to({x:1063.1,y:386.5},0).wait(1).to({x:1077.9,y:394.9},0).wait(1).to({x:1092.7,y:403.3},0).wait(1).to({x:1107.4,y:411.7},0).wait(1).to({x:1122.2,y:420.1},0).wait(1).to({x:1137,y:428.5},0).wait(1).to({x:1151.8,y:436.9},0).wait(130));

	// car.png
	this.instance_12 = new lib.Символ15();
	this.instance_12.parent = this;
	this.instance_12.setTransform(957.5,239.5,1,1,0,0,0,111.5,88.5);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(102).to({_off:false},0).wait(1).to({alpha:0.056},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.278},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.389},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.611},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.722},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.889},0).wait(1).to({alpha:0.944},0).wait(1).to({alpha:1},0).wait(130));

	// line-yellow-top.png
	this.instance_13 = new lib.lineyellowtop();
	this.instance_13.parent = this;
	this.instance_13.setTransform(756,207);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#31ADCE").ss(0.1,1,1).p("A0Tr8MAonAAAIAAX5MgonAAAg");
	this.shape_2.setTransform(886,283.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#31ADCE").s().p("A0TL9IAA35MAonAAAIAAX5g");
	this.shape_3.setTransform(886,283.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.instance_13}]},102).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.instance_13}]},77).wait(71));

	// Слой 17
	this.instance_14 = new lib.Символ20();
	this.instance_14.parent = this;
	this.instance_14.setTransform(761,399.5,1,1,0,0,0,241,203.5);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(141).to({_off:false},0).wait(1).to({regY:203.3,x:752.4,y:388.1},0).wait(1).to({x:743.8,y:376.9},0).wait(1).to({x:735.3,y:365.8},0).wait(1).to({x:726.7,y:354.6},0).wait(1).to({x:718.1,y:343.4},0).wait(1).to({x:709.5,y:332.2},0).wait(1).to({x:701,y:321},0).wait(1).to({x:692.4,y:309.8},0).wait(1).to({x:683.8,y:298.7},0).wait(1).to({x:675.2,y:287.5},0).wait(1).to({x:666.6,y:276.3},0).wait(1).to({x:658.1,y:265.1},0).wait(1).to({x:649.5,y:253.9},0).wait(1).to({x:640.9,y:242.7},0).wait(1).to({x:632.3,y:231.5},0).wait(1).to({x:623.7,y:220.4},0).wait(1).to({x:615.2,y:209.2},0).wait(1).to({x:606.6,y:198},0).wait(1).to({x:598,y:186.8},0).wait(1).to({x:589.4,y:175.7},0).wait(1).to({x:580.8,y:164.5},0).wait(1).to({x:572.3,y:153.3},0).wait(1).to({x:563.7,y:142.1},0).wait(1).to({x:555.1,y:130.9},0).wait(1).to({x:546.5,y:119.7},0).wait(1).to({x:537.9,y:108.6},0).wait(1).to({x:529.4,y:97.4},0).wait(1).to({x:520.8,y:86.2},0).wait(1).to({x:512.2,y:75},0).wait(1).to({x:503.6,y:63.8},0).wait(1).to({x:495.1,y:52.7},0).wait(1).to({x:486.5,y:41.5},0).wait(1).to({x:477.9,y:30.3},0).wait(1).to({x:469.3,y:19.1},0).wait(1).to({x:460.7,y:7.9},0).wait(1).to({x:452.2,y:-3.3},0).wait(1).to({x:443.6,y:-14.5},0).wait(1).to({x:435,y:-25.7},0).wait(1).to({y:-25.2},0).wait(1).to({y:-24.8},0).wait(1).to({x:435.1,y:-24.4},0).wait(1).to({y:-23.9},0).wait(1).to({y:-23.5},0).wait(1).to({y:-23.1},0).wait(1).to({y:-22.7},0).wait(1).to({x:435.2,y:-22.2},0).wait(1).to({y:-21.8},0).wait(1).to({y:-21.4},0).wait(1).to({y:-20.9},0).wait(1).to({y:-20.5},0).wait(1).to({x:435.3,y:-20.1},0).wait(1).to({y:-19.7},0).wait(1).to({y:-19.2},0).wait(1).to({y:-18.8},0).wait(1).to({y:-18.4},0).wait(1).to({x:435.4,y:-17.9},0).wait(1).to({y:-17.5},0).wait(1).to({y:-17.1},0).wait(1).to({y:-16.7},0).wait(1).to({y:-16.2},0).wait(1).to({x:435.5,y:-15.8},0).wait(1).to({y:-15.4},0).wait(1).to({y:-14.9},0).wait(1).to({y:-14.5},0).wait(1).to({x:435.6,y:-14.1},0).wait(1).to({y:-13.7},0).wait(1).to({y:-13.2},0).wait(1).to({y:-12.8},0).wait(1).to({y:-12.4},0).wait(1).to({x:435.7,y:-11.9},0).wait(1).to({y:-11.5},0).wait(1).to({y:-11.1},0).wait(1).to({y:-10.7},0).wait(1).to({y:-10.2},0).wait(1).to({x:435.8,y:-9.8},0).wait(1).to({y:-9.4},0).wait(1).to({y:-8.9},0).wait(1).to({y:-8.5},0).wait(1).to({y:-8.1},0).wait(1).to({x:435.9,y:-7.7},0).wait(1).to({y:-7.2},0).wait(1).to({y:-6.8},0).wait(1).to({y:-6.4},0).wait(1).to({y:-5.9},0).wait(1).to({x:436,y:-5.5},0).wait(1).to({y:-5.1},0).wait(1).to({y:-4.7},0).wait(22));

	// cancel.png
	this.instance_15 = new lib.cancel();
	this.instance_15.parent = this;
	this.instance_15.setTransform(522,214);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(141).to({_off:false},0).wait(109));

	// money-center.png
	this.instance_16 = new lib.Символ19();
	this.instance_16.parent = this;
	this.instance_16.setTransform(649,522.5,1,1,0,0,0,127,77.5);
	this.instance_16.alpha = 0;
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(141).to({_off:false},0).wait(1).to({alpha:0.026},0).wait(1).to({alpha:0.053},0).wait(1).to({alpha:0.079},0).wait(1).to({alpha:0.105},0).wait(1).to({alpha:0.132},0).wait(1).to({alpha:0.158},0).wait(1).to({alpha:0.184},0).wait(1).to({alpha:0.211},0).wait(1).to({alpha:0.237},0).wait(1).to({alpha:0.263},0).wait(1).to({alpha:0.289},0).wait(1).to({alpha:0.316},0).wait(1).to({alpha:0.342},0).wait(1).to({alpha:0.368},0).wait(1).to({alpha:0.395},0).wait(1).to({alpha:0.421},0).wait(1).to({alpha:0.447},0).wait(1).to({alpha:0.474},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.526},0).wait(1).to({alpha:0.553},0).wait(1).to({alpha:0.579},0).wait(1).to({alpha:0.605},0).wait(1).to({alpha:0.632},0).wait(1).to({alpha:0.658},0).wait(1).to({alpha:0.684},0).wait(1).to({alpha:0.711},0).wait(1).to({alpha:0.737},0).wait(1).to({alpha:0.763},0).wait(1).to({alpha:0.789},0).wait(1).to({alpha:0.816},0).wait(1).to({alpha:0.842},0).wait(1).to({alpha:0.868},0).wait(1).to({alpha:0.895},0).wait(1).to({alpha:0.921},0).wait(1).to({alpha:0.947},0).wait(1).to({alpha:0.974},0).wait(1).to({alpha:1},0).wait(71));

	// money-left.png
	this.instance_17 = new lib.Символ23();
	this.instance_17.parent = this;
	this.instance_17.setTransform(410,268,1,1,0,0,0,137,109);
	this.instance_17.alpha = 0;
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(204).to({_off:false},0).wait(1).to({alpha:0.042},0).wait(1).to({alpha:0.083},0).wait(1).to({alpha:0.125},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.208},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.292},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.375},0).wait(1).to({alpha:0.417},0).wait(1).to({alpha:0.458},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.542},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.708},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.792},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.875},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.958},0).wait(1).to({alpha:1},0).wait(22));

	// Слой 18
	this.instance_18 = new lib.Символ22();
	this.instance_18.parent = this;
	this.instance_18.setTransform(374.5,285,1,1,0,0,0,159.5,92);
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(204).to({_off:false},0).wait(1).to({x:387.8,y:277.3},0).wait(1).to({x:401.2,y:269.7},0).wait(1).to({x:414.5,y:262},0).wait(1).to({x:427.8,y:254.3},0).wait(1).to({x:441.2,y:246.7},0).wait(1).to({x:454.5,y:239},0).wait(1).to({x:467.8,y:231.3},0).wait(1).to({x:481.2,y:223.7},0).wait(1).to({x:494.5,y:216},0).wait(1).to({x:507.8,y:208.3},0).wait(1).to({x:521.2,y:200.7},0).wait(1).to({x:534.5,y:193},0).wait(1).to({x:547.8,y:185.3},0).wait(1).to({x:561.2,y:177.7},0).wait(1).to({x:574.5,y:170},0).wait(1).to({x:587.8,y:162.3},0).wait(1).to({x:601.2,y:154.7},0).wait(1).to({x:614.5,y:147},0).wait(1).to({x:627.8,y:139.3},0).wait(1).to({x:641.2,y:131.7},0).wait(1).to({x:654.5,y:124},0).wait(1).to({x:667.8,y:116.3},0).wait(1).to({x:681.2,y:108.7},0).wait(1).to({x:694.5,y:101},0).wait(22));

	// line-red-left.png
	this.instance_19 = new lib.lineredleft();
	this.instance_19.parent = this;
	this.instance_19.setTransform(215,193);
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(204).to({_off:false},0).wait(46));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(625,357,1145,666);
// library properties:
lib.properties = {
	width: 1200,
	height: 700,
	fps: 24,
	color: "#31ADCE",
	opacity: 1.00,
	manifest: [
		{src:"../../img/animation_1/animation_1_atlas_.png?1516113587142", id:"animation_1_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;