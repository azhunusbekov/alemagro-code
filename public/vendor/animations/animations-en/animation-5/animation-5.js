(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"animation_5_atlas_", frames: [[1416,1082,216,125],[1922,62,93,55],[1634,1082,128,187],[1197,1082,217,127],[724,1082,188,190],[450,1082,272,198],[914,1082,281,113],[914,1197,159,106],[0,1082,448,138],[1922,0,103,60],[1922,119,85,52],[1764,1191,181,106],[1764,1082,185,107],[0,0,1920,1080]]}
];


// symbols:



(lib.Ресурс1002x = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс1012x = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс222xpngкопия2 = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс272xpngкопия2 = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс22x = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс472x = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс492x = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс502x = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс512x = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс772xpngкопия4 = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс782xpngкопия4 = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс982x = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс992x = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.d = function() {
	this.spriteSheet = ss["animation_5_atlas_"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Символ112 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс22x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ112, new cjs.Rectangle(0,0,188,190), null);


(lib.Символ111 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс22x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ111, new cjs.Rectangle(0,0,188,190), null);


(lib.Символ110 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс272xpngкопия2();
	this.instance.parent = this;

	this.instance_1 = new lib.Ресурс222xpngкопия2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(331,127);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ110, new cjs.Rectangle(0,0,459,314), null);


(lib.Символ109 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс1002x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ109, new cjs.Rectangle(0,0,216,125), null);


(lib.Символ108 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс772xpngкопия4();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ108, new cjs.Rectangle(0,0,103,60), null);


(lib.Символ107 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс1012x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ107, new cjs.Rectangle(0,0,93,55), null);


(lib.Символ106 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс782xpngкопия4();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ106, new cjs.Rectangle(0,0,85,52), null);


(lib.Символ105 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс992x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ105, new cjs.Rectangle(0,0,185,107), null);


(lib.Символ104 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс982x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ104, new cjs.Rectangle(0,0,181,106), null);


(lib.Символ103 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс512x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ103, new cjs.Rectangle(0,0,448,138), null);


(lib.Символ102 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс502x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ102, new cjs.Rectangle(0,0,159,106), null);


(lib.Символ101 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс492x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ101, new cjs.Rectangle(0,0,281,113), null);


(lib.Символ100 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс472x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ100, new cjs.Rectangle(0,0,272,198), null);


// stage content:
(lib.animation5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Символ 110
	this.instance = new lib.Символ110();
	this.instance.parent = this;
	this.instance.setTransform(571.5,214,1,1,0,0,0,229.5,157);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(49).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.129},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.157},0).wait(1).to({alpha:0.171},0).wait(1).to({alpha:0.186},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.229},0).wait(1).to({alpha:0.243},0).wait(1).to({alpha:0.257},0).wait(1).to({alpha:0.271},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.314},0).wait(1).to({alpha:0.329},0).wait(1).to({alpha:0.343},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.371},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.457},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.486},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.514},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(203).to({alpha:0.981},0).wait(1).to({alpha:0.962},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.904},0).wait(1).to({alpha:0.885},0).wait(1).to({alpha:0.865},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.827},0).wait(1).to({alpha:0.808},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.769},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.731},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.654},0).wait(1).to({alpha:0.635},0).wait(1).to({alpha:0.615},0).wait(1).to({alpha:0.596},0).wait(1).to({alpha:0.577},0).wait(1).to({alpha:0.558},0).wait(1).to({alpha:0.538},0).wait(1).to({alpha:0.519},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.481},0).wait(1).to({alpha:0.462},0).wait(1).to({alpha:0.442},0).wait(1).to({alpha:0.423},0).wait(1).to({alpha:0.404},0).wait(1).to({alpha:0.385},0).wait(1).to({alpha:0.365},0).wait(1).to({alpha:0.346},0).wait(1).to({alpha:0.327},0).wait(1).to({alpha:0.308},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.269},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.192},0).wait(1).to({alpha:0.173},0).wait(1).to({alpha:0.154},0).wait(1).to({alpha:0.135},0).wait(1).to({alpha:0.115},0).wait(1).to({alpha:0.096},0).wait(1).to({alpha:0.077},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.038},0).wait(1).to({alpha:0.019},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 109
	this.instance_1 = new lib.Символ109();
	this.instance_1.parent = this;
	this.instance_1.setTransform(252,215.5,1,1,0,0,0,108,62.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(120).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(133).to({alpha:0.981},0).wait(1).to({alpha:0.962},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.904},0).wait(1).to({alpha:0.885},0).wait(1).to({alpha:0.865},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.827},0).wait(1).to({alpha:0.808},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.769},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.731},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.654},0).wait(1).to({alpha:0.635},0).wait(1).to({alpha:0.615},0).wait(1).to({alpha:0.596},0).wait(1).to({alpha:0.577},0).wait(1).to({alpha:0.558},0).wait(1).to({alpha:0.538},0).wait(1).to({alpha:0.519},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.481},0).wait(1).to({alpha:0.462},0).wait(1).to({alpha:0.442},0).wait(1).to({alpha:0.423},0).wait(1).to({alpha:0.404},0).wait(1).to({alpha:0.385},0).wait(1).to({alpha:0.365},0).wait(1).to({alpha:0.346},0).wait(1).to({alpha:0.327},0).wait(1).to({alpha:0.308},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.269},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.192},0).wait(1).to({alpha:0.173},0).wait(1).to({alpha:0.154},0).wait(1).to({alpha:0.135},0).wait(1).to({alpha:0.115},0).wait(1).to({alpha:0.096},0).wait(1).to({alpha:0.077},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.038},0).wait(1).to({alpha:0.019},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 108
	this.instance_2 = new lib.Символ108();
	this.instance_2.parent = this;
	this.instance_2.setTransform(372.5,183,1,1,0,0,0,51.5,30);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(49).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.129},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.157},0).wait(1).to({alpha:0.171},0).wait(1).to({alpha:0.186},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.229},0).wait(1).to({alpha:0.243},0).wait(1).to({alpha:0.257},0).wait(1).to({alpha:0.271},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.314},0).wait(1).to({alpha:0.329},0).wait(1).to({alpha:0.343},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.371},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.457},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.486},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.514},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(203).to({alpha:0.981},0).wait(1).to({alpha:0.962},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.904},0).wait(1).to({alpha:0.885},0).wait(1).to({alpha:0.865},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.827},0).wait(1).to({alpha:0.808},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.769},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.731},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.654},0).wait(1).to({alpha:0.635},0).wait(1).to({alpha:0.615},0).wait(1).to({alpha:0.596},0).wait(1).to({alpha:0.577},0).wait(1).to({alpha:0.558},0).wait(1).to({alpha:0.538},0).wait(1).to({alpha:0.519},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.481},0).wait(1).to({alpha:0.462},0).wait(1).to({alpha:0.442},0).wait(1).to({alpha:0.423},0).wait(1).to({alpha:0.404},0).wait(1).to({alpha:0.385},0).wait(1).to({alpha:0.365},0).wait(1).to({alpha:0.346},0).wait(1).to({alpha:0.327},0).wait(1).to({alpha:0.308},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.269},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.192},0).wait(1).to({alpha:0.173},0).wait(1).to({alpha:0.154},0).wait(1).to({alpha:0.135},0).wait(1).to({alpha:0.115},0).wait(1).to({alpha:0.096},0).wait(1).to({alpha:0.077},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.038},0).wait(1).to({alpha:0.019},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 107
	this.instance_3 = new lib.Символ107();
	this.instance_3.parent = this;
	this.instance_3.setTransform(694.8,138.7,1,1,0,0,0,114.8,-14.3);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(190).to({_off:false},0).wait(1).to({regX:46.5,regY:27.5,x:626.5,y:180.5,alpha:0.015},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.044},0).wait(1).to({alpha:0.059},0).wait(1).to({alpha:0.074},0).wait(1).to({alpha:0.088},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.118},0).wait(1).to({alpha:0.132},0).wait(1).to({alpha:0.147},0).wait(1).to({alpha:0.162},0).wait(1).to({alpha:0.176},0).wait(1).to({alpha:0.191},0).wait(1).to({alpha:0.206},0).wait(1).to({alpha:0.221},0).wait(1).to({alpha:0.235},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.265},0).wait(1).to({alpha:0.279},0).wait(1).to({alpha:0.294},0).wait(1).to({alpha:0.309},0).wait(1).to({alpha:0.324},0).wait(1).to({alpha:0.338},0).wait(1).to({alpha:0.353},0).wait(1).to({alpha:0.368},0).wait(1).to({alpha:0.382},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.412},0).wait(1).to({alpha:0.426},0).wait(1).to({alpha:0.441},0).wait(1).to({alpha:0.456},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.544},0).wait(1).to({alpha:0.559},0).wait(1).to({alpha:0.574},0).wait(1).to({alpha:0.588},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.618},0).wait(1).to({alpha:0.632},0).wait(1).to({alpha:0.647},0).wait(1).to({alpha:0.662},0).wait(1).to({alpha:0.676},0).wait(1).to({alpha:0.691},0).wait(1).to({alpha:0.706},0).wait(1).to({alpha:0.721},0).wait(1).to({alpha:0.735},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.765},0).wait(1).to({alpha:0.779},0).wait(1).to({alpha:0.794},0).wait(1).to({alpha:0.809},0).wait(1).to({alpha:0.824},0).wait(1).to({alpha:0.838},0).wait(1).to({alpha:0.853},0).wait(1).to({alpha:0.868},0).wait(1).to({alpha:0.882},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.912},0).wait(1).to({alpha:0.926},0).wait(1).to({alpha:0.941},0).wait(1).to({alpha:0.956},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.985},0).wait(1).to({alpha:1},0).wait(1).to({regX:114.8,regY:-14.3,x:694.8,y:138.7},0).wait(63).to({regX:46.5,regY:27.5,x:626.5,y:180.5,alpha:0.981},0).wait(1).to({alpha:0.962},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.904},0).wait(1).to({alpha:0.885},0).wait(1).to({alpha:0.865},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.827},0).wait(1).to({alpha:0.808},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.769},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.731},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.654},0).wait(1).to({alpha:0.635},0).wait(1).to({alpha:0.615},0).wait(1).to({alpha:0.596},0).wait(1).to({alpha:0.577},0).wait(1).to({alpha:0.558},0).wait(1).to({alpha:0.538},0).wait(1).to({alpha:0.519},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.481},0).wait(1).to({alpha:0.462},0).wait(1).to({alpha:0.442},0).wait(1).to({alpha:0.423},0).wait(1).to({alpha:0.404},0).wait(1).to({alpha:0.385},0).wait(1).to({alpha:0.365},0).wait(1).to({alpha:0.346},0).wait(1).to({alpha:0.327},0).wait(1).to({alpha:0.308},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.269},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.192},0).wait(1).to({alpha:0.173},0).wait(1).to({alpha:0.154},0).wait(1).to({alpha:0.135},0).wait(1).to({alpha:0.115},0).wait(1).to({alpha:0.096},0).wait(1).to({alpha:0.077},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.038},0).wait(1).to({alpha:0.019},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 106
	this.instance_4 = new lib.Символ106();
	this.instance_4.parent = this;
	this.instance_4.setTransform(664.5,375,1,1,0,0,0,42.5,26);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(49).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.129},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.157},0).wait(1).to({alpha:0.171},0).wait(1).to({alpha:0.186},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.229},0).wait(1).to({alpha:0.243},0).wait(1).to({alpha:0.257},0).wait(1).to({alpha:0.271},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.314},0).wait(1).to({alpha:0.329},0).wait(1).to({alpha:0.343},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.371},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.457},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.486},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.514},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(203).to({alpha:0.981},0).wait(1).to({alpha:0.962},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.904},0).wait(1).to({alpha:0.885},0).wait(1).to({alpha:0.865},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.827},0).wait(1).to({alpha:0.808},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.769},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.731},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.654},0).wait(1).to({alpha:0.635},0).wait(1).to({alpha:0.615},0).wait(1).to({alpha:0.596},0).wait(1).to({alpha:0.577},0).wait(1).to({alpha:0.558},0).wait(1).to({alpha:0.538},0).wait(1).to({alpha:0.519},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.481},0).wait(1).to({alpha:0.462},0).wait(1).to({alpha:0.442},0).wait(1).to({alpha:0.423},0).wait(1).to({alpha:0.404},0).wait(1).to({alpha:0.385},0).wait(1).to({alpha:0.365},0).wait(1).to({alpha:0.346},0).wait(1).to({alpha:0.327},0).wait(1).to({alpha:0.308},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.269},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.192},0).wait(1).to({alpha:0.173},0).wait(1).to({alpha:0.154},0).wait(1).to({alpha:0.135},0).wait(1).to({alpha:0.115},0).wait(1).to({alpha:0.096},0).wait(1).to({alpha:0.077},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.038},0).wait(1).to({alpha:0.019},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 105
	this.instance_5 = new lib.Символ105();
	this.instance_5.parent = this;
	this.instance_5.setTransform(548.5,288.5,1,1,0,0,0,92.5,53.5);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(49).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.129},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.157},0).wait(1).to({alpha:0.171},0).wait(1).to({alpha:0.186},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.229},0).wait(1).to({alpha:0.243},0).wait(1).to({alpha:0.257},0).wait(1).to({alpha:0.271},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.314},0).wait(1).to({alpha:0.329},0).wait(1).to({alpha:0.343},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.371},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.457},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.486},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.514},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(203).to({alpha:0.981},0).wait(1).to({alpha:0.962},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.904},0).wait(1).to({alpha:0.885},0).wait(1).to({alpha:0.865},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.827},0).wait(1).to({alpha:0.808},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.769},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.731},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.654},0).wait(1).to({alpha:0.635},0).wait(1).to({alpha:0.615},0).wait(1).to({alpha:0.596},0).wait(1).to({alpha:0.577},0).wait(1).to({alpha:0.558},0).wait(1).to({alpha:0.538},0).wait(1).to({alpha:0.519},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.481},0).wait(1).to({alpha:0.462},0).wait(1).to({alpha:0.442},0).wait(1).to({alpha:0.423},0).wait(1).to({alpha:0.404},0).wait(1).to({alpha:0.385},0).wait(1).to({alpha:0.365},0).wait(1).to({alpha:0.346},0).wait(1).to({alpha:0.327},0).wait(1).to({alpha:0.308},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.269},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.192},0).wait(1).to({alpha:0.173},0).wait(1).to({alpha:0.154},0).wait(1).to({alpha:0.135},0).wait(1).to({alpha:0.115},0).wait(1).to({alpha:0.096},0).wait(1).to({alpha:0.077},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.038},0).wait(1).to({alpha:0.019},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 104
	this.instance_6 = new lib.Символ104();
	this.instance_6.parent = this;
	this.instance_6.setTransform(317.5,416,1,1,0,0,0,90.5,53);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(259).to({_off:false},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0.033},0).wait(1).to({alpha:0.05},0).wait(1).to({alpha:0.067},0).wait(1).to({alpha:0.083},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.117},0).wait(1).to({alpha:0.133},0).wait(1).to({alpha:0.15},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.183},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.233},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.267},0).wait(1).to({alpha:0.283},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.317},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.35},0).wait(1).to({alpha:0.367},0).wait(1).to({alpha:0.383},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.417},0).wait(1).to({alpha:0.433},0).wait(1).to({alpha:0.45},0).wait(1).to({alpha:0.467},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.533},0).wait(1).to({alpha:0.55},0).wait(1).to({alpha:0.567},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.617},0).wait(1).to({alpha:0.633},0).wait(1).to({alpha:0.65},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.683},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.717},0).wait(1).to({alpha:0.733},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.767},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.817},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.85},0).wait(1).to({alpha:0.867},0).wait(1).to({alpha:0.883},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.933},0).wait(1).to({alpha:0.95},0).wait(1).to({alpha:0.967},0).wait(1).to({alpha:0.983},0).wait(1).to({alpha:1},0).wait(3).to({alpha:0.981},0).wait(1).to({alpha:0.962},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.904},0).wait(1).to({alpha:0.885},0).wait(1).to({alpha:0.865},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.827},0).wait(1).to({alpha:0.808},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.769},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.731},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.654},0).wait(1).to({alpha:0.635},0).wait(1).to({alpha:0.615},0).wait(1).to({alpha:0.596},0).wait(1).to({alpha:0.577},0).wait(1).to({alpha:0.558},0).wait(1).to({alpha:0.538},0).wait(1).to({alpha:0.519},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.481},0).wait(1).to({alpha:0.462},0).wait(1).to({alpha:0.442},0).wait(1).to({alpha:0.423},0).wait(1).to({alpha:0.404},0).wait(1).to({alpha:0.385},0).wait(1).to({alpha:0.365},0).wait(1).to({alpha:0.346},0).wait(1).to({alpha:0.327},0).wait(1).to({alpha:0.308},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.269},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.192},0).wait(1).to({alpha:0.173},0).wait(1).to({alpha:0.154},0).wait(1).to({alpha:0.135},0).wait(1).to({alpha:0.115},0).wait(1).to({alpha:0.096},0).wait(1).to({alpha:0.077},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.038},0).wait(1).to({alpha:0.019},0).wait(1).to({alpha:0},0).wait(1));

	// Ресурс 2@2x.png
	this.instance_7 = new lib.Символ111();
	this.instance_7.parent = this;
	this.instance_7.setTransform(119,320,1,1,0,0,0,94,95);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.instance_8 = new lib.Ресурс22x();
	this.instance_8.parent = this;
	this.instance_8.setTransform(25,225);

	this.instance_9 = new lib.Символ112();
	this.instance_9.parent = this;
	this.instance_9.setTransform(119,320,1,1,0,0,0,94,95);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_7}]},120).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},131).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(120).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).to({_off:true},1).wait(184));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(321).to({_off:false},0).wait(1).to({alpha:0.981},0).wait(1).to({alpha:0.962},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.904},0).wait(1).to({alpha:0.885},0).wait(1).to({alpha:0.865},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.827},0).wait(1).to({alpha:0.808},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.769},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.731},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.654},0).wait(1).to({alpha:0.635},0).wait(1).to({alpha:0.615},0).wait(1).to({alpha:0.596},0).wait(1).to({alpha:0.577},0).wait(1).to({alpha:0.558},0).wait(1).to({alpha:0.538},0).wait(1).to({alpha:0.519},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.481},0).wait(1).to({alpha:0.462},0).wait(1).to({alpha:0.442},0).wait(1).to({alpha:0.423},0).wait(1).to({alpha:0.404},0).wait(1).to({alpha:0.385},0).wait(1).to({alpha:0.365},0).wait(1).to({alpha:0.346},0).wait(1).to({alpha:0.327},0).wait(1).to({alpha:0.308},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.269},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.192},0).wait(1).to({alpha:0.173},0).wait(1).to({alpha:0.154},0).wait(1).to({alpha:0.135},0).wait(1).to({alpha:0.115},0).wait(1).to({alpha:0.096},0).wait(1).to({alpha:0.077},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.038},0).wait(1).to({alpha:0.019},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 100
	this.instance_10 = new lib.Символ100();
	this.instance_10.parent = this;
	this.instance_10.setTransform(241,257,1,1,0,0,0,136,99);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(120).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(133).to({alpha:0.981},0).wait(1).to({alpha:0.962},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.904},0).wait(1).to({alpha:0.885},0).wait(1).to({alpha:0.865},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.827},0).wait(1).to({alpha:0.808},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.769},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.731},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.654},0).wait(1).to({alpha:0.635},0).wait(1).to({alpha:0.615},0).wait(1).to({alpha:0.596},0).wait(1).to({alpha:0.577},0).wait(1).to({alpha:0.558},0).wait(1).to({alpha:0.538},0).wait(1).to({alpha:0.519},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.481},0).wait(1).to({alpha:0.462},0).wait(1).to({alpha:0.442},0).wait(1).to({alpha:0.423},0).wait(1).to({alpha:0.404},0).wait(1).to({alpha:0.385},0).wait(1).to({alpha:0.365},0).wait(1).to({alpha:0.346},0).wait(1).to({alpha:0.327},0).wait(1).to({alpha:0.308},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.269},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.192},0).wait(1).to({alpha:0.173},0).wait(1).to({alpha:0.154},0).wait(1).to({alpha:0.135},0).wait(1).to({alpha:0.115},0).wait(1).to({alpha:0.096},0).wait(1).to({alpha:0.077},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.038},0).wait(1).to({alpha:0.019},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 103
	this.instance_11 = new lib.Символ103();
	this.instance_11.parent = this;
	this.instance_11.setTransform(417,449,1,1,0,0,0,224,69);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(259).to({_off:false},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0.033},0).wait(1).to({alpha:0.05},0).wait(1).to({alpha:0.067},0).wait(1).to({alpha:0.083},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.117},0).wait(1).to({alpha:0.133},0).wait(1).to({alpha:0.15},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.183},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.233},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.267},0).wait(1).to({alpha:0.283},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.317},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.35},0).wait(1).to({alpha:0.367},0).wait(1).to({alpha:0.383},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.417},0).wait(1).to({alpha:0.433},0).wait(1).to({alpha:0.45},0).wait(1).to({alpha:0.467},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.533},0).wait(1).to({alpha:0.55},0).wait(1).to({alpha:0.567},0).wait(1).to({alpha:0.583},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.617},0).wait(1).to({alpha:0.633},0).wait(1).to({alpha:0.65},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.683},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.717},0).wait(1).to({alpha:0.733},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.767},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.817},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.85},0).wait(1).to({alpha:0.867},0).wait(1).to({alpha:0.883},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.917},0).wait(1).to({alpha:0.933},0).wait(1).to({alpha:0.95},0).wait(1).to({alpha:0.967},0).wait(1).to({alpha:0.983},0).wait(1).to({alpha:1},0).wait(3).to({alpha:0.981},0).wait(1).to({alpha:0.962},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.904},0).wait(1).to({alpha:0.885},0).wait(1).to({alpha:0.865},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.827},0).wait(1).to({alpha:0.808},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.769},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.731},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.654},0).wait(1).to({alpha:0.635},0).wait(1).to({alpha:0.615},0).wait(1).to({alpha:0.596},0).wait(1).to({alpha:0.577},0).wait(1).to({alpha:0.558},0).wait(1).to({alpha:0.538},0).wait(1).to({alpha:0.519},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.481},0).wait(1).to({alpha:0.462},0).wait(1).to({alpha:0.442},0).wait(1).to({alpha:0.423},0).wait(1).to({alpha:0.404},0).wait(1).to({alpha:0.385},0).wait(1).to({alpha:0.365},0).wait(1).to({alpha:0.346},0).wait(1).to({alpha:0.327},0).wait(1).to({alpha:0.308},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.269},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.192},0).wait(1).to({alpha:0.173},0).wait(1).to({alpha:0.154},0).wait(1).to({alpha:0.135},0).wait(1).to({alpha:0.115},0).wait(1).to({alpha:0.096},0).wait(1).to({alpha:0.077},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.038},0).wait(1).to({alpha:0.019},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 102
	this.instance_12 = new lib.Символ102();
	this.instance_12.parent = this;
	this.instance_12.setTransform(582.5,218,1,1,0,0,0,79.5,53);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(190).to({_off:false},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.044},0).wait(1).to({alpha:0.059},0).wait(1).to({alpha:0.074},0).wait(1).to({alpha:0.088},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.118},0).wait(1).to({alpha:0.132},0).wait(1).to({alpha:0.147},0).wait(1).to({alpha:0.162},0).wait(1).to({alpha:0.176},0).wait(1).to({alpha:0.191},0).wait(1).to({alpha:0.206},0).wait(1).to({alpha:0.221},0).wait(1).to({alpha:0.235},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.265},0).wait(1).to({alpha:0.279},0).wait(1).to({alpha:0.294},0).wait(1).to({alpha:0.309},0).wait(1).to({alpha:0.324},0).wait(1).to({alpha:0.338},0).wait(1).to({alpha:0.353},0).wait(1).to({alpha:0.368},0).wait(1).to({alpha:0.382},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.412},0).wait(1).to({alpha:0.426},0).wait(1).to({alpha:0.441},0).wait(1).to({alpha:0.456},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.544},0).wait(1).to({alpha:0.559},0).wait(1).to({alpha:0.574},0).wait(1).to({alpha:0.588},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.618},0).wait(1).to({alpha:0.632},0).wait(1).to({alpha:0.647},0).wait(1).to({alpha:0.662},0).wait(1).to({alpha:0.676},0).wait(1).to({alpha:0.691},0).wait(1).to({alpha:0.706},0).wait(1).to({alpha:0.721},0).wait(1).to({alpha:0.735},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.765},0).wait(1).to({alpha:0.779},0).wait(1).to({alpha:0.794},0).wait(1).to({alpha:0.809},0).wait(1).to({alpha:0.824},0).wait(1).to({alpha:0.838},0).wait(1).to({alpha:0.853},0).wait(1).to({alpha:0.868},0).wait(1).to({alpha:0.882},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.912},0).wait(1).to({alpha:0.926},0).wait(1).to({alpha:0.941},0).wait(1).to({alpha:0.956},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.985},0).wait(1).to({alpha:1},0).wait(64).to({alpha:0.981},0).wait(1).to({alpha:0.962},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.904},0).wait(1).to({alpha:0.885},0).wait(1).to({alpha:0.865},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.827},0).wait(1).to({alpha:0.808},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.769},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.731},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.654},0).wait(1).to({alpha:0.635},0).wait(1).to({alpha:0.615},0).wait(1).to({alpha:0.596},0).wait(1).to({alpha:0.577},0).wait(1).to({alpha:0.558},0).wait(1).to({alpha:0.538},0).wait(1).to({alpha:0.519},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.481},0).wait(1).to({alpha:0.462},0).wait(1).to({alpha:0.442},0).wait(1).to({alpha:0.423},0).wait(1).to({alpha:0.404},0).wait(1).to({alpha:0.385},0).wait(1).to({alpha:0.365},0).wait(1).to({alpha:0.346},0).wait(1).to({alpha:0.327},0).wait(1).to({alpha:0.308},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.269},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.192},0).wait(1).to({alpha:0.173},0).wait(1).to({alpha:0.154},0).wait(1).to({alpha:0.135},0).wait(1).to({alpha:0.115},0).wait(1).to({alpha:0.096},0).wait(1).to({alpha:0.077},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.038},0).wait(1).to({alpha:0.019},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 101
	this.instance_13 = new lib.Символ101();
	this.instance_13.parent = this;
	this.instance_13.setTransform(521.5,250.5,1,1,0,0,0,140.5,56.5);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(49).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.129},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.157},0).wait(1).to({alpha:0.171},0).wait(1).to({alpha:0.186},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.229},0).wait(1).to({alpha:0.243},0).wait(1).to({alpha:0.257},0).wait(1).to({alpha:0.271},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.314},0).wait(1).to({alpha:0.329},0).wait(1).to({alpha:0.343},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.371},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.457},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.486},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.514},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(203).to({alpha:0.981},0).wait(1).to({alpha:0.962},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.923},0).wait(1).to({alpha:0.904},0).wait(1).to({alpha:0.885},0).wait(1).to({alpha:0.865},0).wait(1).to({alpha:0.846},0).wait(1).to({alpha:0.827},0).wait(1).to({alpha:0.808},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.769},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.731},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.692},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.654},0).wait(1).to({alpha:0.635},0).wait(1).to({alpha:0.615},0).wait(1).to({alpha:0.596},0).wait(1).to({alpha:0.577},0).wait(1).to({alpha:0.558},0).wait(1).to({alpha:0.538},0).wait(1).to({alpha:0.519},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.481},0).wait(1).to({alpha:0.462},0).wait(1).to({alpha:0.442},0).wait(1).to({alpha:0.423},0).wait(1).to({alpha:0.404},0).wait(1).to({alpha:0.385},0).wait(1).to({alpha:0.365},0).wait(1).to({alpha:0.346},0).wait(1).to({alpha:0.327},0).wait(1).to({alpha:0.308},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.269},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.231},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.192},0).wait(1).to({alpha:0.173},0).wait(1).to({alpha:0.154},0).wait(1).to({alpha:0.135},0).wait(1).to({alpha:0.115},0).wait(1).to({alpha:0.096},0).wait(1).to({alpha:0.077},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.038},0).wait(1).to({alpha:0.019},0).wait(1).to({alpha:0},0).wait(1));

	// Слой 2
	this.instance_14 = new lib.d();
	this.instance_14.parent = this;
	this.instance_14.setTransform(0,0,0.469,0.556);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(374));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(450,300,900,600);
// library properties:
lib.properties = {
	width: 900,
	height: 600,
	fps: 60,
	color: "#4DAFCD",
	opacity: 1.00,
	manifest: [
		{src:"images/animation_5_atlas_.png?1518774425083", id:"animation_5_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;