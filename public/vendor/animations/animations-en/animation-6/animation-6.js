(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"animation_6_atlas_", frames: [[1672,1190,127,75],[1922,62,102,60],[1922,124,97,58],[1746,1082,181,106],[1326,1082,193,113],[1196,1082,128,187],[977,1082,217,127],[576,1082,188,190],[1521,1177,149,114],[301,1082,273,161],[766,1082,209,134],[0,1082,299,226],[1521,1082,223,93],[1922,0,103,60],[1922,184,85,52],[0,0,1920,1080]]}
];


// symbols:



(lib.Ресурс1022x = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс1032x = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс1042x = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс1052x = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс1062x = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс222xpngкопия3 = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс272xpngкопия3 = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс22x = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс362xpngкопия4 = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс522x = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс532x = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс542x = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс552x = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс772xpngкопия5 = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс782xpngкопия5 = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.d = function() {
	this.spriteSheet = ss["animation_6_atlas_"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Символ128 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс22x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ128, new cjs.Rectangle(0,0,188,190), null);


(lib.Символ127 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс22x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ127, new cjs.Rectangle(0,0,188,190), null);


(lib.Символ126 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс782xpngкопия5();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ126, new cjs.Rectangle(0,0,85,52), null);


(lib.Символ125 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс772xpngкопия5();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ125, new cjs.Rectangle(0,0,103,60), null);


(lib.Символ124 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс1062x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ124, new cjs.Rectangle(0,0,193,113), null);


(lib.Символ122 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс1052x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ122, new cjs.Rectangle(0,0,181,106), null);


(lib.Символ121 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс1042x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ121, new cjs.Rectangle(0,0,97,58), null);


(lib.Символ120 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс1032x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ120, new cjs.Rectangle(0,0,102,60), null);


(lib.Символ119 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс1022x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ119, new cjs.Rectangle(0,0,127,75), null);


(lib.Символ118 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс222xpngкопия3();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ118, new cjs.Rectangle(0,0,128,187), null);


(lib.Символ117 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс222xpngкопия3();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ117, new cjs.Rectangle(0,0,128,187), null);


(lib.Символ116 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс272xpngкопия3();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ116, new cjs.Rectangle(0,0,217,127), null);


(lib.Символ115 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс362xpngкопия4();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ115, new cjs.Rectangle(0,0,149,114), null);


(lib.Символ114 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс542x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ114, new cjs.Rectangle(0,0,299,226), null);


(lib.Символ113 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс552x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ113, new cjs.Rectangle(0,0,223,93), null);


(lib.Символ112 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс532x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ112, new cjs.Rectangle(0,0,209,134), null);


(lib.Символ111 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс522x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ111, new cjs.Rectangle(0,0,273,161), null);


(lib.Символ123 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ121();
	this.instance.parent = this;
	this.instance.setTransform(48.5,85,1,1,0,0,0,48.5,85);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ123, new cjs.Rectangle(0,0,97,58), null);


// stage content:
(lib.animation6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Символ 126
	this.instance = new lib.Символ126();
	this.instance.parent = this;
	this.instance.setTransform(718.5,514,1,1,0,0,0,42.5,26);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(49).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.129},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.157},0).wait(1).to({alpha:0.171},0).wait(1).to({alpha:0.186},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.229},0).wait(1).to({alpha:0.243},0).wait(1).to({alpha:0.257},0).wait(1).to({alpha:0.271},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.314},0).wait(1).to({alpha:0.329},0).wait(1).to({alpha:0.343},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.371},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.457},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.486},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.514},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(214).to({alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 125
	this.instance_1 = new lib.Символ125();
	this.instance_1.parent = this;
	this.instance_1.setTransform(395.5,341,1,1,0,0,0,51.5,30);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(49).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.129},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.157},0).wait(1).to({alpha:0.171},0).wait(1).to({alpha:0.186},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.229},0).wait(1).to({alpha:0.243},0).wait(1).to({alpha:0.257},0).wait(1).to({alpha:0.271},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.314},0).wait(1).to({alpha:0.329},0).wait(1).to({alpha:0.343},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.371},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.457},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.486},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.514},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(214).to({alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 124
	this.instance_2 = new lib.Символ124();
	this.instance_2.parent = this;
	this.instance_2.setTransform(475.3,143,1,1,0,0,0,122.3,56);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(120).to({_off:false},0).wait(1).to({regX:96.5,regY:56.5,x:449.5,y:143.5,alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(1).to({regX:122.3,regY:56,x:475.3,y:143},0).wait(143).to({regX:96.5,regY:56.5,x:449.5,y:143.5,alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 123
	this.instance_3 = new lib.Символ123();
	this.instance_3.parent = this;
	this.instance_3.setTransform(336.5,258,1,1,0,0,0,48.5,85);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(120).to({_off:false},0).wait(1).to({regY:29,y:202,alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(1).to({regY:85,y:258},0).wait(143).to({regY:29,y:202,alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 122
	this.instance_4 = new lib.Символ122();
	this.instance_4.parent = this;
	this.instance_4.setTransform(217.5,127,1,1,0,0,0,90.5,53);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(260).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.129},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.157},0).wait(1).to({alpha:0.171},0).wait(1).to({alpha:0.186},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.229},0).wait(1).to({alpha:0.243},0).wait(1).to({alpha:0.257},0).wait(1).to({alpha:0.271},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.314},0).wait(1).to({alpha:0.329},0).wait(1).to({alpha:0.343},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.371},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.457},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.486},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.514},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(3).to({alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 120
	this.instance_5 = new lib.Символ120();
	this.instance_5.parent = this;
	this.instance_5.setTransform(738,150,1,1,0,0,0,51,30);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(190).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(74).to({alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Ресурс 102@2x.png
	this.instance_6 = new lib.Символ119();
	this.instance_6.parent = this;
	this.instance_6.setTransform(773.3,424.3,1,1,0,0,0,208.3,21.3);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(49).to({_off:false},0).wait(1).to({regX:63.5,regY:37.5,x:628.5,y:440.5,alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.129},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.157},0).wait(1).to({alpha:0.171},0).wait(1).to({alpha:0.186},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.229},0).wait(1).to({alpha:0.243},0).wait(1).to({alpha:0.257},0).wait(1).to({alpha:0.271},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.314},0).wait(1).to({alpha:0.329},0).wait(1).to({alpha:0.343},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.371},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.457},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.486},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.514},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(1).to({regX:208.3,regY:21.3,x:773.3,y:424.3},0).wait(213).to({regX:63.5,regY:37.5,x:628.5,y:440.5,alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 116
	this.instance_7 = new lib.Символ116();
	this.instance_7.parent = this;
	this.instance_7.setTransform(493.5,280.5,1,1,0,0,0,108.5,63.5);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(49).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.129},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.157},0).wait(1).to({alpha:0.171},0).wait(1).to({alpha:0.186},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.229},0).wait(1).to({alpha:0.243},0).wait(1).to({alpha:0.257},0).wait(1).to({alpha:0.271},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.314},0).wait(1).to({alpha:0.329},0).wait(1).to({alpha:0.343},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.371},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.457},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.486},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.514},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(214).to({alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Ресурс 2@2x.png
	this.instance_8 = new lib.Символ127();
	this.instance_8.parent = this;
	this.instance_8.setTransform(150,304,1,1,0,0,0,94,95);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.instance_9 = new lib.Ресурс22x();
	this.instance_9.parent = this;
	this.instance_9.setTransform(56,209);

	this.instance_10 = new lib.Символ128();
	this.instance_10.parent = this;
	this.instance_10.setTransform(150,304,1,1,0,0,0,94,95);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_8}]},120).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},142).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_10}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(120).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).to({_off:true},1).wait(201));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(332).to({_off:false},0).wait(1).to({alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Ресурс 36@2x.png - копия 4
	this.instance_11 = new lib.Символ115();
	this.instance_11.parent = this;
	this.instance_11.setTransform(527.5,86,1,1,0,0,0,74.5,57);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(120).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(144).to({alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Ресурс 54@2x.png
	this.instance_12 = new lib.Символ114();
	this.instance_12.parent = this;
	this.instance_12.setTransform(695.5,224,1,1,0,0,0,149.5,113);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(190).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(74).to({alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Ресурс 22@2x.png - копия 3
	this.instance_13 = new lib.Символ117();
	this.instance_13.parent = this;
	this.instance_13.setTransform(789,412.5,1,1,0,0,0,64,93.5);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.instance_14 = new lib.Ресурс222xpngкопия3();
	this.instance_14.parent = this;
	this.instance_14.setTransform(725,319);

	this.instance_15 = new lib.Символ118();
	this.instance_15.parent = this;
	this.instance_15.setTransform(789,412.5,1,1,0,0,0,64,93.5);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_13}]},49).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},212).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_15}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(49).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.129},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.157},0).wait(1).to({alpha:0.171},0).wait(1).to({alpha:0.186},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.229},0).wait(1).to({alpha:0.243},0).wait(1).to({alpha:0.257},0).wait(1).to({alpha:0.271},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.314},0).wait(1).to({alpha:0.329},0).wait(1).to({alpha:0.343},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.371},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.457},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.486},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.514},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).to({_off:true},1).wait(271));
	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(332).to({_off:false},0).wait(1).to({alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 113
	this.instance_16 = new lib.Символ113();
	this.instance_16.parent = this;
	this.instance_16.setTransform(603.5,382.5,1,1,0,0,0,111.5,46.5);
	this.instance_16.alpha = 0;
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(49).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.129},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.157},0).wait(1).to({alpha:0.171},0).wait(1).to({alpha:0.186},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.229},0).wait(1).to({alpha:0.243},0).wait(1).to({alpha:0.257},0).wait(1).to({alpha:0.271},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.314},0).wait(1).to({alpha:0.329},0).wait(1).to({alpha:0.343},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.371},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.457},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.486},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.514},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(214).to({alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 112
	this.instance_17 = new lib.Символ112();
	this.instance_17.parent = this;
	this.instance_17.setTransform(269.5,178,1,1,0,0,0,104.5,67);
	this.instance_17.alpha = 0;
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(260).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(4).to({alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 111
	this.instance_18 = new lib.Символ111();
	this.instance_18.parent = this;
	this.instance_18.setTransform(334.5,247.5,1,1,0,0,0,136.5,80.5);
	this.instance_18.alpha = 0;
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(120).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(144).to({alpha:0.983},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:0.948},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.845},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.81},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.741},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.707},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.672},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.569},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.431},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.328},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.293},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.259},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.224},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.19},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.155},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.052},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.017},0).wait(1).to({alpha:0},0).wait(1));

	// Слой 2
	this.instance_19 = new lib.d();
	this.instance_19.parent = this;
	this.instance_19.setTransform(0,0,0.469,0.556);

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(391));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(450,300,900,600);
// library properties:
lib.properties = {
	width: 900,
	height: 600,
	fps: 60,
	color: "#4DAFCD",
	opacity: 1.00,
	manifest: [
		{src:"images/animation_6_atlas_.png?1518764314675", id:"animation_6_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;