(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"animation_4_atlas_", frames: [[1922,0,123,73],[1431,1195,129,76],[1301,1082,128,187],[906,1082,217,127],[275,1082,188,190],[1922,75,117,69],[1922,146,99,59],[465,1082,229,134],[1834,1082,149,114],[1431,1082,190,111],[1623,1171,140,82],[1623,1082,209,87],[1125,1082,174,140],[0,1082,273,161],[696,1082,208,135],[0,0,1920,1080]]}
];


// symbols:



(lib.Ресурс142x = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс182x = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс222xpngкопия = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс272xpngкопия = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс22x = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс332xpngкопия = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс342x = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс352xpngкопия = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс362xpngкопия3 = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс3722x = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс372xpngкопия = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс432x = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс442x = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс452x = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.Ресурс462x = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.d = function() {
	this.spriteSheet = ss["animation_4_atlas_"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Символ94 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс22x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ94, new cjs.Rectangle(0,0,188,190), null);


(lib.Символ93 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс22x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ93, new cjs.Rectangle(0,0,188,190), null);


(lib.Символ92 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс372xpngкопия();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ92, new cjs.Rectangle(0,0,140,82), null);


(lib.Символ91 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс352xpngкопия();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ91, new cjs.Rectangle(0,0,229,134), null);


(lib.Символ90 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс342x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ90, new cjs.Rectangle(0,0,99,59), null);


(lib.Символ89 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс332xpngкопия();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ89, new cjs.Rectangle(0,0,117,69), null);


(lib.Символ88 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс3722x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ88, new cjs.Rectangle(0,0,190,111), null);


(lib.Символ87 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс142x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ87, new cjs.Rectangle(0,0,123,73), null);


(lib.Символ86 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс182x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ86, new cjs.Rectangle(0,0,129,76), null);


(lib.Символ85 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс462x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ85, new cjs.Rectangle(0,0,208,135), null);


(lib.Символ84 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс362xpngкопия3();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ84, new cjs.Rectangle(0,0,149,114), null);


(lib.Символ83 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс442x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ83, new cjs.Rectangle(0,0,174,140), null);


(lib.Символ82 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс222xpngкопия();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ82, new cjs.Rectangle(0,0,128,187), null);


(lib.Символ81 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс432x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ81, new cjs.Rectangle(0,0,209,87), null);


(lib.Символ80 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс272xpngкопия();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ80, new cjs.Rectangle(0,0,217,127), null);


(lib.Символ79 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Ресурс452x();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ79, new cjs.Rectangle(0,0,273,161), null);


// stage content:
(lib.animation4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Символ 92
	this.instance = new lib.Символ92();
	this.instance.parent = this;
	this.instance.setTransform(207,145,1,1,0,0,0,70,41);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(189).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0.086},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.129},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.157},0).wait(1).to({alpha:0.171},0).wait(1).to({alpha:0.186},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.229},0).wait(1).to({alpha:0.243},0).wait(1).to({alpha:0.257},0).wait(1).to({alpha:0.271},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.314},0).wait(1).to({alpha:0.329},0).wait(1).to({alpha:0.343},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.371},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.457},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.486},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.514},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.543},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.629},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.657},0).wait(1).to({alpha:0.671},0).wait(1).to({alpha:0.686},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.729},0).wait(1).to({alpha:0.743},0).wait(1).to({alpha:0.757},0).wait(1).to({alpha:0.771},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.814},0).wait(1).to({alpha:0.829},0).wait(1).to({alpha:0.843},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.871},0).wait(1).to({alpha:0.886},0).wait(1).to({alpha:0.9},0).wait(1).to({alpha:0.914},0).wait(1).to({alpha:0.929},0).wait(1).to({alpha:0.943},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.985},0).wait(1).to({alpha:0.97},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.924},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.894},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.803},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.773},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.561},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.47},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.439},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.258},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.197},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.106},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.076},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 91
	this.instance_1 = new lib.Символ91();
	this.instance_1.parent = this;
	this.instance_1.setTransform(302.5,213,1,1,0,0,0,114.5,67);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(50).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(141).to({alpha:0.985},0).wait(1).to({alpha:0.97},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.924},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.894},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.803},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.773},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.561},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.47},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.439},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.258},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.197},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.106},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.076},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 90
	this.instance_2 = new lib.Символ90();
	this.instance_2.parent = this;
	this.instance_2.setTransform(581.5,160.5,1,1,0,0,0,49.5,29.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(120).to({_off:false},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.044},0).wait(1).to({alpha:0.059},0).wait(1).to({alpha:0.074},0).wait(1).to({alpha:0.088},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.118},0).wait(1).to({alpha:0.132},0).wait(1).to({alpha:0.147},0).wait(1).to({alpha:0.162},0).wait(1).to({alpha:0.176},0).wait(1).to({alpha:0.191},0).wait(1).to({alpha:0.206},0).wait(1).to({alpha:0.221},0).wait(1).to({alpha:0.235},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.265},0).wait(1).to({alpha:0.279},0).wait(1).to({alpha:0.294},0).wait(1).to({alpha:0.309},0).wait(1).to({alpha:0.324},0).wait(1).to({alpha:0.338},0).wait(1).to({alpha:0.353},0).wait(1).to({alpha:0.368},0).wait(1).to({alpha:0.382},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.412},0).wait(1).to({alpha:0.426},0).wait(1).to({alpha:0.441},0).wait(1).to({alpha:0.456},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.544},0).wait(1).to({alpha:0.559},0).wait(1).to({alpha:0.574},0).wait(1).to({alpha:0.588},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.618},0).wait(1).to({alpha:0.632},0).wait(1).to({alpha:0.647},0).wait(1).to({alpha:0.662},0).wait(1).to({alpha:0.676},0).wait(1).to({alpha:0.691},0).wait(1).to({alpha:0.706},0).wait(1).to({alpha:0.721},0).wait(1).to({alpha:0.735},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.765},0).wait(1).to({alpha:0.779},0).wait(1).to({alpha:0.794},0).wait(1).to({alpha:0.809},0).wait(1).to({alpha:0.824},0).wait(1).to({alpha:0.838},0).wait(1).to({alpha:0.853},0).wait(1).to({alpha:0.868},0).wait(1).to({alpha:0.882},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.912},0).wait(1).to({alpha:0.926},0).wait(1).to({alpha:0.941},0).wait(1).to({alpha:0.956},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.985},0).wait(1).to({alpha:1},0).wait(72).to({alpha:0.985},0).wait(1).to({alpha:0.97},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.924},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.894},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.803},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.773},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.561},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.47},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.439},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.258},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.197},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.106},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.076},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 89
	this.instance_3 = new lib.Символ89();
	this.instance_3.parent = this;
	this.instance_3.setTransform(579.5,486.5,1,1,0,0,0,58.5,34.5);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(120).to({_off:false},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.044},0).wait(1).to({alpha:0.059},0).wait(1).to({alpha:0.074},0).wait(1).to({alpha:0.088},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.118},0).wait(1).to({alpha:0.132},0).wait(1).to({alpha:0.147},0).wait(1).to({alpha:0.162},0).wait(1).to({alpha:0.176},0).wait(1).to({alpha:0.191},0).wait(1).to({alpha:0.206},0).wait(1).to({alpha:0.221},0).wait(1).to({alpha:0.235},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.265},0).wait(1).to({alpha:0.279},0).wait(1).to({alpha:0.294},0).wait(1).to({alpha:0.309},0).wait(1).to({alpha:0.324},0).wait(1).to({alpha:0.338},0).wait(1).to({alpha:0.353},0).wait(1).to({alpha:0.368},0).wait(1).to({alpha:0.382},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.412},0).wait(1).to({alpha:0.426},0).wait(1).to({alpha:0.441},0).wait(1).to({alpha:0.456},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.544},0).wait(1).to({alpha:0.559},0).wait(1).to({alpha:0.574},0).wait(1).to({alpha:0.588},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.618},0).wait(1).to({alpha:0.632},0).wait(1).to({alpha:0.647},0).wait(1).to({alpha:0.662},0).wait(1).to({alpha:0.676},0).wait(1).to({alpha:0.691},0).wait(1).to({alpha:0.706},0).wait(1).to({alpha:0.721},0).wait(1).to({alpha:0.735},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.765},0).wait(1).to({alpha:0.779},0).wait(1).to({alpha:0.794},0).wait(1).to({alpha:0.809},0).wait(1).to({alpha:0.824},0).wait(1).to({alpha:0.838},0).wait(1).to({alpha:0.853},0).wait(1).to({alpha:0.868},0).wait(1).to({alpha:0.882},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.912},0).wait(1).to({alpha:0.926},0).wait(1).to({alpha:0.941},0).wait(1).to({alpha:0.956},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.985},0).wait(1).to({alpha:1},0).wait(72).to({alpha:0.985},0).wait(1).to({alpha:0.97},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.924},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.894},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.803},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.773},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.561},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.47},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.439},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.258},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.197},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.106},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.076},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 88
	this.instance_4 = new lib.Символ88();
	this.instance_4.parent = this;
	this.instance_4.setTransform(416,147.5,1,1,0,0,0,95,55.5);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(50).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(141).to({alpha:0.985},0).wait(1).to({alpha:0.97},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.924},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.894},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.803},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.773},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.561},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.47},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.439},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.258},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.197},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.106},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.076},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 87
	this.instance_5 = new lib.Символ87();
	this.instance_5.parent = this;
	this.instance_5.setTransform(665.5,557.5,1,1,0,0,0,61.5,36.5);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(120).to({_off:false},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.044},0).wait(1).to({alpha:0.059},0).wait(1).to({alpha:0.074},0).wait(1).to({alpha:0.088},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.118},0).wait(1).to({alpha:0.132},0).wait(1).to({alpha:0.147},0).wait(1).to({alpha:0.162},0).wait(1).to({alpha:0.176},0).wait(1).to({alpha:0.191},0).wait(1).to({alpha:0.206},0).wait(1).to({alpha:0.221},0).wait(1).to({alpha:0.235},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.265},0).wait(1).to({alpha:0.279},0).wait(1).to({alpha:0.294},0).wait(1).to({alpha:0.309},0).wait(1).to({alpha:0.324},0).wait(1).to({alpha:0.338},0).wait(1).to({alpha:0.353},0).wait(1).to({alpha:0.368},0).wait(1).to({alpha:0.382},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.412},0).wait(1).to({alpha:0.426},0).wait(1).to({alpha:0.441},0).wait(1).to({alpha:0.456},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.544},0).wait(1).to({alpha:0.559},0).wait(1).to({alpha:0.574},0).wait(1).to({alpha:0.588},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.618},0).wait(1).to({alpha:0.632},0).wait(1).to({alpha:0.647},0).wait(1).to({alpha:0.662},0).wait(1).to({alpha:0.676},0).wait(1).to({alpha:0.691},0).wait(1).to({alpha:0.706},0).wait(1).to({alpha:0.721},0).wait(1).to({alpha:0.735},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.765},0).wait(1).to({alpha:0.779},0).wait(1).to({alpha:0.794},0).wait(1).to({alpha:0.809},0).wait(1).to({alpha:0.824},0).wait(1).to({alpha:0.838},0).wait(1).to({alpha:0.853},0).wait(1).to({alpha:0.868},0).wait(1).to({alpha:0.882},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.912},0).wait(1).to({alpha:0.926},0).wait(1).to({alpha:0.941},0).wait(1).to({alpha:0.956},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.985},0).wait(1).to({alpha:1},0).wait(72).to({alpha:0.985},0).wait(1).to({alpha:0.97},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.924},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.894},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.803},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.773},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.561},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.47},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.439},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.258},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.197},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.106},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.076},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 86
	this.instance_6 = new lib.Символ86();
	this.instance_6.parent = this;
	this.instance_6.setTransform(369.5,393,1,1,0,0,0,64.5,38);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(50).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(141).to({alpha:0.985},0).wait(1).to({alpha:0.97},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.924},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.894},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.803},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.773},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.561},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.47},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.439},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.258},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.197},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.106},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.076},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0},0).wait(1));

	// Ресурс 2@2x.png
	this.instance_7 = new lib.Символ93();
	this.instance_7.parent = this;
	this.instance_7.setTransform(146,289,1,1,0,0,0,94,95);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.instance_8 = new lib.Ресурс22x();
	this.instance_8.parent = this;
	this.instance_8.setTransform(52,194);

	this.instance_9 = new lib.Символ94();
	this.instance_9.parent = this;
	this.instance_9.setTransform(146,289,1,1,0,0,0,94,95);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_7}]},50).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},179).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_9}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(50).to({_off:false},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.069},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.138},0).wait(1).to({alpha:0.172},0).wait(1).to({alpha:0.207},0).wait(1).to({alpha:0.241},0).wait(1).to({alpha:0.276},0).wait(1).to({alpha:0.31},0).wait(1).to({alpha:0.345},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.414},0).wait(1).to({alpha:0.448},0).wait(1).to({alpha:0.483},0).wait(1).to({alpha:0.517},0).wait(1).to({alpha:0.552},0).wait(1).to({alpha:0.586},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.655},0).wait(1).to({alpha:0.69},0).wait(1).to({alpha:0.724},0).wait(1).to({alpha:0.759},0).wait(1).to({alpha:0.793},0).wait(1).to({alpha:0.828},0).wait(1).to({alpha:0.862},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.931},0).wait(1).to({alpha:0.966},0).wait(1).to({alpha:1},0).to({_off:true},1).wait(246));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(259).to({_off:false},0).wait(34).to({alpha:0.97},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 85
	this.instance_10 = new lib.Символ85();
	this.instance_10.parent = this;
	this.instance_10.setTransform(259,189.5,1,1,0,0,0,104,67.5);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(189).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.716},0).wait(1).to({alpha:0.705},0).wait(1).to({alpha:0.693},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.67},0).wait(1).to({alpha:0.659},0).wait(1).to({alpha:0.648},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.625},0).wait(1).to({alpha:0.614},0).wait(1).to({alpha:0.602},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.568},0).wait(1).to({alpha:0.557},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.534},0).wait(1).to({alpha:0.523},0).wait(1).to({alpha:0.511},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.489},0).wait(1).to({alpha:0.477},0).wait(1).to({alpha:0.466},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.443},0).wait(1).to({alpha:0.432},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.398},0).wait(1).to({alpha:0.386},0).wait(1).to({alpha:0.375},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.352},0).wait(1).to({alpha:0.341},0).wait(1).to({alpha:0.33},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.307},0).wait(1).to({alpha:0.295},0).wait(1).to({alpha:0.284},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.239},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.216},0).wait(1).to({alpha:0.205},0).wait(1).to({alpha:0.193},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.17},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.148},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.125},0).wait(1).to({alpha:0.114},0).wait(1).to({alpha:0.102},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.08},0).wait(1).to({alpha:0.068},0).wait(1).to({alpha:0.057},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.034},0).wait(1).to({alpha:0.023},0).wait(1).to({alpha:0.011},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 84
	this.instance_11 = new lib.Символ84();
	this.instance_11.parent = this;
	this.instance_11.setTransform(468.5,90,1,1,0,0,0,74.5,57);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(50).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(141).to({alpha:0.985},0).wait(1).to({alpha:0.97},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.924},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.894},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.803},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.773},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.561},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.47},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.439},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.258},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.197},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.106},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.076},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 83
	this.instance_12 = new lib.Символ83();
	this.instance_12.parent = this;
	this.instance_12.setTransform(529,206,1,1,0,0,0,87,70);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(120).to({_off:false},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.044},0).wait(1).to({alpha:0.059},0).wait(1).to({alpha:0.074},0).wait(1).to({alpha:0.088},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.118},0).wait(1).to({alpha:0.132},0).wait(1).to({alpha:0.147},0).wait(1).to({alpha:0.162},0).wait(1).to({alpha:0.176},0).wait(1).to({alpha:0.191},0).wait(1).to({alpha:0.206},0).wait(1).to({alpha:0.221},0).wait(1).to({alpha:0.235},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.265},0).wait(1).to({alpha:0.279},0).wait(1).to({alpha:0.294},0).wait(1).to({alpha:0.309},0).wait(1).to({alpha:0.324},0).wait(1).to({alpha:0.338},0).wait(1).to({alpha:0.353},0).wait(1).to({alpha:0.368},0).wait(1).to({alpha:0.382},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.412},0).wait(1).to({alpha:0.426},0).wait(1).to({alpha:0.441},0).wait(1).to({alpha:0.456},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.544},0).wait(1).to({alpha:0.559},0).wait(1).to({alpha:0.574},0).wait(1).to({alpha:0.588},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.618},0).wait(1).to({alpha:0.632},0).wait(1).to({alpha:0.647},0).wait(1).to({alpha:0.662},0).wait(1).to({alpha:0.676},0).wait(1).to({alpha:0.691},0).wait(1).to({alpha:0.706},0).wait(1).to({alpha:0.721},0).wait(1).to({alpha:0.735},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.765},0).wait(1).to({alpha:0.779},0).wait(1).to({alpha:0.794},0).wait(1).to({alpha:0.809},0).wait(1).to({alpha:0.824},0).wait(1).to({alpha:0.838},0).wait(1).to({alpha:0.853},0).wait(1).to({alpha:0.868},0).wait(1).to({alpha:0.882},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.912},0).wait(1).to({alpha:0.926},0).wait(1).to({alpha:0.941},0).wait(1).to({alpha:0.956},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.985},0).wait(1).to({alpha:1},0).wait(72).to({alpha:0.985},0).wait(1).to({alpha:0.97},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.924},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.894},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.803},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.773},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.561},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.47},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.439},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.258},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.197},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.106},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.076},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 82
	this.instance_13 = new lib.Символ82();
	this.instance_13.parent = this;
	this.instance_13.setTransform(737,454.5,1,1,0,0,0,64,93.5);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(120).to({_off:false},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.044},0).wait(1).to({alpha:0.059},0).wait(1).to({alpha:0.074},0).wait(1).to({alpha:0.088},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.118},0).wait(1).to({alpha:0.132},0).wait(1).to({alpha:0.147},0).wait(1).to({alpha:0.162},0).wait(1).to({alpha:0.176},0).wait(1).to({alpha:0.191},0).wait(1).to({alpha:0.206},0).wait(1).to({alpha:0.221},0).wait(1).to({alpha:0.235},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.265},0).wait(1).to({alpha:0.279},0).wait(1).to({alpha:0.294},0).wait(1).to({alpha:0.309},0).wait(1).to({alpha:0.324},0).wait(1).to({alpha:0.338},0).wait(1).to({alpha:0.353},0).wait(1).to({alpha:0.368},0).wait(1).to({alpha:0.382},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.412},0).wait(1).to({alpha:0.426},0).wait(1).to({alpha:0.441},0).wait(1).to({alpha:0.456},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.544},0).wait(1).to({alpha:0.559},0).wait(1).to({alpha:0.574},0).wait(1).to({alpha:0.588},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.618},0).wait(1).to({alpha:0.632},0).wait(1).to({alpha:0.647},0).wait(1).to({alpha:0.662},0).wait(1).to({alpha:0.676},0).wait(1).to({alpha:0.691},0).wait(1).to({alpha:0.706},0).wait(1).to({alpha:0.721},0).wait(1).to({alpha:0.735},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.765},0).wait(1).to({alpha:0.779},0).wait(1).to({alpha:0.794},0).wait(1).to({alpha:0.809},0).wait(1).to({alpha:0.824},0).wait(1).to({alpha:0.838},0).wait(1).to({alpha:0.853},0).wait(1).to({alpha:0.868},0).wait(1).to({alpha:0.882},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.912},0).wait(1).to({alpha:0.926},0).wait(1).to({alpha:0.941},0).wait(1).to({alpha:0.956},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.985},0).wait(1).to({alpha:1},0).wait(72).to({alpha:0.985},0).wait(1).to({alpha:0.97},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.924},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.894},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.803},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.773},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.561},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.47},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.439},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.258},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.197},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.106},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.076},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 81
	this.instance_14 = new lib.Символ81();
	this.instance_14.parent = this;
	this.instance_14.setTransform(553.5,432.5,1,1,0,0,0,104.5,43.5);
	this.instance_14.alpha = 0;
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(120).to({_off:false},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.044},0).wait(1).to({alpha:0.059},0).wait(1).to({alpha:0.074},0).wait(1).to({alpha:0.088},0).wait(1).to({alpha:0.103},0).wait(1).to({alpha:0.118},0).wait(1).to({alpha:0.132},0).wait(1).to({alpha:0.147},0).wait(1).to({alpha:0.162},0).wait(1).to({alpha:0.176},0).wait(1).to({alpha:0.191},0).wait(1).to({alpha:0.206},0).wait(1).to({alpha:0.221},0).wait(1).to({alpha:0.235},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.265},0).wait(1).to({alpha:0.279},0).wait(1).to({alpha:0.294},0).wait(1).to({alpha:0.309},0).wait(1).to({alpha:0.324},0).wait(1).to({alpha:0.338},0).wait(1).to({alpha:0.353},0).wait(1).to({alpha:0.368},0).wait(1).to({alpha:0.382},0).wait(1).to({alpha:0.397},0).wait(1).to({alpha:0.412},0).wait(1).to({alpha:0.426},0).wait(1).to({alpha:0.441},0).wait(1).to({alpha:0.456},0).wait(1).to({alpha:0.471},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.529},0).wait(1).to({alpha:0.544},0).wait(1).to({alpha:0.559},0).wait(1).to({alpha:0.574},0).wait(1).to({alpha:0.588},0).wait(1).to({alpha:0.603},0).wait(1).to({alpha:0.618},0).wait(1).to({alpha:0.632},0).wait(1).to({alpha:0.647},0).wait(1).to({alpha:0.662},0).wait(1).to({alpha:0.676},0).wait(1).to({alpha:0.691},0).wait(1).to({alpha:0.706},0).wait(1).to({alpha:0.721},0).wait(1).to({alpha:0.735},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.765},0).wait(1).to({alpha:0.779},0).wait(1).to({alpha:0.794},0).wait(1).to({alpha:0.809},0).wait(1).to({alpha:0.824},0).wait(1).to({alpha:0.838},0).wait(1).to({alpha:0.853},0).wait(1).to({alpha:0.868},0).wait(1).to({alpha:0.882},0).wait(1).to({alpha:0.897},0).wait(1).to({alpha:0.912},0).wait(1).to({alpha:0.926},0).wait(1).to({alpha:0.941},0).wait(1).to({alpha:0.956},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.985},0).wait(1).to({alpha:1},0).wait(72).to({alpha:0.985},0).wait(1).to({alpha:0.97},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.924},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.894},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.803},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.773},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.561},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.47},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.439},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.258},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.197},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.106},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.076},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 80
	this.instance_15 = new lib.Символ80();
	this.instance_15.parent = this;
	this.instance_15.setTransform(440.5,329.5,1,1,0,0,0,108.5,63.5);
	this.instance_15.alpha = 0;
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(50).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(141).to({alpha:0.985},0).wait(1).to({alpha:0.97},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.924},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.894},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.803},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.773},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.561},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.47},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.439},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.258},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.197},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.106},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.076},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0},0).wait(1));

	// Символ 79
	this.instance_16 = new lib.Символ79();
	this.instance_16.parent = this;
	this.instance_16.setTransform(291.5,248.5,1,1,0,0,0,136.5,80.5);
	this.instance_16.alpha = 0;
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(50).to({_off:false},0).wait(1).to({alpha:0.014},0).wait(1).to({alpha:0.029},0).wait(1).to({alpha:0.043},0).wait(1).to({alpha:0.058},0).wait(1).to({alpha:0.072},0).wait(1).to({alpha:0.087},0).wait(1).to({alpha:0.101},0).wait(1).to({alpha:0.116},0).wait(1).to({alpha:0.13},0).wait(1).to({alpha:0.145},0).wait(1).to({alpha:0.159},0).wait(1).to({alpha:0.174},0).wait(1).to({alpha:0.188},0).wait(1).to({alpha:0.203},0).wait(1).to({alpha:0.217},0).wait(1).to({alpha:0.232},0).wait(1).to({alpha:0.246},0).wait(1).to({alpha:0.261},0).wait(1).to({alpha:0.275},0).wait(1).to({alpha:0.29},0).wait(1).to({alpha:0.304},0).wait(1).to({alpha:0.319},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.362},0).wait(1).to({alpha:0.377},0).wait(1).to({alpha:0.391},0).wait(1).to({alpha:0.406},0).wait(1).to({alpha:0.42},0).wait(1).to({alpha:0.435},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.464},0).wait(1).to({alpha:0.478},0).wait(1).to({alpha:0.493},0).wait(1).to({alpha:0.507},0).wait(1).to({alpha:0.522},0).wait(1).to({alpha:0.536},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.565},0).wait(1).to({alpha:0.58},0).wait(1).to({alpha:0.594},0).wait(1).to({alpha:0.609},0).wait(1).to({alpha:0.623},0).wait(1).to({alpha:0.638},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.681},0).wait(1).to({alpha:0.696},0).wait(1).to({alpha:0.71},0).wait(1).to({alpha:0.725},0).wait(1).to({alpha:0.739},0).wait(1).to({alpha:0.754},0).wait(1).to({alpha:0.768},0).wait(1).to({alpha:0.783},0).wait(1).to({alpha:0.797},0).wait(1).to({alpha:0.812},0).wait(1).to({alpha:0.826},0).wait(1).to({alpha:0.841},0).wait(1).to({alpha:0.855},0).wait(1).to({alpha:0.87},0).wait(1).to({alpha:0.884},0).wait(1).to({alpha:0.899},0).wait(1).to({alpha:0.913},0).wait(1).to({alpha:0.928},0).wait(1).to({alpha:0.942},0).wait(1).to({alpha:0.957},0).wait(1).to({alpha:0.971},0).wait(1).to({alpha:0.986},0).wait(1).to({alpha:1},0).wait(141).to({alpha:0.985},0).wait(1).to({alpha:0.97},0).wait(1).to({alpha:0.955},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.924},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:0.894},0).wait(1).to({alpha:0.879},0).wait(1).to({alpha:0.864},0).wait(1).to({alpha:0.848},0).wait(1).to({alpha:0.833},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.803},0).wait(1).to({alpha:0.788},0).wait(1).to({alpha:0.773},0).wait(1).to({alpha:0.758},0).wait(1).to({alpha:0.742},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.712},0).wait(1).to({alpha:0.697},0).wait(1).to({alpha:0.682},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.652},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.621},0).wait(1).to({alpha:0.606},0).wait(1).to({alpha:0.591},0).wait(1).to({alpha:0.576},0).wait(1).to({alpha:0.561},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.53},0).wait(1).to({alpha:0.515},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.485},0).wait(1).to({alpha:0.47},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.439},0).wait(1).to({alpha:0.424},0).wait(1).to({alpha:0.409},0).wait(1).to({alpha:0.394},0).wait(1).to({alpha:0.379},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.348},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.318},0).wait(1).to({alpha:0.303},0).wait(1).to({alpha:0.288},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.258},0).wait(1).to({alpha:0.242},0).wait(1).to({alpha:0.227},0).wait(1).to({alpha:0.212},0).wait(1).to({alpha:0.197},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.152},0).wait(1).to({alpha:0.136},0).wait(1).to({alpha:0.121},0).wait(1).to({alpha:0.106},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.076},0).wait(1).to({alpha:0.061},0).wait(1).to({alpha:0.045},0).wait(1).to({alpha:0.03},0).wait(1).to({alpha:0.015},0).wait(1).to({alpha:0},0).wait(1));

	// Слой 2
	this.instance_17 = new lib.d();
	this.instance_17.parent = this;
	this.instance_17.setTransform(0,0,0.469,0.556);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(326));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(450,300,900,600);
// library properties:
lib.properties = {
	width: 900,
	height: 600,
	fps: 60,
	color: "#4DAFCD",
	opacity: 1.00,
	manifest: [
		{src:"images/animation_4_atlas_.png?1518771931770", id:"animation_4_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;