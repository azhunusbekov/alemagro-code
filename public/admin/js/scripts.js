$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$.fn.select2.defaults.set('theme', 'bootstrap');

(function($) {
    "use strict";

    /*================================
    Preloader
    ==================================*/

    var preloader = $('#preloader');
    $(window).on('load', function() {
        preloader.fadeOut('slow', function() { $(this).remove(); });
    });

    /*================================
    sidebar collapsing
    ==================================*/
    $('.nav-btn').on('click', function() {
        $('.page-container').toggleClass('sbar_collapsed');
    });

    /*================================
    Start Footer resizer
    ==================================*/
    var e = function() {
        var e = (window.innerHeight > 0 ? window.innerHeight : this.screen.height);
        $(".main-content").css("min-height", e + "px")
    };
    $(window).ready(e), $(window).on("resize", e);

    /*================================
    sidebar menu
    ==================================*/
    $("#menu").metisMenu();

    /*================================
    slimscroll activation
    ==================================*/
    $('.menu-inner').slimScroll({
        height: 'auto'
    });

    /*================================
    stickey Header
    ==================================*/
    $(window).on('scroll', function() {
        var scroll = $(window).scrollTop(),
            mainHeader = $('#sticky-header'),
            mainHeaderHeight = mainHeader.innerHeight();

        // console.log(mainHeader.innerHeight());
        if (scroll > 1) {
            $("#sticky-header").addClass("sticky-menu");
        } else {
            $("#sticky-header").removeClass("sticky-menu");
        }
    });

    /*================================
    form bootstrap validation
    ==================================*/
    $('[data-toggle="popover"]').popover()

    /*------------- Start form Validation -------------*/
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);

    /*================================
    Custom
    ==================================*/

    // delete button event
    $('body').on('click', '[data-action="destroy"]', function (e) {
            e.preventDefault();
            $('#modal-destroy').modal('show');
            let url = $(this).data('href');

            function destroy() {
                let button = $(this);

                if (buttonIsLoading(button)) {
                    return;
                }

                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        _method: 'DELETE',
                        _token: window.core_project.csrfToken,
                    },
                    success: function (response) {
                        if (response.redirect) {
                            window.location.href = response.redirect;
                        } else if (response.error) {
                            alert(response.error);
                        } else {
                            window.location.reload();
                        }
                    },
                    beforeSend: function () {
                        setLoadingButton(button)
                    },
                    complete: function () {
                        setLoadingButton(button);
                    },
                    error: function (error) {
                        console.log(error);
                        alert(error);
                    }
                });
            }

            $('#modal-destroy').off('click', '[data-action="yes_destroy"]');
            $('#modal-destroy').on('click', '[data-action="yes_destroy"]', destroy)
        });

    // init select2
    $('.select2').select2({
        minimumResultsForSearch: 10
    });

    // init CKEditor for textarea
    $('textarea.editor').ckeditor();

})(jQuery);

function setLoadingButton(obj) {
    if ($(obj).hasClass('is-loading')) {
        $(obj).removeClass('is-loading');
        return false;
    } else {
        $(obj).addClass('is-loading');
        return true;
    }
}

function buttonIsLoading(obj) {
    return $(obj).hasClass('is-loading')
}
