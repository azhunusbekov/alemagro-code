<?php

Route::get('/', 'MainController@index')->name('home');
Route::get('about', 'PageController@about')->name('about');
Route::get('contact', 'PageController@contact')->name('contact');
Route::get('consult', 'PageController@consult')->name('consult');
Route::get('career', 'PageController@career')->name('career');
Route::get('internship', 'PageController@internship')->name('internship');

Route::post('feedback/main', 'FeedbackController@main')->name('feedback.main');
Route::post('feedback/internship', 'FeedbackController@internship')->name('feedback.internship');
Route::post('feedback/vacancy', 'FeedbackController@vacancy')->name('feedback.vacancy');
Route::post('feedback/product', 'FeedbackController@product')->name('feedback.product');

Route::get('search', 'SearchController@search')->name('search');

Route::group(['prefix' => 'article'], function () {
    Route::get('/', 'ArticleController@index')->name('article.index');
    Route::get('{article_category}', 'ArticleController@category')->name('article.category');
    Route::get('{article_category}/{article_slug}', 'ArticleController@article')->name('article.article');
});

Route::group(['prefix' => 'catalog'], function () {
    Route::get('/', 'CatalogController@index')->name('catalog.index');

    Route::group(['prefix' => '{country_code}'], function () {
        Route::get('/', 'CatalogController@main')->name('catalog.main');
        Route::get('/{catalog_category}', 'CatalogController@category')->name('catalog.category');
        Route::get('/{section_path}', 'CatalogController@section')->where('section_path', '^(?:(?!\/product).)+')->name('catalog.section');
        Route::get('/{product_path}', 'CatalogController@product')->where('product_path', '.+')->name('catalog.product');
    });
});

Route::group(['prefix' => 'culture'], function () {
    Route::get('/', 'CultureController@index')->name('culture.index');
    Route::get('/{catalog_category}', 'CultureController@category')->name('culture.category');
    Route::get('/{catalog_category}/{culture_path}', 'CultureController@culture')
        ->where('culture_path', '.+')
        ->name('culture.culture');
});

Route::group(['prefix' => 'protection'], function () {
    Route::get('/', 'ProtectionController@index')->name('protection.index');
});

Route::get('sitemap.xml', 'SitemapController@sitemap');
