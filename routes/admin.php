<?php

Route::group(['prefix' => 'inside', 'as' => 'admin.'], function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');

    Route::post('upload-image', 'CkeditorController@upload')->name('ckeditor.upload');

    Route::group(['middleware' => 'admin'], function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');

        Route::resource('office', 'OfficeController')->except(['show']);
        Route::resource('banner', 'BannerController')->except(['show']);
        Route::post('banner/reorder', 'BannerController@reorder')->name('banner.reorder');

        Route::resource('partner', 'PartnerController')->except(['show']);
        Route::post('partner/reorder', 'PartnerController@reorder')->name('partner.reorder');

        Route::resource('feedback', 'FeedbackController')->except(['show', 'create', 'store']);

        Route::resource('vacancy', 'VacancyController')->except(['show']);
        Route::post('vacancy/reorder', 'VacancyController@reorder')->name('vacancy.reorder');
        Route::get('vacancy/{vacancy}/responses', 'VacancyController@responses')->name('vacancy.responses');
        Route::get('vacancy/{vacancy}/response/{response}', 'VacancyController@response')->name('vacancy.response.show');

        Route::group(['prefix' => 'article', 'as' => 'article.'], function () {
            Route::resource('category', 'Article\ArticleCategoryController')->except(['show']);
            Route::group(['prefix' => 'category', 'as' => 'category.'], function () {
                Route::resource('{category}/translation', 'Article\ArticleCategoryTranslationController')->except(['index', 'show']);
            });
            Route::resource('article', 'Article\ArticleController')->except(['show']);
            Route::group(['prefix' => 'article/{article}', 'as' => 'article.'], function () {
                Route::resource('translation', 'Article\ArticleTranslationController')->except(['index', 'show']);
            });
        });

        Route::group(['prefix' => 'catalog', 'as' => 'catalog.'], function () {
            Route::resource('producer', 'Catalog\CatalogProducerController')->except(['show']);
            Route::post('producer/reorder', 'Catalog\CatalogProducerController@reorder')->name('producer.reorder');

            Route::resource('category', 'Catalog\CatalogCategoryController')->except(['show']);
            Route::post('category/reorder', 'Catalog\CatalogCategoryController@reorder')->name('category.reorder');
            Route::post('attribute/reorder', 'Catalog\CatalogAttributeController@reorder')->name('attribute.reorder');
            Route::group(['prefix' => 'category/{category}', 'as' => 'category.'], function () {
                Route::resource('attribute', 'Catalog\CatalogAttributeController')->except(['index', 'show']);
                Route::resource('translation', 'Catalog\CatalogCategoryTranslationController')->except(['index', 'show']);
            });

            Route::resource('culture', 'Catalog\CatalogCultureController')->except(['show']);
            Route::post('culture/reorder', 'Catalog\CatalogCultureController@reorder')->name('culture.reorder');
            Route::group(['prefix' => 'culture/{culture}', 'as' => 'culture.'], function () {
                Route::resource('translation', 'Catalog\CatalogCultureTranslationController')->except(['index', 'show']);
            });

            Route::resource('section', 'Catalog\CatalogSectionController')->except(['show']);
            Route::post('section/reorder', 'Catalog\CatalogSectionController@reorder')->name('section.reorder');
            Route::group(['prefix' => 'section/{section}', 'as' => 'section.'], function () {
                Route::resource('translation', 'Catalog\CatalogSectionTranslationController')->except(['index', 'show']);
            });

            Route::resource('protection', 'Catalog\CatalogProtectionController')->except(['show']);
            Route::group(['prefix' => 'protection/{protection}', 'as' => 'protection.'], function () {
                Route::resource('translation', 'Catalog\CatalogProtectionTranslationController')->except(['index', 'show']);
            });

            Route::resource('disease', 'Catalog\CatalogDiseaseController')->except(['show']);
            Route::group(['prefix' => 'disease/{disease}', 'as' => 'disease.'], function () {
                Route::resource('translation', 'Catalog\CatalogDiseaseTranslationController')->except(['index', 'show']);
            });

            Route::resource('substance', 'Catalog\CatalogSubstanceController')->except(['show']);
            Route::group(['prefix' => 'substance/{substance}', 'as' => 'substance.'], function () {
                Route::resource('translation', 'Catalog\CatalogSubstanceTranslationController')->except(['index', 'show']);
            });

            Route::group(['prefix' => 'product'], function () {
                Route::get('/', 'Catalog\CatalogProductController@index')->name('product.index');
                Route::get('create/{category}', 'Catalog\CatalogProductController@create')->name('product.create');
                Route::post('store/{category}', 'Catalog\CatalogProductController@store')->name('product.store');
                Route::get('{product}/edit', 'Catalog\CatalogProductController@edit')->name('product.edit');
                Route::put('{product}/update/{category}', 'Catalog\CatalogProductController@update')->name('product.update');
                Route::delete('{product}/destroy', 'Catalog\CatalogProductController@destroy')->name('product.destroy');
                Route::group(['prefix' => '{product}', 'as' => 'product.'], function () {
                    Route::resource('translation', 'Catalog\CatalogProductTranslationController')->except(['index', 'show']);
                });
            });

            Route::get('import', 'Catalog\CatalogImportController@form')->name('import.form');
            Route::post('import', 'Catalog\CatalogImportController@store')->name('import.store');
        });
    });
});
