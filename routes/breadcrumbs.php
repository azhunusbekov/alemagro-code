<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push(trans('menu.home'), lang_route('home'));
});

// Home > About
Breadcrumbs::for('about', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('menu.about'), lang_route('about'));
});

// Home > Contact
Breadcrumbs::for('consult', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('menu.contact'), lang_route('consult'));
});

// Home > Offices
Breadcrumbs::for('contact', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('menu.contact'), lang_route('contact'));
});

// Home > Articles
Breadcrumbs::for('article', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('menu.actual'), lang_route('article.index'));
});

// Home > Articles > Category
Breadcrumbs::for('article.category', function ($trail, \App\Models\Article\ArticleCategory $category) {
    $trail->parent('article');
    $trail->push($category->title, lang_route('article.category', ['article_category' => $category->slug]));
});

// Home > Articles > Category -> Single Article
Breadcrumbs::for('article.article', function ($trail, \App\Models\Article\ArticleCategory $category, \App\Models\Article\Article $article) {
    $trail->parent('article.category', $category);
    $trail->push($article->title, lang_route('article.article', [
        'article_category' => $category->slug,
        'article_slug' => $article->slug
    ]));
});

// Home > Cultures
Breadcrumbs::for('culture', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('menu.cultures'), lang_route('culture.index'));
});

// Home > Cultures > Category
Breadcrumbs::for('culture.category', function ($trail, \App\Models\Catalog\CatalogCategory $category) {
    $trail->parent('culture');
    $trail->push($category->title, lang_route('culture.category', ['catalog_category' => $category->slug]));
});

// Home > Cultures > Category > Culture
Breadcrumbs::for('culture.culture', function ($trail, \App\Models\Catalog\CatalogCategory $category, \App\Models\Catalog\CatalogCulture $culture) {
    if ($culture->parent) {
        $trail->parent('culture.culture', $category, $culture->parent);
    } else {
        $trail->parent('culture.category', $category);
    }

    $trail->push($culture->title, lang_route('culture.culture', ['category' => $category->slug, 'culture_path' => $culture->getPath()]));
});

// Home > Catalog
Breadcrumbs::for('catalog', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('catalog.title'), lang_route('catalog.index'));
});

// Home > Catalog > Country
Breadcrumbs::for('catalog.main', function ($trail, $countryCode) {
    $trail->parent('catalog');
    $trail->push(trans('catalog.products_'.$countryCode), lang_route('catalog.main', ['country_code' => $countryCode]));
});

// Home > Catalog > Category
Breadcrumbs::for('catalog.category', function ($trail, $countryCode, \App\Models\Catalog\CatalogCategory $category) {
    $trail->parent('catalog.main', $countryCode);
    $trail->push($category->title, lang_route('catalog.category', [
        'country_code' => $countryCode,
        'catalog_category' => $category->slug
    ]));
});

// Home > Catalog > Category > Section
Breadcrumbs::for('catalog.section', function ($trail, $countryCode, \App\Models\Catalog\CatalogCategory $category, \App\Models\Catalog\CatalogSection $section = null) {
    $trail->parent('catalog.category', $countryCode, $category);

    if ($section) {
        if (!is_null($section->parent_id)) {
            $trail->push($section->parent->title, $section->parent->getLink($countryCode, $category));
        }
        $trail->push($section->title, $section->getLink($countryCode, $category));
    }
});

// Home > Catalog > Category > Product
Breadcrumbs::for('catalog.product', function ($trail,
                                              $countryCode,
                                              \App\Models\Catalog\CatalogCategory $category,
                                              \App\Models\Catalog\CatalogSection $section = null,
                                              \App\Models\Catalog\CatalogProduct $product) {
    $trail->parent('catalog.section', $countryCode, $category, $section);
    $trail->push($product->title, $product->getLink($countryCode, $category, $section));
});