<?php

if (!function_exists('locale_name')) {
    function locale_name($locale)
    {
        $locales = config('app.locales');

        $name = 'NOT DEFINED';

        if (isset($locales[$locale])) {
            $name = $locales[$locale]['title'];
        }

        return $name;
    }
}

if (!function_exists('lang_route')) {
    function lang_route($name, $parameters = [], $locale = null)
    {
        $currentLocale = $locale ? $locale : app()->getLocale();

        return LaravelLocalization::getLocalizedURL($currentLocale, route($name, $parameters));
    }
}

if (!function_exists('supported_locales')) {
    function supported_locales()
    {
        return LaravelLocalization::getSupportedLocales();
    }
}

if (!function_exists('sort_link')) {
    function sort_link($filters, $sortBy, $sortValue = null)
    {
        $filtersQuery = request()->query();
        unset($filtersQuery['sort_by'], $filtersQuery['sort_dir'], $filtersQuery['sort_val'], $filtersQuery['page']);

        $filtersQuery['sort_by'] = $sortBy;

        if (isset($filters['sort_dir'])) {
            if ($filters['sort_dir'] == 'desc') {
                $filtersQuery['sort_dir'] = 'asc';
            } else if ($filters['sort_dir'] == 'asc') {
                $filtersQuery['sort_dir'] = 'desc';
            } else {
                $filtersQuery['sort_dir'] = 'asc';
            }
        } else {
            $filtersQuery['sort_dir'] = 'asc';
        }

        if ($sortValue) {
            $filtersQuery['sort_val'] = $sortValue;
        }

        return request()->url(). (count($filtersQuery) ? '?' . http_build_query($filtersQuery) : '');
    }
}

if (!function_exists('sort_link_active')) {
    function sort_link_active($filters, $sortBy, $sortValue = null)
    {
        if (isset($filters['sort_by'])) {
            if ($filters['sort_by'] == 'attr' && $filters['sort_by'] == $sortBy) {
                if (isset($filters['sort_val']) && $filters['sort_val'] == $sortValue) {
                    return true;
                }
            } elseif ($filters['sort_by'] == $sortBy) {
                return true;
            }
        }

        return false;
    }
}

if (!function_exists('current_url_cleaned')) {
    function current_url_cleaned()
    {
        $filtersQuery = request()->query();
        unset($filtersQuery['sort_asc'], $filtersQuery['sort_desc'], $filtersQuery['page']);

        return request()->url() . (count($filtersQuery) ? '/?' . http_build_query($filtersQuery) : '');
    }
}

if (!function_exists('mb_ucfirst')) {
    function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false)
    {
        $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
        $str_end = "";

        if ($lower_str_end) {
            $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
        } else {
            $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }

        return $first_letter . $str_end;
    }
}
