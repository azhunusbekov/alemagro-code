<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    public function __construct($data = [])
    {
        $this->data = $data;
    }

    public function build()
    {
        $builder = $this->view('mails.feedback')
                ->subject('Обратная связь')
                ->with([
                    'name' => $this->data['name'],
                    'phone' => $this->data['phone'],
                    'email' => @$this->data['email'],
                    'message_text' => @$this->data['message'],
                    'region' => @$this->data['region'],
                    'url' => @$this->data['url'],
                    'product_title' => @$this->data['product_title'],
                    'product_id' => @$this->data['product_id'],
                    'vacancy_id' => @$this->data['vacancy_id'],
                ]);

        if (isset($this->data['resume'])) {
            $builder->attach(public_path($this->data['resume']));
        }

        return $builder;
    }
}
