<?php

namespace App\Repositories;

use App\Models\Catalog\CatalogAttribute;
use App\Models\Catalog\CatalogCategory;
use App\Models\Catalog\CatalogCulture;
use App\Models\Catalog\CatalogDisease;
use App\Models\Catalog\CatalogProducer;
use App\Models\Catalog\CatalogProtection;
use App\Models\Catalog\CatalogSection;
use Illuminate\Support\Facades\Cache;

class CatalogRepository
{
    public $cacheKey = 'catalog';
    public $cacheTime = 86400;

    public function getAllProducers()
    {
        return Cache::remember('producers', $this->cacheTime, function () {
            return CatalogProducer::orderBy('position')->get();
        });
    }

    public function getAllCategories()
    {
        return Cache::remember(
            'categories_' . app()->getLocale(),
            $this->cacheTime,
            function () {
                return CatalogCategory::published()
                    ->activeTranslation()
                    ->with('translations')
                    ->orderBy('position')
                    ->get();
            });
    }

    public function getSectionsTree(CatalogCategory $category)
    {
        return Cache::remember(
            'sections_' . $category->id . '_' . app()->getLocale(),
            $this->cacheTime,
            function () use ($category) {
                return CatalogSection::where('category_id', $category->id)
                    ->isRoot()
                    ->activeTranslation()
                    ->with(['translations', 'children.translations'])
                    ->orderBy('position')
                    ->get();
            });
    }

    public function getCulturesTree()
    {
        return Cache::remember('cultures_tree', $this->cacheTime, function () {
            $items = CatalogCulture::activeTranslation()
                    ->with('translations')
                    ->orderBy('position')
                    ->get()
                    ->keyBy('id');

            return $this->buildTree($items);
        });
    }

    public function getCulturesRoot()
    {
        return Cache::remember('cultures_root', $this->cacheTime, function () {
            return CatalogCulture::isRoot()
                ->activeTranslation()
                ->with('translations')
                ->orderBy('position')
                ->get();
        });
    }

    public function getCulturesFavorites()
    {
        return Cache::remember('cultures_favorite', $this->cacheTime, function () {
            return CatalogCulture::isNotRoot()
                ->activeTranslation()
                ->with(['translations', 'parent.translations'])
                ->where('is_favorite', 1)
                ->orderBy('position')
                ->get();
        });
    }

    public function getCulturesCount()
    {
        return Cache::remember('cultures_count', $this->cacheTime, function () {
            return CatalogCulture::count();
        });
    }

    public function getCategoriesWithSections()
    {
        return Cache::remember(
            'main_categories_' . app()->getLocale(),
            $this->cacheTime,
            function () {
                return CatalogCategory::published()
                    ->activeTranslation()
                    ->with([
                        'translations',
                        'sections' => function ($query) {
                            $query->activeTranslation();
                            $query->isRoot();
                            $query->with('translations');
                        }
                    ])
                    ->orderBy('position')
                    ->get();
            });
    }

    public function getDiseases(CatalogCategory $category)
    {
        return Cache::remember(
            'diseases_' . $category->id . '_' . app()->getLocale(),
            $this->cacheTime,
            function () use ($category) {
                return $category->diseases()
                    ->with('translations')
                    ->get();
            });
    }

    public function getSubstances(CatalogCategory $category)
    {
        return Cache::remember(
            'substances_' . $category->id . '_' . app()->getLocale(),
            $this->cacheTime,
            function () use ($category) {
                return $category->substances()
                    ->with('translations')
                    ->get();
            });
    }

    public function getFilterableAttributes(CatalogCategory $category)
    {
        return Cache::remember(
            'filterable_attributes_' . $category->id . '_' . app()->getLocale(),
            $this->cacheTime,
            function () use ($category) {
                return CatalogAttribute::query()
                    ->where('category_id', $category->id)
                    ->where('locale', app()->getLocale())
                    ->where('is_filtered', 1)
                    ->orderBy('sort')
                    ->get();
            });
    }

    public function buildTree($elements, $parentId = null)
    {
        $branch = [];

        foreach ($elements as $element) {
            if ($element->parent_id == $parentId) {
                $children = $this->buildTree($elements, $element->id);
                if ($children) {
                    $element->children = $children;
                } else {
                    $element->children = null;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }
}
