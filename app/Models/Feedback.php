<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    public $table = 'feedbacks';

    protected $guarded = ['id'];
    protected $casts = ['body' => 'array'];

    const STATUS_NEW = 0;
    const STATUS_CLOSED = 1;

    public static function listStatuses()
    {
        return [
            self::STATUS_CLOSED => 'Закрыт',
            self::STATUS_NEW => 'Новый',
        ];
    }
}
