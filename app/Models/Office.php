<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
        'body' => 'array'
    ];
}
