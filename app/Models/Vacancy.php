<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    protected $guarded = ['id'];

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    public function responses()
    {
        return $this->hasMany(VacancyResponse::class, 'vacancy_id');
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('status', self::STATUS_PUBLISHED);
    }

    public static function listStatuses()
    {
        return [
            self::STATUS_DRAFT => 'Скрыт',
            self::STATUS_PUBLISHED => 'Опубликовано',
        ];
    }
}
