<?php

namespace App\Models\Catalog;

use App\Models\MainModel;
use App\Models\SeoManager;
use App\Traits\Seoable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;

class CatalogSection extends MainModel
{
    use Translatable, Seoable;

    protected $guarded = ['id'];

    public $translationModel = CatalogSectionTranslation::class;
    public $translationForeignKey = 'section_id';
    public $translatedAttributes = [
        'title', 'slug', 'description', 'status'
    ];

    public function category()
    {
        return $this->belongsTo(CatalogCategory::class, 'category_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function seo()
    {
        return $this->morphMany(SeoManager::class, 'seoable');
    }

    public function scopeIsRoot($query)
    {
        return $query->whereNull('parent_id');
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('status', self::STATUS_PUBLISHED);
    }

    public function scopeActiveTranslation(Builder $query, $locale = null)
    {
        $currentLocale = !is_null($locale) ? $locale : app()->getLocale();

        return $query->where(function($query) use ($currentLocale) {
            $query->orWhereHas('translations', function($query) use ($currentLocale) {
                $query->where('locale', $currentLocale);
                $query->where('status', self::STATUS_PUBLISHED);
                $query->select('status', 'locale', 'slug', 'section_id');
            })->orWhere(function($query) use ($currentLocale) {
                $query->orWhere('locale', $currentLocale);
                $query->where('status', self::STATUS_PUBLISHED);
            });
        });
    }

    public function getLink($countryCode = null, CatalogCategory $category = null, CatalogSection $parent = null)
    {
        $path = '';

        if (empty($countryCode)) {
            $countryCode = 'kz';
        }

        $path .= ($category ? $category->slug : $this->category->slug) . '/section/';

        if (!is_null($this->parent_id)) {
            $path .= ($parent ? $parent->slug : $this->parent->slug) . '/';
        }

        $path .= $this->slug;

        return lang_route('catalog.section',
            [
                'country_code' => $countryCode,
                'section_path' => $path,
            ]
        );
    }

    public function getPath(CatalogCategory $category = null, CatalogSection $parent = null)
    {
        $path = '';

        $path .= ($category ? $category->slug : $this->category->slug) . '/section/';

        if (!is_null($this->parent_id)) {
            $path .= ($parent ? $parent->slug : $this->parent->slug) . '/';
        }

        $path .= $this->slug;

        return $path;
    }
}
