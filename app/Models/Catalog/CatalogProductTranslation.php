<?php

namespace App\Models\Catalog;

use App\Models\MainModel;

class CatalogProductTranslation extends MainModel
{
    protected $guarded = ['id'];
}
