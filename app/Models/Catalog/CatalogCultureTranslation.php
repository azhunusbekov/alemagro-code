<?php

namespace App\Models\Catalog;

use App\Models\MainModel;

class CatalogCultureTranslation extends MainModel
{
    protected $guarded = ['id'];
}
