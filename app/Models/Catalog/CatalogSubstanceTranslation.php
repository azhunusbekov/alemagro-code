<?php

namespace App\Models\Catalog;


use Illuminate\Database\Eloquent\Model;

class CatalogSubstanceTranslation extends Model
{
    protected $guarded = ['id'];
}
