<?php

namespace App\Models\Catalog;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class CatalogDisease extends Model
{
    use Translatable;

    protected $guarded = ['id'];

    public $translationModel = CatalogDiseaseTranslation::class;
    public $translationForeignKey = 'disease_id';
    public $translatedAttributes = [
        'title'
    ];

    public function category()
    {
        return $this->belongsTo(CatalogCategory::class, 'category_id');
    }

    public function disableTranslatable()
    {
        return $this->translatedAttributes = [];
    }
}
