<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class CatalogProducer extends Model
{
    protected $guarded = ['id'];
}
