<?php

namespace App\Models\Catalog;

use App\Models\MainModel;
use App\Models\SeoManager;
use App\Traits\Seoable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;

class CatalogCategory extends MainModel
{
    use Translatable, Seoable;

    protected $guarded = ['id'];

    public $translationModel = CatalogCategoryTranslation::class;
    public $translationForeignKey = 'category_id';
    public $translatedAttributes = [
        'title', 'slug', 'description', 'status'
    ];

    const CATEGORY_SEMENA_ID = 1;
    const CATEGORY_FERTILIZER_ID = 2;
    const CATEGORY_PROTECTION_ID = 3;

    public function attributes()
    {
        return $this->hasMany(CatalogAttribute::class, 'category_id', 'id')->orderBy('sort');
    }

    public function diseases()
    {
        return $this->hasMany(CatalogDisease::class, 'category_id');
    }

    public function substances()
    {
        return $this->hasMany(CatalogSubstance::class, 'category_id');
    }

    public function sections()
    {
        return $this->hasMany(CatalogSection::class, 'category_id')->orderBy('position')->orderBy('created_at');
    }

    public function seo()
    {
        return $this->morphMany(SeoManager::class, 'seoable');
    }

    public function isSemena()
    {
        return $this->id == self::CATEGORY_SEMENA_ID;
    }

    public function isFertilizer()
    {
        return $this->id == self::CATEGORY_FERTILIZER_ID;
    }

    public function isProtection()
    {
        return $this->id == self::CATEGORY_PROTECTION_ID;
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('status', self::STATUS_PUBLISHED);
    }

    public function scopeActiveTranslation(Builder $query, $locale = null)
    {
        $currentLocale = !is_null($locale) ? $locale : app()->getLocale();

        return $query->where(function($query) use ($currentLocale) {
            $query->orWhereHas('translations', function($query) use ($currentLocale) {
                $query->where('locale', $currentLocale);
                $query->where('status', self::STATUS_PUBLISHED);
                $query->select('status', 'locale', 'slug', 'category_id');
            })->orWhere(function($query) use ($currentLocale) {
                $query->orWhere('locale', $currentLocale);
                $query->where('status', self::STATUS_PUBLISHED);
            });
        });
    }

    public function resolveSlugBinding($value)
    {
        $value = trim($value, '/');

        return self::where(function ($query) use ($value) {
                $query->orWhereHas('translations', function($query) use ($value) {
                    $query->where('locale', app()->getLocale());
                    $query->where('slug', $value);
                    $query->where('status', self::STATUS_PUBLISHED);
                })->orWhere(function($query) use ($value) {
                    $query->orWhere('locale', app()->getLocale());
                    $query->where('slug', $value);
                    $query->where('status', self::STATUS_PUBLISHED);
                });
            })
            ->with('translations')
            ->firstOrFail();
    }
}
