<?php

namespace App\Models\Catalog;

use App\Models\MainModel;

class CatalogCategoryTranslation extends MainModel
{
    protected $guarded = ['id'];
}
