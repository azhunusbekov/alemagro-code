<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class CatalogProductProtection extends Model
{
    protected $table = 'catalog_products_protections';
}
