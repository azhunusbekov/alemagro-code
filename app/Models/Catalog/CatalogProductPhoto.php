<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class CatalogProductPhoto extends Model
{
    protected $table = 'catalog_products_photos';
    protected $guarded = ['id'];
    public $timestamps = false;
}
