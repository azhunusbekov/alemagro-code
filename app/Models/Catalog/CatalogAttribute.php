<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class CatalogAttribute extends Model
{
    protected $table = 'catalog_attributes';
    public $timestamps = false;
    protected $fillable = ['name', 'locale', 'type', 'required', 'default', 'options', 'sort', 'is_filtered'];
    protected $casts = [
        'options' => 'array',
    ];

    const TYPE_STRING = 'string';
    const TYPE_INTEGER = 'integer';
    const TYPE_FLOAT = 'float';

    public static function listTypes(): array
    {
        return [
            self::TYPE_STRING => 'Строка',
            self::TYPE_INTEGER => 'Число',
            self::TYPE_FLOAT => 'Число с плавающей точкой',
        ];
    }

    public function isString(): bool
    {
        return $this->type === self::TYPE_STRING;
    }

    public function isInteger(): bool
    {
        return $this->type === self::TYPE_INTEGER;
    }

    public function isFloat(): bool
    {
        return $this->type === self::TYPE_FLOAT;
    }

    public function isNumber(): bool
    {
        return $this->isInteger() || $this->isFloat();
    }

    public function isSelect(): bool
    {
        return !is_null($this->options) && count($this->options) > 0;
    }
}
