<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class CatalogProtectionTranslation extends Model
{
    protected $guarded = ['id'];
}
