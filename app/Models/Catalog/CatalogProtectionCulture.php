<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class CatalogProtectionCulture extends Model
{
    protected $table = 'catalog_protections_cultures';
}
