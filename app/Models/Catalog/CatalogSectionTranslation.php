<?php

namespace App\Models\Catalog;

use App\Models\MainModel;

class CatalogSectionTranslation extends MainModel
{
    protected $guarded = ['id'];
}
