<?php

namespace App\Models\Catalog;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class CatalogProtection extends Model
{
    use Translatable;

    protected $guarded = ['id'];

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const PURPOSE_NUTRITION = 'nutrition';
    const PURPOSE_PROTECTION = 'protection';

    public $translationModel = CatalogProtectionTranslation::class;
    public $translationForeignKey = 'protection_id';
    public $translatedAttributes = [
        'title', 'src'
    ];

    public function producer()
    {
        return $this->belongsTo(CatalogProducer::class, 'producer_id');
    }

    public function cultures()
    {
        return $this->belongsToMany(CatalogCulture::class, 'catalog_protections_cultures', 'protection_id', 'culture_id');
    }

    public function disableTranslatable()
    {
        return $this->translatedAttributes = [];
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('status', self::STATUS_PUBLISHED);
    }

    public static function getPurposes()
    {
        return [
            self::PURPOSE_NUTRITION => trans('catalog.purpose_nutrition'),
            self::PURPOSE_PROTECTION => trans('catalog.purpose_protection'),
        ];
    }

    public static function listStatuses()
    {
        return [
            self::STATUS_DRAFT => 'Скрыт',
            self::STATUS_PUBLISHED => 'Опубликовано',
        ];
    }
}
