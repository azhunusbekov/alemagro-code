<?php

namespace App\Models\Catalog;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class CatalogSubstance extends Model
{
    use Translatable;

    protected $guarded = ['id'];

    public $translationModel = CatalogSubstanceTranslation::class;
    public $translationForeignKey = 'substance_id';
    public $translatedAttributes = [
        'title'
    ];

    public function category()
    {
        return $this->belongsTo(CatalogCategory::class, 'category_id');
    }

    public function disableTranslatable()
    {
        return $this->translatedAttributes = [];
    }
}
