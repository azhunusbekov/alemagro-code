<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class CatalogProductDisease extends Model
{
    protected $table = 'catalog_products_diseases';
}
