<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class CatalogProductCulture extends Model
{
    protected $table = 'catalog_products_cultures';
}
