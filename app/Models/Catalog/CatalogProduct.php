<?php

namespace App\Models\Catalog;

use App\Models\MainModel;
use App\Models\SeoManager;
use App\Traits\Seoable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class CatalogProduct extends MainModel implements Searchable
{
    use Translatable, Seoable;

    protected $guarded = ['id'];

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const AVAILABLE_KZ = 'kz';
    const AVAILABLE_KG = 'kg';

    protected $casts = [
        'available' => 'array',
    ];

    public $translationModel = CatalogProductTranslation::class;
    public $translationForeignKey = 'product_id';
    public $translatedAttributes = [
        'title', 'slug', 'description', 'composition'
    ];

    public function category()
    {
        return $this->belongsTo(CatalogCategory::class, 'category_id');
    }

    public function section()
    {
        return $this->belongsTo(CatalogSection::class, 'section_id');
    }

    public function producer()
    {
        return $this->belongsTo(CatalogProducer::class, 'producer_id');
    }

    public function cultures()
    {
        return $this->belongsToMany(CatalogCulture::class, 'catalog_products_cultures', 'product_id', 'culture_id');
    }

    public function diseases()
    {
        return $this->belongsToMany(CatalogDisease::class, 'catalog_products_diseases', 'product_id', 'disease_id');
    }

    public function substances()
    {
        return $this->belongsToMany(CatalogSubstance::class, 'catalog_products_substances', 'product_id', 'substance_id');
    }

    public function photos()
    {
        return $this->hasMany(CatalogProductPhoto::class, 'product_id');
    }

    public function protection()
    {
        return $this->belongsToMany(CatalogProtection::class, 'catalog_products_protections', 'product_id', 'protection_id');
    }

    public function catalogAttributes()
    {
        return $this->hasMany(CatalogProductAttribute::class, 'product_id', 'id');
    }

    public function seo()
    {
        return $this->morphMany(SeoManager::class, 'seoable');
    }

    public function isAvailableOn($countryCode)
    {
        return $this->available && in_array($countryCode, $this->available);
    }

    //public function getPreviewAttribute($value)
    //{
    //    return $value ?? '/uploads/product-default.png';
    //}

    public function scopePublished(Builder $query)
    {
        return $query->where('status', self::STATUS_PUBLISHED);
    }

    public function scopeActiveTranslation(Builder $query, $locale = null)
    {
        $currentLocale = !is_null($locale) ? $locale : app()->getLocale();

        return $query->where(function($query) use ($currentLocale) {
            $query->orWhereHas('translations', function($query) use ($currentLocale) {
                $query->where('locale', $currentLocale);
            })->orWhere(function($query) use ($currentLocale) {
                $query->orWhere('locale', $currentLocale);
            });
        });
    }

    public function scopeWhereAvailable(Builder $query, $countryCode)
    {
        return $query->where('available', 'like', '%'.$countryCode.'%');
    }

    public function scopeJoinAttributeValue(Builder $query, $value)
    {
        return $query->leftJoin('catalog_products_attributes', function ($query) use ($value) {
                $query->on('catalog_products_attributes.product_id', '=', 'catalog_products.id');
                $query->where('catalog_products_attributes.attribute_id', $value);
            });
    }

    public function getValueOfAttribute($id)
    {
        foreach ($this->catalogAttributes as $attribute) {
            if ($attribute->attribute_id == $id) {
                return $attribute->value;
            }
        }

        return null;
    }

    public function getLink($countryCode = null, CatalogCategory $category = null, CatalogSection $section = null)
    {
        $path = '';

        if (empty($countryCode)) {
            $countryCode = 'kz';
        }

        if (!is_null($this->section_id)) {
            $path = $section ? $section->getLink($countryCode, $category) : $this->section->getLink($countryCode, $category);
        } else {
            $category = $category ?? $this->category;
            $path .= lang_route('catalog.category', ['country_code' => $countryCode, 'catalog_category' => $category->slug]);
        }

        return $path . '/product/' . $this->slug;
    }

    public function getSearchResult(): SearchResult
    {
        return new SearchResult(
            $this,
            $this->title,
            $this->getLink()
        );
    }

    public static function listStatuses()
    {
        return [
            self::STATUS_DRAFT => 'Скрыт',
            self::STATUS_PUBLISHED => 'Опубликовано',
        ];
    }

    public static function listAvailables()
    {
        return [
            self::AVAILABLE_KZ => 'В Казахстане',
            self::AVAILABLE_KG => 'В Кыргызстане',
        ];
    }
}
