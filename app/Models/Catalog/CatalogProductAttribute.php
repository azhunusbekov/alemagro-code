<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CatalogProductAttribute extends Model
{
    protected $table = 'catalog_products_attributes';
    protected $primaryKey = ['attribute_id', 'product_id'];
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = ['attribute_id', 'product_id', 'value'];

    protected function setKeysForSaveQuery(Builder $query)
    {
        foreach ($this->getKeyName() as $key) {
            if ($this->$key)
                $query->where($key, '=', $this->$key);
            else
                throw new Exception(__METHOD__ . 'Missing part of the primary key: ' . $key);
        }
        return $query;
    }
}
