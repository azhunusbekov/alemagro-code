<?php

namespace App\Models\Catalog;


use Illuminate\Database\Eloquent\Model;

class CatalogDiseaseTranslation extends Model
{
    protected $guarded = ['id'];
}
