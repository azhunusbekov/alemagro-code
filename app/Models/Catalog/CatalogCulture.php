<?php

namespace App\Models\Catalog;

use App\Models\MainModel;
use App\Models\SeoManager;
use App\Traits\Seoable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;

class CatalogCulture extends MainModel
{
    use Translatable, Seoable;

    protected $guarded = ['id'];

    public $translationModel = CatalogCultureTranslation::class;
    public $translationForeignKey = 'culture_id';
    public $translatedAttributes = [
        'title', 'description', 'status', 'slug'
    ];

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function products()
    {
        return $this->belongsToMany(CatalogProduct::class, 'catalog_products_cultures', 'product_id');
    }

    public function protections()
    {
        return $this->belongsToMany(CatalogProtection::class, 'catalog_protections_cultures', 'protection_id');
    }

    public function seo()
    {
        return $this->morphMany(SeoManager::class, 'seoable');
    }

    public function isFavorite()
    {
        return $this->is_favorite;
    }

    public function onPage()
    {
        return $this->on_page;
    }

    public function getImageAttribute($value)
    {
        return $value ?? '/uploads/culture-default.png';
    }

    public function getIconAttribute($value)
    {
        return $value ?? '/uploads/culture-icon-default.png';
    }

    public function scopeIsRoot($query)
    {
        return $query->whereNull('parent_id');
    }

    public function scopeIsNotRoot($query)
    {
        return $query->whereNotNull('parent_id');
    }

    public function scopeActiveTranslation(Builder $query, $locale = null)
    {
        $currentLocale = !is_null($locale) ? $locale : app()->getLocale();

        return $query->where(function($query) use ($currentLocale) {
            $query->orWhereHas('translations', function($query) use ($currentLocale) {
                $query->where('locale', $currentLocale);
            })->orWhere(function($query) use ($currentLocale) {
                $query->orWhere('locale', $currentLocale);
            });
        });
    }

    public function getPath(CatalogCulture $parent = null, $parentPath = null)
    {
        $path = '';

        if (!is_null($parentPath)) {
            $path .= $parentPath . '/';
        } else if (!is_null($this->parent_id)) {
            $path .= ($parent ? $parent->getPath() : $this->parent->getPath()) . '/';
        }

        $path .= $this->slug;

        return $path;
    }
}
