<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeoManager extends Model
{
    protected $table = 'seo_manager';
    protected $guarded = ['id'];

    public function seoable()
    {
        return $this->morphTo();
    }
}
