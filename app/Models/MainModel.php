<?php

namespace App\Models;

use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class MainModel extends Model
{
    use Sluggable;

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    protected static function boot()
    {
        parent::boot();
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function customizeSlugEngine(Slugify $engine, $attribute)
    {
        $engine->addRule('ә', 'а');
        $engine->addRule('Ә', 'a');
        $engine->addRule('ғ', 'g');
        $engine->addRule('Ғ', 'g');
        $engine->addRule('қ', 'q');
        $engine->addRule('Қ', 'k');
        $engine->addRule('ө', 'o');
        $engine->addRule('Ө', 'o');
        $engine->addRule('ұ', 'u');
        $engine->addRule('Ұ', 'u');
        $engine->addRule('ү', 'u');
        $engine->addRule('Ү', 'u');
        $engine->addRule('һ', '');
        $engine->addRule('Һ', '');
        $engine->addRule('і', 'i');
        $engine->addRule('І', 'i');
        $engine->addRule('ң', 'ng');
        $engine->addRule('Ң', 'ng');

        return $engine;
    }

    public function disableTranslatable()
    {
        $this->translatedAttributes = [];
    }

    public static function listStatuses()
    {
        return [
            self::STATUS_DRAFT => 'Скрыт',
            self::STATUS_PUBLISHED => 'Опубликовано',
        ];
    }
}
