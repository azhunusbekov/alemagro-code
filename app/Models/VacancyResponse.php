<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacancyResponse extends Model
{
    protected $guarded = ['id'];
    protected $casts = ['body' => 'array'];

    public function vacancy()
    {
        return $this->belongsTo(Vacancy::class, 'vacancy_id');
    }
}
