<?php

namespace App\Models\Article;

use App\Models\MainModel;
use App\Models\SeoManager;
use App\Traits\Seoable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;

class Article extends MainModel
{
    use Translatable, Seoable;

    protected $guarded = ['id'];

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    public $translationModel = ArticleTranslation::class;
    public $translationForeignKey = 'article_id';
    public $translatedAttributes = [
        'title', 'slug', 'description', 'locale', 'status'
    ];

    public function category()
    {
        return $this->belongsTo(ArticleCategory::class, 'category_id');
    }

    public function seo()
    {
        return $this->morphMany(SeoManager::class, 'seoable');
    }

    public function isPublished()
    {
        return $this->status == self::STATUS_PUBLISHED;
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('status', self::STATUS_PUBLISHED);
    }

    public function scopeActiveTranslation(Builder $query, $locale = null)
    {
        $currentLocale = !is_null($locale) ? $locale : app()->getLocale();

        return $query->where(function($query) use ($currentLocale) {
            $query->orWhereHas('translations', function($query) use ($currentLocale) {
                $query->where('locale', $currentLocale);
                $query->where('status', self::STATUS_PUBLISHED);
                $query->select('status', 'locale', 'slug', 'article_id');
            })->orWhere(function($query) use ($currentLocale) {
                $query->orWhere('locale', $currentLocale);
                $query->where('status', self::STATUS_PUBLISHED);
            });
        });
    }

    public static function listStatuses()
    {
        return [
            self::STATUS_DRAFT => 'Скрыт',
            self::STATUS_PUBLISHED => 'Опубликовано',
        ];
    }
}
