<?php

namespace App\Models\Article;

use App\Models\MainModel;

class ArticleTranslation extends MainModel
{
    protected $guarded = ['id'];

    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id');
    }
}
