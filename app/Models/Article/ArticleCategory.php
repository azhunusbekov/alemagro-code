<?php

namespace App\Models\Article;

use App\Models\MainModel;
use App\Models\SeoManager;
use App\Traits\Seoable;
use Dimsav\Translatable\Translatable;

class ArticleCategory extends MainModel
{
    use Translatable, Seoable;

    protected $guarded = ['id'];

    public $translationModel = ArticleCategoryTranslation::class;
    public $translationForeignKey = 'category_id';
    public $translatedAttributes = [
        'title', 'slug', 'locale'
    ];

    public function articles()
    {
        return $this->hasMany(Article::class, 'category_id');
    }

    public function seo()
    {
        return $this->morphMany(SeoManager::class, 'seoable');
    }
}
