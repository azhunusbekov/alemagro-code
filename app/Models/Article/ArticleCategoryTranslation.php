<?php

namespace App\Models\Article;

use App\Models\MainModel;

class ArticleCategoryTranslation extends MainModel
{
    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(ArticleCategory::class, 'category_id');
    }
}
