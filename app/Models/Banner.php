<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $guarded = ['id'];

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    public static function listStatuses()
    {
        return [
            self::STATUS_DRAFT => 'Скрыт',
            self::STATUS_PUBLISHED => 'Опубликовано',
        ];
    }
}
