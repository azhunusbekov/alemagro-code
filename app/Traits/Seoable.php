<?php

namespace App\Traits;

use App\Models\SeoManager;

trait Seoable
{
    public function storeSeoData($data, $locale = null)
    {
        $locale = $locale ?? config('app.locale_default');

        $seo = new SeoManager();
        $seoData = $data;
        $seoData['locale'] = $locale;
        $seo->fill($seoData);

        $this->seo()->save($seo);
    }

    public function updateSeoData($data, $locale = null)
    {
        $locale = $locale ?? config('app.locale_default');

        $seo = $this->seo->where('locale', $locale);

        if ($seo->count()) {
            $this->seo()->where('locale', $locale)->update($data);
        } else {
            $seo = new SeoManager();
            $seoData = $data;
            $seoData['locale'] = $locale;
            $seo->fill($seoData);
            $this->seo()->save($seo);
        }
    }
}