<?php

namespace App\Services\Search;

use Illuminate\Support\Collection;
use Spatie\Searchable\SearchAspect;
use App\Models\Catalog\CatalogProduct;

class CatalogProductSearchAspect extends SearchAspect
{
    public function getResults(string $term): Collection
    {
        return CatalogProduct::query()
            ->where(function($query) use ($term) {
                $query->orWhere('title', 'LIKE', "%{$term}%");
                $query->orWhere('description', 'LIKE', "%{$term}%");
            })
            ->published()
            ->with([
                'translations',
                'category' => function($query) {
                    $query->with('translations');
                },
                'section' => function($query) {
                    $query->with([
                        'translations',
                        'parent.translations'
                    ]);
                }
            ])
            ->get();
    }
}