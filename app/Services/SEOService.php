<?php

namespace App\Services;

use App\Models\SeoManager;

class SEOService
{
    public static function init($tags, $type = false, $owner_id = false, $locale = null)
    {
        $prefilledTags = false;
        $locale = $locale ?? app()->getLocale();

        if ($type && $owner_id) {
            $seoData = \Cache::remember($type . ':' . $owner_id, 1440, function () use ($owner_id, $type, $locale) {
                return SeoManager::where([
                    'seoable_id' => $owner_id,
                    'seoable_type' => $type,
                    'locale' => $locale
                ])->first();
            });

            if ($seoData && $seoData->title) {
                $tags = $prefilledTags = $seoData;
            }
        }

        if (!$prefilledTags && is_string($tags)) {
            $title = $tags;
            data_set($tags, 'title', $title . ' – ' . trans('seo.default.title'));
            data_set($tags, 'description', $title . ', ' . trans('seo.default.description'));
            data_set($tags, 'keywords', $title . ', ' . trans('seo.default.keywords'));
        }

        \SEOMeta::setTitle(\Arr::get($tags, 'title'), false);
        \SEOMeta::setDescription(\Arr::get($tags, 'description'));
        \SEOMeta::setKeywords(mb_strtolower(\Arr::get($tags, 'keywords')));
        \OpenGraph::setTitle(\Arr::get($tags, 'title'));
        \OpenGraph::setDescription(\Arr::get($tags, 'description'));
        \OpenGraph::setUrl(url()->current());
    }
}