<?php

namespace App\Console\Commands;

use App\Models\Catalog\CatalogProduct;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProductImagesUpload extends Command
{
    protected $signature = 'product-images:upload';

    protected $description = 'Upload product images from external folder';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('START: product-image-uploader');

        $directory = 'uploads/external/products';

        $notFounds = [];

        foreach (File::files(public_path('uploads/external/products')) as $file) {
            $vendorCode = $file->getFilenameWithoutExtension();
            $product = CatalogProduct::where('vendor_code', $vendorCode)->first();

            if (!$product) {
                $this->error('Product not exists with vendor_code  = ' . $vendorCode);
                $notFounds[] = $vendorCode;
                continue;
            }

            if ($product->preview) {
                $this->error('Preview exists: product_id = ' . $product->id);
                continue;
            }

            $path = '/uploads/catalog/products/' . $product->id;
            $previewName = uniqid() . '.' . $file->getExtension();
            $previewPath = $path . '/' . $previewName;

            if(!File::isDirectory(public_path($path))) {
                File::makeDirectory(public_path($path), 0777, true, true);
            }

            if (File::copy($file->getRealPath(), public_path($previewPath))) {
                $img = Image::make(public_path($previewPath))->resize(225, 225);
                $img->save(public_path($previewPath));

                $product->disableTranslatable();
                $product->update([
                    'preview' => $previewPath,
                ]);

                $this->info($vendorCode . ' -- copied');
            }
        }

        dump($notFounds);

        $this->info('END: product-image-uploader');
    }
}