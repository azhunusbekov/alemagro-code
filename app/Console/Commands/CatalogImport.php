<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CatalogImport extends Command
{
    protected $signature = 'import:catalog';

    protected $description = 'Import catalog data from catalog.xlsx';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->output->title('Starting import');
        (new \App\Imports\CatalogImport)->withOutput($this->output)->import(public_path('uploads/catalog.xlsx'));
        $this->output->success('Import successful');
    }
}