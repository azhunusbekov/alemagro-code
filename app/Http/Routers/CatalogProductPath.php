<?php

namespace App\Http\Routers;

use App\Models\Catalog\CatalogCategory;
use App\Models\Catalog\CatalogProduct;
use App\Models\Catalog\CatalogSection;
use Illuminate\Contracts\Routing\UrlRoutable;

class CatalogProductPath extends CatalogSectionPath implements UrlRoutable
{
    public $product;

    public function __construct(CatalogCategory $category, CatalogSection $section, CatalogProduct $product)
    {
        parent::__construct($category, $section);
        $this->product = $product;
    }

    public function getRouteKey()
    {
        //if (!$this->section) {
        //    throw new \BadMethodCallException('Empty page.');
        //}
        //
        //return $this->section;
    }

    public function getRouteKeyName(): string
    {
        return 'product_path';
    }

    public function resolveRouteBinding($value)
    {
        $segments = explode('/', $value);
        $chunks = explode('product', $value);
        $slug = trim($chunks[1], '/');

        if ($segments[1] == 'section') {
            parent::resolveRouteBinding($chunks[0]);
        } else if ($segments[1] == 'product') {
            $this->category = $this->category->resolveSlugBinding($chunks[0]);
            $this->section = null;
        }

        $this->product = CatalogProduct::query()
            ->where('category_id', $this->category->id)
            ->where('section_id', optional($this->section)->id)
            ->where(function($query) use ($slug) {
                $query->whereHas('translations', function ($query) use ($slug) {
                    $query->Where('slug', $slug);
                    $query->where('locale', app()->getLocale());
                    $query->where('status', CatalogProduct::STATUS_PUBLISHED);
                })->orWhere(function ($query) use ($slug) {
                    $query->Where('slug', $slug);
                    $query->Where('locale', app()->getLocale());
                    $query->where('status', CatalogProduct::STATUS_PUBLISHED);
                });
            })
            ->with(['translations'])
            ->firstOrFail();

        return $this;
    }
}
