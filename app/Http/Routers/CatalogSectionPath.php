<?php

namespace App\Http\Routers;

use App\Models\Catalog\CatalogCategory;
use App\Models\Catalog\CatalogSection;
use Illuminate\Contracts\Routing\UrlRoutable;

class CatalogSectionPath implements UrlRoutable
{
    public $section;
    public $category;

    public function __construct(CatalogCategory $category, CatalogSection $section)
    {
        $this->category = $category;
        $this->section = $section;
    }

    public function getRouteKey()
    {
        //if (!$this->section) {
        //    throw new \BadMethodCallException('Empty page.');
        //}
        //
        //return $this->section;
    }

    public function getRouteKeyName(): string
    {
        return 'section_path';
    }

    public function resolveRouteBinding($value)
    {
        $chunks = explode('section', $value);
        $categorySlug = trim($chunks[0], '/');
        $sectionChunks = explode('/',trim($chunks[1], '/'));

        $this->category = $this->category->resolveSlugBinding($categorySlug);
        $section = null;

        do {
            if (!$slug = reset($sectionChunks)) {
                continue;
            }
            $next = CatalogSection::query()
                ->where('category_id', $this->category->id)
                ->where('parent_id', optional($section)->id)
                ->where(function ($query) use ($slug) {
                    $query->whereHas('translations', function($query) use ($slug) {
                        $query->where('locale', app()->getLocale());
                        $query->where('slug', $slug);
                        $query->where('status', CatalogSection::STATUS_PUBLISHED);
                    })->orWhere(function($query) use ($slug) {
                        $query->where('locale', app()->getLocale());
                        $query->where('slug', $slug);
                        $query->where('status', CatalogSection::STATUS_PUBLISHED);
                    });
                })
                ->with(['translations'])
                ->when($section, function ($query) {
                    $query->with('parent');
                })
                ->first();

            if ($slug && $next) {
                $this->section = $section = $next;
                array_shift($sectionChunks);
            }
        } while (!empty($slug) && !empty($next));

        if (!empty($sectionChunks)) {
            abort(404);
        }

        return $this;
    }
}
