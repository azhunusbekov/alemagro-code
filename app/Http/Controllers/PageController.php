<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Office;
use App\Models\Partner;
use App\Models\Vacancy;
use App\Services\SEOService;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function about()
    {
        SEOService::init(trans('seo.about.title'));

        $partners = Partner::orderBy('position')->get();

        return view('pages.about', compact('partners'));
    }

    public function contact()
    {
        $offices = Office::all();

        SEOService::init(trans('seo.contact.title'));

        return view('pages.contact', compact('offices'));
    }

    public function consult()
    {
        SEOService::init(trans('seo.consult.title'));

        return view('pages.consult');
    }

    public function career()
    {
        $vacancies = Vacancy::whereLocale(app()->getLocale())->published()->orderBy('position')->get();

        SEOService::init(trans('seo.career.title'));

        return view('pages.career', compact('vacancies'));
    }

    public function internship()
    {
        SEOService::init(trans('seo.internship.title'));

        return view('pages.internship');
    }
}
