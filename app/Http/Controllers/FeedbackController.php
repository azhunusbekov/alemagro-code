<?php

namespace App\Http\Controllers;

use App\Mail\FeedbackEmail;
use App\Models\Feedback;
use App\Models\Office;
use App\Models\VacancyResponse;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public $toEmail = null;

    public function __construct()
    {
        $this->toEmail = env('MAIL_TO_ADDRESS');
    }

    public function main(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'region' => 'required',
        ]);

        $body = [
            'name' => $request->name,
            'phone' => $request->phone,
            'region' => $request->region,
        ];

        Feedback::create([
            'title' => $request->title,
            'type' => 'main',
            'body' => $body,
            'status' => Feedback::STATUS_NEW
        ]);

        //$office = Office::where('alias', $request->region)->first();
        //
        //if ($office->email) {
        //    $this->toEmail = $office->email;
        //}

        try {
            \Mail::to( $this->toEmail)->send(new FeedbackEmail($body));
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

        if ($request->ajax()) {
            return response()->json(['success' => true, 'message' => 'form save']);
        } else {
            return redirect()->back()->with('success', 'saved');
        }
    }

    public function internship(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'message' => 'required',
            'resume' => 'required|file|mimes:pdf,docx,doc',
        ]);

        $body = [
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'message' => $request->message,
        ];

        if ($request->hasFile('resume')) {
            $body['resume'] = '/uploads/feedbacks/' . $request->file('resume')->store('career', 'feedbacks');
        }

        Feedback::create([
            'title' => $request->title,
            'type' => 'internship',
            'body' => $body,
            'status' => Feedback::STATUS_NEW
        ]);

        $office = Office::where('alias', $request->region)->first();

        if ($office->email) {
            $this->toEmail = $office->email;
        }

        try {
            \Mail::to( $this->toEmail)->send(new FeedbackEmail($body));
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

        if ($request->ajax()) {
            return response()->json(['success' => true, 'message' => 'form save']);
        } else {
            return redirect()->back()->with('success', 'saved');
        }
    }

    public function vacancy(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'resume' => 'file|mimes:pdf,docx,doc',
            'vacancy_id' => 'required',
            'region' => 'required',
        ]);

        $body = [
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'vacancy_id' => $request->vacancy_id,
            'region' =>  $request->region,
        ];

        if ($request->hasFile('resume')) {
            $body['resume'] = '/uploads/feedbacks/' . $request->file('resume')->store('career', 'feedbacks');
        }

        Feedback::create([
            'title' => $request->title,
            'type' => 'vacancy',
            'body' => $body,
            'status' => Feedback::STATUS_NEW
        ]);

        VacancyResponse::create([
            'vacancy_id' => $request->vacancy_id,
            'body' => $body,
        ]);

        $office = Office::where('alias', $request->region)->first();

        if ($office->email) {
            $this->toEmail = $office->email;
        }

        try {
            \Mail::to( $this->toEmail)->send(new FeedbackEmail($body));
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

        if ($request->ajax()) {
            return response()->json(['success' => true, 'message' => 'form save']);
        } else {
            return redirect()->back()->with('success', 'saved');
        }
    }

    public function product(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required',
            'product_title' => 'required',
            'url' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'region' => 'required',
        ]);

        $body = [
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'product_title' => $request->product_title,
            'product_id' => $request->product_id,
            'url' => $request->url,
            'region' => $request->region,
        ];

        Feedback::create([
            'title' => $request->title,
            'type' => 'product',
            'body' => $body,
            'status' => Feedback::STATUS_NEW
        ]);

        $office = Office::where('alias', $request->region)->first();

        if ($office->email) {
            $this->toEmail = $office->email;
        }

        try {
            \Mail::to($this->toEmail)->send(new FeedbackEmail($body));
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

        if ($request->ajax()) {
            return response()->json(['success' => true, 'message' => 'form save']);
        } else {
            return redirect()->back()->with('success', 'saved');
        }
    }
}
