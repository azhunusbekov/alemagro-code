<?php

namespace App\Http\Controllers;

use App\Models\Catalog\CatalogProtection;
use App\Repositories\CatalogRepository;
use App\Services\SEOService;
use Illuminate\Http\Request;

class ProtectionController extends Controller
{
    protected $catalog;

    public function __construct(CatalogRepository $catalog)
    {
        $this->catalog = $catalog;
    }

    public function index(Request $request)
    {
        $purposes = CatalogProtection::getPurposes();
        $producers = $this->catalog->getAllProducers();
        $cultures = $this->catalog->getCulturesTree();
        $culturesFavorites = $this->catalog->getCulturesFavorites();

        $protections = CatalogProtection::query();
        $filters = [];

        if ($values = $request->input('purposes')) {
            $filters['purposes'] = $values;
            $protections->whereIn('purpose', $values);
        }

        if ($values = $request->input('cultures')) {
            $filters['cultures'] = $values;
            $protections->whereHas('cultures', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($values = $request->input('producers')) {
            $filters['producers'] = $values;
            $protections->whereIn('producer_id', $values);
        }

        $protections = $protections->published()
            ->with(['translations', 'producer'])
            ->paginate(24)
            ->appends($filters);

        SEOService::init(trans('seo.protections.title'));

        return view('protection.index', compact(
            'purposes',
            'producers',
            'cultures',
            'culturesFavorites',
            'protections',
            'filters'
        ));
    }
}
