<?php

namespace App\Http\Controllers;

use App\Services\Search\CatalogProductSearchAspect;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $searchterm = $request->input('search');

        $searchResults = (new Search())
            ->registerAspect(CatalogProductSearchAspect::class)
            ->search($searchterm);

        return view('pages.search', compact('searchResults', 'searchterm'));
    }
}
