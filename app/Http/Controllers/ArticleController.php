<?php

namespace App\Http\Controllers;

use App\Models\Article\Article;
use App\Models\Article\ArticleCategory;
use App\Services\SEOService;

class ArticleController extends Controller
{
    public function index()
    {
        $categories = ArticleCategory::with('translations')->get();
        $articles = Article::activeTranslation()
            ->with(['translations:id,article_id,title,slug,locale,status,created_at', 'category.translations'])
            ->select(['id', 'locale', 'title', 'slug', 'preview', 'image_header','status','created_at', 'category_id'])
            ->latest()
            ->paginate(6);

        SEOService::init(trans('article.title'));

        return view('article.index', compact('categories', 'articles'));
    }

    public function category(ArticleCategory $category)
    {
        $categories = ArticleCategory::with('translations')->get();
        $articles = Article::activeTranslation()
            ->where('category_id', $category->id)
            ->with(['translations:id,article_id,title,slug,locale,status,created_at', 'category.translations'])
            ->select(['id', 'locale', 'title', 'slug', 'preview', 'image_header','status','created_at', 'category_id'])
            ->latest()
            ->paginate('6');

        SEOService::init($category->title, 'article_category', $category->id);

        return view('article.category', compact('category', 'categories', 'articles'));
    }

    public function article(ArticleCategory $category, Article $article)
    {
        SEOService::init($article->title, 'article', $article->id);

        return view('article.article', compact('category', 'article'));
    }
}
