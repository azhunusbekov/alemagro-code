<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Requests\Article\ArticleTranslationRequest;
use App\Models\Article\Article;
use App\Models\Article\ArticleTranslation;
use App\Models\SeoManager;
use Illuminate\Routing\Controller;

class ArticleTranslationController extends Controller
{
    public function create(Article $article)
    {
        $locales = config('app.locales');
        $statuses = Article::listStatuses();
        $seoData = null;

        return view('admin.article.article.translation.create', compact(
            'article',
            'locales',
            'statuses',
            'seoData'
        ));
    }

    public function store(ArticleTranslationRequest $request, Article $article)
    {
        $articleTranslation = $article->translations()->create($request->except('seo'));

        if ($request->has('seo')) {
            $article->storeSeoData($request->seo, $request['locale']);
        }

        return redirect()->route('admin.article.article.translation.edit', [$article, $articleTranslation]);
    }

    public function edit(Article $article, ArticleTranslation $translation)
    {
        $articleTranslation = $translation;
        $locales = config('app.locales');
        $statuses = Article::listStatuses();

        $seoData = $article->seo->where('locale', $articleTranslation->locale)->first();

        return view('admin.article.article.translation.edit', compact(
            'article',
            'articleTranslation',
            'locales',
            'statuses',
            'seoData'
        ));
    }

    public function update(ArticleTranslationRequest $request, Article $article, ArticleTranslation $translation)
    {
        $translation->update($request->except('seo'));

        if ($request->has('seo')) {
            $article->updateSeoData($request->seo, $request['locale']);
        }

        return redirect()->route('admin.article.article.translation.edit', [$article, $translation]);
    }

    public function destroy(Article $article, ArticleTranslation $translation)
    {
        try {
            $translation->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.article.article.edit', $article)]);
        } else {
            return redirect()->to(route('admin.article.article.edit', $article));
        }
    }
}
