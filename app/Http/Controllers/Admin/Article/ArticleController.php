<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Requests\Article\ArticleRequest;
use App\Models\Article\Article;
use App\Models\Article\ArticleCategory;
use App\Models\SeoManager;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        $articles = Article::query();
        $statuses = Article::listStatuses();
        $categories = ArticleCategory::all();

        if ($value = $request->get('category_id')) {
            $articles = $articles->where('category_id', $value);
        }

        $articles = $articles->with(['category', 'translations'])->get();

        return view('admin.article.article.index', compact('articles', 'statuses', 'categories'));
    }

    public function create()
    {
        $statuses = Article::listStatuses();
        $categories = ArticleCategory::all();

        return view('admin.article.article.create', compact('statuses', 'categories'));
    }

    public function store(ArticleRequest $request)
    {
        $article = new Article();
        $article->disableTranslatable();
        $article->fill($request->except('seo'));
        $article->save();

        if ($request->hasFile('preview')) {
            $article->preview = '/uploads/articles/' . $request->file('preview')->store($article->id, 'article');
        }

        if ($request->hasFile('image_header')) {
            $article->image_header = '/uploads/articles/' . $request->file('image_header')->store($article->id, 'article');
        }

        if ($request->has('seo')) {
            $article->storeSeoData($request->seo);
        }

        $article->update();

        return redirect()->route('admin.article.article.edit', $article);
    }

    public function edit(Article $article)
    {
        $statuses = Article::listStatuses();
        $categories = ArticleCategory::all();
        $seoData = $article->seo->where('locale', config('app.locale_default'))->first();

        return view('admin.article.article.edit', compact('article', 'statuses', 'categories', 'seoData'));
    }

    public function update(Article $article, ArticleRequest $request)
    {
        $article->disableTranslatable();
        $oldPreview = $article->preview;
        $oldImageHeader = $article->image_header;

        $article->fill($request->except('seo'));

        if ($request->hasFile('preview')) {
            \File::delete(public_path($oldPreview));
            $article->preview = '/uploads/articles/' . $request->file('preview')->store($article->id, 'article');
        }

        if ($request->hasFile('image_header')) {
            \File::delete(public_path($oldImageHeader));
            $article->image_header = '/uploads/articles/' . $request->file('image_header')->store($article->id, 'article');
        }

        if ($request->has('seo')) {
            $article->updateSeoData($request->seo);
        }

        $article->update();

        return redirect()->route('admin.article.article.edit', $article);
    }

    public function destroy(Article $article)
    {
        $directory = $article->id;

        try {
            $article->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        \Storage::disk('article')->deleteDirectory('articles/' . $directory);

        if (request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.article.article.index')]);
        } else {
            return redirect()->to(route('admin.article.article.index'));
        }
    }
}
