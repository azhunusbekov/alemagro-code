<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Requests\Article\ArticleCategoryRequest;
use App\Http\Requests\Article\ArticleCategoryTranslationRequest;
use App\Models\Article\ArticleCategory;
use App\Models\Article\ArticleCategoryTranslation;
use App\Models\SeoManager;
use Illuminate\Routing\Controller;

class ArticleCategoryTranslationController extends Controller
{
    public function create(ArticleCategory $category)
    {
        $locales = config('app.locales');
        $seoData = null;

        return view('admin.article.category.translation.create', compact(
            'category',
            'locales',
            'seoData'
        ));
    }

    public function store(ArticleCategoryTranslationRequest $request, ArticleCategory $category)
    {
        $categoryTranslation = $category->translations()->create($request->except('seo'));

        if ($request->has('seo')) {
            $category->storeSeoData($request->seo, $request['locale']);
        }

        return redirect()->route('admin.article.category.translation.edit', [$category, $categoryTranslation]);
    }

    public function edit(ArticleCategory $category, ArticleCategoryTranslation $translation)
    {
        $locales = config('app.locales');
        $categoryTranslation = $translation;
        $seoData = $category->seo->where('locale', $categoryTranslation->locale)->first();

        return view('admin.article.category.translation.edit', compact(
            'category',
            'categoryTranslation',
            'locales',
            'seoData'
        ));
    }

    public function update(ArticleCategoryRequest $request, ArticleCategory $category, ArticleCategoryTranslation $translation)
    {
        $translation->update($request->except('seo'));

        if ($request->has('seo')) {
            $category->updateSeoData($request->seo, $request['locale']);
        }

        return redirect()->route('admin.article.category.translation.edit', [$category, $translation]);
    }

    public function destroy(ArticleCategory $category, ArticleCategoryTranslation $translation)
    {
        try {
            $translation->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.article.category.edit', $category)]);
        } else {
            return redirect()->to(route('admin.article.category.edit', $category));
        }
    }
}
