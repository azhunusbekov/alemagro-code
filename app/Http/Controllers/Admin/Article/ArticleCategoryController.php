<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Requests\Article\ArticleCategoryRequest;
use App\Models\Article\ArticleCategory;
use App\Models\SeoManager;
use Illuminate\Routing\Controller;

class ArticleCategoryController extends Controller
{
    public function index()
    {
        $categories = ArticleCategory::all();

        return view('admin.article.category.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.article.category.create');
    }

    public function store(ArticleCategoryRequest $request)
    {
        $category = new ArticleCategory();
        $category->disableTranslatable();
        $category->fill($request->except('seo'));

        $category->save();

        if ($request->has('seo')) {
            $category->storeSeoData($request->seo);
        }

        return redirect()->route('admin.article.category.edit', $category);
    }

    public function edit(ArticleCategory $category)
    {
        $seoData = $category->seo->where('locale', config('app.locale_default'))->first();

        return view('admin.article.category.edit', compact('category', 'seoData'));
    }

    public function update(ArticleCategory $category, ArticleCategoryRequest $request)
    {
        $category->disableTranslatable();
        $category->fill($request->except('seo'));
        $category->update();

        if ($request->has('seo')) {
            $category->updateSeoData($request->seo);
        }

        return redirect()->route('admin.article.category.edit', $category);
    }

    public function destroy(ArticleCategory $category)
    {
        try {
            $category->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.article.category.index')]);
        } else {
            return redirect()->to(route('admin.article.category.index'));
        }
    }
}
