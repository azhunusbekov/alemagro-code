<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PartnerRequest;
use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Storage;

class PartnerController extends Controller
{
    public function index()
    {
        $partners = Partner::orderBy('position')->get();

        return view('admin.partner.index', compact('partners'));
    }

    public function create()
    {
        return view('admin.partner.create');
    }

    public function store(PartnerRequest $request)
    {
        $partner = new Partner();
        $partner->fill($request->all());

        if ($request->hasFile('image')) {
            $partner->image = '/uploads/' . $request->file('image')->store('partners', 'uploads');
        }

        $partner->save();

        return redirect()->route('admin.partner.edit', $partner);
    }

    public function edit(Partner $partner)
    {
        return view('admin.partner.edit', compact('partner'));
    }

    public function update(Partner $partner, PartnerRequest $request)
    {
        $oldImage = $partner->image;

        $partner->fill($request->all());

        if ($request->hasFile('image')) {
            \File::delete(public_path($oldImage));
            $partner->image = '/uploads/' . $request->file('image')->store('partners', 'uploads');
        }

        $partner->update();

        return redirect()->route('admin.partner.edit', $partner);
    }

    public function destroy(Partner $partner)
    {
        Storage::delete($partner->image);
        $partner->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.partner.index')]);
        } else {
            return redirect()->to(route('admin.partner.index'));
        }
    }

    public function reorder(Request $request)
    {
        $list = $request->items;

        \DB::transaction(function () use ($list) {
            foreach ($list as $item) {
                Partner::where('id', '=', $item['id'])->update(['position' => $item['position']]);
            }
        });

        return response()->json(['request' => $request->all()]);
    }
}
