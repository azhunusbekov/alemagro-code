<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BannerRequest;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Storage;

class BannerController extends Controller
{
    public function index()
    {
        $banners = Banner::orderBy('position')->get();
        $statuses = Banner::listStatuses();

        return view('admin.banner.index', compact('banners', 'statuses'));
    }

    public function create()
    {
        $statuses = Banner::listStatuses();

        return view('admin.banner.create', compact('statuses'));
    }

    public function store(BannerRequest $request)
    {
        $banner = new Banner();
        $banner->fill($request->all());

        if ($request->hasFile('image')) {
            $banner->image = '/uploads/' . $request->file('image')->store('banners', 'uploads');
        }

        $banner->save();

        return redirect()->route('admin.banner.edit', $banner);
    }

    public function edit(Banner $banner)
    {
        $statuses = Banner::listStatuses();

        return view('admin.banner.edit', compact('banner', 'statuses'));
    }

    public function update(Banner $banner, BannerRequest $request)
    {
        $oldImage = $banner->image;

        $banner->fill($request->all());

        if ($request->hasFile('image')) {
            \File::delete(public_path($oldImage));
            $banner->image = '/uploads/' . $request->file('image')->store('banners', 'uploads');
        }

        $banner->update();

        return redirect()->route('admin.banner.edit', $banner);
    }

    public function destroy(Banner $banner)
    {
        Storage::delete($banner->image);
        $banner->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.banner.index')]);
        } else {
            return redirect()->to(route('admin.banner.index'));
        }
    }

    public function reorder(Request $request)
    {
        $list = $request->items;

        \DB::transaction(function () use ($list) {
            foreach ($list as $item) {
                Banner::where('id', '=', $item['id'])->update(['position' => $item['position']]);
            }
        });

        return response()->json(['request' => $request->all()]);
    }
}
