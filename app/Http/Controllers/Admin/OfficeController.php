<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OfficeRequest;
use App\Models\Office;
use Illuminate\Routing\Controller;

class OfficeController extends Controller
{
    public function index()
    {
        $offices = Office::all();

        return view('admin.office.index', compact('offices'));
    }

    public function create()
    {
        return view('admin.office.create');
    }

    public function store(OfficeRequest $request)
    {
        $office = new Office();
        $office->fill($request->all());

        $office->save();

        return redirect()->route('admin.office.edit', $office);
    }

    public function edit(Office $office)
    {
        return view('admin.office.edit', compact('office'));
    }

    public function update(Office $office, OfficeRequest $request)
    {
        $office->fill($request->all());
        $office->update();

        return redirect()->route('admin.office.edit', $office);
    }

    public function destroy(Office $office)
    {
        $office->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.office.index')]);
        } else {
            return redirect()->to(route('admin.office.index'));
        }
    }
}
