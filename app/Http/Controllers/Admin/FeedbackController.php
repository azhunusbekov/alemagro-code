<?php

namespace App\Http\Controllers\Admin;

use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class FeedbackController extends Controller
{
    public function index()
    {
        $feedbacks = Feedback::latest()->get();
        $statuses = Feedback::listStatuses();

        return view('admin.feedback.index', compact('feedbacks', 'statuses'));
    }

    public function edit(Feedback $feedback)
    {
        $statuses = Feedback::listStatuses();

        return view('admin.feedback.edit', compact('feedback', 'statuses'));
    }

    public function update(Feedback $feedback, Request $request)
    {
        $feedback->update($request->all());

        return redirect()->route('admin.feedback.edit', $feedback);
    }

    public function destroy(Feedback $feedback)
    {
        $feedback->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.feedback.index')]);
        } else {
            return redirect()->to(route('admin.feedback.index'));
        }
    }
}
