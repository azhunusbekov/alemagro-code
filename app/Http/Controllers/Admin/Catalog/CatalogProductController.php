<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogProductRequest;
use App\Models\Catalog\{
    CatalogCategory,
    CatalogCulture,
    CatalogProducer,
    CatalogProduct,
    CatalogProtection,
    CatalogSection
};
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;

class CatalogProductController extends Controller
{
    public function index(Request $request)
    {
        $products = CatalogProduct::query();
        $categories = CatalogCategory::all();
        $producers = CatalogProducer::all();
        $appends = [];

        if ($value = $request->get('category_id')) {
            $appends['category_id'] = $value;
            $products = $products->where('category_id', $value);
        }

        if ($value = $request->get('producer_id')) {
            $appends['producer_id'] = $value;
            $products = $products->where('producer_id', $value);
        }

        $products = $products
            ->with([
                'producer',
                'category',
                'section.parent'
            ])
            ->paginate(50)
            ->appends($appends);

        return view('admin.catalog.product.index', compact('products', 'categories', 'producers'));
    }

    public function create(CatalogCategory $category)
    {
        $statuses = CatalogProduct::listStatuses();
        $availables = CatalogProduct::listAvailables();
        $producers = CatalogProducer::all();
        $cultures = CatalogCulture::isRoot()->with([
            'translations',
            'children' => function ($query) {
                $query->with(['translations', 'children.translations', 'children.children.translations']);
            }
        ])->get();
        $sections = CatalogSection::isRoot()->where('category_id', $category->id)->with(['children', 'translations'])->get();
        $protections = CatalogProtection::with('translations')->get();
        $diseases = $category->diseases->load('translations');
        $substances = $category->substances->load('translations');
        $attributes = $category->attributes;

        return view('admin.catalog.product.create', compact(
            'categories',
            'producers',
            'cultures',
            'statuses',
            'category',
            'sections',
            'attributes',
            'diseases',
            'availables',
            'protections',
            'substances'
        ));
    }

    public function store(CatalogProductRequest $request, CatalogCategory $category)
    {
        $product = DB::transaction(function () use ($request, $category) {
            $product = new CatalogProduct();
            $product->disableTranslatable();
            $product = $product->fill($request->except('cultures', 'attributes', 'diseases', 'substances', 'photos', 'protection', 'seo'));
            $product->saveOrFail();

            $product->cultures()->sync($request->cultures);
            $product->diseases()->sync($request->diseases);
            $product->protection()->sync($request->protection);
            $product->substances()->sync($request->substances);

            foreach ($category->attributes as $attribute) {
                $value = $request['attributes'][$attribute->id] ?? null;
                if (!empty($value)) {
                    $product->catalogAttributes()->updateOrCreate(
                        ['attribute_id' => $attribute->id],
                        ['value' => $value]
                    );
                }
            }

            if ($request->hasFile('preview')) {
                $product->preview = '/uploads/catalog/' . $request->file('preview')->store('products/' . $product->id, 'catalog');
            }

            if ($request->has('photos')) {
                foreach ($request['photos'] as $photo) {
                    $product->photos()->create([
                        'src' => '/uploads/catalog/' . $photo->store('products/' . $product->id, 'catalog')
                    ]);
                }
            }

            if ($request->has('seo')) {
                $product->storeSeoData($request->seo);
            }

            $product->update();

            return $product;
        });

        return redirect()->route('admin.catalog.product.edit', $product);
    }

    public function edit(CatalogProduct $product)
    {
        $product->load(['catalogAttributes','protection']);
        $statuses = CatalogProduct::listStatuses();
        $availables = CatalogProduct::listAvailables();
        $producers = CatalogProducer::all();
        $cultures = CatalogCulture::isRoot()->with([
            'translations',
            'children' => function ($query) {
                $query->with(['translations', 'children.translations', 'children.children.translations']);
            }
        ])->get();
        $category = CatalogCategory::find($product->category_id);
        $sections = CatalogSection::isRoot()->where('category_id', $category->id)->with(['children.translations', 'translations'])->get();
        $protections = CatalogProtection::with('translations')->get();
        $diseases = $category->diseases->load('translations');
        $substances = $category->substances->load('translations');
        $attributes = $category->attributes;

        $seoData = $product->seo->where('locale', config('app.locale_default'))->first();

        return view('admin.catalog.product.edit', compact(
            'product',
            'producers',
            'cultures',
            'statuses',
            'sections',
            'category',
            'attributes',
            'diseases',
            'availables',
            'protections',
            'substances',
            'seoData'
        ));
    }

    public function update(CatalogProductRequest $request, CatalogProduct $product, CatalogCategory $category)
    {
        $product = DB::transaction(function () use ($request, $category, $product) {
            $product->disableTranslatable();
            $product->fill($request->except('cultures', 'attributes', 'diseases', 'substances', 'photos', 'protection', 'seo'));

            $product->cultures()->sync($request->cultures);
            $product->diseases()->sync($request->diseases);
            $product->protection()->sync($request->protection);
            $product->substances()->sync($request->substances);

            foreach ($category->attributes as $attribute) {
                $value = $request['attributes'][$attribute->id] ?? null;
                if (!empty($value)) {
                    $product->catalogAttributes()->updateOrCreate(
                        ['attribute_id' => $attribute->id],
                        ['value' => $value]
                    );
                }
            }

            if ($request->hasFile('preview')) {
                $product->preview = '/uploads/catalog/' . $request->file('preview')->store('products/' . $product->id, 'catalog');
            }

            if ($request->has('photos')) {
                foreach ($request['photos'] as $photo) {
                    $product->photos()->create([
                        'src' => '/uploads/catalog/' . $photo->store('products/' . $product->id, 'catalog')
                    ]);
                }
            }

            if ($request->has('seo')) {
                $product->updateSeoData($request->seo);
            }

            $product->update();

            return $product;
        });

        return redirect()->route('admin.catalog.product.edit', $product);
    }

    public function destroy(CatalogProduct $product)
    {
        $directory = $product->id;

        try {
            $product->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        \Storage::disk('catalog')->deleteDirectory('products/' . $directory);

        if (request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.product.index')]);
        } else {
            return redirect()->to(route('admin.catalog.product.index'));
        }
    }
}
