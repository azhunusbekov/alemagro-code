<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogProtectionRequest;
use App\Models\Catalog\CatalogCulture;
use App\Models\Catalog\CatalogProducer;
use App\Models\Catalog\CatalogProtection;
use Illuminate\Routing\Controller;

class CatalogProtectionController extends Controller
{
    public function index()
    {
        $protections = CatalogProtection::with('producer', 'cultures')->get();

        return view('admin.catalog.protection.index', compact('protections'));
    }

    public function create()
    {
        $statuses = CatalogProtection::listStatuses();
        $cultures = CatalogCulture::isRoot()->with('children')->get();
        $producers = CatalogProducer::all();
        $purposes = CatalogProtection::getPurposes();

        return view('admin.catalog.protection.create', compact('producers', 'cultures', 'statuses', 'purposes'));
    }

    public function store(CatalogProtectionRequest $request)
    {
        $protection = new CatalogProtection();
        $protection->disableTranslatable();
        $protection->fill($request->except('cultures'));
        $protection->save();

        $protection->cultures()->sync($request->cultures);

        if ($request->hasFile('src')) {
            $protection->src = '/uploads/catalog/' . $request->file('src')->store('protections/' . $protection->id, 'catalog');
        }

        $protection->update();

        return redirect()->route('admin.catalog.protection.edit', $protection);
    }

    public function edit(CatalogProtection $protection)
    {
        $statuses = CatalogProtection::listStatuses();
        $purposes = CatalogProtection::getPurposes();
        $cultures = CatalogCulture::isRoot()->with('children')->get();
        $producers = CatalogProducer::all();

        return view('admin.catalog.protection.edit', compact(
            'protection',
            'producers',
            'cultures',
            'statuses',
            'purposes'
        ));
    }

    public function update(CatalogProtection $protection, CatalogProtectionRequest $request)
    {
        $protection->disableTranslatable();
        $protection->fill($request->except('cultures'));

        $protection->cultures()->sync($request->cultures);

        if ($request->hasFile('src')) {
            $protection->src = '/uploads/catalog/' . $request->file('src')->store('protections/' . $protection->id, 'catalog');
        }

        $protection->update();

        return redirect()->route('admin.catalog.protection.edit', $protection);
    }

    public function destroy(CatalogProtection $protection)
    {
        $directory = $protection->id;

        try {
            $protection->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        \Storage::disk('catalog')->deleteDirectory('protections/' . $directory);

        if (request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.protection.index')]);
        } else {
            return redirect()->to(route('admin.catalog.protection.index'));
        }
    }
}
