<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogProductTranslationRequest;
use App\Models\Catalog\CatalogProduct;
use App\Models\Catalog\CatalogProductTranslation;
use Illuminate\Routing\Controller;

class CatalogProductTranslationController extends Controller
{
    public function create(CatalogProduct $product)
    {
        $locales = config('app.locales');

        return view('admin.catalog.product.translation.create', compact('product', 'locales'));
    }

    public function store(CatalogProductTranslationRequest $request, CatalogProduct $product)
    {
        $productTranslation = $product->translations()->create($request->except('seo'));

        if ($request->has('seo')) {
            $product->storeSeoData($request->seo, $request['locale']);
        }

        return redirect()->route('admin.catalog.product.translation.edit', [$product, $productTranslation]);
    }

    public function edit(CatalogProduct $product, CatalogProductTranslation $translation)
    {
        $productTranslation = $translation;
        $locales = config('app.locales');
        $seoData = $product->seo->where('locale', $translation->locale)->first();

        return view('admin.catalog.product.translation.edit', compact('product', 'productTranslation', 'locales', 'seoData'));
    }

    public function update(CatalogProductTranslationRequest $request, CatalogProduct $product, CatalogProductTranslation $translation)
    {
        $translation->fill($request->except('seo'));
        $translation->update();

        if ($request->has('seo')) {
            $product->updateSeoData($request->seo, $request['locale']);
        }

        return redirect()->route('admin.catalog.product.translation.edit', [$product, $translation]);
    }

    public function destroy(CatalogProduct $product, CatalogProductTranslation $translation)
    {
        try {
            $translation->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.product.edit', $product)]);
        } else {
            return redirect()->to(route('admin.catalog.product.edit', $product));
        }
    }
}
