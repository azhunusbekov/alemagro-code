<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Imports\CatalogImport;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Excel;

class CatalogImportController extends Controller
{
    public function form()
    {
        return view('admin.catalog.import.form');
    }

    public function store(Request $request)
    {
        Excel::import(new CatalogImport(), $request->file('file'));
    }
}
