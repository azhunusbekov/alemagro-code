<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogSubstanceRequest;
use App\Models\Catalog\CatalogCategory;
use App\Models\Catalog\CatalogSubstance;
use Illuminate\Routing\Controller;

class CatalogSubstanceController extends Controller
{
    public function index()
    {
        $substances = CatalogSubstance::with('category')->get();

        return view('admin.catalog.substance.index', compact('substances'));
    }

    public function create()
    {
        $categories = CatalogCategory::all();

        return view('admin.catalog.substance.create', compact('categories'));
    }

    public function store(CatalogSubstanceRequest $request)
    {
        $substance = new CatalogSubstance();
        $substance->disableTranslatable();
        $substance->fill($request->all());
        $substance->save();

        return redirect()->route('admin.catalog.substance.edit', $substance);
    }

    public function edit(CatalogSubstance $substance)
    {
        $categories = CatalogCategory::all();

        return view('admin.catalog.substance.edit', compact('substance', 'categories'));
    }

    public function update(CatalogSubstance $substance, CatalogSubstanceRequest $request)
    {
        $substance->disableTranslatable();
        $substance->update($request->all());

        return redirect()->route('admin.catalog.substance.edit', $substance);
    }

    public function destroy(CatalogSubstance $substance)
    {
        try {
            $substance->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.substance.index')]);
        } else {
            return redirect()->to(route('admin.catalog.substance.index'));
        }
    }
}
