<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogSubstanceTranslationRequest;
use App\Models\Catalog\CatalogSubstance;
use App\Models\Catalog\CatalogSubstanceTranslation;
use Illuminate\Routing\Controller;

class CatalogSubstanceTranslationController extends Controller
{
    public function create(CatalogSubstance $substance)
    {
        $locales = config('app.locales');

        return view('admin.catalog.substance.translation.create', compact('substance', 'locales'));
    }

    public function store(CatalogSubstanceTranslationRequest $request, CatalogSubstance $substance)
    {
        $substanceTranslation = $substance->translations()->create($request->all());

        return redirect()->route('admin.catalog.substance.translation.edit', [$substance, $substanceTranslation]);
    }

    public function edit(CatalogSubstance $substance, CatalogSubstanceTranslation $translation)
    {
        $substanceTranslation = $translation;
        $locales = config('app.locales');

        return view('admin.catalog.substance.translation.edit', compact('substance', 'substanceTranslation', 'locales'));
    }

    public function update(CatalogSubstanceTranslationRequest $request, CatalogSubstance $substance, CatalogSubstanceTranslation $translation)
    {
        $translation->fill($request->all());
        $translation->update();

        return redirect()->route('admin.catalog.substance.translation.edit', [$substance, $translation]);
    }

    public function destroy(CatalogSubstance $substance, CatalogSubstanceTranslation $translation)
    {
        try {
            $translation->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.substance.edit', $substance)]);
        } else {
            return redirect()->to(route('admin.catalog.substance.edit', $substance));
        }
    }
}
