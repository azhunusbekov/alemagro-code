<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\CatalogProducerRequest;
use App\Models\Catalog\CatalogProducer;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CatalogProducerController extends Controller
{
    public function index()
    {
        $producers = CatalogProducer::orderBy('position')->get();

        return view('admin.catalog.producer.index', compact('producers'));
    }

    public function create()
    {
        return view('admin.catalog.producer.create');
    }

    public function store(CatalogProducerRequest $request)
    {
        $producer = new CatalogProducer();
        $producer->fill($request->all());
        $producer->save();

        if ($request->hasFile('image')) {
            $producer->image = '/uploads/catalog/' . $request->file('image')->store('producers/' . $producer->id, 'catalog');
            $producer->update();
        }

        return redirect()->route('admin.catalog.producer.edit', $producer);
    }

    public function edit(CatalogProducer $producer)
    {
        return view('admin.catalog.producer.edit', compact('producer'));
    }

    public function update(CatalogProducer $producer, CatalogProducerRequest $request)
    {
        $producer->fill($request->all());

        if ($request->hasFile('image')) {
            $producer->image = '/uploads/catalog/' . $request->file('image')->store('producers/' . $producer->id, 'catalog');
        }

        $producer->update();

        return redirect()->route('admin.catalog.producer.edit', $producer);
    }

    public function destroy(CatalogProducer $producer)
    {
        $directory = $producer->id;

        try {
            $producer->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        \Storage::disk('catalog')->deleteDirectory('producers/' . $directory);

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.producer.index')]);
        } else {
            return redirect()->to(route('admin.catalog.producer.index'));
        }
    }

    public function reorder(Request $request)
    {
        $list = $request->items;

        \DB::transaction(function () use ($list) {
            foreach ($list as $item) {
                CatalogProducer::where('id', '=', $item['id'])->update(['position' => $item['position']]);
            }
        });

        return response()->json(['request' => $request->all()]);
    }
}
