<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\CatalogAttributeRequest;
use App\Models\Catalog\CatalogAttribute;
use App\Models\Catalog\CatalogCategory;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CatalogAttributeController extends Controller
{
    public function index()
    {
        $attributes = CatalogAttribute::all();

        return view('admin.catalog.attribute.index', compact('attributes'));
    }

    public function create(CatalogCategory $category)
    {
        $locales = config('app.locales');
        $types = CatalogAttribute::listTypes();

        return view('admin.catalog.attribute.create', compact('category', 'types', 'locales'));
    }

    public function store(CatalogAttributeRequest $request, CatalogCategory $category)
    {
        $attribute = $category->attributes()->create([
            'name' => $request['name'],
            'locale' => $request['locale'],
            'type' => $request['type'],
            'required' => (bool)$request['required'],
            'options' => array_map('trim', preg_split('#[\r\n]+#', $request['options'])),
            'sort' => $request['sort'],
            'is_filtered' => (bool)$request['is_filtered'],
        ]);

        return redirect()->route('admin.catalog.category.attribute.edit', [$category, $attribute]);
    }

    public function edit(CatalogCategory $category, CatalogAttribute $attribute)
    {
        $locales = config('app.locales_all');
        $types = CatalogAttribute::listTypes();

        return view('admin.catalog.attribute.edit', compact('attribute', 'category', 'types', 'locales'));
    }

    public function update(CatalogAttributeRequest $request, CatalogCategory $category, CatalogAttribute $attribute)
    {
        $category->attributes()->findOrFail($attribute->id)->update([
            'name' => $request['name'],
            'locale' => $request['locale'],
            'type' => $request['type'],
            'required' => (bool)$request['required'],
            'options' => !empty($request['options']) ? array_map('trim', preg_split('#[\r\n]+#', $request['options'])) : null,
            'sort' => $request['sort'],
            'is_filtered' => (bool)$request['is_filtered'],
        ]);

        return redirect()->route('admin.catalog.category.attribute.edit', [$category, $attribute]);
    }

    public function destroy(CatalogCategory $category, CatalogAttribute $attribute)
    {
        try {
            $attribute->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.category.edit', $category)]);
        } else {
            return redirect()->to(route('admin.catalog.category.edit', $category));
        }
    }

    public function reorder(Request $request)
    {
        $list = $request->items;

        \DB::transaction(function () use ($list) {
            foreach ($list as $item) {
                CatalogAttribute::where('id', '=', $item['id'])->update(['sort' => $item['position']]);
            }
        });

        return response()->json(['request' => $request->all()]);
    }
}
