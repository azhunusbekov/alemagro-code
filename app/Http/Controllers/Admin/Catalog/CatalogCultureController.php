<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogCultureRequest;
use App\Models\Catalog\CatalogCulture;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CatalogCultureController extends Controller
{
    public function index()
    {
        $parent = null;

        $cultures = CatalogCulture::query();

        if ($parent_id = request()->input('parent_id')) {
            $parent = CatalogCulture::find($parent_id);
            $cultures = $cultures->where('parent_id', $parent_id);
        } else {
            $cultures = $cultures->whereNull('parent_id');
        }

        $cultures = $cultures->with('children')->orderBy('position')->get();

        return view('admin.catalog.culture.index', compact('cultures', 'parent'));
    }

    public function create()
    {
        $parent = null;

        if ($parent_id = request()->input('parent_id')) {
            $parent = CatalogCulture::find($parent_id);
        }

        return view('admin.catalog.culture.create', compact('parent'));
    }

    public function store(CatalogCultureRequest $request)
    {
        $culture = new CatalogCulture();
        $culture->disableTranslatable();
        $culture->fill($request->except('seo'));
        $culture->save();

        if ($request->hasFile('icon')) {
            $culture->icon = '/uploads/catalog/' . $request->file('icon')->store('cultures/' . $culture->id, 'catalog');
        }

        if ($request->hasFile('image')) {
            $culture->image = '/uploads/catalog/' . $request->file('image')->store('cultures/' . $culture->id, 'catalog');
        }

        if ($request->hasFile('image_header')) {
            $culture->image_header = '/uploads/catalog/' . $request->file('image_header')->store('cultures/' . $culture->id, 'catalog');
        }

        if ($request->has('seo')) {
            $culture->storeSeoData($request->seo);
        }

        $culture->update();

        return redirect()->route('admin.catalog.culture.edit', $culture);
    }

    public function edit(CatalogCulture $culture)
    {
        $seoData = $culture->seo->where('locale', config('app.locale_default'))->first();

        return view('admin.catalog.culture.edit', compact('culture', 'seoData'));
    }

    public function update(CatalogCulture $culture, CatalogCultureRequest $request)
    {
        $culture->disableTranslatable();
        $culture->fill($request->except('seo'));

        if ($request->hasFile('icon')) {
            $culture->icon = '/uploads/catalog/' . $request->file('icon')->store('cultures/' . $culture->id, 'catalog');
        }

        if ($request->hasFile('image')) {
            $culture->image = '/uploads/catalog/' . $request->file('image')->store('cultures/' . $culture->id, 'catalog');
        }

        if ($request->hasFile('image_header')) {
            $culture->image_header = '/uploads/catalog/' . $request->file('image_header')->store('cultures/' . $culture->id, 'catalog');
        }

        if ($request->has('seo')) {
            $culture->updateSeoData($request->seo);
        }

        $culture->update();

        return redirect()->route('admin.catalog.culture.edit', $culture);
    }

    public function destroy(CatalogCulture $culture)
    {
        $directory = $culture->id;

        try {
            $culture->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        \Storage::disk('catalog')->deleteDirectory('culture/' . $directory);

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.culture.index')]);
        } else {
            return redirect()->to(route('admin.catalog.culture.index'));
        }
    }

    public function reorder(Request $request)
    {
        $list = $request->items;

        \DB::transaction(function () use ($list) {
            foreach ($list as $item) {
                CatalogCulture::where('id', '=', $item['id'])->update(['position' => $item['position']]);
            }
        });

        return response()->json(['request' => $request->all()]);
    }
}
