<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogDiseaseTranslationRequest;
use App\Models\Catalog\CatalogDisease;
use App\Models\Catalog\CatalogDiseaseTranslation;
use Illuminate\Routing\Controller;

class CatalogDiseaseTranslationController extends Controller
{
    public function create(CatalogDisease $disease)
    {
        $locales = config('app.locales');

        return view('admin.catalog.disease.translation.create', compact('disease', 'locales'));
    }

    public function store(CatalogDiseaseTranslationRequest $request, CatalogDisease $disease)
    {
        $diseaseTranslation = $disease->translations()->create($request->all());

        return redirect()->route('admin.catalog.disease.translation.edit', [$disease, $diseaseTranslation]);
    }

    public function edit(CatalogDisease $disease, CatalogDiseaseTranslation $translation)
    {
        $diseaseTranslation = $translation;
        $locales = config('app.locales');

        return view('admin.catalog.disease.translation.edit', compact('disease', 'diseaseTranslation', 'locales'));
    }

    public function update(CatalogDiseaseTranslationRequest $request, CatalogDisease $disease, CatalogDiseaseTranslation $translation)
    {
        $translation->fill($request->all());
        $translation->update();

        return redirect()->route('admin.catalog.disease.translation.edit', [$disease, $translation]);
    }

    public function destroy(CatalogDisease $disease, CatalogDiseaseTranslation $translation)
    {
        try {
            $translation->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.disease.edit', $disease)]);
        } else {
            return redirect()->to(route('admin.catalog.disease.edit', $disease));
        }
    }
}
