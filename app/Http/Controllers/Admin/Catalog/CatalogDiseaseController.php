<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogDiseaseRequest;
use App\Models\Catalog\CatalogCategory;
use App\Models\Catalog\CatalogDisease;
use Illuminate\Routing\Controller;

class CatalogDiseaseController extends Controller
{
    public function index()
    {
        $diseases = CatalogDisease::with('category')->get();

        return view('admin.catalog.disease.index', compact('diseases'));
    }

    public function create()
    {
        $categories = CatalogCategory::all();

        return view('admin.catalog.disease.create', compact('categories'));
    }

    public function store(CatalogDiseaseRequest $request)
    {
        $disease = new CatalogDisease();
        $disease->disableTranslatable();
        $disease->fill($request->all());
        $disease->save();

        return redirect()->route('admin.catalog.disease.edit', $disease);
    }

    public function edit(CatalogDisease $disease)
    {
        $categories = CatalogCategory::all();

        return view('admin.catalog.disease.edit', compact('disease', 'categories'));
    }

    public function update(CatalogDisease $disease, CatalogDiseaseRequest $request)
    {
        $disease->disableTranslatable();
        $disease->update($request->all());

        return redirect()->route('admin.catalog.disease.edit', $disease);
    }

    public function destroy(CatalogDisease $disease)
    {
        try {
            $disease->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.disease.index')]);
        } else {
            return redirect()->to(route('admin.catalog.disease.index'));
        }
    }
}
