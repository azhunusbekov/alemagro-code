<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogCultureTranslationRequest;
use App\Models\Catalog\CatalogCulture;
use App\Models\Catalog\CatalogCultureTranslation;
use Illuminate\Routing\Controller;

class CatalogCultureTranslationController extends Controller
{
    public function create(CatalogCulture $culture)
    {
        $locales = config('app.locales');

        return view('admin.catalog.culture.translation.create', compact('culture', 'locales'));
    }

    public function store(CatalogCultureTranslationRequest $request, CatalogCulture $culture)
    {
        $cultureTranslation = $culture->translations()->create($request->except('seo'));

        if ($request->has('seo')) {
            $culture->storeSeoData($request->seo, $request['locale']);
        }

        return redirect()->route('admin.catalog.culture.translation.edit', [$culture, $cultureTranslation]);
    }

    public function edit(CatalogCulture $culture, CatalogCultureTranslation $translation)
    {
        $cultureTranslation = $translation;
        $locales = config('app.locales');
        $seoData = $culture->seo->where('locale', $translation->locale)->first();

        return view('admin.catalog.culture.translation.edit', compact(
            'culture',
            'cultureTranslation',
            'locales',
            'seoData'
        ));
    }

    public function update(CatalogCultureTranslationRequest $request, CatalogCulture $culture, CatalogCultureTranslation $translation)
    {
        $translation->fill($request->except('seo'));
        $translation->update();

        if ($request->has('seo')) {
            $culture->updateSeoData($request->seo, $request['locale']);
        }

        return redirect()->route('admin.catalog.culture.translation.edit', [$culture, $translation]);
    }

    public function destroy(CatalogCulture $culture, CatalogCultureTranslation $translation)
    {
        try {
            $translation->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.culture.edit', $culture)]);
        } else {
            return redirect()->to(route('admin.catalog.culture.edit', $culture));
        }
    }
}
