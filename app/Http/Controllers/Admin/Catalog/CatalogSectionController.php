<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogSectionRequest;
use App\Models\Catalog\CatalogCategory;
use App\Models\Catalog\CatalogSection;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CatalogSectionController extends Controller
{
    public function index(Request $request)
    {
        $parent = null;

        $cultures = CatalogSection::query();
        $categories = CatalogCategory::all();

        if ($parent_id = request()->input('parent_id')) {
            $parent = CatalogSection::find($parent_id);
            $cultures = $cultures->where('parent_id', $parent_id);
        } else {
            $cultures = $cultures->whereNull('parent_id');
        }

        if ($value = $request->get('category_id')) {
            $cultures = $cultures->where('category_id', $value);
        }

        $sections = $cultures->with('category')->orderBy('position')->get();

        return view('admin.catalog.section.index', compact('categories', 'sections', 'parent'));
    }

    public function create()
    {
        $parent = null;
        $categories = CatalogCategory::all();

        if ($parent_id = request()->input('parent_id')) {
            $parent = CatalogSection::find($parent_id);
        }

        return view('admin.catalog.section.create', compact('categories', 'parent'));
    }

    public function store(CatalogSectionRequest $request)
    {
        $section = new CatalogSection();
        $section->disableTranslatable();
        $section->fill($request->except('seo'));
        $section->save();

        if ($request->has('seo')) {
            $section->storeSeoData($request->seo);
        }

        return redirect()->route('admin.catalog.section.edit', $section);
    }

    public function edit(CatalogSection $section)
    {
        $categories = CatalogCategory::all();
        $seoData = $section->seo->where('locale', config('app.locale_default'))->first();

        return view('admin.catalog.section.edit', compact('categories', 'section', 'seoData'));
    }

    public function update(CatalogSection $section, CatalogSectionRequest $request)
    {
        $section->disableTranslatable();
        $section->fill($request->except('seo'));
        $section->update();

        if ($request->has('seo')) {
            $section->updateSeoData($request->seo);
        }

        return redirect()->route('admin.catalog.section.edit', $section);
    }

    public function destroy(CatalogSection $section)
    {
        try {
            $section->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.section.index')]);
        } else {
            return redirect()->to(route('admin.catalog.section.index'));
        }
    }

    public function reorder(Request $request)
    {
        $list = $request->items;

        \DB::transaction(function () use ($list) {
            foreach ($list as $item) {
                CatalogSection::where('id', '=', $item['id'])->update(['position' => $item['position']]);
            }
        });

        return response()->json(['request' => $request->all()]);
    }
}
