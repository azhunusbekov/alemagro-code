<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogSectionTranslationRequest;
use App\Models\Catalog\CatalogSection;
use App\Models\Catalog\CatalogSectionTranslation;
use Illuminate\Routing\Controller;

class CatalogSectionTranslationController extends Controller
{
    public function create(CatalogSection $section)
    {
        $locales = config('app.locales');
        $statuses = CatalogSection::listStatuses();

        return view('admin.catalog.section.translation.create', compact('section', 'locales', 'statuses'));
    }

    public function store(CatalogSectionTranslationRequest $request, CatalogSection $section)
    {
        $sectionTranslation = $section->translations()->create($request->except('seo'));

        if ($request->has('seo')) {
            $section->storeSeoData($request->seo, $request['locale']);
        }

        return redirect()->route('admin.catalog.section.translation.edit', [$section, $sectionTranslation]);
    }

    public function edit(CatalogSection $section, CatalogSectionTranslation $translation)
    {
        $sectionTranslation = $translation;
        $locales = config('app.locales');
        $statuses = CatalogSection::listStatuses();
        $seoData = $section->seo->where('locale', $translation->locale)->first();

        return view('admin.catalog.section.translation.edit', compact(
            'section',
            'sectionTranslation',
            'locales',
            'statuses',
            'seoData'
        ));
    }

    public function update(CatalogSectionTranslationRequest $request, CatalogSection $section, CatalogSectionTranslation $translation)
    {
        $translation->fill($request->except('seo'));
        $translation->update();

        if ($request->has('seo')) {
            $section->updateSeoData($request->seo, $request['locale']);
        }

        return redirect()->route('admin.catalog.section.translation.edit', [$section, $translation]);
    }

    public function destroy(CatalogSection $section, CatalogSectionTranslation $translation)
    {
        try {
            $translation->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.section.edit', $section)]);
        } else {
            return redirect()->to(route('admin.catalog.section.edit', $section));
        }
    }
}
