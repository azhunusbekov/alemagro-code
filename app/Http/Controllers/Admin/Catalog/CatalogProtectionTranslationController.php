<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogProtectionTranslationRequest;
use App\Models\Catalog\CatalogProtection;
use App\Models\Catalog\CatalogProtectionTranslation;
use Illuminate\Routing\Controller;

class CatalogProtectionTranslationController extends Controller
{
    public function create(CatalogProtection $protection)
    {
        $locales = config('app.locales');

        return view('admin.catalog.protection.translation.create', compact('protection', 'locales'));
    }

    public function store(CatalogProtectionTranslationRequest $request, CatalogProtection $protection)
    {
        $protectionTranslation = $protection->translations()->create($request->all());

        return redirect()->route('admin.catalog.protection.translation.edit', [$protection, $protectionTranslation]);
    }

    public function edit(CatalogProtection $protection, CatalogProtectionTranslation $translation)
    {
        $protectionTranslation = $translation;
        $locales = config('app.locales');

        return view('admin.catalog.protection.translation.edit', compact('protection', 'protectionTranslation', 'locales'));
    }

    public function update(CatalogProtectionTranslationRequest $request, CatalogProtection $protection, CatalogProtectionTranslation $translation)
    {
        $translation->fill($request->all());
        $translation->update();

        return redirect()->route('admin.catalog.protection.translation.edit', [$protection, $translation]);
    }

    public function destroy(CatalogProtection $protection, CatalogProtectionTranslation $translation)
    {
        try {
            $translation->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.protection.edit', $protection)]);
        } else {
            return redirect()->to(route('admin.catalog.protection.edit', $protection));
        }
    }
}
