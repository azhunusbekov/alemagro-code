<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogCategoryRequest;
use App\Models\Catalog\CatalogCategory;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CatalogCategoryController extends Controller
{
    public function index()
    {
        $categories = CatalogCategory::orderBy('position')->get();

        return view('admin.catalog.category.index', compact('categories'));
    }

    public function create()
    {
        $statuses = CatalogCategory::listStatuses();

        return view('admin.catalog.category.create', compact('statuses'));
    }

    public function store(CatalogCategoryRequest $request)
    {
        $category = new CatalogCategory();
        $category->disableTranslatable();
        $category->fill($request->except('seo'));
        $category->save();

        if ($request->hasFile('icon')) {
            $category->icon = '/uploads/catalog/' . $request->file('icon')->store('categories/' . $category->id, 'catalog');
        }

        if ($request->hasFile('image_header')) {
            $category->image_header = '/uploads/catalog/' . $request->file('image_header')->store('categories/' . $category->id, 'catalog');
        }

        if ($request->has('seo')) {
            $category->storeSeoData($request->seo);
        }

        $category->update();

        return redirect()->route('admin.catalog.category.edit', $category);
    }

    public function edit(CatalogCategory $category)
    {
        $attributes = $category->attributes;
        $statuses = CatalogCategory::listStatuses();
        $seoData = $category->seo->where('locale', config('app.locale_default'))->first();

        return view('admin.catalog.category.edit', compact('category', 'attributes', 'statuses', 'seoData'));
    }

    public function update(CatalogCategoryRequest $request, CatalogCategory $category)
    {
        $category->disableTranslatable();
        $category->fill($request->except('seo'));

        if ($request->hasFile('icon')) {
            $category->icon = '/uploads/catalog/' . $request->file('icon')->store('categories/' . $category->id, 'catalog');
        }

        if ($request->hasFile('image_header')) {
            $category->image_header = '/uploads/catalog/' . $request->file('image_header')->store('categories/' . $category->id, 'catalog');
        }

        if ($request->has('seo')) {
            $category->updateSeoData($request->seo);
        }

        $category->update();

        return redirect()->route('admin.catalog.category.edit', $category);
    }

    public function destroy(CatalogCategory $category)
    {
        $directory = $category->id;

        try {
            $category->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        \Storage::disk('catalog')->deleteDirectory('categories/' . $directory);

        if (request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.category.index')]);
        } else {
            return redirect()->to(route('admin.catalog.category.index'));
        }
    }

    public function reorder(Request $request)
    {
        $list = $request->items;

        \DB::transaction(function () use ($list) {
            foreach ($list as $item) {
                CatalogCategory::where('id', '=', $item['id'])->update(['position' => $item['position']]);
            }
        });

        return response()->json(['request' => $request->all()]);
    }
}
