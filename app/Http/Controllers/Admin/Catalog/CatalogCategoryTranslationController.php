<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Catalog\CatalogCategoryTranslationRequest;
use App\Models\Catalog\CatalogCategory;
use App\Models\Catalog\CatalogCategoryTranslation;
use Illuminate\Routing\Controller;

class CatalogCategoryTranslationController extends Controller
{
    public function create(CatalogCategory $category)
    {
        $locales = config('app.locales');
        $statuses = CatalogCategory::listStatuses();

        return view('admin.catalog.category.translation.create', compact('category', 'locales', 'statuses'));
    }

    public function store(CatalogCategoryTranslationRequest $request, CatalogCategory $category)
    {
        $categoryTranslation = $category->translations()->create($request->except('seo'));

        if ($request->has('seo')) {
            $category->storeSeoData($request->seo, $request['locale']);
        }

        return redirect()->route('admin.catalog.category.translation.edit', [$category, $categoryTranslation]);
    }

    public function edit(CatalogCategory $category, CatalogCategoryTranslation $translation)
    {
        $categoryTranslation = $translation;
        $locales = config('app.locales');
        $statuses = CatalogCategory::listStatuses();
        $seoData = $category->seo->where('locale', $translation->locale)->first();

        return view('admin.catalog.category.translation.edit', compact(
            'category',
            'categoryTranslation',
            'locales',
            'statuses',
            'seoData'
        ));
    }

    public function update(CatalogCategoryTranslationRequest $request, CatalogCategory $category, CatalogCategoryTranslation $translation)
    {
        $translation->fill($request->except('seo'));
        $translation->update();

        if ($request->has('seo')) {
            $category->updateSeoData($request->seo, $request['locale']);
        }

        return redirect()->route('admin.catalog.category.translation.edit', [$category, $translation]);
    }

    public function destroy(CatalogCategory $category, CatalogCategoryTranslation $translation)
    {
        try {
            $translation->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.catalog.category.edit', $category)]);
        } else {
            return redirect()->to(route('admin.catalog.category.edit', $category));
        }
    }
}
