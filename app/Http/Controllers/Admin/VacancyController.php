<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VacancyRequest;
use App\Models\Vacancy;
use App\Models\VacancyResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class VacancyController extends Controller
{
    public function index()
    {
        $vacancies = Vacancy::with('responses')->orderBy('position')->get();
        $statuses = Vacancy::listStatuses();
        $locales = config('app.locales');

        return view('admin.vacancy.index', compact('vacancies', 'statuses', 'locales'));
    }

    public function create()
    {
        $statuses = Vacancy::listStatuses();
        $locales = config('app.locales_all');

        return view('admin.vacancy.create', compact('statuses', 'locales'));
    }

    public function store(VacancyRequest $request)
    {
        $vacancy = new Vacancy();
        $vacancy->fill($request->all());

        if ($request->hasFile('image')) {
            $vacancy->image = '/uploads/' . $request->file('image')->store('vacancys', 'uploads');
        }

        $vacancy->save();

        return redirect()->route('admin.vacancy.edit', $vacancy);
    }

    public function edit(Vacancy $vacancy)
    {
        $statuses = Vacancy::listStatuses();
        $locales = config('app.locales_all');

        return view('admin.vacancy.edit', compact('vacancy', 'statuses', 'locales'));
    }

    public function update(Vacancy $vacancy, VacancyRequest $request)
    {
        $vacancy->update($request->all());

        return redirect()->route('admin.vacancy.edit', $vacancy);
    }

    public function destroy(Vacancy $vacancy)
    {
        $vacancy->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => true, 'redirect' => route('admin.vacancy.index')]);
        } else {
            return redirect()->to(route('admin.vacancy.index'));
        }
    }

    public function responses(Vacancy $vacancy)
    {
        $responses = $vacancy->responses;

        return view('admin.vacancy.responses', compact('vacancy', 'responses'));
    }

    public function response(Vacancy $vacancy, VacancyResponse $response)
    {
        return view('admin.vacancy.response', compact('vacancy', 'response'));
    }

    public function reorder(Request $request)
    {
        $list = $request->items;

        \DB::transaction(function () use ($list) {
            foreach ($list as $item) {
                Vacancy::where('id', '=', $item['id'])->update(['position' => $item['position']]);
            }
        });

        return response()->json(['request' => $request->all()]);
    }
}
