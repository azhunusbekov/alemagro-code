<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;

class CkeditorController extends Controller
{
    public function upload(Request $request)
    {
        $CKEditor = $request->input('CKEditor');
        $funcNum = $request->input('CKEditorFuncNum');
        $message = $url = '';

        if ($request->hasFile('upload')) {
            $file = $request->file('upload');
            if ($file->isValid()) {
                $filename = $file->getClientOriginalName();

                $path = public_path() . '/uploads/images/';

                if (!File::isDirectory($path)) {
                    File::makeDirectory($path, 0777, true, true);
                }

                if (!$file_path = $file->move($path, $filename)) {
                    $message = 'Error saving the file.';
                }

                $url = '/uploads/images/' . $filename;
            } else {
                $message = 'An error occured while uploading the file.';
            }
        } else {
            $message = 'No file uploaded.';
        }

        echo '<script>window.parent.CKEDITOR.tools.callFunction('.$funcNum.', "'.$url.'", "'.$message.'")</script>';
    }
}
