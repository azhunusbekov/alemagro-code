<?php

namespace App\Http\Controllers;

use App\Models\Catalog\CatalogCategory;
use App\Models\Catalog\CatalogProduct;
use App\Models\Catalog\CatalogSection;
use App\Services\SEOService;

class SitemapController extends Controller
{
    public function sitemap()
    {
        $items = array_merge(
            $this->getStaticLinks(),
            $this->getCatalogCategoriesLinks(),
            $this->getCatalogSectionsLinks(),
            $this->getCatalogProductsLinks()
        );

        return response()
            ->view('sitemap.index', ['items' => $items])
            ->header('Content-Type', 'text/xml');
    }

    protected function getStaticLinks()
    {
        return [
            ['link' => trim(lang_route('home', null, config('app.locale_default')), '/')],
            ['link' => lang_route('about', null, config('app.locale_default'))],
            ['link' => lang_route('contact', null, config('app.locale_default'))],
            ['link' => lang_route('consult', null, config('app.locale_default'))],
            ['link' => lang_route('career', null, config('app.locale_default'))],
            ['link' => lang_route('internship', null, config('app.locale_default'))],
        ];
    }

    protected function getCatalogCategoriesLinks()
    {
        $result = [];

        $categories = CatalogCategory::published()
            ->activeTranslation()
            ->with('translations')
            ->orderBy('position')
            ->get();

        foreach ($categories as $category) {
            $result[] = ['link' => lang_route('catalog.category', ['country_code' => 'kz', 'catalog_category' => $category->slug])];
        }

        return $result;
    }

    protected function getCatalogSectionsLinks()
    {
        $result = [];

        $sections = CatalogSection::isRoot()
            ->published()
            ->with([
                'translations',
                'category.translations',
                'parent.translations',
                'children.translations',
            ])
            ->orderBy('position')
            ->get();

        foreach ($sections as $section) {
            $result[] = ['link' => lang_route('catalog.section', ['country_code' => 'kz', 'section_path' => $section->getPath()])];

            if ($section->children) {
                foreach ($section->children as $child) {
                    $result[] = ['link' => lang_route('catalog.section', ['country_code' => 'kz', 'section_path' => $child->getPath()])];
                }
            }
        }

        return $result;
    }

    protected function getCatalogProductsLinks()
    {
        $result = [];

        $products = CatalogProduct::published()
            ->with([
                'translations',
                'category.translations',
                'section' => function ($query) {
                    $query->with(['translations', 'parent.translations']);
                }
            ])
            ->get();

        foreach ($products as $product) {
            $result[] = ['link' => $product->getLink('kz', $product->category)];
        }

        return $result;
    }
}
