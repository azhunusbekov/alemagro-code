<?php

namespace App\Http\Controllers;

use App\Http\Routers\CatalogProductPath;
use App\Http\Routers\CatalogSectionPath;
use App\Models\Catalog\{CatalogCategory, CatalogCulture, CatalogProduct};
use App\Repositories\CatalogRepository;
use App\Services\SEOService;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    protected $catalog;

    public function __construct(CatalogRepository $catalog)
    {
        $this->catalog = $catalog;
    }

    public function index()
    {
        SEOService::init(trans('seo.catalog'));

        return view('catalog.index');
    }

    public function main($countryCode)
    {
        $producers = $this->catalog->getAllProducers();
        $cultures = $this->catalog->getCulturesTree();
        $culturesFavorites = $this->catalog->getCulturesFavorites();
        $categories = $this->catalog->getCategoriesWithSections();

        SEOService::init(trans('seo.catalog_'.$countryCode));

        return view('catalog.main', compact(
            'countryCode',
            'producers',
            'cultures',
            'culturesFavorites',
            'categories'
        ));
    }

    public function category(Request $request, $countryCode, CatalogCategory $category)
    {
        $producers = $this->catalog->getAllProducers();
        $cultures = $this->catalog->getCulturesTree();
        $culturesFavorites = $this->catalog->getCulturesFavorites();
        $categories = $this->catalog->getAllCategories();
        $sections = $this->catalog->getSectionsTree($category);

        $diseases = $this->catalog->getDiseases($category);
        $substances = $this->catalog->getSubstances($category);

        $products = CatalogProduct::query();
        $filters = [];

        if ($values = $request->input('cultures')) {
            $filters['cultures'] = $values;
            $products->whereHas('cultures', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($values = $request->input('producers')) {
            $filters['producers'] = $values;
            $products->whereHas('producer', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($values = $request->input('diseases')) {
            $filters['diseases'] = $values;
            $products->whereHas('diseases', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($values = $request->input('substances')) {
            $filters['substances'] = $values;
            $products->whereHas('substances', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($value = $request->input('search')) {
            $filters['search'] = $value;
            $products->where('title', 'like', '%' . $value . '%')
                ->orWhere('title', 'like', '%' . $value . '%');
        }

        if ($value = $request->input('sort')) {
            $filters['sort'] = $value;

            if ($value == 'alphabetically') {
                $products->orderBy('title');
            }
        }

        $products = $products->where('category_id', $category->id)
            ->whereAvailable($countryCode)
            ->with([
                'translations',
                'section' => function ($query) {
                    $query->with(['translations', 'parent.translations']);
                },
                'cultures',
                'producer'
            ])
            ->paginate(24)->appends($filters);

        $cultureProducts = null;

        if ($category->isSemena()) {
            $cultureProducts = $this->groupProducts($cultures, $products);
        }

        SEOService::init($category->title, 'catalog_category', $category->id);

        return view('catalog.category', compact(
            'countryCode',
            'producers',
            'cultures',
            'culturesFavorites',
            'categories',
            'category',
            'sections',
            'products',
            'filters',
            'diseases',
            'substances',
            'cultureProducts'
        ));
    }

    public function section(Request $request, $countryCode, CatalogSectionPath $sectionPath)
    {
        $category = $sectionPath->category;
        $section = $sectionPath->section;

        $producers = $this->catalog->getAllProducers();
        $cultures = $this->catalog->getCulturesTree();
        $culturesFavorites = $this->catalog->getCulturesFavorites();
        $categories = $this->catalog->getAllCategories();
        $sections = $this->catalog->getSectionsTree($category);

        $diseases = $this->catalog->getDiseases($category);
        $substances = $this->catalog->getSubstances($category);

        $products = CatalogProduct::query();
        $filters = [];

        if ($values = $request->input('cultures')) {
            $filters['cultures'] = $values;
            $products->whereHas('cultures', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($values = $request->input('producers')) {
            $filters['producers'] = $values;
            $products->whereHas('producer', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($values = $request->input('diseases')) {
            $filters['diseases'] = $values;
            $products->whereHas('diseases', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($values = $request->input('substances')) {
            $filters['substances'] = $values;
            $products->whereHas('substances', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($value = $request->input('search')) {
            $filters['search'] = $value;
            $products->where('title', 'like', '%' . $value . '%')
                ->orWhere('title', 'like', '%' . $value . '%');
        }

        if ($value = $request->input('sort')) {
            $filters['sort'] = $value;

            if ($value == 'alphabetically') {
                $products->orderBy('title');
            }
        }

        $products = $products
            ->where('category_id', $category->id)
            ->whereAvailable($countryCode)
            ->when(is_null($section->parent_id), function ($query) use ($section) {
                return $query->whereIn('section_id', $section->children->pluck('id')->toArray());
            }, function ($query) use ($section) {
                return $query->where('section_id', $section->id);
            })
            ->with([
                'translations',
                'section' => function ($query) {
                    $query->with(['translations', 'parent.translations']);
                }
            ])
            ->paginate(24)
            ->appends($filters);

        SEOService::init($section->title, 'catalog_section', $section->id);

        return view('catalog.section', compact(
            'countryCode',
            'producers',
            'cultures',
            'culturesFavorites',
            'categories',
            'category',
            'sections',
            'section',
            'products',
            'filters',
            'diseases',
            'substances'
        ));
    }

    public function product($countryCode, CatalogProductPath $productPath)
    {
        $product = $productPath->product;
        $productDiseases = null;
        $productSubstances = null;

        if (!$product->isAvailableOn($countryCode)) {
            abort(404);
        }

        $category = $productPath->category;
        $section = $productPath->section;
        $categoryAttributes = $category->attributes->where('locale', app()->getLocale());
        $productAttributes = $product->catalogAttributes;
        $productCultures = $product->cultures->load('translations');

        if ($product->has('diseases')) {
            $productDiseases = $product->diseases->load('translations');
        }

        if ($product->has('substances')) {
            $productSubstances = $product->substances->load('translations');
        }

        SEOService::init($product->title, 'catalog_product', $product->id);

        return view('catalog.product', compact(
            'countryCode',
            'category',
            'section',
            'product',
            'categoryAttributes',
            'productAttributes',
            'productCultures',
            'productDiseases',
            'productSubstances'
        ));
    }

    private function groupProducts($cultures, $products)
    {
        $result = [];

        foreach ($cultures as $culture) {
            $cultureProducts = [];

            foreach ($products as $product) {
                if ($product->cultures->contains($culture->id)) {
                    $cultureProducts[] = $product;
                }
            }

            if ($cultureProducts) {
                $culture->productItems = $cultureProducts;
            }

            if ($culture->children) {
                $culture->children = $this->groupProducts($culture->children, $products);
            }

            $result[] = $culture;
        }

        return $result;
    }
}
