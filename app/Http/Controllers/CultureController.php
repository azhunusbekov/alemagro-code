<?php

namespace App\Http\Controllers;

use App\Models\Catalog\CatalogCategory;
use App\Models\Catalog\CatalogCulture;
use App\Models\Catalog\CatalogProducer;
use App\Models\Catalog\CatalogProduct;
use App\Repositories\CatalogRepository;
use App\Services\SEOService;
use Illuminate\Http\Request;

class CultureController extends Controller
{
    protected $catalog;

    public function __construct(CatalogRepository $catalog)
    {
        $this->catalog = $catalog;
    }

    public function index()
    {
        $cultures = CatalogCulture::query()
            ->where('on_page', 1)
            ->with([
                'translations',
                'children.translations'
            ])
            ->orderBy('position')
            ->get();
        $categoryDefault = CatalogCategory::published()
            ->where('id', '=', CatalogCategory::CATEGORY_SEMENA_ID)
            ->activeTranslation()
            ->with('translations')
            ->orderBy('position')
            ->first();

        SEOService::init(trans('seo.culture.title'));

        return view('culture.index', compact(
            'cultures',
            'categoryDefault'
        ));
    }

    public function category(Request $request, CatalogCategory $category)
    {
        $producers = $this->catalog->getAllProducers();
        $categories = $this->catalog->getAllCategories();
        $cultures = $this->catalog->getCulturesTree();
        $culturesFavorites = $this->catalog->getCulturesFavorites();
        $sections = $this->catalog->getSectionsTree($category);
        $diseases = $this->catalog->getDiseases($category);
        $filterableAttributes = $this->catalog->getFilterableAttributes($category);
        $categoryAttributes = $category->attributes()
            ->whereLocale(app()->getLocale())
            ->orderBy('sort')
            ->take(2)
            ->get();

        $products = CatalogProduct::query();

        $filters = [];

        if ($values = $request->input('producers')) {
            $filters['producers'] = $values;
            $products->whereHas('producer', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($values = $request->input('diseases')) {
            $filters['diseases'] = $values;
            $products->whereHas('diseases', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($values = $request->input('sections')) {
            $filters['sections'] = $values;
            $products->whereHas('section', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($values = $request->input('attributes')) {
            $filters['attributes'] = $values;
            $products = $products->whereHas('catalogAttributes', function ($query) use ($values) {
                $query->whereIn('value', $values);
            });
        }

        if ($request->has('sort_by') && $request->has('sort_dir')) {
            $sortBy = $request->input('sort_by');
            $sortDir = $request->input('sort_dir');

            $filters['sort_by'] = $sortBy;
            $filters['sort_dir'] = $sortDir;

            if ($sortBy == 'title') {
                $products = $products->orderBy('title', $sortDir);
            }

            if ($sortBy == 'description') {
                $products = $products->orderBy('description', $sortDir);
            }

            if ($sortBy == 'composition') {
                $products = $products->orderBy('composition', $sortDir);
            }

            if ($sortBy == 'section') {
                $products = $products
                    ->leftJoin('catalog_sections as section', 'section.id', '=', 'catalog_products.section_id')
                    ->leftJoin('catalog_sections as parent_section', 'parent_section.id', '=', 'section.parent_id')
                    ->select('catalog_products.*', 'parent_section.title')
                    ->where('section.locale', app()->getLocale())
                    ->orderBy('parent_section.title', $sortDir);
            }

            if ($sortBy == 'sub_section') {
                $products = $products
                    ->join('catalog_sections as section', 'section.id', '=', 'catalog_products.section_id')
                    ->select('catalog_products.*', 'section.title')
                    ->where('section.locale', app()->getLocale())
                    ->orderBy('section.title', $sortDir);
            }

            if ($sortBy == 'attr' && $request->has('sort_val')) {
                $filters['sort_val'] = $request->input('sort_val');
                $products = $products
                    ->joinAttributeValue($request->input('sort_val'))
                    ->orderBy('value', $sortDir);
            }

            //if ($sortBy == 'diseases') {
            //    $products = $products
            //        ->leftJoin('catalog_products_diseases as pivot', 'pivot.product_id', '=', 'catalog_products.id')
            //        ->leftJoin('catalog_diseases as diseases', 'diseases.id', '=', 'pivot.disease_id')
            //        ->select('catalog_products.*', 'diseases.title')
            //        ->orderBy('diseases.title', $sortDir);
            //}
        }

        $products = $products
            ->where('catalog_products.category_id', $category->id)
            ->with([
                'translations',
                'section.translations',
                'section.parent.translations',
                'catalogAttributes',
                'diseases.translations'
            ])
            ->paginate(10)
            ->appends($filters);

        SEOService::init($category->title, 'catalog_category', $category->id);

        return view('culture.category', compact(
            'category',
            'categoryAttributes',
            'producers',
            'sections',
            'diseases',
            'cultures',
            'culturesFavorites',
            'filterableAttributes',
            'categories',
            'products',
            'filters'
        ));
    }

    public function culture(Request $request, CatalogCategory $category, CatalogCulture $culture)
    {
        $producers = $this->catalog->getAllProducers();
        $categories = $this->catalog->getAllCategories();
        $diseases = $this->catalog->getDiseases($category);
        $cultures = $this->catalog->getCulturesTree();
        $culturesFavorites = $this->catalog->getCulturesFavorites();
        $filterableAttributes = $this->catalog->getFilterableAttributes($category);
        $sections = $this->catalog->getSectionsTree($category);
        $categoryAttributes = $category->attributes()
            ->whereLocale(app()->getLocale())
            ->orderBy('sort')
            ->take(2)
            ->get();

        $products = CatalogProduct::query();
        $filters = [];

        if ($values = $request->input('producers')) {
            $filters['producers'] = $values;
            $products->whereHas('producer', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($values = $request->input('diseases')) {
            $filters['diseases'] = $values;
            $products->whereHas('diseases', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($values = $request->input('sections')) {
            $filters['sections'] = $values;
            $products->whereHas('section', function($query) use ($values) {
                $query->whereIn('id', $values);
            });
        }

        if ($values = $request->input('attributes')) {
            $filters['attributes'] = $values;
            $products = $products->whereHas('catalogAttributes', function ($query) use ($values) {
                $query->whereIn('value', $values);
            });
        }

        if ($request->has('sort_by') && $request->has('sort_dir')) {
            $sortBy = $request->input('sort_by');
            $sortDir = $request->input('sort_dir');

            $filters['sort_by'] = $sortBy;
            $filters['sort_dir'] = $sortDir;

            if ($sortBy == 'title') {
                $products = $products->orderBy('title', $sortDir);
            }

            if ($sortBy == 'description') {
                $products = $products->orderBy('description', $sortDir);
            }

            if ($sortBy == 'composition') {
                $products = $products->orderBy('composition', $sortDir);
            }

            if ($sortBy == 'section') {
                $products = $products
                    ->leftJoin('catalog_sections as section', 'section.id', '=', 'catalog_products.section_id')
                    ->leftJoin('catalog_sections as parent_section', 'parent_section.id', '=', 'section.parent_id')
                    ->select('catalog_products.*', 'parent_section.title')
                    ->where('section.locale', app()->getLocale())
                    ->orderBy('parent_section.title', $sortDir);
            }

            if ($sortBy == 'sub_section') {
                $products = $products
                    ->join('catalog_sections as section', 'section.id', '=', 'catalog_products.section_id')
                    ->select('catalog_products.*', 'section.title')
                    ->where('section.locale', app()->getLocale())
                    ->orderBy('section.title', $sortDir);
            }

            if ($sortBy == 'attr' && $request->has('sort_val')) {
                $products = $products
                    ->joinAttributeValue($request->input('sort_val'))
                    ->orderBy('value', $sortDir);
            }
        }

        $products = $products
            ->where('category_id', $category->id)
            ->whereHas('cultures', function($query) use ($culture) {
                $query->when(is_null($culture->parent_id), function ($query) use ($culture) {
                    return $query->whereIn('culture_id', $culture->children->pluck('id')->toArray());
                }, function ($query) use ($culture) {
                    return $query->where('culture_id', $culture->id);
                });
            })
            ->with([
                'translations',
                'section.translations',
                'section.parent.translations',
                'catalogAttributes',
                'diseases.translations'
            ])
            ->paginate(10)
            ->appends($filters);

        SEOService::init($culture->title, 'catalog_culture', $culture->id);

        return view('culture.culture', compact(
            'category',
            'categoryAttributes',
            'categories',
            'sections',
            'diseases',
            'producers',
            'cultures',
            'culturesFavorites',
            'filterableAttributes',
            'culture',
            'products',
            'filters'
        ));
    }
}
