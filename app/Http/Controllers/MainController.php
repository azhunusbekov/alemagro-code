<?php

namespace App\Http\Controllers;

use App\Mail\FeedbackEmail;
use App\Models\Article\Article;
use App\Models\Banner;
use App\Models\Catalog\CatalogCategory;
use App\Models\Catalog\CatalogCulture;
use App\Models\Office;
use App\Models\Partner;
use App\Services\SEOService;

class MainController extends Controller
{
    public function index()
    {
        $banners = Banner::where('status', Banner::STATUS_PUBLISHED)->orderBy('position')->get();
        $partners = Partner::orderBy('position')->get();
        $offices = Office::all();
        $news = Article::published()
            ->with('category.translations')
            ->latest()
            ->take(6)
            ->get();
        $cultures = CatalogCulture::isRoot()
            ->with([
                'translations',
                'children.translations'
            ])
            ->orderBy('position')
            ->limit(2)
            ->get();
        $category = CatalogCategory::where('id', CatalogCategory::CATEGORY_SEMENA_ID)->first();

        SEOService::init([
            'title' => trans('seo.home.title'),
            'description' => trans('seo.home.description'),
            'keywords' => trans('seo.home.keywords'),
        ]);

        return view('pages.home', compact('banners', 'offices', 'news', 'cultures', 'category', 'partners'));
    }
}
