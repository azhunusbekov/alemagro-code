<?php

namespace App\Http\Middleware;

class AdminMiddleware
{
    public function handle($request, \Closure $next)
    {
        if (!\Auth::guest() && \Auth::user()->isAdmin()) {
            return $next($request);
        }

        return redirect()->route('admin.login');
    }
}
