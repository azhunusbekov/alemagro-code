<?php

namespace App\Http\Requests\Catalog;

use Illuminate\Foundation\Http\FormRequest;

class CatalogDiseaseTranslationRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'locale' => 'required',
        ];

        return $rules;
    }
}
