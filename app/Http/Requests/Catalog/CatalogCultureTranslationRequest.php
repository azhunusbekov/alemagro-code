<?php

namespace App\Http\Requests\Catalog;

use Illuminate\Foundation\Http\FormRequest;

class CatalogCultureTranslationRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'locale' => 'required',
        ];

        return $rules;
    }
}
