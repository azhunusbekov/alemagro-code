<?php

namespace App\Http\Requests\Catalog;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CatalogProductRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'vendor_code' => 'required',
            'category_id' => 'required',
            'producer_id' => 'required',
            'photos.*' => 'image|max:200|dimensions:max_width=500,max_height=500'
        ];

        if (request()->isMethod('put') || request()->isMethod('patch')) {
            $rules['preview'] = 'image|max:200|dimensions:max_width=500,max_height=500';
        } else {
            $rules['preview'] = 'required|image|max:200|dimensions:max_width=500,max_height=500';
        }

        if ($this->category->sections->count()) {
            $rules['section_id'] = 'required';
        }

        if ($this->category->diseases->count()) {
            $rules['diseases'] = 'required';
        }

        $attribute_rules = [];

        foreach ($this->category->attributes as $attribute) {
            $rule = [
                $attribute->required ? 'required' : 'nullable',
            ];
            if ($attribute->isInteger()) {
                $rule[] = 'integer';
            } elseif ($attribute->isFloat()) {
                $rule[] = 'numeric';
            } else {
                $rule[] = 'string';
                $rule[] = 'max:255';
            }
            if ($attribute->isSelect()) {
                $rule[] = Rule::in($attribute->options);
            }
            $attribute_rules['attributes.' . $attribute->id] = $rule;
        }

        return array_merge($rules, $attribute_rules);
    }
}
