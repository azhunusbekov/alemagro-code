<?php

namespace App\Http\Requests\Catalog;

use Illuminate\Foundation\Http\FormRequest;

class CatalogCategoryTranslationRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
        ];

        return $rules;
    }
}
