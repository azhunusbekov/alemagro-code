<?php

namespace App\Http\Requests\Catalog;

use Illuminate\Foundation\Http\FormRequest;

class CatalogProtectionRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'producer_id' => 'required',
        ];

        if (request()->isMethod('put') || request()->isMethod('patch')) {
            $rules['src'] = 'file|max:500|mimes:pdf';
        } else {
            $rules['src'] = 'required|file|max:500|mimes:pdf';
        }

        return $rules;
    }
}
