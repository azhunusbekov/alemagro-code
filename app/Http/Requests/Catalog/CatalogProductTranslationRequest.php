<?php

namespace App\Http\Requests\Catalog;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CatalogProductTranslationRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'locale' => 'required',
        ];

        return $rules;
    }
}
