<?php

namespace App\Http\Requests\Catalog;

use Illuminate\Foundation\Http\FormRequest;

class CatalogCultureRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'icon' => 'image|max:100|dimensions:max_width=150,max_height=150',
            'image' => 'image|max:300|dimensions:max_width=500,max_height=500',
            'image_header' => 'image|max:500|dimensions:max_width=1920,max_height=1080',
        ];

        return $rules;
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->has('is_favorite')) {
                $this->merge(['is_favorite' => 1]);
            } else {
                $this->merge(['is_favorite' => 0]);
            }
            if ($this->has('on_page')) {
                $this->merge(['on_page' => 1]);
            } else {
                $this->merge(['on_page' => 0]);
            }
        });
    }
}
