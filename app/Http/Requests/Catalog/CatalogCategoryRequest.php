<?php

namespace App\Http\Requests\Catalog;

use Illuminate\Foundation\Http\FormRequest;

class CatalogCategoryRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'icon' => 'image|max:100|dimensions:max_width=150,max_height=150',
            'image_header' => 'image|max:500|dimensions:max_width=1920,max_height=1080',
        ];

        return $rules;
    }
}
