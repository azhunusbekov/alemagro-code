<?php

namespace App\Http\Requests\Catalog;

use Illuminate\Foundation\Http\FormRequest;

class CatalogSubstanceRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'category_id' => 'required',
        ];

        return $rules;
    }
}
