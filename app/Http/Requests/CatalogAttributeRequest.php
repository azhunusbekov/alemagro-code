<?php

namespace App\Http\Requests;

use App\Models\Catalog\CatalogAttribute;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CatalogAttributeRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'type' => ['required', 'string', 'max:255', Rule::in(array_keys(CatalogAttribute::listTypes()))],
            'required' => 'nullable|string|max:255',
            'options' => 'nullable|string',
            'sort' => 'integer',
        ];

        return $rules;
    }
}
