<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PartnerRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
        ];

        if (request()->isMethod('put') || request()->isMethod('patch')) {
            $rules['image'] = 'image|max:500|dimensions:max_width=300,max_height=300';
        } else {
            $rules['image'] = 'required|image|max:500|dimensions:max_width=300,max_height=300';
        }

        return $rules;
    }
}
