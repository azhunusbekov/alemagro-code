<?php

namespace App\Http\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;

class ArticleCategoryRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
        ];

        return $rules;
    }
}
