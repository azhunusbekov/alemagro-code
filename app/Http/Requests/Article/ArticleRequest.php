<?php

namespace App\Http\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'status' => 'required|integer',
            'category_id' => 'required|integer',
            'description' => 'required',
        ];

        if (request()->isMethod('put') || request()->isMethod('patch')) {
            $rules['preview'] = 'image|max:500|dimensions:max_width=500,max_height=500';
            $rules['image_header'] = 'image|max:500|dimensions:max_width=1920,max_height=1080';
        } else {
            $rules['preview'] = 'required|image|max:500|dimensions:max_width=500,max_height=500';
            $rules['image_header'] = 'image|max:500|dimensions:max_width=1920,max_height=1080';
        }

        return $rules;
    }
}
