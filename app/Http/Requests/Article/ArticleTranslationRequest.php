<?php

namespace App\Http\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;

class ArticleTranslationRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'locale' => 'required',
            'status' => 'required|integer',
            'description' => 'required',
        ];

        return $rules;
    }
}
