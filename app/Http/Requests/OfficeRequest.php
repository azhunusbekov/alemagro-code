<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OfficeRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'region' => 'required',
            'alias' => 'required',
            'body' => 'required',
            //'email' => 'required',
        ];

        return $rules;
    }
}
