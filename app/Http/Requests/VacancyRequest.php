<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VacancyRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'status' => 'required|integer',
        ];

        return $rules;
    }
}
