<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'status' => 'required|integer',
        ];

        if (request()->isMethod('put') || request()->isMethod('patch')) {
            $rules['image'] = 'image|max:500|dimensions:width=1366,height=390';
        } else {
            $rules['image'] = 'required|image|max:500|dimensions:width=1366,height=390';
        }

        return $rules;
    }
}
