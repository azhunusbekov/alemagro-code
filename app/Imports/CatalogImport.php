<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithProgressBar;

class CatalogImport implements WithMultipleSheets, WithProgressBar
{
    use Importable;

    public $descriptionKeysSzr = [
        'mekhanizm_deystviya' => 'Механизм действия',
        'preparativnaya_forma' => 'Препаративная форма',
        'khimicheskiy_klass' => 'Химический класс',
        'sposoby_primeneniya' => 'Способы применения',
        'sroki_proyavleniya_effekta_primechaniya' => 'Сроки проявления эффекта (примечания)'
    ];

    public $descriptionKeysUdobreniya = [
        'opisanie' => null,
        'sposob_vneseniya' => 'Способ внесения',
        'deystvie' => 'Действие',
        'sostav' => 'Состав'
    ];

    public function sheets(): array
    {
        return [
            //'Культуры' => new CatalogCulturesSheetImport(),
            'Семена' => new CatalogProductsSheetImport('Семена'),
            'Удобрения' => new CatalogProductsSheetImport('Удобрения', $this->descriptionKeysUdobreniya),
            'СЗР' => new CatalogProductsSheetImport('СЗР', $this->descriptionKeysSzr),
            //'Системы орошения' => new CatalogProductsSheetImport('Системы орошения'),
        ];
    }
}
