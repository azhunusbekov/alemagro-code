<?php

namespace App\Imports;

use App\Models\Catalog\CatalogAttribute;
use App\Models\Catalog\CatalogCategory;
use App\Models\Catalog\CatalogCulture;
use App\Models\Catalog\CatalogDisease;
use App\Models\Catalog\CatalogProducer;
use App\Models\Catalog\CatalogProduct;
use App\Models\Catalog\CatalogSection;
use App\Models\Catalog\CatalogSubstance;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CatalogProductsSheetImport implements ToCollection, WithHeadingRow
{
    public $category = null;
    public $descriptionKeys = [];

    public function __construct($title, $descriptionKeys = null)
    {
        if ($descriptionKeys) {
            $this->descriptionKeys = $descriptionKeys;
        }

        $category = new CatalogCategory();
        $category->disableTranslatable();

        if (!$model = $category->where('title', $title)->first()) {
            $category->fill([
                'title' => $title,
                'status' => CatalogCategory::STATUS_PUBLISHED
            ]);
            $category->save();
            $model = $category;
        }

        $this->category = $model;
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $index => $row) {
            if (is_null($row['code'])) continue;
            if (is_null($row['title'])) continue;
            if (is_null($row['producer'])) continue;

            $insert = [
                'title' => $row['title'],
                'vendor_code' => $row['code'],
                'producer_id' => $this->getProducer($row)->id,
                'category_id' => $this->category->id,
                'section_id' => $this->getSection($row) ? $this->getSection($row)->id : null,
                'description' => $this->getDescription($row),
                'composition' => $this->getComposition($row),
                'available' => ['kz','kg'],
                'status' => CatalogProduct::STATUS_PUBLISHED,
            ];

            if (!CatalogProduct::where('title', $insert['title'])->where('vendor_code', $insert['vendor_code'])->exists()) {
                $model = new CatalogProduct();
                $model->disableTranslatable();
                $model->fill($insert)->save();

                $model->cultures()->sync($this->getCultures($row));
                $model->diseases()->sync($this->getDiseases($row));
                $model->substances()->sync($this->getSubstances($row));

                $attributes = $this->getAttributes($row);

                if ($attributes) {
                    foreach ($attributes as $attributeId => $attributeValue) {
                        $model->catalogAttributes()->updateOrCreate(
                            ['attribute_id' => $attributeId],
                            ['value' => $attributeValue]
                        );
                    }
                }
            }
        }
    }

    public function getProducer($row)
    {
        return CatalogProducer::firstOrCreate(['title' => $row['producer']]);
    }

    public function getSection($row)
    {
        $columnKeys = ['section_0', 'section_1'];

        $parent = null;

        foreach ($columnKeys as $key) {
            if (isset($row[$key])) {
                $title = $row[$key];

                if (is_null($title)) continue;

                $model = new CatalogSection();
                $model->disableTranslatable();

                if (!$culture = $model->where('title', $title)->where('parent_id', optional($parent)->id)->first()) {
                    $model->fill([
                        'title' => $title,
                        'category_id' => $this->category->id,
                        'status' => CatalogSection::STATUS_PUBLISHED,
                        'parent_id' => !is_null($parent) ? $parent->id : null
                    ]);
                    $model->save();
                    $culture = $model;
                }

                $parent = $culture;
            }
        }

        return $parent;
    }

    public function getDescription($row)
    {
        $result = null;

        if (isset($row['description'])) {
            $result .= '<p>' . $row['description'] . '</p>';
        }

        if ($this->descriptionKeys) {
            foreach ($this->descriptionKeys as $key => $title) {
                if (isset($row[$key]) && !is_null($row[$key])) {
                    if ($title) {
                        $result .= '<p><b>' . $title . '</b></p>';
                    }
                    $result .= '<p>' . $row[$key] . '</p>';
                }
            }
        }

        return $result;
    }

    public function getCultures($row)
    {
        $cultureIds = [];

        if (isset($row['cultures']) && !is_null($row['cultures'])) {
            $values = explode(',', $row['cultures']);

            foreach ($values as $value) {
                $cultureIds = array_merge($cultureIds, $this->getCulture($value));
            }

            return $cultureIds;
        }

        return null;
    }

    public function getCulture($title)
    {
        $model = new CatalogCulture();
        $model->disableTranslatable();
        $id = [];

        $title = mb_ucfirst(trim($title));

        $culture = $model->where('title', $title)->first();

        if ($culture) {
            $id[] = $culture->id;

            if ($parent = $culture->parent) {
                $id[] = $parent->id;

                if ($parentParent = $parent->parent) {
                    $id[] = $parentParent->id;
                }
            }
        }

        return $id;
    }

    public function getAttributes($row)
    {
        $result = [];

        for ($i = 0; $i < 20; $i++) {
            if (isset($row['attribute_'.$i])) {
                $attribute = explode(':', $row['attribute_'.$i]);
                if (!isset($attribute[1]) || empty($attribute[1])) continue;

                $attributeKey = trim(trim($attribute[0], '.'));
                $attributeValue = trim($attribute[1]);

                $attribute = $this->category->attributes()->firstOrCreate([
                    'name' => $attributeKey,
                    'locale' => 'ru',
                    'type' => 'string',
                ]);

                $result[$attribute->id] = $attributeValue;
            }
        }

        return $result;
    }

    public function getDiseases($row)
    {
        if (isset($row['diseases']) && !is_null($row['diseases'])) {
            $columnValues = explode(',', $row['diseases']);

            $ids = [];

            foreach ($columnValues as $value) {
                $ids[] = $this->getDisease(trim($value))->id;
            }

            return $ids;
        }

        return null;
    }

    public function getDisease($title)
    {
        $model = new CatalogDisease();
        $model->disableTranslatable();

        $title = mb_ucfirst($title);

        if (!$disease = $model->where('title', mb_ucfirst($title))->first()) {
            $model->fill([
                'title' => $title,
                'category_id' => $this->category->id
            ]);
            $model->save();

            return $model;
        }

        return $disease;
    }

    public function getSubstances($row)
    {
        if (isset($row['substances']) && !is_null($row['substances'])) {
            $columnValues = explode(',', $row['substances']);

            $ids = [];

            foreach ($columnValues as $value) {
                $ids[] = $this->getSubstance(trim($value))->id;
            }

            return $ids;
        }

        return null;
    }

    public function getSubstance($title)
    {
        $model = new CatalogSubstance();
        $model->disableTranslatable();

        $title = mb_ucfirst($title);

        if (!$disease = $model->where('title', $title)->first()) {
            $model->fill([
                'title' => $title,
                'category_id' => $this->category->id
            ]);
            $model->save();

            return $model;
        }

        return $disease;
    }

    public function getComposition($row)
    {
        $result = null;

        if (isset($row['composition'])) {
            $result .= '<p>' . $row['composition'] . '</p>';
        }

        return $result;
    }
}
