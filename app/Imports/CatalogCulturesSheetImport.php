<?php

namespace App\Imports;

use App\Models\Catalog\CatalogCulture;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class CatalogCulturesSheetImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $index => $row) {
            if ($index == 0) continue;

            $parent = null;

            foreach ($row as $column) {
                if (is_null($column)) continue;

                $title = mb_ucfirst($column);

                $model = new CatalogCulture();
                $model->disableTranslatable();

                if (!$culture = $model->where('title', $title)->first()) {
                    $model->fill([
                        'title' => $title,
                        'parent_id' => !is_null($parent) ? $parent->id : null
                    ]);
                    $model->save();
                    $culture = $model;
                }

                $parent = $culture;
            }
        }
    }
}