<?php

namespace App\Providers;

use App\Http\Routers\CatalogProductPath;
use App\Http\Routers\CatalogSectionPath;
use App\Models\Article\Article;
use App\Models\Article\ArticleCategory;
use App\Models\Catalog\CatalogCategory;
use App\Models\Catalog\CatalogCulture;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Route::bind('article_category', function ($value) {
            $category = ArticleCategory::query();

            if (app()->getLocale() != config('app.locale_default')) {
                $category = $category->whereHas('translations', function($query) use ($value) {
                    $query->where('slug', $value);
                    $query->where('locale', app()->getLocale());
                });
            } else {
                $category = $category->where('slug', $value);
            }

            return $category->with('translations')->firstOrFail();
        });

        Route::bind('article_slug', function ($value, $route) {
            $category = $route->article_category;
            $article = Article::query();

            $article = $article->orWhereHas('translations', function($query) use ($value) {
                $query->where('locale', app()->getLocale());
                $query->where('slug', $value);
                $query->where('status', Article::STATUS_PUBLISHED);
            })->orWhere(function($query) use ($value) {
                $query->orWhere('locale', app()->getLocale());
                $query->where('slug', $value);
                $query->where('status', Article::STATUS_PUBLISHED);
            });

            $article = $article->where('category_id', $category->id)->published();

            return $article->with('translations')->firstOrFail();
        });

        Route::bind('catalog_category', function ($value) {
            return (new CatalogCategory)->resolveSlugBinding($value);
        });

        Route::model('section_path', CatalogSectionPath::class);

        Route::bind('culture_path', function($value) {
            $chunks = explode('/', $value);
            $culture = null;

            do {
                $slug = reset($chunks);
                $next = CatalogCulture::query()
                    ->where('parent_id', optional($culture)->id)
                    ->where(function ($query) use ($slug) {
                        $query->whereHas('translations', function($query) use ($slug) {
                            $query->where('locale', app()->getLocale());
                            $query->where('slug', $slug);
                        })->orWhere(function($query) use ($slug) {
                            $query->where('locale', app()->getLocale());
                            $query->where('slug', $slug);
                        });
                    })
                    ->with(['translations'])
                    ->first();

                if ($slug && $next) {
                    if ($culture) {
                        $next->parent = $culture;
                    }
                    $culture = $next;
                    array_shift($chunks);
                }
            } while (!empty($slug) && !empty($next));

            if (!empty($chunks)) {
                abort(404);
            }

            return $culture;
        });

        Route::model('product_path', CatalogProductPath::class);
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapAdminRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware(['web', 'localizationRedirect'])
            ->prefix(LaravelLocalization::setLocale())
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapAdminRoutes()
    {
        Route::middleware('web')
            ->namespace('App\Http\Controllers\Admin')
            ->group(base_path('routes/admin.php'));
    }
}
