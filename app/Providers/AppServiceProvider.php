<?php

namespace App\Providers;
use App\Models\Article\Article;
use App\Models\Article\ArticleCategory;
use App\Models\Catalog\CatalogCategory;
use App\Models\Catalog\CatalogCulture;
use App\Models\Catalog\CatalogProduct;
use App\Models\Catalog\CatalogSection;
use App\Models\Office;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Relation::morphMap([
            'article' => Article::class,
            'article_category' => ArticleCategory::class,
            'catalog_category' => CatalogCategory::class,
            'catalog_section' => CatalogSection::class,
            'catalog_culture' => CatalogCulture::class,
            'catalog_product' => CatalogProduct::class,
        ]);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->share('feedbackOffices', Office::all());
    }
}
