<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogProtectionsCulturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog_protections_cultures', function (Blueprint $table) {
            $table->integer('protection_id')->unsigned();
            $table->foreign('protection_id')->references('id')->on('catalog_protections')->onDelete('cascade');
            $table->integer('culture_id')->unsigned();
            $table->foreign('culture_id')->references('id')->on('catalog_cultures')->onDelete('cascade');
            $table->unique(['protection_id', 'culture_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog_protections_cultures');
    }
}
