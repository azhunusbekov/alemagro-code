<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogProductTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalog_products', function (Blueprint $table) {
            $table->enum('locale', ['ru', 'kk', 'en'])->default('ru')->after('id');
        });

        Schema::create('catalog_product_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('locale', ['ru', 'kk', 'en']);
            $table->unsignedInteger('product_id');
            $table->string('title');
            $table->string('slug');
            $table->text('description')->nullable();
            $table->foreign('product_id')->references('id')->on('catalog_products')->onDelete('cascade');
            $table->unique(['product_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalog_products', function (Blueprint $table) {
            $table->dropColumn('locale');
        });
        Schema::dropIfExists('catalog_product_translations');
    }
}
