<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->enum('locale', ['ru', 'kk', 'en'])->default('ru')->after('id');
        });

        Schema::create('article_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('locale', ['ru', 'kk', 'en']);
            $table->integer('article_id')->unsigned();
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('description')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
            $table->unique(['article_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropColumn('locale');
        });
        Schema::dropIfExists('article_translations');
    }
}
