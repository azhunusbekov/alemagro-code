<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatlaogSubsctancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog_substances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('catalog_categories')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('catalog_substance_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('locale', ['ru', 'kk', 'en']);
            $table->unsignedInteger('substance_id');
            $table->string('title');
            $table->foreign('substance_id')->references('id')->on('catalog_substances')->onDelete('cascade');
            $table->unique(['substance_id', 'locale']);
            $table->timestamps();
        });

        Schema::create('catalog_products_substances', function (Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('catalog_products')->onDelete('cascade');
            $table->integer('substance_id')->unsigned();
            $table->foreign('substance_id')->references('id')->on('catalog_substances')->onDelete('cascade');
            $table->primary(['product_id', 'substance_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog_substances');
        Schema::dropIfExists('catalog_substance_translations');
        Schema::dropIfExists('catalog_products_substances');
    }
}
