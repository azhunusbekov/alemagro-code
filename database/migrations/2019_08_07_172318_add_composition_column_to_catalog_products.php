<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompositionColumnToCatalogProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalog_products', function (Blueprint $table) {
            $table->text('composition')->nullable()->default(null)->after('description');
        });

        Schema::table('catalog_product_translations', function (Blueprint $table) {
            $table->text('composition')->nullable()->default(null)->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalog_products', function (Blueprint $table) {
            $table->dropColumn('composition');
        });

        Schema::table('catalog_product_translations', function (Blueprint $table) {
            $table->dropColumn('composition');
        });
    }
}
