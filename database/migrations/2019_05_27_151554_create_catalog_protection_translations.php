<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogProtectionTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalog_protections', function (Blueprint $table) {
            $table->enum('locale', ['ru', 'kk', 'en'])->default('ru')->after('id');
        });

        Schema::create('catalog_protection_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('locale', ['ru', 'kk', 'en']);
            $table->unsignedInteger('protection_id');
            $table->string('title');
            $table->string('src')->nullable();
            $table->foreign('protection_id')->references('id')->on('catalog_protections')->onDelete('cascade');
            $table->unique(['protection_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalog_protections', function (Blueprint $table) {
            $table->dropColumn('locale');
        });
        Schema::dropIfExists('catalog_protection_translations');
    }
}
