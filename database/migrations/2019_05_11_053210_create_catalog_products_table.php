<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('vendor_code')->unique();
            $table->text('description')->nullable();
            $table->string('preview');
            $table->integer('producer_id')->unsigned();
            $table->foreign('producer_id')->references('id')->on('catalog_producers');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('catalog_categories');
            $table->integer('section_id')->unsigned()->nullable();
            $table->foreign('section_id')->references('id')->on('catalog_sections');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog_products');
    }
}
