<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogProtectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog_protections', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('purpose', ['nutrition', 'protection']);
            $table->string('title');
            $table->string('src')->nullable();
            $table->integer('producer_id')->unsigned();
            $table->foreign('producer_id')->references('id')->on('catalog_producers');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog_protections');
    }
}
