<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_manager', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('locale', ['ru', 'kk', 'en']);
            $table->integer('seoable_id')->unsigned();
            $table->string('seoable_type', 100);
            $table->string('title')->nullable();
            $table->mediumText('description')->nullable();
            $table->mediumText('keywords')->nullable();
            $table->unique(['locale', 'seoable_id', 'seoable_type']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo_manager');
    }
}
