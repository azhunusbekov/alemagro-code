<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogSectionTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalog_sections', function (Blueprint $table) {
            $table->enum('locale', ['ru', 'kk', 'en'])->default('ru')->after('id');
        });

        Schema::create('catalog_section_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('locale', ['ru', 'kk', 'en']);
            $table->unsignedInteger('section_id');
            $table->string('title');
            $table->string('slug');
            $table->mediumText('description')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->foreign('section_id')->references('id')->on('catalog_sections')->onDelete('cascade');
            $table->unique(['section_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalog_sections', function (Blueprint $table) {
            $table->dropColumn('locale');
        });
        Schema::dropIfExists('catalog_section_translations');
    }
}
