<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocaleToCatalogAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalog_attributes', function (Blueprint $table) {
            $table->enum('locale', ['ru', 'kk', 'en'])->default('ru')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalog_attributes', function (Blueprint $table) {
            $table->dropColumn('locale');
        });
    }
}
