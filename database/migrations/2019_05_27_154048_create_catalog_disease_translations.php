<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogDiseaseTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalog_diseases', function (Blueprint $table) {
            $table->enum('locale', ['ru', 'kk', 'en'])->default('ru')->after('id');
        });

        Schema::create('catalog_disease_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('locale', ['ru', 'kk', 'en']);
            $table->unsignedInteger('disease_id');
            $table->string('title');
            $table->foreign('disease_id')->references('id')->on('catalog_diseases')->onDelete('cascade');
            $table->unique(['disease_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalog_diseases', function (Blueprint $table) {
            $table->dropColumn('locale');
        });
        Schema::dropIfExists('catalog_disease_translations');
    }
}
