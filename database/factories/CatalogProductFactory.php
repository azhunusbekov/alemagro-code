<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Catalog\CatalogProduct::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('ru_RU');
    $title = ucfirst(trim($faker->unique()->sentence(3), '.'));
    $category = \App\Models\Catalog\CatalogCategory::inRandomOrder()->first();
    $section = \App\Models\Catalog\CatalogSection::whereNotNull('parent_id')
         ->where('category_id', $category->id)
        ->inRandomOrder()
        ->first();

    return [
        'title' => $title,
        'vendor_code' => $faker->unique()->randomNumber(5),
        'preview' => $faker->imageUrl($width = 500, $height = 500),
        'description' => $faker->text,
        'status' => 1,
        'slug' => \Cviebrock\EloquentSluggable\Services\SlugService::createSlug(\App\Models\Catalog\CatalogProduct::class, 'slug', $title),
        'category_id' => $category->id,
        'producer_id' => function () {
            return \App\Models\Catalog\CatalogProducer::inRandomOrder()->first()->id;
        },
        'section_id' => $category->sections && $section ? $section->id : null,
    ];
});
