<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Catalog\CatalogCategory::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('ru_RU');
    $title = ucfirst($faker->unique()->word);
    return [
        'title' => $title,
        'description' => $faker->text,
        'status' => 1,
        'slug' => \Cviebrock\EloquentSluggable\Services\SlugService::createSlug(\App\Models\Catalog\CatalogCategory::class, 'slug', $title),
    ];
});
