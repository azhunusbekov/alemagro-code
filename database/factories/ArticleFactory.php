<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Article\Article::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('ru_RU');
    $title = ucfirst($faker->unique()->sentence);
    return [
        'title' => $title,
        'description' => $faker->realText(1500, 5),
        'status' => 1,
        'preview' => '/uploads/articles/1/VMTOwiQ7mHpSMLHIImEwGSzQpk7L2jaIy3mROu3x.jpeg',
        'slug' => \Cviebrock\EloquentSluggable\Services\SlugService::createSlug(\App\Models\Article\Article::class, 'slug', $title),
        'category_id' => function () {
            return \App\Models\Article\ArticleCategory::inRandomOrder()->first()->id;
        },
    ];
});
