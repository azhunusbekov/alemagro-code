<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Catalog\CatalogProtection::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('ru_RU');
    return [
        'title' => ucfirst(trim($faker->unique()->sentence(3), '.')),
        'purpose' => Arr::random(['nutrition', 'protection']),
        'src' => '/uploads/catalog/protections/default.pdf',
        'status' => 1,
        'producer_id' => function () {
            return \App\Models\Catalog\CatalogProducer::inRandomOrder()->first()->id;
        },
    ];
});
