<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Catalog\CatalogCulture::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('ru_RU');
    return [
        'title' => ucfirst($faker->unique()->word),
        'description' => $faker->text,
    ];
});
