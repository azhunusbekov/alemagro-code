<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Catalog\CatalogSection::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('ru_RU');
    $title = ucfirst($faker->unique()->word);
    return [
        'title' => $title,
        'description' => $faker->text,
        'status' => 1,
        'slug' => \Cviebrock\EloquentSluggable\Services\SlugService::createSlug(\App\Models\Catalog\CatalogSection::class, 'slug', $title),
        'category_id' => function () {
            return \App\Models\Catalog\CatalogCategory::inRandomOrder()->first()->id;
        },
    ];
});
