<?php

use Illuminate\Database\Seeder;

class CatalogCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Catalog\CatalogCategory::class, 4)->create();
    }
}
