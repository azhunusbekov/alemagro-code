<?php

use Illuminate\Database\Seeder;

class CatalogProducersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Catalog\CatalogProducer::class, 10)->create();
    }
}
