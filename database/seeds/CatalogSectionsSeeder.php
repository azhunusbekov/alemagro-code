<?php

use Illuminate\Database\Seeder;

class CatalogSectionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Catalog\CatalogSection::class, 5)
            ->create()
            ->each(function($section) {
                $section->children()->saveMany(factory(\App\Models\Catalog\CatalogSection::class, 5)->make([
                    'category_id' => $section->category_id
                ]));
            });
    }
}
