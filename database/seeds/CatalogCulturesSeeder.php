<?php

use Illuminate\Database\Seeder;

class CatalogCulturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Catalog\CatalogCulture::class, 5)
            ->create()
            ->each(function($section) {
                $section->children()->saveMany(factory(\App\Models\Catalog\CatalogCulture::class, 5)->make());
            });
    }
}
