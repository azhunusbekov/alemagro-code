<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
//            UserAdminSeeder::class,
            CatalogProducersSeeder::class,
            CatalogCategoriesSeeder::class,
            CatalogSectionsSeeder::class,
            CatalogCulturesSeeder::class,
            CatalogProductsSeeder::class,
            CatalogProtectionsSeeder::class,
            ArticlesSeeder::class,
        ]);
    }
}
