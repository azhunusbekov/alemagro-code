<?php

use Illuminate\Database\Seeder;

class CatalogProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Catalog\CatalogProduct::class, 100)
            ->create()
            ->each(function($product) {
                $product->cultures()->sync(\App\Models\Catalog\CatalogCulture::whereNotNull('parent_id')->get()->random(5));
            })
        ;
    }
}
