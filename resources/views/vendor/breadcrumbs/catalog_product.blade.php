@if (count($breadcrumbs))
    <ul class="breadcrumbs breadcrumbs--content product-page__breadcrumbs">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <li class="breadcrumbs__item">
                    <a href="{{ $breadcrumb->url }}" class="breadcrumbs__name">{!! $breadcrumb->title !!}</a>
                </li>
            @else
                <li class="breadcrumbs__item">
                    <span class="breadcrumbs__name">{!! $breadcrumb->title !!}</span>
                </li>
            @endif
        @endforeach
    </ul>
@endif