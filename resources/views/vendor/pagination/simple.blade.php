@if ($paginator->hasPages())
    <ul class="pagination__list">
        {{-- Previous Page Link --}}
        @if (!$paginator->onFirstPage())
            <li class="pagination__item"><a class="pagination__icon pagination__icon--arrow-prev" href="{{ str_replace('?page=1','', $paginator->previousPageUrl()) }}"></a></li>
        @endif
        {{--        @if($paginator->currentPage() > 3)--}}
        {{--            <li class="pagination__item"><a href="{{ $paginator->url(1) }}" class="pagination__number">1</a></li>--}}
        {{--        @endif--}}
        {{--        @if($paginator->currentPage() > 4)--}}
        {{--            <li class="pagination__item"><span class="pagination__number">...</span></li>--}}
        {{--        @endif--}}
        @foreach(range(1, $paginator->lastPage()) as $i)
            @if($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2)
                @if ($i == $paginator->currentPage())
                    <li class="pagination__item pagination__item--active"><span class="pagination__number">{{ $i }}</span></li>
                @else
                    <li class="pagination__item"><a href="{{ $paginator->url($i) }}" class="pagination__number">{{ $i }}</a></li>
                @endif
            @endif
        @endforeach
        {{--        @if($paginator->currentPage() < $paginator->lastPage() - 3)--}}
        {{--            <li class="pagination__item"><span class="pagination__number">...</span></li>--}}
        {{--        @endif--}}
        {{--        @if($paginator->currentPage() < $paginator->lastPage() - 2)--}}
        {{--            <li class="pagination__item hidden-xs"><a href="{{ $paginator->url($paginator->lastPage()) }}" class="pagination__number">{{ $paginator->lastPage() }}</a></li>--}}
        {{--        @endif--}}
        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="pagination__item"><a class="pagination__icon pagination__icon--arrow-next" href="{{ $paginator->nextPageUrl() }}"></a></li>
        @endif
    </ul>
@endif
