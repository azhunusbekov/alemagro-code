@if ($paginator->hasPages())
    <ul class="pagination__list">
        @if (!$paginator->onFirstPage())
            <li class="pagination__item"><a class="pagination__icon pagination__icon--arrow-prev" href="{{ str_replace('?page=1','', $paginator->previousPageUrl()) }}"></a></li>
        @endif
        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="pagination__item"><span class="pagination__number">{{ $element }}</span></li>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="pagination__item pagination__item--active"><span class="pagination__number">{{ $page }}</span></li>
                    @else
                        <li class="pagination__item"><a class="pagination__number" href="{{ $loop->parent->first ? str_replace('?page=1', '', $url) : $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach
        @if ($paginator->hasMorePages())
            <li class="pagination__item"><a class="pagination__icon pagination__icon--arrow-next" href="{{ $paginator->nextPageUrl() }}"></a></li>
        @endif
    </ul>
@endif
