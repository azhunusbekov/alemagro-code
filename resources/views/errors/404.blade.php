@extends('layouts.app')

@section('content')
    <div class="page-not-found">
        <div class="container">
            <h2 class="page-not-found__title">404</h2>
            <p class="page-not-found__desc">{{ __('general.404_description') }}</p>
            <a class="page-not-found__return" href="{{ lang_route('home') }}">{{ __('button.go_home') }}</a>
        </div>
    </div>
@endsection
