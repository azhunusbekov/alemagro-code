<!DOCTYPE html>
<html class="page" lang="{{ app()->getLocale() }}">
@include('layouts.partial._head')
<body>
    @include('layouts.partial._header')
    <main class="page__content" role="main">
        @yield('content')
    </main>
    @include('layouts.partial._footer')
    @include('layouts.partial._modals')
    @include('layouts.partial._scripts')
</body>
</html>