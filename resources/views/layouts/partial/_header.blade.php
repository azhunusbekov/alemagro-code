<div class="page-header-wrap">
    <header class="page-header" role="banner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-5 page-header__col">
                    <a class="page-header__logo-wrap" href="{{ lang_route('home') }}"><img class="page-header__logo" src="{{ asset('img/logo.png') }}" alt="AlemAgro"/></a>
                    {{--<div class="langs page-header__langs">
                        @foreach(supported_locales() as $langKey => $lang)
                            <a class="langs__item {{ app()->getLocale() == $langKey ? 'langs__item--active' : '' }}" href="{{ lang_route('home', null, $langKey) }}">{{  $lang['code'] }}</a>
                        @endforeach
                    </div>--}}
                </div>
                <div class="col-xs-12 col-md-7 page-header__col">
                    <div class="page-header__info">
                        <a class="page-header__info-item page-header__info-item--email" href="mailto:{{ __('contact.email') }}">{{ __('contact.email') }}</a>
                        <a class="page-header__info-item page-header__info-item--phone" href="tel:{{ __('contact.phone') }}">{{ __('contact.phone') }}</a>
                        <a class="page-header__info-icon page-header__info-icon--facebook" href="#" target="_blank"></a>
                        <a class="page-header__info-icon page-header__info-icon--instagram" href="#" target="_blank"></a>
                    </div>
                    <div class="page-header__btn-wrap">
                        <a class="btn btn--blue page-header__btn" href="#" data-toggle="modal" data-target="#modal-feedback">{{ __('button.feedback') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <nav class="page-nav" role="navigation">
        <div class="container">
            <div class="page-nav__items">
                <div class="page-nav__item-wrap">
                    <a class="page-nav__item {{ Request::is('*about') ? 'page-nav__item--active' : '' }}" href="{{ lang_route('about') }}">
                        {{ __('menu.about') }}
                    </a>
                </div>
                <div class="page-nav__item-wrap">
                    <a class="page-nav__item {{ Request::is('*catalog*') ? 'page-nav__item--active' : '' }}" href="{{ lang_route('catalog.index') }}" data-dropdown="true">
                        {{ __('menu.products') }}
                    </a>
                    <div class="page-nav__dropdown">
                        <div class="page-nav__items page-nav__items--dropdown">
                            <a class="page-nav__item page-nav__item--dropdown" href="{{ lang_route('catalog.main', 'kz') }}">{{ __('menu.products_kz') }}</a>
                            <a class="page-nav__item page-nav__item--dropdown" href="{{ lang_route('catalog.main', 'kg') }}">{{ __('menu.products_kg') }}</a>
                        </div>
                    </div>
                </div>
                <div class="page-nav__item-wrap">
                    <a class="page-nav__item {{ Request::is('*culture*') ? 'page-nav__item--active' : '' }}" href="{{ lang_route('culture.index') }}">
                        {{ __('menu.cultures') }}
                    </a>
                </div>
                <div class="page-nav__item-wrap">
                    <a class="page-nav__item {{ Request::is('*protection*') ? 'page-nav__item--active' : '' }}" href="{{ lang_route('protection.index') }}">
                        {{ __('menu.protections') }}
                    </a>
                </div>
                <div class="page-nav__item-wrap">
                    <a class="page-nav__item {{ Request::is('*article*') && !Request::is('*article/akcii*') ? 'page-nav__item--active' : '' }}" href="{{ lang_route('article.index') }}">
                        {{ __('menu.actual') }}
                    </a>
                </div>
                <div class="page-nav__item-wrap">
                    <a class="page-nav__item {{ Request::is('*career') || Request::is('*internship') ? 'page-nav__item--active' : '' }}" href="{{ lang_route('career') }}">
                        {{ __('menu.career') }}
                    </a>
                </div>
                <div class="page-nav__item-wrap">
                    <a class="page-nav__item {{ Request::is('*article/akcii*') ? 'page-nav__item--active' : '' }}" href="{{ lang_route('article.category', ['article_category' => 'akcii']) }}">
                        {{ __('menu.stock') }}
                    </a>
                </div>
                <div class="page-nav__item-wrap">
                    <a class="page-nav__item {{ Request::is('*contact') || Request::is('*consult')? 'page-nav__item--active' : '' }}" href="{{ lang_route('contact') }}">
                        {{ __('menu.contact') }}
                    </a>
                </div>
            </div>
            <div class="page-search page-nav__search js-page-search">
                <a class="js-page-search__icon page-search__icon" href="#"></a>
                <form class="page-search__form" action="{{ lang_route('search') }}" method="GET">
                    <input class="page-search__input" type="text" name="search"/>
                    <button class="page-search__submit" type="submit"></button>
                </form>
            </div>
        </div>
    </nav>
</div>
