<script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('/vendor/scroll/mousewheel.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('/vendor/slick/slick.min.js') }}"></script>
<script src="{{ asset('/vendor/scroll/jscroll.js') }}"></script>
<script src="{{ asset('/vendor/fancybox/jquery.fancybox.min.js') }}"></script>
<script src="{{ asset('/vendor/pdfobject.min.js') }}"></script>
<script src="{{ asset('/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('/vendor/inputmask/inputmask.js') }}"></script>
<script src="{{ asset('/js/all.min.js') }}"></script>

<script type="text/javascript">
    $(function () {
        // inputmask for phone
        $('[name="phone"]').inputmask("+7-999-9999999");

        // ajax-form submit
        $('body').on('submit', 'form.ajax-form', function (e) {
            e.preventDefault();

            let form = $(this);
            let formData = new FormData(this);

            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                success: function (response) {
                    if (response.success) {
                        //
                    }
                    if (response.redirect) {
                        window.location = response.redirect;
                    }
                    form.find('#alert-error').hide();
                    form.find('#alert-success').show();
                    form.trigger('reset');
                },
                error: function (response) {
                    if (response.status === 422 || response.status === 423) {
                        form.find('#alert-error').show();
                        for (let inputName in response.responseJSON.errors) {
                            form.find(`[name=${inputName}]`).parent('.form-group').addClass('has-error');
                        }
                    } else if (response.status === 200) {
                        form.find('#alert-success').show();
                        form.find('#alert-error').hide();
                    } else {
                        form.find('#alert-error').show();
                    }
                },
                beforeSend: function () {
                    form.find('.form-control').parent('.form-group').removeClass('has-error');
                    form.addClass('is-loading');
                },
                complete: function () {
                    form.removeClass('is-loading');
                }
            })
        });

        // reset form
        $('form').on('click', 'button[type="reset"]', function (e) {
            e.preventDefault();

            let myForm = $(this).closest('form');
            myForm.find('input[type="checkbox"]').prop('checked', false);
            myForm.find('input[type="radio"]').prop('checked', false);

            myForm.submit();
        });

        // filter cultures
        $("#cultures-search").on("keyup", function() {
            let value = $(this).val().toLowerCase();

            $("#cultures-filter-group .filters__label").filter(function() {
                let inputWrap = $(this).parent();

                inputWrap.toggle($(this).text().toLowerCase().indexOf(value) > -1);

                $("#cultures-filter-group > .jspContainer > .jspPane > .filters__input-wrap").each(function () {
                    let inputGroup = $(this);

                    inputGroup.find('.filters__input-wrap').each(function () {
                        if($(this).css('display') !== 'none') {
                            inputGroup.show();
                        }

                        let childGroup = $(this);

                        childGroup.find('.filters__input-wrap').each(function () {
                            if($(this).css('display') !== 'none') {
                                inputGroup.show();
                                childGroup.show();
                            }
                        });
                    });
                });
            });
        });

        // filter diseases
        $("#diseases-search").on("keyup", function() {
            let value = $(this).val().toLowerCase();

            $("#diseases-filter-group .filters__label").filter(function() {
                let inputWrap = $(this).parent();

                inputWrap.toggle($(this).text().toLowerCase().indexOf(value) > -1);

                $("#diseases-filter-group > .jspContainer > .jspPane > .filters__input-wrap").each(function () {
                    let inputGroup = $(this);

                    inputGroup.find('.filters__input-wrap').each(function () {
                        if($(this).css('display') !== 'none') {
                            inputGroup.show();
                        }
                    });
                });
            });
        });

        // filter substances
        $("#substances-search").on("keyup", function() {
            let value = $(this).val().toLowerCase();

            $("#substances-filter-group .filters__label").filter(function() {
                let inputWrap = $(this).parent();

                inputWrap.toggle($(this).text().toLowerCase().indexOf(value) > -1);

                $("#substances-filter-group > .jspContainer > .jspPane > .filters__input-wrap").each(function () {
                    let inputGroup = $(this);

                    inputGroup.find('.filters__input-wrap').each(function () {
                        if($(this).css('display') !== 'none') {
                            inputGroup.show();
                        }
                    });
                });
            });
        });

        // filter sections
        $("#sections-search").on("keyup", function() {
            let value = $(this).val().toLowerCase();

            $("#sections-filter-group .filters__label").filter(function() {
                let inputWrap = $(this).parent();

                inputWrap.toggle($(this).text().toLowerCase().indexOf(value) > -1);

                $("#sections-filter-group > .jspContainer > .jspPane > .filters__input-wrap").each(function () {
                    let inputGroup = $(this);

                    inputGroup.find('.filters__input-wrap').each(function () {
                        if($(this).css('display') !== 'none') {
                            inputGroup.show();
                        }
                    });
                });
            });
        });
    });
</script>

@stack('scripts')
