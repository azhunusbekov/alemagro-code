<div class="modal fade" id="modal-safe-program" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal">&times;</button>
            <div class="modal-pdf" id="js-modal-pdf"></div>
            <div class="modal-text">
                <h3 class="modal-name"></h3>
                <a class="modal-link" href="#" target="_blank">{{ __('button.download_doc') }}</a>
            </div>
        </div>
    </div>
</div>

<div class="modal-feedback modal fade" id="modal-feedback" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal">&times;</button>
            <div class="modal__pic" style="background-image:url('/img/modal.png')"></div>
            <div class="modal__content">
                <h3 class="modal__title">{{ __('general.feedback.title') }}</h3>
                <form class="feedback-request__form ajax-form" action="{{ lang_route('feedback.main') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div id="alert-error" class="alert alert-danger" style="display: none">
                        {{ __('general.feedback.error') }}
                    </div>
                    <div id="alert-success" class="alert alert-success" style="display: none">
                        {{ __('general.feedback.success') }}
                    </div>
                    <input type="hidden" name="title" value="{{ __('general.feedback.title') }}">
                    <div class="modal__form-group">
                        <select name="region" class="modal__select selectpicker">
                            <option disabled selected>{{ __('general.feedback.input_region') }}</option>
                            @foreach($feedbackOffices as $office)
                                <option value="{{ $office->alias }}">{{ $office->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal__form-group">
                        <input class="modal__input" type="text" name="name" placeholder="{{ __('general.feedback.input_name') }}"/>
                    </div>
                    <div class="modal__form-group">
                        <input class="modal__input" type="phone" name="phone" placeholder="{{ __('general.feedback.input_phone') }}"/>
                    </div>
                    <div class="modal__btn-wrap">
                        <button class="btn btn--primary btn--blue" type="submit">{{ __('button.submit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>