<footer class="page-footer" role="contentinfo">
    <div class="container">
        <div class="page-footer__row">
            <div class="page-footer__col">
                <a class="page-footer__logo-wrap" href="/">
                    <img class="page-footer__logo" src="/img/logo-footer.png"/>
                </a>
                <div class="page-footer__copyright">2019, AlemAgro</div>
                <a class="js-page-scroll-top page-footer__top" href="#">{{ __('button.go_up') }}</a>
            </div>
            <div class="page-footer__col">
                <nav class="page-footer__nav-items" role="navigation">
                    <div class="row">
                        <div class="col-xs-6"><a class="page-footer__nav-item" href="{{ lang_route('about') }}">{{ __('menu.about') }}</a></div>
                        <div class="col-xs-6"><a class="page-footer__nav-item" href="{{ lang_route('article.index') }}">{{ __('menu.actual') }}</a></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6"><a class="page-footer__nav-item" href="{{ lang_route('about') }}">{{ __('menu.products') }}</a></div>
                        <div class="col-xs-6"><a class="page-footer__nav-item" href="{{ lang_route('career') }}">{{ __('menu.career') }}</a></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6"><a class="page-footer__nav-item" href="{{ lang_route('culture.index') }}">{{ __('menu.cultures') }}</a></div>
                        <div class="col-xs-6"><a class="page-footer__nav-item" href="{{ lang_route('contact') }}">{{ __('menu.contact') }}</a></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6"><a class="page-footer__nav-item" href="{{ lang_route('protection.index') }}">{{ __('menu.protections') }}</a></div>
                    </div>
                </nav>
            </div>
            <div class="page-footer__col">
                <div class="page-footer__info">
                    <a class="page-footer__info-item page-footer__info-item--phone" href="tel:{{ __('contact.phone') }}">{{ __('contact.phone') }}</a>
                    <a class="page-footer__info-item page-footer__info-item--email" href="mailto:{{ __('contact.email') }}">{{ __('contact.email') }}</a>
                </div>
                <div class="page-footer__btn-wrap"><a class="btn btn--blue page-footer__btn" href="#" data-toggle="modal" data-target="#modal-feedback">{{ __('button.feedback') }}</a></div>
            </div>
            <div class="page-footer__col">
                <div class="page-footer__info">
                    <p class="page-footer__info-item page-footer__info-item--position">{!! __('contact.address') !!}</p>
                </div>
            </div>
        </div>
    </div>
</footer>