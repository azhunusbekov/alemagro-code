@php echo '<?xml version="1.0" encoding="UTF-8"?>'; @endphp
<urlset xmlns="http://www.google.com/schemas/sitemap/0.9">
    @foreach ($items as $item)
        <url>
            <loc>{{ $item['link'] }}</loc>
            <changefreq>monthly</changefreq>
            <priority>1</priority>
        </url>
    @endforeach
</urlset>