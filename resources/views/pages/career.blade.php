@extends('layouts.app')

@section('content')
    <section class="page-preview" style="background-image:url('/img/about/header_bg.jpg')">
        <div class="container page-preview__container">
            <h1 class="page-preview__title">{{ __('career.page_title') }}</h1>
        </div>
    </section>
    <div class="page-tabs">
        <div class="page-tabs__items">
            <a class="page-tabs__item page-tabs__item--active" href="{{ lang_route('career') }}">{{ __('career.vacancies') }}</a>
            <a class="page-tabs__item" href="{{ lang_route('internship') }}">{{ __('career.internship') }}</a>
        </div>
    </div>
    <section class="career-vacancy">
        <div class="container">
            <h3 class="career-vacancy__title">{{ __('career.title') }}</h3>
            <div class="career-vacancy__text">
                {!! __('career.text') !!}
            </div>
        </div>
    </section>
    <section class="open-vacancies">
        <div class="container">
            <h4 class="section-title open-vacancies__section-title section-title--open-vacancies">{{ __('career.open_vacancies') }}</h4>
            <div class="open-vacancies__items">
                @foreach($vacancies as $vacancy)
                    <div class="js-open-vacancies__item open-vacancies__item">
                        <a class="js-open-vacancies__header open-vacancies__header" href="#">{{ $vacancy->title }}</a>
                        <div class="open-vacancies__body">
                            <div class="open-vacancies__info">
                                <div class="open-vacancies__info-item">
                                    {!! $vacancy->description !!}
                                </div>
                                <div class="open-vacancies__btn-wrap">
                                    <a class="btn open-vacancies__btn btn--green" href="#" data-toggle="modal" data-target="#modal-vacancy-{{ $vacancy->id }}">{{ __('button.to_respond') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="vacancies-directions">
        <div class="container">
            <h4 class="section-title vacancies-directions__section-title section-title--vacancies-directions">{{ __('career.direction.title') }}</h4>
            <div class="vacancies-directions__items">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="vacancies-directions__item">
                            <div class="vacancies-directions__icon" style="background-image:url('/img/vacancies-direction-item-1.png')"></div>
                            <h5 class="vacancies-directions__name">{{ __('career.direction.tech_title') }}</h5>
                            <p class="vacancies-directions__desc">{{ __('career.direction.tech_text') }}</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="vacancies-directions__item">
                            <div class="vacancies-directions__icon" style="background-image:url('/img/vacancies-direction-item-2.png')"></div>
                            <h5 class="vacancies-directions__name">{{ __('career.direction.office_title') }}</h5>
                            <p class="vacancies-directions__desc">{{ __('career.direction.office_text') }}</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="vacancies-directions__item">
                            <div class="vacancies-directions__icon" style="background-image:url('/img/vacancies-direction-item-3.png')"></div>
                            <h5 class="vacancies-directions__name">{{ __('career.direction.department_title') }}</h5>
                            <p class="vacancies-directions__desc">{{ __('career.direction.department_text') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @foreach($vacancies as $vacancy)
        <div class="modal-career modal fade" id="modal-vacancy-{{ $vacancy->id }}" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal">&times;</button>
                    <div class="modal__pic" style="background-image:url('/img/modal.png')"></div>
                    <div class="modal__content">
                        <h3 class="modal__title">{{ __('career.feedback.title') }}</h3>
                        <form class="feedback-request__form ajax-form" action="{{ lang_route('feedback.vacancy') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div id="alert-error" class="alert alert-danger" style="display: none">
                                {{ __('career.feedback.error') }}
                            </div>
                            <div id="alert-success" class="alert alert-success" style="display: none">
                                {{ __('career.feedback.success') }}
                            </div>
                            <input type="hidden" name="title" value="{{ $vacancy->title }}">
                            <input type="hidden" name="vacancy_id" value="{{ $vacancy->id }}">
                            <div class="modal__form-group">
                                <select name="region" class="modal__select selectpicker">
                                    <option disabled selected>{{ __('general.feedback.input_region') }}</option>
                                    @foreach($feedbackOffices as $office)
                                        <option value="{{ $office->alias }}">{{ $office->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="modal__form-group">
                                <input class="modal__input" type="text" name="name" placeholder="{{ __('career.feedback.input_name') }}"/>
                            </div>
                            <div class="modal__form-group">
                                <input class="modal__input" type="tel" name="phone" placeholder="{{ __('career.feedback.input_phone') }}"/>
                            </div>
                            <div class="modal__form-group">
                                <input class="modal__input" type="tel" name="email" placeholder="{{ __('career.feedback.input_email') }}"/>
                            </div>
                            <div class="modal__btn-wrap">
                                <button class="btn btn--primary btn--blue" type="submit">{{ __('button.submit') }}</button>
                                <div class="modal__resume">
                                    <input class="js-modal__file modal__file" id="modal__file_{{ $vacancy->id }}" type="file" name="resume"/>
                                    <label class="js-modal__file-label modal__file-label" for="modal__file_{{ $vacancy->id }}">{{ __('career.form.input_attach') }}</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
