@extends('layouts.app')

@section('content')
    <div class="page-search page-search--main">
        <div class="container">
            <h2 class="page-search__title">
                @if ($searchResults->isEmpty())
                   {!! __('search.no_result', ['term' => $searchterm]) !!}
                @else
                   {!! __('search.title', ['term' => $searchterm]) !!}
                @endif
            </h2>
            <div class="page-search__items">
                @if ($searchResults->isNotEmpty())
                    @foreach($searchResults->groupByType() as $type => $modelSearchResults)
                        @foreach($modelSearchResults as $searchResult)
                            <div class="page-search__item">
                                <a class="page-search__item-link" href="{{ $searchResult->url }}">{{ $searchResult->title }}</a>
                            </div>
                        @endforeach
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection
