@extends('layouts.app')

@section('content')
    <section class="page-preview" style="background-image:url('/img/about/header_bg.jpg')">
        <div class="container page-preview__container">
            <h1 class="page-preview__title">{{ __('about.page_title') }}</h1>
        </div>
    </section>
    <section class="page-substrate">
        <h4 class="page-substrate__title page-substrate__title--history-company">{{ __('about.history.title') }}</h4>
    </section>
    <section class="page-about">
        <div class="page-about__pic animated" style="background-image:url('/img/about/about-right.jpg')"></div>
        <div class="container page-about__container">
            <div class="page-about__content animated">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        {!! __('about.history.text') !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="our-directions our-directions--about">
        <div class="container">
            <h4 class="section-title our-directions__section-title section-title--direction">{{ __('about.company_profile.title') }}</h4>
            <div class="our-directions__items">
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <div class="our-directions__item animated animate-delay-1">
                            <div class="our-directions__icon" style="background-image:url('/img/about/icon_seeds.png')"></div>
                            <div class="our-directions__name">{{ __('about.company_profile.seeds') }}</div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="our-directions__item animated animate-delay-2">
                            <div class="our-directions__icon" style="background-image:url('/img/about/icon_grow.png'); background-position: 50% 66%;"></div>
                            <div class="our-directions__name">{{ __('about.company_profile.grow') }}</div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="our-directions__item animated animate-delay-3">
                            <div class="our-directions__icon" style="background-image:url('/img/about/icon_lab.png')"></div>
                            <div class="our-directions__name">{{ __('about.company_profile.lab') }}</div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="our-directions__item animated animate-delay-4">
                            <div class="our-directions__icon" style="background-image:url('/img/about/icon_secure.png')"></div>
                            <div class="our-directions__name">{{ __('about.company_profile.secure') }}</div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="our-directions__item animated animate-delay-6">
                            <div class="our-directions__icon" style="background-image:url('/img/about/icon_prof_agro.png')"></div>
                            <div class="our-directions__name">{{ __('about.company_profile.prof_agro') }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about-mission">
        <div class="container">
            <h4 class="section-title about-mission__section-title section-title--mission">{{ __('about.mission.title') }}</h4>
            <div class="content-default about-mission__content-default">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 animated animate-left-in">
                        {!! __('about.mission.text_left') !!}
                    </div>
                    <div class="col-xs-12 col-sm-6 animated animate-right-in">
                        {!! __('about.mission.text_right') !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="our-values">
        <div class="container">
            <h4 class="section-title our-values__section-title section-title--our-values">Наши ценности</h4>
            <div class="our-values__items">
                <div class="row">
                    <div class="col-xs-6 col-md-3">
                        <div class="our-values__item animated animate-delay-1">
                            <div class="our-values__icon" style="background-image: url('/img/about/our-values-1.png')"></div>
                            <p class="our-values__text">{!! __('about.values.item_1') !!}</p>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div class="our-values__item animated animate-delay-2">
                            <div class="our-values__icon" style="background-image: url('/img/about/our-values-2.png')"></div>
                            <p class="our-values__text">{!! __('about.values.item_2') !!}</p>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div class="our-values__item animated animate-delay-3">
                            <div class="our-values__icon" style="background-image: url('/img/about/our-values-3.png')"></div>
                            <p class="our-values__text">{!! __('about.values.item_3') !!}</p>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div class="our-values__item animated animate-delay-4">
                            <div class="our-values__icon" style="background-image: url('/img/about/our-values-4.png')"></div>
                            <p class="our-values__text">{!! __('about.values.item_4') !!}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-md-3">
                        <div class="our-values__item animated animate-delay-5">
                            <div class="our-values__icon" style="background-image: url('/img/about/our-values-5.png')"></div>
                            <p class="our-values__text">{!! __('about.values.item_5') !!}</p>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div class="our-values__item animated animate-delay-6">
                            <div class="our-values__icon" style="background-image: url('/img/about/our-values-6.png')"></div>
                            <p class="our-values__text">{!! __('about.values.item_6') !!}</p>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div class="our-values__item animated animate-delay-7">
                            <div class="our-values__icon" style="background-image: url('/img/about/our-values-7.png')"></div>
                            <p class="our-values__text">{!! __('about.values.item_7') !!}</p>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div class="our-values__item animated animate-delay-8">
                            <div class="our-values__icon" style="background-image: url('/img/about/our-values-8.png')"></div>
                            <p class="our-values__text">{!! __('about.values.item_8') !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="our-strategy">
        <div class="container">
            <h4 class="section-title our-strategy__section-title section-title--strategy">{{ __('about.strategy.title') }}</h4>
            <div class="content-default our-strategy__content-default">
                <div class="row">
                    <div class="col-xs-12 col-sm-6  animated animate-left-in">
                        <p>{!! __('about.strategy.text_left') !!}</p>
                    </div>
                    <div class="col-xs-12 col-sm-6  animated animate-right-in">
                        <p>{!! __('about.strategy.text_right') !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="for-investors">
        <div class="container">
            <h4 class="section-title for-investors__section-title section-title--for-investors">{{ __('about.investors.title') }}</h4>
            <div class="content-default for-investors__content-default">
                <p><b>{{ __('about.investors.audit') }}</b></p>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 animated animate-left-in">
                        <p>{!! __('about.investors.text_left') !!}</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 animated animate-right-in">
                        <p>{!! __('about.investors.text_right') !!}</p>
                    </div>
                </div>
            </div>
            <div class="for-investors__pdf-wrap">
                <a class="for-investors__pdf" href="#">{{ __('about.download_financial_reporting') }}</a>
            </div>
        </div>
    </section>
    <section class="partners">
        <div class="container">
            <h4 class="section-title section-title--partners">{{ __('about.partners_title') }}</h4>
            <div class="partners__items-wrap">
                <div class="partners__items" data-slider="mainPartners">
                    @foreach($partners as $partner)
                        <div class="partners__col">
                            @if($partner->link)
                                <a class="partners__item" href="{{ $partner->link  }}" target="_blank" style="background-image:url('{{ asset($partner->image) }}')"></a>
                            @else
                                <span class="partners__item" style="background-image:url('{{ asset($partner->image) }}')"></span>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <section class="career-preview" style="background-image:url('/img/career-main.jpg')">
        <div class="container">
            <h3 class="career-preview__title">{{ __('career.title') }}</h3>
            <div class="career-preview__btn-wrap">
                <a class="btn btn--green" href="{{ lang_route('career') }}">{{ __('button.more') }}</a>
            </div>
        </div>
    </section>
@endsection
