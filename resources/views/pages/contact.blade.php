@extends('layouts.app')

@section('content')
    <section class="page-preview page-preview--contacts" style="background-image:url('/img/about/header_bg.jpg')">
        <div class="container page-preview__container">
            {{ Breadcrumbs::view('vendor.breadcrumbs.header', 'contact') }}
            <h1 class="page-preview__title">{{ __('contact.offices') }}</h1>
        </div>
    </section>
    <div class="page-tabs">
        <div class="page-tabs__items">
            <a class="page-tabs__item page-tabs__item--active" href="{{ lang_route('contact') }}">{{ __('contact.offices') }}</a>
            <a class="page-tabs__item" href="{{ lang_route('consult') }}">{{ __('contact.consults') }}</a>
        </div>
    </div>
    <div class="js-page-contacts-map page-contacts-map">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 page-contacts-map__col">
                    <h4 class="section-title section-title--map">{{ __('contact.offices_map') }}</h4>
                    <div class="page-contacts-map__pic-wrap">
                        <img class="js-page-contacts-map__pic page-contacts-map__pic" src="/img/map.png"/>
                        @foreach($offices->unique('region') as $office)
                            <div class="js-page-contacts-map__region page-contacts-map__region page-contacts-map__region--{{ $office->region }}" data-region="{{ $office->region }}"></div>
                        @endforeach
                        @foreach($offices as $office)
                            <div class="js-page-contacts-map__city page-contacts-map__city page-contacts-map__city--{{ $office->alias }}" data-region="{{ $office->region }}">{{ $office->title }}</div>
                        @endforeach
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 page-contacts-map__col">
                    <div class="contacts-cities">
                        @foreach($offices as $office)
                            <div class="js-contacts-cities__item contacts-cities__item">
                                <a class="js-contacts-cities__title contacts-cities__title" data-region="{{ $office->region }}" href="#">{{ $office->title }}</a>
                                <div class="js-contacts-cities__body contacts-cities__body">
                                    <div class="map-popup__info-item">
                                        <ul class="map-popup__info-item_list">
                                            @isset($office->address)
                                                <li class="map-popup__info-item_list-item"><p>Офис</p>{{ $office->address }}</li>                                    
                                            @endisset
                                            @isset($office->stock)
                                                <li class="map-popup__info-item_list-item"><p>Склад</p>{{ $office->stock }}</li>
                                            @endisset
                                        </ul>
                                    </div>
                                    @foreach($office->body as $body)
                                        <div class="contacts-cities__info-name">{{ $body['person'] }}</div>
                                        <div class="contacts-cities__info-address">{{ $body['address'] }}</div>
                                        @foreach($body['phones'] as $phone)
                                            <a class="contacts-cities__info-phone" href="tel:{{ $phone }}">{{ $phone }}</a>
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
