@extends('layouts.app')

@section('content')
    <section class="page-preview" style="background-image:url('/img/about/header_bg.jpg')">
        <div class="container page-preview__container">
            <h1 class="page-preview__title">{{ __('career.page_title') }}</h1>
        </div>
    </section>
    <div class="page-tabs">
        <div class="page-tabs__items">
            <a class="page-tabs__item" href="{{ lang_route('career') }}">{{ __('career.vacancies') }}</a>
            <a class="page-tabs__item page-tabs__item--active" href="{{ lang_route('internship') }}">{{ __('career.internship') }}</a>
        </div>
    </div>
    <section class="career-vacancy">
        <div class="container">
            <h3 class="career-vacancy__title">{{ __('career.title') }}</h3>
            <div class="career-vacancy__text">
                {!! __('career.text') !!}
            </div>
        </div>
    </section>
    <div class="section career-request">
        <form class="career-request__form ajax-form" action="{{ lang_route('feedback.internship') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <h4 class="career-request__title">{{ __('career.form.title') }}</h4>
            <p class="career-request__desc">{{ __('career.form.description') }}</p>
            <div id="alert-error" class="alert alert-danger" style="display: none">
                {{ __('career.form.error') }}
            </div>
            <div id="alert-success" class="alert alert-success" style="display: none">
                {{ __('career.form.success') }}
            </div>
            <input type="hidden" name="title" value="{{ __('career.page_title') }}">
            <div class="career-request__group">
                <input class="career-request__input" type="text" name="name" placeholder="{{ __('career.form.input_name') }}"/>
            </div>
            <div class="career-request__group">
                <input class="career-request__input" type="tel" name="phone" placeholder="{{ __('career.form.input_phone') }}"/>
            </div>
            <div class="career-request__group">
                <input class="career-request__input" type="email" name="email" placeholder="{{ __('career.form.input_email') }}"/>
            </div>
            <div class="career-request__group">
                <textarea class="career-request__textarea" name="message" placeholder="{{ __('career.form.input_message') }}"></textarea>
            </div>
            <div class="career-request__group">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="career-request__resume">
                            <input class="js-career-request__file career-request__file" id="career-request__file"
                                   type="file" name="resume"/>
                            <label class="js-career-request__file-label career-request__file-label"
                                   for="career-request__file">{{ __('career.form.input_attach') }}</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="career-request__btn-wrap">
                            <button class="btn btn--blue btn--small" type="submit">{{ __('button.submit') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <section class="vacancies-directions">
        <div class="container">
            <h4 class="section-title vacancies-directions__section-title section-title--vacancies-directions">{{ __('career.direction.title') }}</h4>
            <div class="vacancies-directions__items">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="vacancies-directions__item">
                            <div class="vacancies-directions__icon" style="background-image:url('/img/vacancies-direction-item-1.png')"></div>
                            <h5 class="vacancies-directions__name">{{ __('career.direction.tech_title') }}</h5>
                            <p class="vacancies-directions__desc">{{ __('career.direction.tech_text') }}</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="vacancies-directions__item">
                            <div class="vacancies-directions__icon" style="background-image:url('/img/vacancies-direction-item-2.png')"></div>
                            <h5 class="vacancies-directions__name">{{ __('career.direction.office_title') }}</h5>
                            <p class="vacancies-directions__desc">{{ __('career.direction.office_text') }}</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="vacancies-directions__item">
                            <div class="vacancies-directions__icon" style="background-image:url('/img/vacancies-direction-item-3.png')"></div>
                            <h5 class="vacancies-directions__name">{{ __('career.direction.department_title') }}</h5>
                            <p class="vacancies-directions__desc">{{ __('career.direction.department_text') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
