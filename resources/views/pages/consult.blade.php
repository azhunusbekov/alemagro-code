@extends('layouts.app')

@section('content')
    <section class="page-preview page-preview--contacts" style="background-image:url('/img/about/header_bg.jpg')">
        <div class="container page-preview__container">
            {{ Breadcrumbs::view('vendor.breadcrumbs.header', 'consult') }}
            <h1 class="page-preview__title">{{ __('contact.consults') }}</h1>
        </div>
    </section>
    <div class="page-tabs">
        <div class="page-tabs__items">
            <a class="page-tabs__item" href="{{ lang_route('contact') }}">{{ __('contact.offices') }}</a>
            <a class="page-tabs__item page-tabs__item--active" href="{{ lang_route('consult') }}">{{ __('contact.consults') }}</a>
        </div>
    </div>
    <div class="contacts-consults">
        <div class="container">
            <div class="contacts-consults__items">
                <div class="contacts-consults__item">
                    <div class="contacts-consults__icon" style="background-image:url('/img/contacts/icon_seeds.png'); background-position: 50% 70%"></div>
                    <h4 class="contacts-consults__name">{{ __('contact.consult_seeds.title') }}</h4>
                    <div class="contacts-consults__desc"><a href="tel:{{ __('contact.consult_seeds.phone') }}">{{ __('contact.consult_seeds.phone') }}</a>
                        <p>{{ __('contact.label_inner_phone') }} <a href="tel:{{ __('contact.consult_seeds.phone_inner') }}">{{ __('contact.consult_seeds.phone_inner') }}</a></p>
                    </div>
                </div>
                <div class="contacts-consults__item">
                    <div class="contacts-consults__icon" style="background-image:url('/img/contacts/icon_secure.png');background-position: 50% 70%"></div>
                    <h4 class="contacts-consults__name">{{ __('contact.consult_secure.title') }}</h4>
                    <div class="contacts-consults__desc"><a href="tel:{{ __('contact.consult_secure.phone') }}">{{ __('contact.consult_secure.phone') }}</a>
                        <p>{{ __('contact.label_inner_phone') }} <a href="tel:{{ __('contact.consult_secure.phone_inner') }}">{{ __('contact.consult_secure.phone_inner') }}</a></p>
                    </div>
                </div>
                <div class="contacts-consults__item">
                    <div class="contacts-consults__icon" style="background-image:url('/img/contacts/icon_lab.png')"></div>
                    <h4 class="contacts-consults__name">{{ __('contact.consult_lab.title') }}</h4>
                    <div class="contacts-consults__desc"><a href="tel:{{ __('contact.consult_lab.phone') }}">{{ __('contact.consult_lab.phone') }}</a>
                        <p>{{ __('contact.label_inner_phone') }} <a href="tel:{{ __('contact.consult_lab.phone_inner') }}">{{ __('contact.consult_lab.phone_inner') }}</a></p>
                    </div>
                </div>
                <div class="contacts-consults__item">
                    <div class="contacts-consults__icon" style="background-image:url('/img/contacts/icon_reduce.png')"></div>
                    <h4 class="contacts-consults__name">{{ __('contact.consult_reduce.title') }}</h4>
                    <div class="contacts-consults__desc"><a href="tel:{{ __('contact.consult_reduce.phone') }}">{{ __('contact.consult_reduce.phone') }}</a>
                        <p>{{ __('contact.label_inner_phone') }} <a href="tel:{{ __('contact.consult_reduce.phone_inner') }}">{{ __('contact.consult_reduce.phone_inner') }}</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
