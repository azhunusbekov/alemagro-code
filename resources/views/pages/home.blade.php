@extends('layouts.app')

@section('content')
    @if ($banners->isNotEmpty())
        <section class="main-slider">
            <div class="main-slider__items" data-slider="mainSlider">
                @foreach ($banners as $banner)
                    <div class="main-slider__item" style="background-image:url({{ $banner->image }})">
                        <div class="container">
                            <h2 class="main-slider__title">{{ $banner->title }}</h2>
                            <h4 class="main-slider__desc">{!! $banner->description !!}</h4>
                            @if ($banner->link)
                                <div class="main-slider__btn-wrap">
                                    <a class="btn main-slider__btn btn--green" href="{{ $banner->link }}">{{ __('button.more') }}</a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    @endif

    <section class="page-substrate">
        <h4 class="page-substrate__title page-substrate__title--direction">{{ __('about.company_profile.title') }}</h4>
    </section>

    <section class="our-directions">
        <div class="container">
            <div class="our-directions__items">
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <div class="our-directions__item animated animate-delay-1">
                            <div class="our-directions__icon" style="background-image:url('/img/about/icon_seeds.png')"></div>
                            <div class="our-directions__name">{{ __('about.company_profile.seeds') }}</div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="our-directions__item animated animate-delay-2">
                            <div class="our-directions__icon" style="background-image:url('/img/about/icon_grow.png'); background-position: 50% 66%;"></div>
                            <div class="our-directions__name">{{ __('about.company_profile.grow') }}</div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="our-directions__item animated animate-delay-3">
                            <div class="our-directions__icon" style="background-image:url('/img/about/icon_lab.png')"></div>
                            <div class="our-directions__name">{{ __('about.company_profile.lab') }}</div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="our-directions__item animated animate-delay-4">
                            <div class="our-directions__icon" style="background-image:url('/img/about/icon_secure.png')"></div>
                            <div class="our-directions__name">{{ __('about.company_profile.secure') }}</div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="our-directions__item animated animate-delay-6">
                            <div class="our-directions__icon" style="background-image:url('/img/about/icon_prof_agro.png')"></div>
                            <div class="our-directions__name">{{ __('about.company_profile.prof_agro') }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="main-map">
        <div class="custom-map-container">
            <h4 class="section-title section-title--map">{{ __('home.offices_map') }}</h4>
            <div class="main-map__pic-wrap">
                <img class="js-main-map__pic main-map__pic" src="/img/map.png"/>
                @foreach($offices->unique('region') as $office)
                    <div class="js-main-map__region main-map__region main-map__region--{{ $office->region }}" data-region="{{ $office->region }}"></div>
                @endforeach
                @foreach($offices as $office)
                    <div class="js-main-map__city main-map__city main-map__city--{{ $office->alias }}" data-region="{{ $office->region }}">
                        {{ $office->title }}
                        <div class="map-popup main-map__map-popup">
                            <div class="map-popup__title">{{ $office->title }}</div>
                            <div class="map-popup__info">
                                <div class="map-popup__info-item">
                                    <ul class="map-popup__info-item_list">
                                        @isset($office->address)
                                            <li class="map-popup__info-item_list-item"><p>Офис</p>{{ $office->address }}</li>                                    
                                        @endisset
                                        @isset($office->stock)
                                            <li class="map-popup__info-item_list-item"><p>Склад</p>{{ $office->stock }}</li>
                                        @endisset
                                    </ul>
                                </div>
                                @foreach($office->body as $body)
                                    <div class="map-popup__info-item">
                                        <div class="map-popup__info-name">{{ $body['person'] }}</div>
                                        <div class="map-popup__info-address">{{ $body['address'] }}</div>
                                        @foreach($body['phones'] as $phone)
                                            <a class="map-popup__info-phone" href="tel:77019358444">{{ $phone }}</a>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="main-map__btn-wrap"><a class="btn btn--blue" href="{{ lang_route('contact') }}">{{ __('button.go_to') }}</a></div>
        </div>
    </section>

    @if($news->isNotEmpty())
        <section class="news">
            <div class="container">
                <h4 class="section-title section-title--news">{{ __('home.news') }}</h4>
                <div class="news__items">
                    <div class="row">
                        @foreach($news as $newsItem)
                            <div class="col-xs-12 col-sm-4">
                                <div class="news__item">
                                    <span class="news__date">15 февраля, 2019</span>
                                    <a class="news__title" href="{{ lang_route('article.article', ['article_category' => $newsItem->category->slug, 'article_slug' => $newsItem->slug]) }}">{{ $newsItem->title }}</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="news__btn-wrap">
                    <a class="btn news__btn btn--blue" href="{{ lang_route('article.index') }}">{{ __('button.read_more') }}</a>
                </div>
            </div>
        </section>
    @endif

    <section class="page-substrate page-substrate--production">
        <h4 class="page-substrate__title page-substrate__title--production">{{ __('home.products') }}</h4>
    </section>

    <section class="main-production">
        <div class="main-production__item" style="background-image:url('/img/kz.jpg')">
            <div class="main-production__text">
                <h4 class="main-production__title">{{ __('home.products_kz') }}</h4>
                <div class="main-production__btn-wrap">
                    <a class="btn btn--green" href="{{ lang_route('catalog.main', ['county_code' => 'kz']) }}">{{ __('button.more') }}</a>
                </div>
            </div>
        </div>
        <div class="main-production__item" style="background-image:url('/img/kg.jpg')">
            <div class="main-production__text">
                <h4 class="main-production__title">{{ __('home.products_kg') }}</h4>
                <div class="main-production__btn-wrap">
                    <a class="btn btn--green" href="{{ lang_route('catalog.main', ['county_code' => 'kg']) }}">{{ __('button.more') }}</a>
                </div>
            </div>
        </div>
    </section>

    @foreach($cultures as $culture)
        <section class="main-products {{ $loop->first ? 'main-products--vegetables' : '' }}">
            <div class="container">
                <h4 class="section-title section-title--{{ $culture->slug }}">{{ $culture->title }}</h4>
                <div class="main-products__items-wrap">
                    <div class="main-products__items" data-slider="mainProducts">
                        @foreach($culture->children as $child)
                            <div class="main-products__col">
                                <a class="main-products__item" href="{{ lang_route('culture.culture', ['catalog_category' => $category->slug, 'culture_path' => $child->getPath($culture)]) }}">
                                    <div class="main-products__name">{{ $child->title }}</div>
                                    <div class="main-products__icon" style="background-image:url({{ $child->icon }})"></div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endforeach

    <section class="partners">
        <div class="container">
            <h4 class="section-title section-title--partners">{{ __('about.partners_title') }}</h4>
            <div class="partners__items-wrap">
                <div class="partners__items" data-slider="mainPartners">
                    @foreach($partners as $partner)
                        <div class="partners__col">
                            @if($partner->link)
                                <a class="partners__item" href="{{ $partner->link  }}" target="_blank" style="background-image:url('{{ asset($partner->image) }}')"></a>
                            @else
                                <span class="partners__item" style="background-image:url('{{ asset($partner->image) }}')"></span>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section class="career-preview career-preview--main" style="background-image:url('/img/career-main.jpg')">
        <div class="container">
            <h3 class="career-preview__title">{{ __('home.career') }}</h3>
            <div class="career-preview__btn-wrap">
                <a class="btn btn--green" href="{{ lang_route('career') }}">{{ __('button.more') }}</a>
            </div>
        </div>
    </section>
@endsection
