@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Актуальное - Категории</h4>
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <a href="{{ route('admin.article.category.create') }}" class="btn btn-primary">Создать новую категорию</a>
                </div>
            </div>
            <div class="card-body">
                @if ($categories->isNotempty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="min-width: 50px;">ID</th>
                                        <th style="width: 100%;">Заголовок</th>
                                        <th style="min-width: 100px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($categories as $category)
                                        <tr data-id="{{ $category->id }}">
                                            <th>{{ $category->id }}</th>
                                            <td>{{ $category->title }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.article.article.index', ['category_id' => $category]) }}" class="btn btn-xs btn-info">
                                                        <i class="fa fa-list"></i>
                                                    </a>
                                                    <a href="{{ route('admin.article.category.edit', $category) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>
@endsection