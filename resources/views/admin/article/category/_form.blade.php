<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.article.category.index') }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($category) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.article.category.destroy', $category) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить</span>
                </button>
            @endif
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <span>Сохранить</span></button>
        </div>
        @if (isset($category))
            <div class="card-header__actions">
                @foreach($category->translations as $translation)
                    <a href="{{ route('admin.article.category.translation.edit', [$category, $translation]) }}" class="btn btn-outline-info"><span>{{ locale_name($translation->locale) }}</span></a>
                @endforeach
                <a href="{{ route('admin.article.category.translation.create', $category) }}" class="btn btn-info"><span>Добавить перевод</span></a>
            </div>
        @endif
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Заголовок</label>
            <input name="title" type="text" class="form-control" value="{{ isset($category) ? $category->title : old('title') }}">
            {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
        </div>
        @include('admin.partial.seo_fields', ['seoData' => isset($seoData) ? $seoData : null])
    </div>
</div>