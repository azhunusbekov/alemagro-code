@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Актуальное - Категории</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.article.category.index') }}">Все категории</a></li>
            <li><a href="{{ route('admin.article.category.edit', $category) }}">{{ $category->title }}</a></li>
            <li><span>Перевод</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.article.category.translation.update', [$category, $categoryTranslation]) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.article.category.translation._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection