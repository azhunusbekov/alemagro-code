<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.article.article.index') }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($article) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.article.article.destroy', $article) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> <span>Сохранить</span></button>
        </div>
        @if (isset($article))
            <div class="card-header__actions">
                @foreach($article->translations as $translation)
                    <a href="{{ route('admin.article.article.translation.edit', [$article, $translation]) }}" class="btn btn-outline-info"><span>{{ locale_name($translation->locale) }}</span></a>
                @endforeach
                <a href="{{ route('admin.article.article.translation.create', $article) }}" class="btn btn-info"><i class="fa fa-language"></i> <span>Добавить перевод</span></a>
            </div>
        @endif
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            <label class="col-form-label">Статус</label>
            <select name="status" class="custom-select">
                @foreach ($statuses as $key => $status)
                    <option value="{{ $key }}"
                            {{ isset($article) && $article->status == $key || old('status') == $key ? 'selected' : '' }}>{{ $status }}
                    </option>
                @endforeach
            </select>
            {!! $errors->first('status','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('category_id') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Категория</label>
            <select name="category_id" class="form-control select2" data-placeholder="Выбрать категорию">
                <option></option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}"{{ isset($article) && $article->category_id == $category->id || old('category_id') == $category->id ? 'selected' : '' }}>
                        {{ $category->title }}
                    </option>
                @endforeach
            </select>
            {!! $errors->first('category_id','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Заголовок</label>
            <input name="title" type="text" class="form-control" value="{{ isset($article) ? $article->title : old('title') }}">
            {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('description') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Описание</label>
            <textarea name="description" class="form-control editor" rows="3">{{ isset($article) ? $article->description : old('description') }}</textarea>
            {!! $errors->first('description','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('preview') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Изображение</label>
            <input name="preview" type="file" class="form-control" accept="image/jpg,image/jpeg,image/png">
            {!! $errors->first('preview','<span class="invalid-feedback">:message</span>') !!}
            @if (isset($article))
                <div class="image-preview">
                    <img src="{{ $article->preview }}" width="600">
                </div>
            @endif
        </div>
        <div class="form-group {{ $errors->has('image_header') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Фоновое изображение шапки (опционально)</label>
            <input name="image_header" type="file" class="form-control" accept="image/jpg,image/jpeg,image/png">
            {!! $errors->first('image_header','<span class="invalid-feedback">:message</span>') !!}
            @if (isset($article))
                <div class="image-preview">
                    <img src="{{ $article->image_header }}" width="600">
                </div>
            @endif
        </div>
        @include('admin.partial.seo_fields', ['seoData' => isset($seoData) ? $seoData : null])
    </div>
</div>