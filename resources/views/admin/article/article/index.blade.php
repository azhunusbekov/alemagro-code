@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Актуальное - Материалы</h4>
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <a href="{{ route('admin.article.article.create') }}" class="btn btn-primary">Создать новый материал</a>
                </div>
                <div class="card-header__filter">
                    <form action="{{ route('admin.article.article.index') }}" method="get">
                        <div class="form-group">
                            <select name="category_id" class="form-control select2">
                                <option value="">Все категории</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}"
                                        {!! request()->get('category_id') == $category->id ? 'selected' : '' !!}>
                                        {{ $category->title }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-warning" type="submit">Фильтр</button>
                    </form>
                </div>
            </div>
            <div class="card-body">
                @if ($articles->isNotempty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="min-width: 50px;">ID</th>
                                        <th style="width: 50%;">Заголовок</th>
                                        <th style="width: 30%;">Категория</th>
                                        <th style="width: 20%;">Статус</th>
                                        <th style="min-width: 100px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($articles as $article)
                                        <tr data-id="{{ $article->id }}">
                                            <th>{{ $article->id }}</th>
                                            <td>{{ $article->title }}</td>
                                            <td>{{ $article->category->title }}</td>
                                            <td>
                                                <span class="badge {{ $article->isPublished() ? 'badge-success' : 'badge-danger' }}">{{ $statuses[$article->status] }}</span>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.article.article.edit', $article) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>
@endsection