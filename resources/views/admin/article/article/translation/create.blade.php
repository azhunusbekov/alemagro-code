@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Актуальное - Материалы</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.article.article.index') }}">Все Материалы</a></li>
            <li><a href="{{ route('admin.article.article.edit', $article) }}">{{ $article->title }}</a></li>
            <li><span>Создать перевод</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.article.article.translation.store', $article) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @include('admin.article.article.translation._form', ['mode' => 'create'])
        </form>
    </div>
@endsection