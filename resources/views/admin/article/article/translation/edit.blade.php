@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Актуальное - Материалы</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.article.article.index') }}">Все материалы</a></li>
            <li><a href="{{ route('admin.article.article.edit', $article) }}">{{ $article->title }}</a></li>
            <li><span>Перевод</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.article.article.translation.update', [$article, $articleTranslation]) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.article.article.translation._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection