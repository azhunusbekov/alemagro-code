<h4 class="header-title">SEO:</h4>
<div class="form-group {{ $errors->has('seo.title') ? 'is-invalid' : '' }}">
    <label class="col-form-label">Заголовок</label>
    <input name="seo[title]"
           type="text"
           class="form-control"
           value="{{ $seoData ? $seoData->title : old('seo.title') }}"
    >
    {!! $errors->first('seo.title','<span class="invalid-feedback">:message</span>') !!}
</div>
<div class="form-group {{ $errors->has('seo.desciption') ? 'is-invalid' : '' }}">
    <label class="col-form-label">Описание</label>
    <textarea name="seo[description]" type="text" class="form-control">{{ $seoData ? $seoData->description : old('seo.title') }}</textarea>
    {!! $errors->first('seo.description','<span class="invalid-feedback">:message</span>') !!}
</div>
<div class="form-group {{ $errors->has('seo.keywords') ? 'is-invalid' : '' }}">
    <label class="col-form-label">Ключевые слова</label>
    <input name="seo[keywords]"
           type="text"
           class="form-control"
           value="{{  $seoData ? $seoData->keywords : old('seo.keywords') }}"
    >
    {!! $errors->first('seo.keywords','<span class="invalid-feedback">:message</span>') !!}
</div>