@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Баннеры</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.banner.index') }}">Все баннеры</a></li>
            <li><span>Редактировать баннер</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.banner.update', $banner) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.banner._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection