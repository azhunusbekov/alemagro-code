@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Баннеры</h4>
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <a href="{{ route('admin.banner.create') }}" class="btn btn-primary">Создать новый баннер</a>
                </div>
            </div>
            <div class="card-body">
                @if ($banners->isNotempty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="width: 50px;">ID</th>
                                        <th style="width: 300px;">Заголовок</th>
                                        <th style="width: 300px;">Превью</th>
                                        <th style="width: 200px;">Статус</th>
                                        <th style="width: 200px;">Дата создания</th>
                                        <th style="width: 100px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($banners as $banner)
                                        <tr data-id="{{ $banner->id }}">
                                            <th>{{ $banner->id }}</th>
                                            <td>{{ $banner->title }}</td>
                                            <td><img src="{{ $banner->image }}" width="100"></td>
                                            <td>
                                                <span class="badge {{ $banner->status == 0 ? 'badge-danger' : 'badge-success' }}">{{ $statuses[$banner->status] }}</span>
                                            </td>
                                            <td>{{ $banner->created_at->format('m.d.Y') }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.banner.edit', $banner) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <button class="btn btn-xs btn-secondary btn-sort-handle">
                                                        <i class="fa fa-arrows"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function(){
            sortable('.sortable', {
                items: 'tr' ,
                forcePlaceholderSize: true,
                placeholderClass: 'sortable-placeholder',
                hoverClass: 'sortable-hover',
                handle: '.btn-sort-handle',
                itemSerializer: (serializedItem, sortableContainer) => {
                    return {
                        id: serializedItem.node.getAttribute('data-id'),
                        position:  serializedItem.index + 1
                    }
                }
            })[0].addEventListener('sortupdate', function(e) {
                const serialize = sortable('.sortable', 'serialize');
                const items = serialize[0].items;

                $.post({
                    url: '/inside/banner/reorder',
                    data: {
                        items: items
                    },
                    beforeSend: function() {
                        $('.overlay').show();
                    },
                    success: function(response){
                        $('.overlay').hide();
                        $('.sortable').find('.updated').removeClass('updated');
                    }
                });
            });
        });
    </script>
@endpush