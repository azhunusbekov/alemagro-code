@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Баннеры</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.banner.index') }}">Все баннеры</a></li>
            <li><span>Создать новый баннер</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.banner.store') }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @include('admin.banner._form', ['mode' => 'create'])
        </form>
    </div>
@endsection