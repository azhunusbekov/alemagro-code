@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Обратные связи</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.feedback.index') }}">Обратные связи</a></li>
            <li><span>Редактировать</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.feedback.update', $feedback) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.feedback._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection