@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Обратные связи</h4>
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-body">
                @if ($feedbacks->isNotempty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="width: 50px;">ID</th>
                                        <th style="width: 300px;">Заголовок</th>
                                        <th style="width: 300px;">Тип</th>
                                        <th style="width: 300px;">Регион</th>
                                        <th style="width: 200px;">Статус</th>
                                        <th style="width: 200px;">Дата создания</th>
                                        <th style="width: 100px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($feedbacks as $feedback)
                                        <tr data-id="{{ $feedback->id }}">
                                            <th>{{ $feedback->id }}</th>
                                            <td>{{ $feedback->title }}</td>
                                            <td>{{ $feedback->type }}</td>
                                            <td>
                                                @if(isset($feedback->body['region']))
                                                    {{ $feedback->body['region'] }}
                                                @else
                                                    --
                                                @endif
                                            </td>
                                            <td>
                                                <span class="badge {{ $feedback->status == 0 ? 'badge-info' : 'badge-success' }}">{{ $statuses[$feedback->status] }}</span>
                                            </td>
                                            <td>{{ $feedback->created_at->format('m.d.Y') }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.feedback.edit', $feedback) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>
@endsection