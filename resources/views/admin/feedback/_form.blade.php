<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.feedback.index') }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($feedback) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.feedback.destroy', $feedback) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> <span>Сохранить</span></button>
        </div>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            <label class="col-form-label">Статус</label>
            <select name="status" class="custom-select">
                @foreach ($statuses as $key => $status)
                    <option value="{{ $key }}" {{ isset($feedback) && $feedback->status == $key ? 'selected' : '' }}>{{ $status }}</option>
                @endforeach
            </select>
            {!! $errors->first('status','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Заголовок</label>
            <input name="title" type="text" class="form-control" value="{{ isset($feedback) ? $feedback->title : old('title') }}" disabled>
            {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('type') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Тип</label>
            <input name="type" type="text" class="form-control" value="{{ isset($feedback) ? $feedback->type : old('type') }}" disabled>
            {!! $errors->first('type','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group">
            @foreach($feedback->body as $key => $value)
                <label class="col-form-label">{{ $key }}</label>
                @if ($key == 'resume')
                    <p><a href="{{ $value }}" target="_blank">{{ $value }}</a></p>
                @else
                    <input name="{{ $key }}" class="form-control" value="{{ $value }}" disabled>
                @endif
            @endforeach
        </div>
    </div>
</div>