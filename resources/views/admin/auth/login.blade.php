<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin - Dashboard | AlemAgro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="/admin/images/icon/favicon.ico">
    <link rel="stylesheet" href="/admin/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin/css/font-awesome.min.css">
    <link rel="stylesheet" href="/admin/css/themify-icons.css">
    <link rel="stylesheet" href="/admin/css/metisMenu.css">
    <link rel="stylesheet" href="/admin/css/slicknav.min.css">
    <link rel="stylesheet" href="/admin/css/typography.css">
    <link rel="stylesheet" href="/admin/css/default-css.css">
    <link rel="stylesheet" href="/admin/css/styles.css">
    <link rel="stylesheet" href="/admin/css/responsive.css">
</head>
<body>
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <div class="login-area">
        <div class="container">
            <div class="login-box">
                <form method="POST" action="{{ route('admin.login') }}">
                    @csrf
                    <div class="login-form-head">
                        <h4>Sign In</h4>
                    </div>
                    <div class="login-form-body">
                        <div class="form-gp">
                            <label for="exampleInputLogin">{{ __('Login') }}</label>
                            <input type="text" name="login" id="exampleInputLogin" class="{{ $errors->has('login') ? ' is-invalid' : '' }}" value="{{ old('email') }}">
                            <i class="ti-email"></i>
                            @if ($errors->has('login'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('login') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-gp">
                            <label for="exampleInputPassword">Password</label>
                            <input type="password" name="password" id="exampleInputPassword" {{ old('remember') ? 'checked' : '' }}>
                            <i class="ti-lock"></i>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="row mb-4 rmber-area">
                            <div class="col-6">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                    <label class="custom-control-label" for="customControlAutosizing">{{ __('Remember Me') }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="submit-btn-area">
                            <button id="form_submit" type="submit">{{ __('Login') }}<i class="ti-arrow-right"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- scripts start -->
    <script src="/admin/js/jquery-3.4.1.min.js"></script>
    <script src="/admin/js/popper.min.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/metisMenu.min.js"></script>
    <script src="/admin/js/jquery.slimscroll.min.js"></script>
    <script src="/admin/js/html5sortable.js"></script>
    <script src="/admin/js/select2.min.js"></script>
    <script src="/admin/js/plugins.js"></script>
    <script src="/admin/js/scripts.js"></script>
    <!-- scripts end -->
</body>
</html>
