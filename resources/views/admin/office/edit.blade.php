@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Представительства</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.office.index') }}">Все представительства</a></li>
            <li><span>Редактировать <i>{{ $office->title }}</i></span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.office.update', $office) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.office._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection