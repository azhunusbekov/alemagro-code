<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.office.index') }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($office) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.office.destroy', $office) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> <span>Сохранить</span></button>
        </div>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Заголовок</label>
            <input name="title" type="text" class="form-control" value="{{ isset($office) ? $office->title : old('title') }}">
            {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('alias') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Алиас</label>
            <input name="alias" type="text" class="form-control" value="{{ isset($office) ? $office->alias : old('alias') }}">
            {!! $errors->first('alias','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('region') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Регион (алиас на латинице)</label>
            <input name="region" type="text" class="form-control" value="{{ isset($office) ? $office->region : old('region') }}">
            {!! $errors->first('region','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('email') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Электронная почта</label>
            <input name="email" type="text" class="form-control" value="{{ isset($office) ? $office->email : old('email') }}">
            {!! $errors->first('email','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('address') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Офис</label>
            <input name="address" type="text" class="form-control" value="{{ isset($office) ? $office->address : old('address') }}">
            {!! $errors->first('address','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('stock') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Склад</label>
            <input name="stock" type="text" class="form-control" value="{{ isset($office) ? $office->stock : old('stock') }}">
            {!! $errors->first('stock','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <h4 class="header-title">Представители:</h4>
            @if (isset($office) && !empty($office->body))
                @foreach($office->body as $field)
                    <div class="form-group body-fields-group">
                        <label class="col-form-label">ФИО представителя</label>
                        <input name="body[{{ $loop->iteration }}][person]" type="text" class="form-control form-control-sm" value="{{ $field['person'] }}">
                        <label class="col-form-label">Адрес</label>
                        <input name="body[{{ $loop->iteration }}][address]" type="text" class="form-control form-control-sm" value="{{ $field['address'] }}">
                        <label class="col-form-label">Телефоны</label>
                        @foreach($field['phones'] as $phone => $value)
                            <div class="input-group">
                                <input name="body[{{ $loop->parent->iteration }}][phones][]" type="text" class="form-control form-control-sm phone" value="{{ $value }}">
                                <div class="input-group-prepend">
                                    <button class="btn btn-sm btn-info btn-add-phone" type="button"><i class="ti ti-plus"></i></button>
                                    <button class="btn btn-sm btn-danger btn-remove-phone" type="button"><i class="ti ti-minus"></i></button>
                                </div>
                            </div>
                        @endforeach
                        <button class="btn btn-sm btn-danger btn-remove-group">Удалить представителя</button>
                    </div>
                @endforeach
            @endif
        <div id="body-fields"></div>
        <div class="form-group">
            <button class="btn btn-info" id="add-new">Добавить представителя и адрес</button>
        </div>
        <div class="form-group body-fields-group" id="body-group" style="display: none;">
            <label class="col-form-label">ФИО представителя</label>
            <input type="text" class="form-control form-control-sm" data-body-person value="">
            <label class="col-form-label">Адрес</label>
            <input type="text" class="form-control form-control-sm" data-body-address value="">
            <label class="col-form-label">Телефоны</label>
            <div class="input-group">
                <input type="text" class="form-control form-control-sm phone" data-body-phones value="">
                <div class="input-group-prepend">
                    <button class="btn btn-sm btn-info btn-add-phone" type="button"><i class="ti ti-plus"></i></button>
                    <button class="btn btn-sm btn-danger btn-remove-phone" type="button" style="display: none;"><i class="ti ti-minus"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(function() {
            $('input.phone').inputmask("+7(999)-999-9999");

            $('#add-new').on('click', function(e) {
                e.preventDefault();

                let bodyFieldGroupsCount = $('.body-fields-group').length;
                let formGroup = $('#body-group').clone().attr('id', '').show();
                formGroup.find('[data-body-person]').attr('name', 'body['+bodyFieldGroupsCount+'][person]');
                formGroup.find('[data-body-address]').attr('name', 'body['+bodyFieldGroupsCount+'][address]');
                formGroup.find('[data-body-phones]').attr('name', 'body['+bodyFieldGroupsCount+'][phones][]');

                $('#body-fields').append(formGroup);
                $('input.phone').inputmask("+7-(999)-999-9999");
            });

            $('body').on('click', '.btn-add-phone', function(e) {
                e.preventDefault();

                let inputGroup = $(this).closest('.input-group');
                let wrapper = inputGroup.parent();
                let newInputGroup = inputGroup.clone();
                newInputGroup.find('input').val('');
                wrapper.append(newInputGroup);
            });

            $('body').on('click', '.btn-remove-phone', function(e) {
                e.preventDefault();
                let inputGroup = $(this).closest('.input-group').remove();
            });

            $('body').on('click', '.btn-remove-group', function(e) {
                e.preventDefault();
                $(this).closest('.body-fields-group').remove();
            })
        });
    </script>
@endpush