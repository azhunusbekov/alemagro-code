@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Представительства (офисы)</h4>
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <a href="{{ route('admin.office.create') }}" class="btn btn-primary">Создать новый</a>
                </div>
            </div>
            <div class="card-body">
                @if ($offices->isNotempty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="min-width: 50px;">ID</th>
                                        <th style="width: 50%;">Название</th>
                                        <th style="width: 30%;">Алиас</th>
                                        <th style="width: 20%;">Регион</th>
                                        <th style="min-width: 100px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($offices as $office)
                                        <tr data-id="{{ $office->id }}">
                                            <th>{{ $office->id }}</th>
                                            <td>{{ $office->title }}</td>
                                            <td>{{ $office->alias }}</td>
                                            <td>{{ $office->region }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.office.edit', $office) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>
@endsection