<div class="main-menu">
    <div class="menu-inner">
        <nav>
            <ul class="metismenu" id="menu">
                <li class="{{ request()->routeIs('admin.dashboard') ? 'active' : '' }}">
                    <a href="{{ route('admin.dashboard') }}">
                        <i class="ti-dashboard"></i><span>Dashboard</span>
                    </a>
                </li>
                <li class="{{ request()->is('*catalog*') ? 'active' : '' }}">
                    <a href="#"><i class="ti-list"></i><span>Каталог</span></a>
                    <ul class="collapse">
                        <li class="{{ request()->is('*catalog/import*') ? 'active' : '' }}">
                            <a href="{{ route('admin.catalog.import.form') }}">Импорт</a>
                        </li>
                        <li class="{{ request()->is('*catalog/producer*') ? 'active' : '' }}">
                            <a href="{{ route('admin.catalog.producer.index') }}">Производители</a>
                        </li>
                        <li class="{{ request()->is('*catalog/category*') ? 'active' : '' }}">
                            <a href="{{ route('admin.catalog.category.index') }}">Категория</a>
                        </li>
                        <li class="{{ request()->is('*catalog/section*') ? 'active' : '' }}">
                            <a href="{{ route('admin.catalog.section.index') }}">Разделы</a>
                        </li>
                        <li class="{{ request()->is('*catalog/culture*') ? 'active' : '' }}">
                            <a href="{{ route('admin.catalog.culture.index') }}">Культуры</a>
                        </li>
                        <li class="{{ request()->is('*catalog/disease*') ? 'active' : '' }}">
                            <a href="{{ route('admin.catalog.disease.index') }}">Вредные объекты</a>
                        </li>
                        <li class="{{ request()->is('*catalog/substance*') ? 'active' : '' }}">
                            <a href="{{ route('admin.catalog.substance.index') }}">Действующие вещества</a>
                        </li>
                        <li class="{{ request()->is('*catalog/protection*') ? 'active' : '' }}">
                            <a href="{{ route('admin.catalog.protection.index') }}">Защита и питание</a>
                        </li>
                        <li class="{{ request()->is('*catalog/product*') ? 'active' : '' }}">
                            <a href="{{ route('admin.catalog.product.index') }}">Товары</a>
                        </li>
                    </ul>
                </li>
                <li class="{{ request()->is('*article*') ? 'active' : '' }}">
                    <a href="#"><i class="ti-list"></i><span>Актуальное</span></a>
                    <ul class="collapse">
                        <li class="{{ request()->is('*article/category*') ? 'active' : '' }}">
                            <a href="{{ route('admin.article.category.index') }}">Категории</a>
                        </li>
                        <li class="{{ request()->is('*article/article*') ? 'active' : '' }}">
                            <a href="{{ route('admin.article.article.index') }}">Материалы</a>
                        </li>
                    </ul>
                </li>
                <li class="{{ request()->is('*banner*') ? 'active' : '' }}">
                    <a href="{{ route('admin.banner.index') }}">
                        <i class="ti-image"></i><span>Баннеры</span>
                    </a>
                </li>
                <li class="{{ request()->is('*office*') ? 'active' : '' }}">
                    <a href="{{ route('admin.office.index') }}">
                        <i class="ti-map"></i><span>Представительства</span>
                    </a>
                </li>
                <li class="{{ request()->is('*vacancy*') ? 'active' : '' }}">
                    <a href="{{ route('admin.vacancy.index') }}">
                        <i class="ti-agenda"></i><span>Вакансии</span>
                    </a>
                </li>
                <li class="{{ request()->is('*feedback*') ? 'active' : '' }}">
                    <a href="{{ route('admin.feedback.index') }}">
                        <i class="ti-agenda"></i><span>Обратные связи</span>
                    </a>
                </li>
                <li class="{{ request()->is('*partner*') ? 'active' : '' }}">
                    <a href="{{ route('admin.partner.index') }}">
                        <i class="ti-image"></i><span>Партнеры</span>
                    </a>
                </li>
                <li>
                    <a href="/inside/translations">
                        <i class="ti-agenda"></i><span>Константы</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
