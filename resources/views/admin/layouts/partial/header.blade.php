<div class="header-area">
    <div class="row align-items-center">
        <div class="col-md-6 col-sm-8 clearfix">
            <div class="nav-btn pull-left">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="col-md-6 col-sm-4 clearfix">
            <ul class="notification-area pull-right">
                <li>
                    <a href="{{ route('home') }}" class="notification-area__link" target="_blank">Перейти на сайт</a>
                </li>
                <li>
                    <form action="{{ route('admin.logout') }}" method="POST">
                        @csrf
                        <button class="notification-area__link">Выйти</button>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>