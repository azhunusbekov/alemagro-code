<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin Panel | AlemAgro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/admin/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin/css/font-awesome.min.css">
    <link rel="stylesheet" href="/admin/css/themify-icons.css">
    <link rel="stylesheet" href="/admin/css/metisMenu.css">
    <link rel="stylesheet" href="/admin/css/slicknav.min.css">
    <link rel="stylesheet" href="/admin/css/select2.min.css">
    <link rel="stylesheet" href="/admin/css/typography.css">
    <link rel="stylesheet" href="/admin/css/default-css.css">
    <link rel="stylesheet" href="/admin/css/styles.css">
    <link rel="stylesheet" href="/admin/css/responsive.css">
    <script>
        window.core_project = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    @stack('head_scripts')
</head>
<body>
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <div class="page-container">
        <!-- sidebar start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo">
                    <a href="/inside">AlemAgro</a>
                </div>
            </div>
            @include('admin.layouts.partial.sidebar')
        </div>
        <!-- sidebar end -->
        <!-- main content start -->
        <div class="main-content">
            <!-- header start -->
            @include('admin.layouts.partial.header')
            <!-- header end -->
            <!-- content start -->
            @yield('content')
            <!-- content end -->
        </div>
        <!-- main content end -->
    </div>
    <!-- modals start -->
    <div class="modal modal-danger fade" role="dialog" id="modal-destroy">
        <div class="modal-dialog modal-sm ">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Внимание</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                <div class="modal-body">
                    <p>Вы уверены что хотите удалить?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-action="yes_destroy">Да</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Нет</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modals end -->
    <!-- scripts start -->
    <script src="/admin/js/jquery-3.4.1.min.js"></script>
    <script src="/admin/js/popper.min.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/metisMenu.min.js"></script>
    <script src="/admin/js/jquery.slimscroll.min.js"></script>
    <script src="/admin/js/html5sortable.js"></script>
    <script src="/admin/js/select2.min.js"></script>
    <script src="/admin/js/jquery.inputmask.min.js"></script>
    <script src="/admin/js/plugins.js"></script>
    <script src="/admin/js/ckeditor/ckeditor.js"></script>
    <script src="/admin/js/ckeditor/adapters/jquery.js"></script>
    <script src="/admin/js/scripts.js"></script>
    @stack('scripts')
<!-- scripts end -->
</body>
</html>
