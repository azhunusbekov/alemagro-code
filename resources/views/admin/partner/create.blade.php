@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Партнеры</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.partner.index') }}">Все партнеры</a></li>
            <li><span>Создать новый</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.partner.store') }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @include('admin.partner._form', ['mode' => 'create'])
        </form>
    </div>
@endsection
