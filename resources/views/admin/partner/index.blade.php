@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Партнеры</h4>
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <a href="{{ route('admin.partner.create') }}" class="btn btn-primary">Создать новый</a>
                </div>
            </div>
            <div class="card-body">
                @if ($partners->isNotempty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="width: 50px;">ID</th>
                                        <th style="width: 300px;">Заголовок</th>
                                        <th style="width: 300px;">Превью</th>
                                        <th style="width: 100px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($partners as $partner)
                                        <tr data-id="{{ $partner->id }}">
                                            <th>{{ $partner->id }}</th>
                                            <td>{{ $partner->title }}</td>
                                            <td><img src="{{ $partner->image }}" width="100"></td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.partner.edit', $partner) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <button class="btn btn-xs btn-secondary btn-sort-handle">
                                                        <i class="fa fa-arrows"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function(){
            sortable('.sortable', {
                items: 'tr' ,
                forcePlaceholderSize: true,
                placeholderClass: 'sortable-placeholder',
                hoverClass: 'sortable-hover',
                handle: '.btn-sort-handle',
                itemSerializer: (serializedItem, sortableContainer) => {
                    return {
                        id: serializedItem.node.getAttribute('data-id'),
                        position:  serializedItem.index + 1
                    }
                }
            })[0].addEventListener('sortupdate', function(e) {
                const serialize = sortable('.sortable', 'serialize');
                const items = serialize[0].items;

                $.post({
                    url: '/inside/partner/reorder',
                    data: {
                        items: items
                    },
                    beforeSend: function() {
                        $('.overlay').show();
                    },
                    success: function(response){
                        $('.overlay').hide();
                        $('.sortable').find('.updated').removeClass('updated');
                    }
                });
            });
        });
    </script>
@endpush
