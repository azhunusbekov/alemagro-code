<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.partner.index') }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($partner) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.partner.destroy', $partner) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> <span>Сохранить</span></button>
        </div>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Заголовок</label>
            <input name="title" type="text" class="form-control" value="{{ isset($partner) ? $partner->title : old('title') }}">
            {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('link') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Ссылка (опционально)</label>
            <input name="link" type="text" class="form-control" value="{{ isset($partner) ? $partner->link : old('link') }}">
            {!! $errors->first('link','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('image') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Изображение</label>
            <input name="image" type="file" class="form-control" accept="image/jpg,image/jpeg,image/png">
            {!! $errors->first('image','<span class="invalid-feedback">:message</span>') !!}
            @if (isset($partner))
                <div class="image-preview">
                    <img src="{{ $partner->image }}" width="600">
                </div>
            @endif
        </div>
    </div>
</div>
