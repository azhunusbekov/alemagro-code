@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Вакансии</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.vacancy.index') }}">Все вакансии</a></li>
            <li><a href="{{ route('admin.vacancy.responses', $vacancy) }}">{{ $vacancy->title }}</a></li>
            <li><span>Отклик</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <a href="{{ route('admin.vacancy.responses', $vacancy) }}" class="btn btn-secondary">
                        <i class="fa fa-arrow-left"></i>
                        <span class="hidden-xs hidden-sm">Вернуться</span>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label class="col-form-label">Дата создания</label>
                    <input name="create_at" class="form-control" value="{{ $response->created_at->format('m.d.Y H:i:s') }}" disabled>
                </div>
                <div class="form-group">
                    @foreach($response->body as $key => $value)
                        <label class="col-form-label">{{ $key }}</label>
                        @if ($key == 'resume')
                            <p><a href="{{ $value }}" target="_blank">{{ $value }}</a></p>
                        @else
                            <input name="{{ $key }}" class="form-control" value="{{ $value }}" disabled>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection