<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.vacancy.index') }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($vacancy) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.vacancy.destroy', $vacancy) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> <span>Сохранить</span></button>
        </div>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            <label class="col-form-label">Статус</label>
            <select name="status" class="custom-select">
                @foreach ($statuses as $key => $status)
                    <option value="{{ $key }}" {{ isset($vacancy) && $vacancy->status == $key ? 'selected' : '' }}>{{ $status }}</option>
                @endforeach
            </select>
            {!! $errors->first('status','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('locale') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Язык</label>
            <select name="locale" class="form-control select2">
                @foreach ($locales as $langCode => $locale)
                    <option value="{{ $langCode }}"
                            {{ (isset($vacancy) && $vacancy->locale == $langCode) || old('locale') == $langCode ? 'selected' : '' }}>
                        {{ $locale['title'] }}
                    </option>
                @endforeach
            </select>
            {!! $errors->first('locale','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Заголовок</label>
            <input name="title" type="text" class="form-control" value="{{ isset($vacancy) ? $vacancy->title : old('title') }}">
            {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('description') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Описание</label>
            <textarea name="description" class="form-control editor" rows="3">{{ isset($vacancy) ? $vacancy->description : old('description') }}</textarea>
            {!! $errors->first('description','<span class="invalid-feedback">:message</span>') !!}
        </div>
    </div>
</div>