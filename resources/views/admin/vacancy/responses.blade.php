@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Вакансии - <i>{{ $vacancy->title }}</i> - отклики</h4>
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <a href="{{ route('admin.vacancy.index') }}" class="btn btn-secondary">
                        <i class="fa fa-arrow-left"></i>
                        <span class="hidden-xs hidden-sm">Вернуться</span>
                    </a>
                </div>
            </div>
            <div class="card-body">
                @if ($responses->isNotempty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="width: 50px;">ID</th>
                                        <th style="width: 200px;">Дата создания</th>
                                        <th style="width: 100px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($responses as $response)
                                        <tr data-id="{{ $response->id }}">
                                            <th>{{ $response->id }}</th>
                                            <td>{{ $response->created_at->format('m.d.Y') }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.vacancy.response.show', ['vacancy' => $vacancy, 'response' => $response]) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>
@endsection