@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Вакансии</h4>
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <a href="{{ route('admin.vacancy.create') }}" class="btn btn-primary">Создать новую вакансию</a>
                </div>
            </div>
            <div class="card-body">
                @if ($vacancies->isNotempty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="width: 50px;">ID</th>
                                        <th style="width: 300px;">Заголовок</th>
                                        <th style="width: 200px;">Статус</th>
                                        <th style="width: 200px;">Язык</th>
                                        <th style="width: 200px;">Дата создания</th>
                                        <th style="width: 200px;">Кол-во откликов</th>
                                        <th style="width: 100px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($vacancies as $vacancy)
                                        <tr data-id="{{ $vacancy->id }}">
                                            <th>{{ $vacancy->id }}</th>
                                            <td>{{ $vacancy->title }}</td>
                                            <td>
                                                <span class="badge {{ $vacancy->status == 0 ? 'badge-danger' : 'badge-success' }}">{{ $statuses[$vacancy->status] }}</span>
                                            </td>
                                            <td>
                                                <span class="badge">{{ locale_name($vacancy->locale) }}</span>
                                            </td>
                                            <td>{{ $vacancy->created_at->format('m.d.Y') }}</td>
                                            <td>{{ $vacancy->responses->count() }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.vacancy.edit', $vacancy) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a href="{{ route('admin.vacancy.responses', $vacancy) }}" class="btn btn-xs btn-info">
                                                        отклики
                                                    </a>
                                                    <button class="btn btn-xs btn-secondary btn-sort-handle">
                                                        <i class="fa fa-arrows"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function(){
            sortable('.sortable', {
                items: 'tr' ,
                forcePlaceholderSize: true,
                placeholderClass: 'sortable-placeholder',
                hoverClass: 'sortable-hover',
                handle: '.btn-sort-handle',
                itemSerializer: (serializedItem, sortableContainer) => {
                    return {
                        id: serializedItem.node.getAttribute('data-id'),
                        position:  serializedItem.index + 1
                    }
                }
            })[0].addEventListener('sortupdate', function(e) {
                const serialize = sortable('.sortable', 'serialize');
                const items = serialize[0].items;

                $.post({
                    url: '/inside/vacancy/reorder',
                    data: {
                        items: items
                    },
                    beforeSend: function() {
                        $('.overlay').show();
                    },
                    success: function(response){
                        $('.overlay').hide();
                        $('.sortable').find('.updated').removeClass('updated');
                    }
                });
            });
        });
    </script>
@endpush