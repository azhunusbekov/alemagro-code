@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Вакансии</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.vacancy.index') }}">Все вакансии</a></li>
            <li><span>Создать новыую вакансию</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.vacancy.store') }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @include('admin.vacancy._form', ['mode' => 'create'])
        </form>
    </div>
@endsection