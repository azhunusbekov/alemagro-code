<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.catalog.category.edit', $category) }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($category) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.catalog.category.attribute.destroy', [$category, $attribute]) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i><span>Сохранить</span></button>
        </div>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        <div class="form-group {{ $errors->has('locale') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Язык</label>
            <select name="locale" class="form-control select2">
                @foreach ($locales as $langCode => $locale)
                    <option value="{{ $langCode }}"
                        {{ (isset($attribute) && $attribute->locale == $langCode) || old('locale') == $langCode ? 'selected' : '' }}>
                        {{ $locale['title'] }}
                    </option>
                @endforeach
            </select>
            {!! $errors->first('locale','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('type') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Тип атрибута</label>
            <select name="type" class="form-control select2">
                @foreach ($types as $key => $value)
                    <option value="{{ $key }}" {{ isset($attribute) && $attribute->type == $key ? 'selected' : '' }}>{{ $value }}</option>
                @endforeach
            </select>
            {!! $errors->first('type','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('name') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Название</label>
            <input name="name" type="text" class="form-control" value="{{ isset($attribute) ? $attribute->name : old('name') }}">
            {!! $errors->first('name','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('options') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Варианты</label>
            <textarea name="options" class="form-control" rows="3">{{ isset($attribute) && !is_null($attribute->options) ? implode("\n", $attribute->options) : old('options') }}</textarea>
            {!! $errors->first('options','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('sort') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Позиция</label>
            <input name="sort"  type="text" class="form-control" value="{{ isset($attribute) ? $attribute->sort : old('sort') }}">
            {!! $errors->first('sort','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('required') ? 'is-invalid' : '' }}">
            <div class="checkbox">
                <label>
                    @if (isset($attribute))
                        <input type="checkbox" name="required" {{ $attribute->required ? 'checked' : '' }}>
                    @else
                        <input type="checkbox" name="required" {{ old('required') ? 'checked' : ''  }}>
                    @endif
                    Обязательный для заполнения
                </label>
            </div>
            {!! $errors->first('required','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('is_filtered') ? 'is-invalid' : '' }}">
            <div class="checkbox">
                <label>
                    @if (isset($attribute))
                        <input type="checkbox" name="is_filtered" {{ $attribute->is_filtered ? 'checked' : '' }}>
                    @else
                        <input type="checkbox" name="is_filtered" {{ old('is_filtered') ? 'checked' : ''  }}>
                    @endif
                    Показывать в фильтре товаров
                </label>
            </div>
            {!! $errors->first('is_filtered','<span class="invalid-feedback">:message</span>') !!}
        </div>
    </div>
</div>
