@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Новый атрибут для категории <i>{{ $category->title }}</i></h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.category.index') }}">Каталог - Все категории</a></li>
            <li><a href="{{ route('admin.catalog.category.edit', $category) }}">{{ $category->title }}</a></li>
            <li><span>Создать новый атрибут</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.category.attribute.store', $category) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @include('admin.catalog.attribute._form', ['mode' => 'create'])
        </form>
    </div>
@endsection