@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Защита и питание</h4>
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <a href="{{ route('admin.catalog.protection.create') }}" class="btn btn-primary">Создать новую программу</a>
                </div>
            </div>
            <div class="card-body">
                @if ($protections->isNotEmpty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="min-width: 60px;">ID</th>
                                        <th style="width: 50%;">Заголовок</th>
                                        <th style="width: 20%;">Производитель</th>
                                        <th style="width: 30%;">Культуры</th>
                                        <th style="min-width: 120px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($protections as $protection)
                                        <tr data-id="{{ $protection->id }}">
                                            <th>{{ $protection->id }}</th>
                                            <td>{{ $protection->title }}</td>
                                            <td>{{ $protection->producer->title }}</td>
                                            <td>
                                                @foreach($protection->cultures as $culture)
                                                    <span class="badge badge-info">{{ $culture->title }}</span>
                                                @endforeach
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.catalog.protection.edit', $protection) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>

    </script>
@endpush