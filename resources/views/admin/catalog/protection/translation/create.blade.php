@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Защита и питание</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.protection.index') }}">Каталог - Защита и питание</a></li>
            <li><a href="{{ route('admin.catalog.protection.edit', $protection) }}">{{ $protection->title }}</a></li>
            <li><span>Создать перевод</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.protection.translation.store', $protection) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @include('admin.catalog.protection.translation._form', ['mode' => 'create'])
        </form>
    </div>
@endsection