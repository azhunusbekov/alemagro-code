@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Защита и питание</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.protection.index') }}">Каталог - Защита и питание</a></li>
            <li><a href="{{ route('admin.catalog.protection.edit', $protection) }}">{{ $protection->title }}</a></li>
            <li><span>Перевод</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.protection.translation.update', [$protection, $protectionTranslation]) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.catalog.protection.translation._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection