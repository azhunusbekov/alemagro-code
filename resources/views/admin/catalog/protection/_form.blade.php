<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.catalog.protection.index') }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($protection) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.catalog.protection.destroy', $protection) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> <span>Сохранить</span></button>
        </div>
        @if (isset($protection))
            <div class="card-header__actions">
                @foreach($protection->translations as $translation)
                    <a href="{{ route('admin.catalog.protection.translation.edit', [$protection, $translation]) }}" class="btn btn-outline-info"><span>{{ locale_name($translation->locale) }}</span></a>
                @endforeach
                <a href="{{ route('admin.catalog.protection.translation.create', $protection) }}" class="btn btn-info"><i class="fa fa-language"></i><span>Добавить перевод</span></a>
            </div>
        @endif
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            <label class="col-form-label">Статус</label>
            <select name="status" class="custom-select">
                @foreach ($statuses as $key => $status)
                    <option value="{{ $key }}" {{ isset($protection) && $protection->status == $key ? 'selected' : '' }}>{{ $status }}</option>
                @endforeach
            </select>
            {!! $errors->first('status','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            <label class="col-form-label">Назначение</label>
            <select name="purpose" class="custom-select">
                @foreach ($purposes as $key => $purpose)
                    <option value="{{ $key }}" {{ isset($protection) && $protection->purpose == $key ? 'selected' : '' }}>{{ $purpose }}</option>
                @endforeach
            </select>
            {!! $errors->first('purpose','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('producer_id') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Производитель</label>
            <select name="producer_id" class="form-control select2" data-placeholder="Выбрать производителя">
                <option></option>
                @foreach($producers as $producer)
                    <option value="{{ $producer->id }}"
                            {{ old('producer_id') == $producer->id ? 'selected' : '' }}
                        {{ isset($protection) && $protection->producer_id == $producer->id ? 'selected' : '' }}>
                        {{ $producer->title }}
                    </option>
                @endforeach
            </select>
            {!! $errors->first('producer_id','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('cultures') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Культуры</label>
            <select name="cultures[]" class="form-control select2" data-placeholder="Выбрать культуры" multiple>
                <option></option>
                @foreach($cultures as $culture)
                    <option value="{{ $culture->id }}"
                            {{ isset($protection) && $protection->cultures->keyBy('id')->has($culture->id) ? 'selected' : '' }}>
                        {{ $culture->title }}
                    </option>
                    @if ($culture->children->count())
                        @foreach($culture->children as $child)
                            <option value="{{ $child->id }}"
                                    {{ isset($protection) && $protection->cultures->keyBy('id')->has($child->id) ? 'selected' : '' }}>
                                -- {{ $child->title }}
                            </option>
                        @endforeach
                    @endif
                @endforeach
            </select>
            {!! $errors->first('cultures','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Заголовок</label>
            <input name="title" type="text" class="form-control" value="{{ isset($protection) ? $protection->title : old('title') }}">
            {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('src') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Файл</label>
            <input name="src" type="file" class="form-control" accept="application/pdf">
            {!! $errors->first('src','<span class="invalid-feedback">:message</span>') !!}
            @if (isset($protection))
                <div class="file-preview">
                    <a href="{{ $protection->src }}" target="_blank">{{ $protection->src }}</a>
                </div>
            @endif
        </div>
    </div>
</div>