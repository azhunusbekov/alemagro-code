@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Новый производитель</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.producer.index') }}">Каталог - Все производители</a></li>
            <li><span>Создать нового производителя</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.producer.store') }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @include('admin.catalog.producer._form', ['mode' => 'create'])
        </form>
    </div>
@endsection