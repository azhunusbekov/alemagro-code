@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Производители</h4>
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <a href="{{ route('admin.catalog.producer.create') }}" class="btn btn-primary">Создать нового производителя</a>
                </div>
            </div>
            <div class="card-body">
                @if ($producers->isNotempty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="min-width: 60px;">ID</th>
                                        <th style="width: 100%;">Заголовок</th>
                                        <th style="min-width: 100px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($producers as $producer)
                                        <tr data-id="{{ $producer->id }}">
                                            <th>{{ $producer->id }}</th>
                                            <td>{{ $producer->title }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.catalog.producer.edit', $producer) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <button class="btn btn-xs btn-secondary btn-sort-handle">
                                                        <i class="fa fa-arrows"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function(){
            sortable('.sortable', {
                items: 'tr' ,
                forcePlaceholderSize: true,
                placeholderClass: 'sortable-placeholder',
                hoverClass: 'sortable-hover',
                handle: '.btn-sort-handle',
                itemSerializer: (serializedItem, sortableContainer) => {
                    return {
                        id: serializedItem.node.getAttribute('data-id'),
                        position:  serializedItem.index + 1
                    }
                }
            })[0].addEventListener('sortupdate', function(e) {
                const serialize = sortable('.sortable', 'serialize');
                const items = serialize[0].items;

                $.post({
                    url: '/inside/catalog/producer/reorder',
                    data: {
                        items: items
                    },
                    beforeSend: function() {
                        $('.overlay').show();
                    },
                    success: function(response){
                        $('.overlay').hide();
                        $('.sortable').find('.updated').removeClass('updated');
                    }
                });
            });
        });
    </script>
@endpush