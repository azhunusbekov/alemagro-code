@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Культуры</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.culture.index') }}">Каталог - Все культуры</a></li>
            <li><a href="{{ route('admin.catalog.culture.edit', $culture) }}">{{ $culture->title }}</a></li>
            <li><span>Перевод</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.culture.translation.update', [$culture, $cultureTranslation]) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.catalog.culture.translation._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection