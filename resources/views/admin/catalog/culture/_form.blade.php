<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.catalog.culture.index') }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($culture) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.catalog.culture.destroy', $culture) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> <span>Сохранить</span></button>
        </div>
        @if (isset($culture))
            <div class="card-header__actions">
                @foreach($culture->translations as $translation)
                    <a href="{{ route('admin.catalog.culture.translation.edit', [$culture, $translation]) }}" class="btn btn-outline-info"><span>{{ locale_name($translation->locale) }}</span></a>
                @endforeach
                <a href="{{ route('admin.catalog.culture.translation.create', $culture) }}" class="btn btn-info"><i class="fa fa-language"></i><span>Добавить перевод</span></a>
            </div>
        @endif
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        @if (isset($parent))
            <input type="hidden" name="parent_id" value="{{ $parent->id }}">
        @endif
        @if (isset($culture) && $culture->parent)
            <div class="form-check {{ $errors->has('is_favorite') ? 'is-invalid' : '' }}">
                <input name="is_favorite"
                       type="checkbox"
                       id="is_favorite"
                       class="form-check-input"
                    {{ (isset($culture) && $culture->is_favorite) || old('is_favorite') ? 'checked' : '' }}>
                <label class="form-check-label" for="is_favorite">Избранное</label>
                {!! $errors->first('is_favorite','<span class="invalid-feedback">:message</span>') !!}
            </div>
        @endif
        <div class="form-check {{ $errors->has('on_page') ? 'is-invalid' : '' }}">
            <input name="on_page"
                   type="checkbox"
                   id="on_page"
                   class="form-check-input"
                    {{ (isset($culture) && $culture->on_page) || old('on_page') ? 'checked' : '' }}>
            <label class="form-check-label" for="on_page">Ссылка на странице</label>
            {!! $errors->first('on_page','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Заголовок</label>
            <input name="title" type="text" class="form-control" value="{{ isset($culture) ? $culture->title : old('title') }}">
            {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('slug') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Slug (алиас в URL)</label>
            <input name="slug" type="text" class="form-control" value="{{ isset($culture) ? $culture->slug : old('slug') }}">
            {!! $errors->first('slug','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('description') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Описание</label>
            <textarea name="description" class="form-control editor" rows="10">{{ isset($culture) ? $culture->description : old('title') }}</textarea>
            {!! $errors->first('description','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('icon') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Иконка</label>
            <input name="icon" type="file" class="form-control" accept="image/jpg,image/jpeg,image/png">
            {!! $errors->first('icon','<span class="invalid-feedback">:message</span>') !!}
            @if (isset($culture))
                <div class="image-preview">
                    <img src="{{ $culture->icon }}" width="150">
                </div>
            @endif
        </div>
        <div class="form-group {{ $errors->has('image') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Изображение</label>
            <input name="image" type="file" class="form-control" accept="image/jpg,image/jpeg,image/png">
            {!! $errors->first('image','<span class="invalid-feedback">:message</span>') !!}
            @if (isset($culture))
                <div class="image-preview">
                    <img src="{{ $culture->image }}" width="300">
                </div>
            @endif
        </div>
        <div class="form-group {{ $errors->has('image_header') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Фоновое изображение для шапки</label>
            <input name="image_header" type="file" class="form-control" accept="image/jpg,image/jpeg,image/png">
            {!! $errors->first('image_header','<span class="invalid-feedback">:message</span>') !!}
            @if (isset($culture))
                <div class="image-preview">
                    <img src="{{ $culture->image_header }}" width="300">
                </div>
            @endif
        </div>
        @include('admin.partial.seo_fields', ['seoData' => isset($seoData) ? $seoData : null])
    </div>
</div>