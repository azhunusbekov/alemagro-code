@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Культуры</h4>
        @if ($parent)
            <ul class="breadcrumbs">
                <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                <li><a href="{{ route('admin.catalog.culture.index') }}">Каталог - Все культуры</a></li>
                <li><span>{{ $parent->title }}</span></li>
            </ul>
        @endif
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <a href="{{ route('admin.catalog.culture.create', ['parent_id' => $parent]) }}" class="btn btn-primary">Создать новую культуру</a>
                </div>
            </div>
            <div class="card-body">
                @if ($cultures->isNotEmpty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="min-width: 60px;">ID</th>
                                        <th style="width: 100%;">Заголовок</th>
                                        <th style="min-width: 200px;">Избранное</th>
                                        <th style="min-width: 200px;">Подкультуры</th>
                                        <th style="min-width: 120px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($cultures as $culture)
                                        <tr data-id="{{ $culture->id }}">
                                            <th>{{ $culture->id }}</th>
                                            <td>
                                                @if ($culture->children->count())
                                                    <a href="{{ route('admin.catalog.culture.index', ['parent_id' => $culture]) }}">
                                                        {{ $culture->title }}
                                                    </a>
                                                @else
                                                    {{ $culture->title }}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($culture->isFavorite())
                                                    <span class="badge badge-success badge-pill">Избранное</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($culture->children->count())
                                                    <span class="badge badge-primary badge-pill">{{ $culture->children->count() }}</span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.catalog.culture.edit', $culture) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a href="{{ route('admin.catalog.culture.index', ['parent_id' => $culture]) }}" class="btn btn-xs btn-warning">
                                                        <i class="fa fa-list"></i>
                                                    </a>
                                                    <button class="btn btn-xs btn-secondary btn-sort-handle">
                                                        <i class="fa fa-arrows"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
		$(function(){
            sortable('.sortable', {
                items: 'tr' ,
                forcePlaceholderSize: true,
                placeholderClass: 'sortable-placeholder',
                hoverClass: 'sortable-hover',
                handle: '.btn-sort-handle',
                itemSerializer: (serializedItem, sortableContainer) => {
                    return {
                        id: serializedItem.node.getAttribute('data-id'),
                        position:  serializedItem.index + 1
                    }
                }
            })[0].addEventListener('sortupdate', function(e) {
                const serialize = sortable('.sortable', 'serialize');
                const items = serialize[0].items;

                $.post({
                    url: '/inside/catalog/culture/reorder',
                    data: {
                        items: items
                    },
                    beforeSend: function() {
                        $('.overlay').show();
                    },
                    success: function(response){
                        $('.overlay').hide();
                        $('.sortable').find('.updated').removeClass('updated');
                    }
                });
            });
		});
    </script>
@endpush