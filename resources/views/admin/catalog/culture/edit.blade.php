@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Культуры - <i>{{ $culture->title }}</i></h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.culture.index') }}">Каталог - Все культуры</a></li>
            @if ($culture->parent)
                <li><a href="{{ route('admin.catalog.culture.index', ['parent_id' => $culture->parent]) }}">{{ $culture->parent->title }}</a></li>
            @endif
            <li><span>{{ $culture->title }}</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.culture.update', $culture) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.catalog.culture._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection