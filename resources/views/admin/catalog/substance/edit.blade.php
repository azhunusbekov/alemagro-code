@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Действующие вещества - <i>{{ $substance->title }}</i></h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.substance.index') }}">Каталог - Действующие вещества</a></li>
            <li><span>{{ $substance->title }}</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.substance.update', $substance) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.catalog.substance._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection