@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Действующие вещества</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.substance.index') }}">Каталог - Действующие вещества</a></li>
            <li><a href="{{ route('admin.catalog.substance.edit', $substance) }}">{{ $substance->title }}</a></li>
            <li><span>Перевод</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.substance.translation.update', [$substance, $substanceTranslation]) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.catalog.substance.translation._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection