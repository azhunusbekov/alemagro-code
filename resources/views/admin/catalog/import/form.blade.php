@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Импорт</h4>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.import.store') }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            <div class="card">
                <div class="card-header">
                    <div class="card-header__actions">
                        <a href="{{ route('admin.catalog.producer.index') }}" class="btn btn-secondary">
                            <i class="fa fa-arrow-left"></i>
                            <span class="hidden-xs hidden-sm">Вернуться</span>
                        </a>
                        <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> <span>Загрузить</span></button>
                    </div>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">Ошибка валидации</div>
                    @endif
                    <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
                        <label class="col-form-label">Файл</label>
                        <input name="file" type="file" class="form-control">
                        {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
                        <small class="form-text text-muted">Файл должен быть только Excel формата, не больше 1 мб.</small>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection