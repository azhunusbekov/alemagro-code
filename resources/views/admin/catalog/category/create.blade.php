@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Новая категория</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.category.index') }}">Каталог - Все категории</a></li>
            <li><span>Создать новую категорию</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.category.store') }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @include('admin.catalog.category._form', ['mode' => 'create'])
        </form>
    </div>
@endsection