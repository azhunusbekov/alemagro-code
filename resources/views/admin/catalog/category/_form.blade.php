<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.catalog.category.index') }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($category) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.catalog.category.destroy', $category) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i><span>Сохранить</span></button>
        </div>
        @if (isset($category))
            <div class="card-header__actions">
                @foreach($category->translations as $translation)
                    <a href="{{ route('admin.catalog.category.translation.edit', [$category, $translation]) }}" class="btn btn-outline-info"><span>{{ locale_name($translation->locale) }}</span></a>
                @endforeach
                <a href="{{ route('admin.catalog.category.translation.create', $category) }}" class="btn btn-info"><i class="fa fa-language"></i><span>Добавить перевод</span></a>
            </div>
        @endif
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            <label class="col-form-label">Статус</label>
            <select name="status" class="custom-select">
                @foreach ($statuses as $key => $status)
                    <option value="{{ $key }}"
                        {{ (isset($category) && $category->status == $key) || old('status') == $key? 'selected' : '' }}>
                        {{ $status }}
                    </option>
                @endforeach
            </select>
            {!! $errors->first('status','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Заголовок</label>
            <input name="title" type="text" class="form-control" value="{{ isset($category) ? $category->title : old('title') }}">
            {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('slug') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Slug (алиас в URL)</label>
            <input name="slug" type="text" class="form-control" value="{{ isset($category) ? $category->slug : old('slug') }}">
            {!! $errors->first('slug','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('description') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Описание</label>
            <textarea name="description" class="form-control editor" rows="10">{{ isset($category) ? $category->description : old('title') }}</textarea>
            {!! $errors->first('description','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('icon') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Иконка</label>
            <input name="icon" type="file" class="form-control" accept="image/jpg,image/jpeg,image/png">
            {!! $errors->first('icon','<span class="invalid-feedback">:message</span>') !!}
            @if (isset($category))
                <div class="image-preview">
                    <img src="{{ $category->icon }}" width="150">
                </div>
            @endif
        </div>
        <div class="form-group {{ $errors->has('image_header') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Фоновое изображение для шапки</label>
            <input name="image_header" type="file" class="form-control" accept="image/jpg,image/jpeg,image/png">
            {!! $errors->first('image_header','<span class="invalid-feedback">:message</span>') !!}
            @if (isset($category))
                <div class="image-preview">
                    <img src="{{ $category->image_header }}" width="300">
                </div>
            @endif
        </div>
        @include('admin.partial.seo_fields', ['seoData' => isset($seoData) ? $seoData : null])
    </div>
</div>