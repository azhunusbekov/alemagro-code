<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.catalog.category.attribute.create', $category) }}" class="btn btn-info">Создать новый атрибут</a>
        </div>
    </div>
    <div class="card-body">
        @if ($attributes->isNotEmpty())
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="text-uppercase bg-light">
                        <tr>
                            <th style="min-width: 60px;">ID</th>
                            <th style="width: 90%;">Заголовок</th>
                            <th style="width: 10%;">Язык</th>
                            <th style="min-width: 120px;">Действия</th>
                        </tr>
                        </thead>
                        <tbody class="sortable">
                            @foreach($attributes as $attribute)
                                <tr data-id="{{ $attribute->id }}">
                                    <th>{{ $attribute->id }}</th>
                                    <td>{{ $attribute->name }}</td>
                                    <th>{{ $attribute->locale }}</th>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('admin.catalog.category.attribute.edit', [$category, $attribute]) }}"
                                               class="btn btn-xs btn-primary">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <button class="btn btn-xs btn-secondary btn-sort-handle" draggable="true">
                                                <i class="fa fa-arrows"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @else
            <h4 class="header-title">Empty data</h4>
        @endif
    </div>
</div>

@push('scripts')
    <script>
        $(function(){
            sortable('.sortable', {
                items: 'tr' ,
                forcePlaceholderSize: true,
                placeholderClass: 'sortable-placeholder',
                hoverClass: 'sortable-hover',
                handle: '.btn-sort-handle',
                itemSerializer: (serializedItem, sortableContainer) => {
                    return {
                        id: serializedItem.node.getAttribute('data-id'),
                        position:  serializedItem.index + 1
                    }
                }
            })[0].addEventListener('sortupdate', function(e) {
                const serialize = sortable('.sortable', 'serialize');
                const items = serialize[0].items;

                $.post({
                    url: '/inside/catalog/attribute/reorder',
                    data: {
                        items: items
                    },
                    beforeSend: function() {
                        $('.overlay').show();
                    },
                    success: function(response){
                        $('.overlay').hide();
                        $('.sortable').find('.updated').removeClass('updated');
                    }
                });
            });
        });
    </script>
@endpush