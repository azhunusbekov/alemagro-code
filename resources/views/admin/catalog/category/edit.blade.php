@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Категория - <i>{{ $category->title }}</i></h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.category.index') }}">Каталог - Все категории</a></li>
            <li><span>{{ $category->title }}</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <ul class="nav nav-pills nav-cards" role="tablist">
            <li class="nav-item">
                <a class="nav-link active show" data-toggle="tab" href="#main" role="tab" aria-controls="home" aria-selected="true">Основное</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#attributes" role="tab" aria-controls="profile" aria-selected="false">Атрибуты</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active show" id="main" role="tabpanel" aria-labelledby="home-tab">
                <form action="{{ route('admin.catalog.category.update', $category) }}" enctype="multipart/form-data" method="POST">
                    {{ csrf_field() }}
                    @method('PUT')
                    @include('admin.catalog.category._form', ['mode' => 'edit'])
                </form>
            </div>
            <div class="tab-pane fade" id="attributes" role="tabpanel" aria-labelledby="profile-tab">
                @include('admin.catalog.category._attributes')
            </div>
        </div>
    </div>
@endsection