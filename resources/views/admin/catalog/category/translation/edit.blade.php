@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Категории</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.category.index') }}">Каталог - Все категории</a></li>
            <li><a href="{{ route('admin.catalog.category.edit', $category) }}">{{ $category->title }}</a></li>
            <li><span>Перевод</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.category.translation.update', [$category, $categoryTranslation]) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.catalog.category.translation._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection