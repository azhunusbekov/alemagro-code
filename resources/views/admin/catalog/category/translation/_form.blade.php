<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.catalog.category.edit', $category) }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($categoryTranslation) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.catalog.category.translation.destroy', [$category, $categoryTranslation]) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить перевод</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> <span>Сохранить</span></button>
        </div>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        <div class="form-group {{ $errors->has('locale') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Язык</label>
            <select name="locale" class="form-control select2">
                @foreach ($locales as $langCode => $locale)
                    <option value="{{ $langCode }}"
                        {{ (isset($categoryTranslation) && $categoryTranslation->locale == $langCode) || old('locale') == $langCode ? 'selected' : '' }}>
                        {{ $locale['title'] }}
                    </option>
                @endforeach
            </select>
            {!! $errors->first('locale','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            <label class="col-form-label">Статус</label>
            <select name="status" class="custom-select">
                @foreach ($statuses as $key => $status)
                    <option value="{{ $key }}"
                        {{ (isset($categoryTranslation) && $categoryTranslation->status == $key) || old('status') == $key? 'selected' : '' }}>
                        {{ $status }}
                    </option>
                @endforeach
            </select>
            {!! $errors->first('status','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Заголовок</label>
            <input name="title" type="text" class="form-control" value="{{ isset($categoryTranslation) ? $categoryTranslation->title : old('title') }}">
            {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('slug') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Slug (алиас в URL)</label>
            <input name="slug" type="text" class="form-control" value="{{ isset($categoryTranslation) ? $categoryTranslation->slug : old('slug') }}">
            {!! $errors->first('slug','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('description') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Описание</label>
            <textarea name="description" class="form-control" rows="3">{{ isset($categoryTranslation) ? $categoryTranslation->description : old('description') }}</textarea>
            {!! $errors->first('description','<span class="invalid-feedback">:message</span>') !!}
        </div>
        @include('admin.partial.seo_fields', ['seoData' => isset($seoData) ? $seoData : null])
    </div>
</div>