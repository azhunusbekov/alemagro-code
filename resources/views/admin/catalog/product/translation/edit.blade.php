@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Товары</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.product.index') }}">Каталог - Товары</a></li>
            <li><a href="{{ route('admin.catalog.product.edit', $product) }}">{{ $product->title }}</a></li>
            <li><span>Перевод</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.product.translation.update', [$product, $productTranslation]) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.catalog.product.translation._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection