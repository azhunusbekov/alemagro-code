<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.catalog.product.edit', $product) }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($productTranslation) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.catalog.product.translation.destroy', [$product, $productTranslation]) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить перевод</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> <span>Сохранить</span></button>
        </div>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        <div class="form-group {{ $errors->has('locale') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Язык</label>
            <select name="locale" class="form-control select2">
                @foreach ($locales as $langCode => $locale)
                    <option value="{{ $langCode }}"
                        {{ (isset($productTranslation) && $productTranslation->locale == $langCode) || old('locale') == $langCode ? 'selected' : '' }}>
                        {{ $locale['title'] }}
                    </option>
                @endforeach
            </select>
            {!! $errors->first('locale','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Заголовок</label>
            <input name="title" type="text" class="form-control" value="{{ isset($productTranslation) ? $productTranslation->title : old('title') }}">
            {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('slug') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Slug (алиас в URL)</label>
            <input name="slug" type="text" class="form-control" value="{{ isset($productTranslation) ? $productTranslation->slug : old('slug') }}">
            {!! $errors->first('slug','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('composition') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Состав</label>
            <textarea name="composition" class="form-control" rows="3">{{ isset($productTranslation) ? $productTranslation->composition : old('composition') }}</textarea>
            {!! $errors->first('composition','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('description') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Описание</label>
            <textarea name="description" class="form-control editor" rows="5">{{ isset($productTranslation) ? $productTranslation->description : old('description') }}</textarea>
            {!! $errors->first('description','<span class="invalid-feedback">:message</span>') !!}
        </div>
        @include('admin.partial.seo_fields', ['seoData' => isset($seoData) ? $seoData : null])
    </div>
</div>
