@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Товары</h4>
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#category-select">Создать новый товар</button>
                </div>
                <div class="card-header__filter">
                    <form action="{{ route('admin.catalog.product.index') }}" method="get">
                        <div class="form-group">
                            <select name="producer_id" class="form-control select2">
                                <option value="">Все производители</option>
                                @foreach($producers as $producer)
                                    <option value="{{ $producer->id }}"
                                        {!! request()->get('producer_id') == $producer->id ? 'selected' : '' !!}>
                                        {{ $producer->title }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="category_id" class="form-control select2">
                                <option value="">Все категории</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}"
                                        {!! request()->get('category_id') == $category->id ? 'selected' : '' !!}>
                                        {{ $category->title }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-warning" type="submit">Фильтр</button>
                    </form>
                </div>
            </div>
            <div class="card-body">
                @if ($products->isNotEmpty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="min-width: 60px;">ID</th>
                                        <th style="width: 40%;">Заголовок</th>
                                        <th style="width: 20%;">Категория</th>
                                        <th style="width: 20%;">Раздел</th>
                                        <th style="width: 20%;">Производитель</th>
                                        <th style="min-width: 120px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($products as $product)
                                        <tr data-id="{{ $product->id }}">
                                            <th>{{ $product->id }}</th>
                                            <td>{{ $product->title }}</td>
                                            <td>{{ $product->category->title }}</td>
                                            <td>
                                                @if ($product->section)
                                                    @if ($product->section->parent)
                                                        {{ $product->section->parent->title . ' - ' }}
                                                    @endif
                                                    {{ $product->section->title }}
                                                @endif
                                            </td>
                                            <td>{{ $product->producer->title }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.catalog.product.edit', $product) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $products->links() }}
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>

    <div class="modal fade" id="category-select" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Выберите категорию</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                <div class="modal-body">
                    <ul class="list-group">
                        @foreach($categories as $category)
                            <a href="{{ route('admin.catalog.product.create', $category) }}" class="list-group-item list-group-item-action">{{ $category->title }}</a>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection