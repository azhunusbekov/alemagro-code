<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.catalog.product.index') }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($product) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.catalog.product.destroy', $product) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> <span>Сохранить</span></button>
        </div>
        @if (isset($product))
            <div class="card-header__actions">
                @foreach($product->translations as $translation)
                    <a href="{{ route('admin.catalog.product.translation.edit', [$product, $translation]) }}" class="btn btn-outline-info"><span>{{ locale_name($translation->locale) }}</span></a>
                @endforeach
                <a href="{{ route('admin.catalog.product.translation.create', $product) }}" class="btn btn-info"><i class="fa fa-language"></i><span>Добавить перевод</span></a>
            </div>
        @endif
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Основное</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#attributes" role="tab" aria-controls="profile" aria-selected="false">Атрибуты</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#images" role="tab" aria-controls="contact" aria-selected="false">Изображения</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#protection" role="tab" aria-controls="contact" aria-selected="false">Программа защиты</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active show" id="main" role="tabpanel">
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label class="col-form-label">Статус</label>
                    <select name="status" class="custom-select">
                        @foreach ($statuses as $key => $status)
                            <option value="{{ $key }}" {{ isset($product) && $product->status == $key ? 'selected' : '' }}>{{ $status }}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('status','<span class="invalid-feedback">:message</span>') !!}
                </div>
                <div class="form-group {{ $errors->has('available') ? 'is-invalid' : '' }}">
                    <label class="col-form-label">Достуность</label>
                    <select name="available[]" class="form-control select2" data-placeholder="Выбрать страну" multiple>
                        <option></option>
                        @foreach ($availables as $key => $available)
                            <option value="{{ $key }}"
                                {{ !empty($product->available) && in_array($key, $product->available) ? 'selected' : '' }}
                                {{ old('available') && in_array($key, old('available')) ? 'selected' : '' }}>
                                {{ $available }}
                            </option>
                        @endforeach
                    </select>
                    {!! $errors->first('producer_id','<span class="invalid-feedback">:message</span>') !!}
                </div>
                <div class="form-group {{ $errors->has('category_id') ? 'is-invalid' : '' }}">
                    <label class="col-form-label">Категория</label>
                    <select name="category_id" class="form-control select2" data-placeholder="Выбрать категорию">
                        @if (isset($product))
                            <option value="{{ $product->category->id }}" selected>{{ $product->category->title }}</option>
                        @else
                            <option value="{{ $category->id }}" selected>{{ $category->title }}</option>
                        @endif
                    </select>
                    {!! $errors->first('category_id','<span class="invalid-feedback">:message</span>') !!}
                </div>
                @if (count($sections))
                    <div class="form-group {{ $errors->has('section_id') ? 'is-invalid' : '' }}">
                        <label class="col-form-label">Раздел</label>
                        <select name="section_id" class="form-control select2" data-placeholder="Выбрать раздел">
                            <option></option>
                            @foreach($sections as $section)
                                <option value="{{ $section->id }}"
                                    {{ isset($product) && $product->section_id == $section->id ? 'selected' : '' }}
                                    {!! $section->children->count() ? 'disabled' : '' !!}>
                                    {{ $section->title }}
                                </option>
                                @if ($section->children->count())
                                    @foreach($section->children as $child)
                                        <option value="{{ $child->id }}"
                                            {{ isset($product) && $product->section_id == $child->id ? 'selected' : '' }}>
                                            -- {{ $child->title }}
                                        </option>
                                    @endforeach
                                @endif
                            @endforeach
                        </select>
                        {!! $errors->first('section_id','<span class="invalid-feedback">:message</span>') !!}
                    </div>
                @endif
                <div class="form-group {{ $errors->has('cultures') ? 'is-invalid' : '' }}">
                    <label class="col-form-label">Культуры</label>
                    <select name="cultures[]" class="form-control select2" data-placeholder="Выбрать культуры" multiple>
                        <option></option>
                        @foreach($cultures as $culture)
                            @include('admin.catalog.product.partials._culture_option', ['culture' => $culture, 'depth' => ''])
                        @endforeach
                    </select>
                    {!! $errors->first('cultures','<span class="invalid-feedback">:message</span>') !!}
                </div>
                @if (count($diseases))
                    <div class="form-group {{ $errors->has('diseases') ? 'is-invalid' : '' }}">
                        <label class="col-form-label">Вредные объекты, болезни</label>
                        <select name="diseases[]" class="form-control select2" data-placeholder="Выбрать вредные объекты" multiple>
                            <option></option>
                            @foreach($diseases as $disease)
                                <option value="{{ $disease->id }}" {{ isset($product) && $product->diseases->keyBy('id')->has($disease->id) ? 'selected' : '' }}>
                                    {{ $disease->title }}
                                </option>
                            @endforeach
                        </select>
                        {!! $errors->first('diseases','<span class="invalid-feedback">:message</span>') !!}
                    </div>
                @endif
                @if (count($substances))
                    <div class="form-group {{ $errors->has('substances') ? 'is-invalid' : '' }}">
                        <label class="col-form-label">Действующее вещество</label>
                        <select name="substances[]" class="form-control select2" data-placeholder="Действующее вещество" multiple>
                            <option></option>
                            @foreach($substances as $substance)
                                <option value="{{ $substance->id }}" {{ isset($product) && $product->substances->keyBy('id')->has($substance->id) ? 'selected' : '' }}>
                                    {{ $substance->title }}
                                </option>
                            @endforeach
                        </select>
                        {!! $errors->first('substances','<span class="invalid-feedback">:message</span>') !!}
                    </div>
                @endif
                <div class="form-group {{ $errors->has('producer_id') ? 'is-invalid' : '' }}">
                    <label class="col-form-label">Производитель</label>
                    <select name="producer_id" class="form-control select2" data-placeholder="Выбрать производителя">
                        <option></option>
                        @foreach($producers as $producer)
                            <option value="{{ $producer->id }}"
                                {{ isset($product) && $product->producer_id == $producer->id ? 'selected' : '' }}
                                {{ old('producer_id') == $producer->id ? 'selected' : '' }}>
                                {{ $producer->title }}
                            </option>
                        @endforeach
                    </select>
                    {!! $errors->first('producer_id','<span class="invalid-feedback">:message</span>') !!}
                </div>
                <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
                    <label class="col-form-label">Заголовок</label>
                    <input name="title" type="text" class="form-control" value="{{ isset($product) ? $product->title : old('title') }}">
                    {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
                </div>
                <div class="form-group {{ $errors->has('slug') ? 'is-invalid' : '' }}">
                    <label class="col-form-label">Slug (алиас в URL)</label>
                    <input name="slug" type="text" class="form-control" value="{{ isset($product) ? $product->slug : old('slug') }}">
                    {!! $errors->first('slug','<span class="invalid-feedback">:message</span>') !!}
                </div>
                <div class="form-group {{ $errors->has('vendor_code') ? 'is-invalid' : '' }}">
                    <label class="col-form-label">Артикул товара</label>
                    <input name="vendor_code" type="text" class="form-control" value="{{ isset($product) ? $product->vendor_code : old('vendor_code') }}">
                    {!! $errors->first('vendor_code','<span class="invalid-feedback">:message</span>') !!}
                </div>
                <div class="form-group {{ $errors->has('composition') ? 'is-invalid' : '' }}">
                    <label class="col-form-label">Состав</label>
                    <textarea name="composition" class="form-control" rows="3">{{ isset($product) ? $product->composition : old('composition') }}</textarea>
                    {!! $errors->first('composition','<span class="invalid-feedback">:message</span>') !!}
                </div>
                <div class="form-group {{ $errors->has('description') ? 'is-invalid' : '' }}">
                    <label class="col-form-label">Описание</label>
                    <textarea name="description" class="form-control editor" rows="5">{{ isset($product) ? $product->description : old('description') }}</textarea>
                    {!! $errors->first('description','<span class="invalid-feedback">:message</span>') !!}
                </div>
                @include('admin.partial.seo_fields', ['seoData' => isset($seoData) ? $seoData : null])
            </div>
            <div class="tab-pane fade" id="attributes" role="tabpanel">
                @if ($attributes->count())
                    @foreach($attributes as $attribute)
                        <div class="form-group {{ $errors->has('attributes.' . $attribute->id) ? 'is-invalid' : '' }}">
                            <label class="col-form-label">{{ $attribute->name }} -- ({{ $attribute->locale }})</label>
                            @if ($attribute->isSelect())
                                <select name="attributes[{{ $attribute->id }}]" class="form-control select2" data-placeholder="Выбрать">
                                    <option></option>
                                    @foreach ($attribute->options as $option)
                                        <option value="{{ $option }}"
                                            {{ old('attributes.' . $attribute->id) ? ' selected' : '' }}
                                            {{ isset($product) && $product->getValueOfAttribute($attribute->id) == $option ? 'selected' : '' }}>
                                            {{ $option }}
                                        </option>
                                    @endforeach
                                </select>
                            @elseif ($attribute->isNumber())
                                <input type="number"
                                       name="attributes[{{ $attribute->id }}]"
                                       class="form-control"
                                       value="{{ isset($product) ? $product->getValueOfAttribute($attribute->id) : old('attributes.' . $attribute->id) }}">
                            @else
                                <input type="text"
                                       name="attributes[{{ $attribute->id }}]"
                                       class="form-control"
                                       value="{{ isset($product) ? $product->getValueOfAttribute($attribute->id) : old('attributes.' . $attribute->id) }}">
                            @endif
                            {!! $errors->first('attributes.' . $attribute->id,'<span class="invalid-feedback">:message</span>') !!}
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="tab-pane fade" id="images" role="tabpanel">
                <div class="form-group {{ $errors->has('preview') ? 'is-invalid' : '' }}">
                    <label class="col-form-label">Превью-изображение</label>
                    <input name="preview" type="file" class="form-control" accept="image/jpg,image/jpeg,image/png">
                    {!! $errors->first('preview','<span class="invalid-feedback">:message</span>') !!}
                    @if (isset($product))
                        <div class="image-preview">
                            <img src="{{ $product->preview }}" width="300">
                        </div>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('photos') ? 'is-invalid' : '' }}">
                    <label class="col-form-label">Галерея изображении</label>
                    <input name="photos[]" type="file" class="form-control" accept="image/jpg,image/jpeg,image/png" multiple>
                    {!! $errors->first('photos.*','<span class="invalid-feedback">:message</span>') !!}
                    @if (isset($product))
                        <div class="gallery">
                            @foreach($product->photos as $photo)
                                <div class="gallery__item">
                                    <img src="{{ $photo->src }}">
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="tab-pane fade" id="protection" role="tabpanel">
                <div class="form-group {{ $errors->has('protection') ? 'is-invalid' : '' }}">
                    <label class="col-form-label">Программа защиты</label>
                    <select name="protection" class="form-control select2" data-placeholder="Выбрать">
                        <option></option>
                        @foreach ($protections as $protection)
                            <option value="{{ $protection->id }}"
                                {{ old('protection.') == $protection->id ? ' selected' : '' }}
                                {{ isset($product) && $product->protection->keyBy('id')->has($protection->id) ? 'selected' : '' }}>
                                {{ $protection->title }}
                            </option>
                        @endforeach
                    </select>
                    {!! $errors->first('protection', '<span class="invalid-feedback">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(function() {

        });
    </script>
@endpush
