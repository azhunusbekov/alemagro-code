<option value="{{ $culture->id }}"
    {{ isset($product) && $product->cultures->keyBy('id')->has($culture->id) ? 'selected' : '' }}>
    {{ $depth . $culture->title }}
</option>
@if ($culture->children->count())
    @foreach($culture->children as $child)
        @include('admin.catalog.product.partials._culture_option', ['culture' => $child, 'depth' => $depth . '-'])
    @endforeach
@endif
