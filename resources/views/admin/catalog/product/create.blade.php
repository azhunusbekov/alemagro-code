@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Товары - новый</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.product.index') }}">Каталог - Товары</a></li>
            <li><span>Создать новый товар</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.product.store', $category) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @include('admin.catalog.product._form', ['mode' => 'create'])
        </form>
    </div>
@endsection
