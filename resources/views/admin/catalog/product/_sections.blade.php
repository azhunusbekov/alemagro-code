<div class="form-group {{ $errors->has('section_id') ? 'is-invalid' : '' }}">
    <label class="col-form-label">Раздел</label>
    <select name="section_id" class="form-control select2" data-placeholder="Выбрать раздел">
        <option></option>
        @foreach($sections as $section)
            <option value="{{ $section->id }}"
                    {{ isset($product) && $product->section_id == $section->id ? 'selected' : '' }}
                    {!! $section->children->count() ? 'disabled' : '' !!}>
                {{ $section->title }}
            </option>
            @if ($section->children->count())
                @foreach($section->children as $child)
                    <option value="{{ $child->id }}"
                        {{ isset($product) && $product->section_id == $child->id ? 'selected' : '' }}>
                         -- {{ $child->title }}
                    </option>
                @endforeach
            @endif
        @endforeach
    </select>
</div>