@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Разделы</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.section.index') }}">Каталог - Все Разделы</a></li>
            <li><a href="{{ route('admin.catalog.section.edit', $section) }}">{{ $section->title }}</a></li>
            <li><span>Создать перевод</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.section.translation.store', $section) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @include('admin.catalog.section.translation._form', ['mode' => 'create'])
        </form>
    </div>
@endsection