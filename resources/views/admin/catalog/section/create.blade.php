@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Новый раздел</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.section.index') }}">Каталог - Все разделы</a></li>
            <li><span>Создать новый раздел</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.section.store') }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @include('admin.catalog.section._form', ['mode' => 'create'])
        </form>
    </div>
@endsection