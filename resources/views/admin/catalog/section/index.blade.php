@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Разделы</h4>
        @if ($parent)
            <ul class="breadcrumbs">
                <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                <li><a href="{{ route('admin.catalog.section.index') }}">Каталог - Все разделы</a></li>
                <li><span>{{ $parent->title }}</span></li>
            </ul>
        @endif
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <a href="{{ route('admin.catalog.section.create', ['parent_id' => $parent]) }}" class="btn btn-primary">Создать новый раздел</a>
                </div>
                @if (!$parent)
                    <div class="card-header__filter">
                        <form action="{{ route('admin.catalog.section.index') }}" method="get">
                            <div class="form-group">
                                <select name="category_id" class="form-control select2">
                                    <option value="">Все категории</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}"
                                            {!! request()->get('category_id') == $category->id ? 'selected' : '' !!}>
                                            {{ $category->title }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn btn-warning" type="submit">Фильтр</button>
                        </form>
                    </div>
                @endif
            </div>
            <div class="card-body">
                @if ($sections->isNotEmpty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="min-width: 60px;">ID</th>
                                        <th style="width: 70%;">Заголовок</th>
                                        <th style="width: 20%;">Категория</th>
                                        <th style="min-width: 200px;">Подразделы</th>
                                        <th style="min-width: 120px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($sections as $section)
                                        <tr data-id="{{ $section->id }}">
                                            <th>{{ $section->id }}</th>
                                            <td>
                                                @if ($section->children->count())
                                                    <a href="{{ route('admin.catalog.section.index', ['parent_id' => $section]) }}">
                                                        {{ $section->title }}
                                                    </a>
                                                @else
                                                    {{ $section->title }}
                                                @endif
                                            </td>
                                            <td>{{ $section->category->title }}</td>
                                            <td>
                                                @if ($section->children->count())
                                                    <span class="badge badge-primary badge-pill">{{ $section->children->count() }}</span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.catalog.section.edit', $section) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a href="{{ route('admin.catalog.section.index', ['parent_id' => $section]) }}" class="btn btn-xs btn-warning">
                                                        <i class="fa fa-list"></i>
                                                    </a>
                                                    <button class="btn btn-xs btn-secondary btn-sort-handle">
                                                        <i class="fa fa-arrows"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
		$(function(){
            sortable('.sortable', {
                items: 'tr' ,
                forcePlaceholderSize: true,
                placeholderClass: 'sortable-placeholder',
                hoverClass: 'sortable-hover',
                handle: '.btn-sort-handle',
                itemSerializer: (serializedItem, sortableContainer) => {
                    return {
                        id: serializedItem.node.getAttribute('data-id'),
                        position:  serializedItem.index + 1
                    }
                }
            })[0].addEventListener('sortupdate', function(e) {
                const serialize = sortable('.sortable', 'serialize');
                const items = serialize[0].items;

                $.post({
                    url: '/inside/catalog/section/reorder',
                    data: {
                        items: items
                    },
                    beforeSend: function() {
                        $('.overlay').show();
                    },
                    success: function(response){
                        $('.overlay').hide();
                        $('.sortable').find('.updated').removeClass('updated');
                    }
                });
            });
		});
    </script>
@endpush