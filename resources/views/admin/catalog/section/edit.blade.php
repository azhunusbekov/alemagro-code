@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Разделы - <i>{{ $section->title }}</i></h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.section.index') }}">Каталог - Все разделы</a></li>
            @if ($section->parent)
                <li><a href="{{ route('admin.catalog.section.index', ['parent_id' => $section->parent->id]) }}">{{ $section->parent->title }}</a></li>
            @endif
            <li><span>{{ $section->title }}</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.section.update', $section) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.catalog.section._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection