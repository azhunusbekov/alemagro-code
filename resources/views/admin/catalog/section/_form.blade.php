<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.catalog.section.index') }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($section) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.catalog.section.destroy', $section) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i><span>Сохранить</span></button>
        </div>
        @if (isset($section))
            <div class="card-header__actions">
                @foreach($section->translations as $translation)
                    <a href="{{ route('admin.catalog.section.translation.edit', [$section, $translation]) }}" class="btn btn-outline-info"><span>{{ locale_name($translation->locale) }}</span></a>
                @endforeach
                <a href="{{ route('admin.catalog.section.translation.create', $section) }}" class="btn btn-info"><i class="fa fa-language"></i><span>Добавить перевод</span></a>
            </div>
        @endif
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        @if (isset($parent))
            <input type="hidden" name="parent_id" value="{{ $parent->id }}">
        @endif
        <div class="form-group {{ $errors->has('category_id') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Категория</label>
            <select name="category_id" class="form-control select2" data-placeholder="Выбрать категорию">
                <option></option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}"
                            {{ isset($section) && $section->category_id == $category->id ? 'selected' : '' }}
                            {{ isset($parent) && $parent->category_id == $category->id ? 'selected' : '' }}>
                        {{ $category->title }}
                    </option>
                @endforeach
            </select>
            {!! $errors->first('category_id','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Заголовок</label>
            <input name="title" type="text" class="form-control" value="{{ isset($section) ? $section->title : old('title') }}">
            {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('slug') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Slug (алиас в URL)</label>
            <input name="slug" type="text" class="form-control" value="{{ isset($section) ? $section->slug : old('slug') }}">
            {!! $errors->first('slug','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('description') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Описание</label>
            <textarea name="description" class="form-control editor" rows="10">{{ isset($section) ? $section->description : old('title') }}</textarea>
            {!! $errors->first('description','<span class="invalid-feedback">:message</span>') !!}
        </div>
        @include('admin.partial.seo_fields', ['seoData' => isset($seoData) ? $seoData : null])
    </div>
</div>