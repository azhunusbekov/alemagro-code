<div class="card">
    <div class="card-header">
        <div class="card-header__actions">
            <a href="{{ route('admin.catalog.disease.index') }}" class="btn btn-secondary">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
            @if (isset($disease) && isset($mode) && $mode === 'edit')
                <button class="btn btn-danger"
                        title="Удалить"
                        data-action="destroy"
                        data-href="{{ route('admin.catalog.disease.destroy', $disease) }}">
                    <i class="fa fa-trash"></i>
                    <span>Удалить</span>
                </button>
            @endif
            <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> <span>Сохранить</span></button>
        </div>
        @if (isset($disease))
            <div class="card-header__actions">
                @foreach($disease->translations as $translation)git
                    <a href="{{ route('admin.catalog.disease.translation.edit', [$disease, $translation]) }}" class="btn btn-outline-info"><span>{{ locale_name($translation->locale) }}</span></a>
                @endforeach
                <a href="{{ route('admin.catalog.disease.translation.create', $disease) }}" class="btn btn-info"><i class="fa fa-language"></i><span>Добавить перевод</span></a>
            </div>
        @endif
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">Ошибка валидации</div>
        @endif
        <div class="form-group {{ $errors->has('category_id') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Категория</label>
            <select name="category_id" class="form-control select2" data-placeholder="Выбрать категорию">
                <option></option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}"
                        {{ isset($disease) && $disease->category_id == $category->id ? 'selected' : '' }}>
                        {{ $category->title }}
                    </option>
                @endforeach
            </select>
            {!! $errors->first('category_id','<span class="invalid-feedback">:message</span>') !!}
        </div>
        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
            <label class="col-form-label">Заголовок</label>
            <input name="title" type="text" class="form-control" value="{{ isset($disease) ? $disease->title : old('title') }}">
            {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
        </div>
    </div>
</div>