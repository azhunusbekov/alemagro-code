@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Вредные объекты, болезни</h4>
    </div>
    <div class="main-content-inner">
        <div class="card">
            <div class="card-header">
                <div class="card-header__actions">
                    <a href="{{ route('admin.catalog.disease.create') }}" class="btn btn-primary">Создать новый</a>
                </div>
            </div>
            <div class="card-body">
                @if ($diseases->isNotEmpty())
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="text-uppercase bg-light">
                                    <tr>
                                        <th style="min-width: 60px;">ID</th>
                                        <th style="width: 60%;">Заголовок</th>
                                        <th style="width: 40%;">Категория</th>
                                        <th style="min-width: 120px;">Действия</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    @foreach($diseases as $disease)
                                        <tr data-id="{{ $disease->id }}">
                                            <th>{{ $disease->id }}</th>
                                            <td>{{ $disease->title }}</td>
                                            <td>{{ $disease->category->title }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('admin.catalog.disease.edit', $disease) }}" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <h4 class="header-title">Empty data</h4>
                @endif
            </div>
        </div>
    </div>
@endsection