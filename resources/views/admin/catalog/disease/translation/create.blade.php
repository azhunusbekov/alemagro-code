@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Вредные объекты, болезни</h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.disease.index') }}">Каталог - Вредные объекты, болезни</a></li>
            <li><a href="{{ route('admin.catalog.disease.edit', $disease) }}">{{ $disease->title }}</a></li>
            <li><span>Создать перевод</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.disease.translation.store', $disease) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @include('admin.catalog.disease.translation._form', ['mode' => 'create'])
        </form>
    </div>
@endsection