@extends('admin.layouts.main')

@section('content')
    <div class="page-title-area">
        <h4 class="page-title">Каталог - Вредные объекты, болезни - <i>{{ $disease->title }}</i></h4>
        <ul class="breadcrumbs">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.catalog.disease.index') }}">Каталог - Все вредные объекты</a></li>
            <li><span>{{ $disease->title }}</span></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <form action="{{ route('admin.catalog.disease.update', $disease) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.catalog.disease._form', ['mode' => 'edit'])
        </form>
    </div>
@endsection