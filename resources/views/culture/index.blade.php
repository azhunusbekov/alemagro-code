@extends('layouts.app')

@section('content')
    <section class="page-preview" style="background-image:url('/img/crops/crops-bg.jpg')">
        <div class="container page-preview__container">
            <h1 class="page-preview__title">{{ __('catalog.cultures') }}</h1>
        </div>
    </section>
    <section class="page-substrate">
        <h4 class="page-substrate__title page-substrate__title--crops">{{ __('catalog.about_cultures') }}</h4>
    </section>
    <section class="crops">
        <div class="container">
            <div class="crops__items">
                <div class="row">
                    @foreach($cultures as $culture)
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <a class="crops__item"
                               href="{{ lang_route('culture.culture', ['catalog_category' => $categoryDefault->slug, 'culture_path' => $culture->getPath()]) }}"
                               style="background-image:url({{ $culture->image }})">
                                <div class="crops__name">{{ $culture->title }}</div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
