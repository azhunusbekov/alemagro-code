@php
    $isActive = false;
    if (isset($culture) && $culture->id == $cultureItem->id) {
        $isActive = true;
    } else if (isset($parentIsActive) && $parentIsActive) {
        $isActive = true;
    }
@endphp
<div class="filters__input-wrap">
    @if($cultureItem->children)
        <a class="filters__dropdown" href="#"></a>
    @endif
    <a href="{{ lang_route('culture.culture', ['catalog_category' => $category->slug, 'culture_path' => $parentPath = $cultureItem->getPath(null, isset($parentPath) ? $parentPath : null)]) }}"
       class="filters__label filters__link filters__link--radio {{ $isActive ? 'filters__link--active' : '' }}">
        {{ $cultureItem->title }}
    </a>
    @if($cultureItem->children)
        <div class="filters__children">
            @foreach($cultureItem->children as $child)
                @include('culture.partials._culture_item', ['cultureItem' => $child, 'parentIsActive' => $isActive, 'parentPath' => $parentPath])
            @endforeach
        </div>
    @endif
</div>
