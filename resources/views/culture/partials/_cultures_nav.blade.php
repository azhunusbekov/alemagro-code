<div class="filters__item">
    <a class="js-filters__header filters__header" href="#">
        {{ __('catalog.cultures') }}
    </a>
    <div class="filters__body">
        <div class="filters__search-wrap">
            <input type="text" id="cultures-search" class="filters__search" placeholder="{{ __('catalog.filter.search_cultures') }}"/>
            <button class="filters__search-btn" type="button"></button>
        </div>
        <div class="js-filters__group filters__group" id="cultures-filter-group">
            @if(isset($culturesFavorites) && $culturesFavorites->isNotEmpty())
                <div class="filters__input-wrap">
                    <span class="filters__label filters__link filters__link--radio">
                        {{ __('catalog.filter.favorite_cultures') }}
                    </span>
                    <div class="filters__children">
                        @foreach($culturesFavorites as $cultureFavorite)
                            <div class="filters__input-wrap">
                                <a href="{{ lang_route('culture.culture', ['catalog_category' => $category->slug, 'culture_path' => $cultureFavorite->getPath(isset($parent) ? $parent : null)]) }}"
                                   class="filters__label filters__link filters__link--radio {{ isset($culture) && $culture->id == $cultureFavorite->id ? 'filters__link--active' : '' }}">
                                    {{ $cultureFavorite->title }}
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            @foreach($cultures as $cultureItem)
                @include('culture.partials._culture_item', ['cultureItem' => $cultureItem])
            @endforeach
        </div>
    </div>
</div>
