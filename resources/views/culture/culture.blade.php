@extends('layouts.app')

@section('content')
    <section class="page-preview" style="background-image:url({{ $culture->image_header ?? '/img/crops-catalog.jpg' }})">
        <div class="container page-preview__container">
            <h1 class="page-preview__title">{{ $culture->title }}</h1>
        </div>
    </section>
    <section class="crops-catalog">
        <div class="container crops-catalog__container">
            <aside class="filters crops-catalog__filters">
                <form class="filters__form" action="{{ lang_route('culture.culture', ['catalog_category' => $category->slug, 'culture_path' => $culture->getPath()]) }}" method="GET">
                    <div class="filters__items">
                        @if (isset($categories) && $categories->count())
                            @include('culture.partials._categories_nav', ['categories' => $categories])
                        @endif
                        @if (isset($cultures) && count($cultures))
                            @include('culture.partials._cultures_nav', ['cultures' => $cultures])
                        @endif
                        @if (isset($sections) && $sections->count())
                            @include('catalog.filters._sections', ['sections' => $sections])
                        @endif
                        @if (isset($diseases) && $diseases->count())
                            @include('catalog.filters._diseases', ['diseases' => $diseases])
                        @endif
                        @if (isset($filterableAttributes) && $filterableAttributes->count())
                            @include('catalog.filters._attributes', ['attributes' => $filterableAttributes])
                        @endif
                        @if (isset($producers) && $producers->count())
                            @include('catalog.filters._producers', ['producers' => $producers])
                        @endif
                    </div>
                    <div class="filters__btn-group">
                        <div class="filters__btn-wrap">
                            <button class="btn btn--blue btn--filters" type="submit">{{ __('catalog.filter.submit') }}</button>
                        </div>
                        <div class="filters__reset-wrap">
                            <button class="btn btn--reset" type="reset">{{ __('catalog.filter.reset') }}</button>
                        </div>
                    </div>
                </form>
            </aside>
            <div class="crops-catalog__content">
                {{ Breadcrumbs::view('vendor.breadcrumbs.culture', 'culture.culture', $category, $culture) }}
                @if($products->isNotEmpty())
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-sm-push-7">
                            <div class="pagination crops-catalog__pagination">
                                {{ $products->onEachSide(1)->links('vendor.pagination.simple') }}
                            </div>
                        </div>
                    </div>
                    <div class="crops-catalog__items">
                        <div class="crops-catalog__header-wrap">
                            <div class="crops-catalog__header">
                                <a class="crops-catalog__col crops-catalog__col--header {{ sort_link_active($filters, 'title') ? 'crops-catalog__col--active' : '' }}"
                                   href="{{ sort_link($filters, 'title') }}">
                                    <div class="crops-catalog__header-text crops-catalog__header-text--sort">{{ __('catalog.product_label') }}</div>
                                </a>
                                @if($category->isSemena())
                                    @foreach($categoryAttributes as $attribute)
                                        <a class="crops-catalog__col crops-catalog__col--header {{ sort_link_active($filters, 'attr', $attribute->id) ? 'crops-catalog__col--active' : '' }}"
                                           href="{{ sort_link($filters, 'attr', $attribute->id) }}">
                                            <div class="crops-catalog__header-text crops-catalog__header-text--sort">{{ $attribute->name }}</div>
                                        </a>
                                    @endforeach
                                    <a class="crops-catalog__col crops-catalog__col--header {{ sort_link_active($filters, 'description') ? 'crops-catalog__col--active' : '' }}"
                                       href="{{ sort_link($filters, 'description') }}">
                                        <div class="crops-catalog__header-text crops-catalog__header-text--sort">{{ __('catalog.description_label') }}</div>
                                    </a>
                                @elseif($category->isFertilizer())
                                    <a class="crops-catalog__col crops-catalog__col--header {{ sort_link_active($filters, 'section') ? 'crops-catalog__col--active' : '' }}"
                                       href="{{ sort_link($filters, 'section') }}">
                                        <div class="crops-catalog__header-text crops-catalog__header-text--sort">{{ __('catalog.category_label') }}</div>
                                    </a>
                                    <a class="crops-catalog__col crops-catalog__col--header {{ sort_link_active($filters, 'sub_section') ? 'crops-catalog__col--active' : '' }}"
                                       href="{{ sort_link($filters, 'sub_section') }}">
                                        <div class="crops-catalog__header-text crops-catalog__header-text--sort">{{ __('catalog.purpose_label') }}</div>
                                    </a>
                                    <a class="crops-catalog__col crops-catalog__col--header {{ sort_link_active($filters, 'composition') ? 'crops-catalog__col--active' : '' }}"
                                       href="{{ sort_link($filters, 'composition') }}">
                                        <div class="crops-catalog__header-text crops-catalog__header-text--sort">{{ __('catalog.composition_label') }}</div>
                                    </a>
                                @elseif($category->isProtection())
                                    <a class="crops-catalog__col crops-catalog__col--header {{ sort_link_active($filters, 'section') ? 'crops-catalog__col--active' : '' }}"
                                       href="{{ sort_link($filters, 'section') }}">
                                        <div class="crops-catalog__header-text crops-catalog__header-text--sort">{{ __('catalog.category_label') }}</div>
                                    </a>
                                    <span class="crops-catalog__col">
                                        <div class="crops-catalog__header-text">{{ __('catalog.diseases') }}</div>
                                    </span>
                                    @foreach($categoryAttributes as $attribute)
                                        <a class="crops-catalog__col crops-catalog__col--header {{ sort_link_active($filters, 'attr', $attribute->id) ? 'crops-catalog__col--active' : '' }}"
                                           href="{{ sort_link($filters, 'attr', $attribute->id) }}">
                                            <div class="crops-catalog__header-text crops-catalog__header-text--sort">{{ $attribute->name }}</div>
                                        </a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="crops-catalog__body">
                            @foreach($products as $product)
                                <div class="crops-catalog__item">
                                    <div class="crops-catalog__col">
                                        <a class="crops-catalog__pic"
                                           href="{{ $product->getLink(null, $category) }}"
                                           style="background-image:url({{ $product->preview }})">
                                            <div class="crops-catalog__name">{{ $product->title }}</div>
                                        </a>
                                    </div>
                                    @if($category->isSemena())
                                        @foreach($categoryAttributes as $attribute)
                                            <div class="crops-catalog__col">
                                                <div class="crops-catalog__desc">
                                                    {{ $product->getValueOfAttribute($attribute->id) }}
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="crops-catalog__col">
                                            <div class="crops-catalog__desc">
                                                {!! $product->description !!}
                                            </div>
                                        </div>
                                    @elseif($category->isFertilizer())
                                        <div class="crops-catalog__col">
                                            <div class="crops-catalog__desc">
                                                @if($product->section->parent)
                                                    {{ $product->section->parent->title }}
                                                @else
                                                    {{ $product->section->title }}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="crops-catalog__col">
                                            <div class="crops-catalog__desc">
                                                @if($product->section->parent)
                                                    {{ $product->section->title }}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="crops-catalog__col">
                                            <div class="crops-catalog__desc">
                                                {!! $product->composition !!}
                                            </div>
                                        </div>
                                    @elseif($category->isProtection())
                                        <div class="crops-catalog__col">
                                            <div class="crops-catalog__desc">
                                                @if($product->section->parent)
                                                    {{ $product->section->parent->title }}
                                                @else
                                                    {{ $product->section->title }}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="crops-catalog__col">
                                            <div class="crops-catalog__desc">
                                                @foreach($product->diseases as $disease)
                                                    {{ $disease->title }} {{ !$loop->last ? ', ' : '' }}
                                                @endforeach
                                            </div>
                                        </div>
                                        @foreach($categoryAttributes as $attribute)
                                            <div class="crops-catalog__col">
                                                <div class="crops-catalog__desc">
                                                    {{ $product->getValueOfAttribute($attribute->id) }}
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <a class="js-page-scroll-top page-scroll-top" href="#">{{ __('button.go_up') }}</a>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="pagination">
                                {{ $products->onEachSide(1)->links('vendor.pagination.simple') }}
                            </div>
                        </div>
                    </div>
                @else
                    <div class="alert">{!! __('catalog.empty_data') !!}</div>
                @endif
                @if($culture->description)
                    <div class="crops-catalog__footer-text">
                        {!! $culture->description !!}
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
