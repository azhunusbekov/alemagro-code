@extends('layouts.app')

@section('content')
    <section class="page-preview" style="background-image:url('/img/about/header_bg.jpg')">
        <div class="container page-preview__container">
            <h1 class="page-preview__title">{{ __('catalog.products_title_' . $countryCode) }}</h1>
        </div>
    </section>
    <section class="page-substrate">
        <h4 class="page-substrate__title page-substrate__title--solution">{{ __('catalog.subtitle') }}</h4>
    </section>
    <section class="products">
        <div class="container products__container">
            <aside class="filters">
                <form class="filters__form" action="{{ lang_route('catalog.category', ['country_code' => $countryCode, 'catalog_category' => optional($categories->first())->slug]) }}" method="GET">
                    <div class="filters__items">
                        @if (isset($producers) && $producers->count())
                            @include('catalog.filters._producers', ['producers' => $producers])
                        @endif
                        @if (isset($cultures) && count($cultures))
                            @include('catalog.filters._cultures', ['cultures' => $cultures])
                        @endif
                    </div>
                    <div class="filters__btn-group">
                        <div class="filters__btn-wrap">
                            <button class="btn btn--blue btn--filters" type="submit">{{ __('catalog.filter.submit') }}</button>
                        </div>
                        <div class="filters__reset-wrap">
                            <button class="btn btn--reset" type="reset">{{ __('catalog.filter.reset') }}</button>
                        </div>
                    </div>
                </form>
            </aside>
            <div class="products__content">
                {{ Breadcrumbs::view('vendor.breadcrumbs.catalog_main', 'catalog.main', $countryCode) }}
                <div class="products__items">
                    <div class="row">
                        @foreach($categories as $category)
                            <div class="col-xs-6 col-sm-4">
                                <div class="products__item">
                                    <div class="products__icon-wrap">
                                        <div class="products__icon" style="background-image:url({{ $category->icon }})"></div>
                                    </div>
                                    <h3 class="products__name">
                                        <a href="{{ lang_route('catalog.category', ['country_code' => $countryCode, 'catalog_category' => $category->slug]) }}">{{ $category->title }}</a>
                                    </h3>
                                    @if ($loop->first)
                                        @foreach($cultures as $culture)
                                            <a href="{{ lang_route('catalog.category', ['country_code' => $countryCode, 'catalog_category' => $category->slug, 'cultures[]' => $culture->id]) }}" class="products__link">{{ $culture->title }}</a>
                                        @endforeach
                                    @elseif($category->sections->count())
                                        @foreach($category->sections as $section)
                                            <a href="{{ $section->getLink($countryCode, $category) }}" class="products__link">{{ $section->title }}</a>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
