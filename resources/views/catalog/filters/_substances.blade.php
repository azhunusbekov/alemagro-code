<div class="filters__item">
    <a class="js-filters__header filters__header" href="#">{{ __('catalog.substances') }}</a>
    <div class="filters__body">
        <div class="filters__search-wrap">
            <input type="text" id="substances-search" class="filters__search" placeholder="{{ __('catalog.filter.search_substances') }}"/>
            <button class="filters__search-btn" type="button"></button>
        </div>
        <div class="js-filters__group filters__group" id="substances-filter-group">
            @foreach($substances as $substance)
                <div class="filters__input-wrap">
                    <input type="checkbox"
                           class="js-filters__checkbox filters__checkbox"
                           id="substance_{{ $substance->id }}"
                           name="substances[]"
                           value="{{ $substance->id }}"
                        {{ isset($filters['substances']) && in_array($substance->id, $filters['substances']) ? 'checked' : '' }}
                    />
                    <label class="filters__label filters__label--checkbox" for="substance_{{ $substance->id }}">{{ $substance->title }}</label>
                </div>
            @endforeach
        </div>
    </div>
</div>
