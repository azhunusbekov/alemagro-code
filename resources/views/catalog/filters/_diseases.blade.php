<div class="filters__item">
    <a class="js-filters__header filters__header" href="#">{{ __('catalog.diseases') }}</a>
    <div class="filters__body">
        <div class="filters__search-wrap">
            <input type="text" id="diseases-search" class="filters__search" placeholder="{{ __('catalog.filter.search_diseases') }}"/>
            <button class="filters__search-btn" type="button"></button>
        </div>
        <div class="js-filters__group filters__group" id="diseases-filter-group">
            @foreach($diseases as $disease)
                <div class="filters__input-wrap">
                    <input type="checkbox"
                           class="js-filters__checkbox filters__checkbox"
                           id="disease_{{ $disease->id }}"
                           name="diseases[]"
                           value="{{ $disease->id }}"
                        {{ isset($filters['diseases']) && in_array($disease->id, $filters['diseases']) ? 'checked' : '' }}
                    />
                    <label class="filters__label filters__label--checkbox" for="disease_{{ $disease->id }}">{{ $disease->title }}</label>
                </div>
            @endforeach
        </div>
    </div>
</div>
