<div class="filters__item">
    <a class="js-filters__header filters__header" href="#">
        {{ __('catalog.cultures') }}
    </a>
    <div class="filters__body">
        <div class="filters__search-wrap">
            <input type="text" id="cultures-search" class="filters__search" placeholder="{{ __('catalog.filter.search_cultures') }}"/>
            <button class="filters__search-btn" type="button"></button>
        </div>
        <div class="js-filters__group filters__group" id="cultures-filter-group">
            @if(isset($culturesFavorites) && $culturesFavorites->isNotEmpty())
                <div class="filters__input-wrap">
                    <input type="checkbox"
                           class="js-filters__checkbox filters__checkbox"
                           id="culture_favorite_group"
                    />
                    <label class="filters__label filters__label--checkbox"
                           for="culture_favorite_group">
                        {{ __('catalog.filter.favorite_cultures') }}
                    </label>
                    <div class="filters__children">
                        @foreach($culturesFavorites as $culture)
                            <div class="filters__input-wrap" id="cultures-favorite">
                                <input type="checkbox"
                                       class="js-filters__checkbox filters__checkbox"
                                       id="culture_favorite_{{ $culture->id }}"
                                       name="cultures[]"
                                       value="{{ $culture->id }}"
                                       data-culture-id="{{ $culture->id }}"
                                    {{ isset($filters['cultures']) && in_array($culture->id, $filters['cultures']) ? 'checked' : '' }}
                                />
                                <label class="filters__label filters__label--checkbox"
                                       for="culture_favorite_{{ $culture->id }}">
                                    {{ $culture->title }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            @foreach($cultures as $culture)
                @include('catalog.filters._culture_item', ['culture' => $culture])
            @endforeach
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(function() {
            $('#cultures-favorite .filters__checkbox').on('change', function(e) {
                $('#culture_' + $(this).data('culture-id')).trigger('click');
            });
        });
    </script>
@endpush
