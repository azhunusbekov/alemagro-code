<div class="filters__input-wrap">
    @if($culture->children)
        <a class="filters__dropdown" href="#"></a>
    @endif
    <input type="checkbox"
           class="js-filters__checkbox filters__checkbox"
           id="culture_{{ $culture->id }}"
           name="cultures[]"
           value="{{ $culture->id }}"
            {{ isset($filters['cultures']) && in_array($culture->id, $filters['cultures']) ? 'checked' : '' }}
    />
    <label class="filters__label filters__label--checkbox"
           for="culture_{{ $culture->id }}">
        {{ $culture->title }}
    </label>
    @if($culture->children)
        <div class="filters__children">
            @foreach($culture->children as $child)
                @include('catalog.filters._culture_item', ['culture' => $child])
            @endforeach
        </div>
    @endif
</div>
