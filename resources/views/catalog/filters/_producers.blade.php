<div class="filters__item">
    <a class="js-filters__header filters__header" href="#">{{ __('catalog.producers') }}</a>
    <div class="filters__body">
        <div class="js-filters__group filters__group filters__group--manufacturers">
            @foreach($producers as $producer)
                <div class="filters__input-wrap">
                    <input type="checkbox"
                           class="js-filters__checkbox filters__checkbox"
                           id="producer_{{ $producer->id }}"
                           name="producers[]"
                           value="{{ $producer->id }}"
                        {{ isset($filters['producers']) && in_array($producer->id, $filters['producers']) ? 'checked' : '' }}
                    />
                    <label class="filters__label filters__label--checkbox" for="producer_{{ $producer->id }}">{{ $producer->title }}</label>
                </div>
            @endforeach
        </div>
    </div>
</div>