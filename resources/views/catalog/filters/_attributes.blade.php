@foreach($attributes as $attribute)
    <div class="filters__item">
        <a class="js-filters__header filters__header" href="#">{{ $attribute->name }}</a>
        <div class="filters__body">
            <div class="js-filters__group filters__group filters__group--attributes">
                @foreach((array) $attribute->options as $index => $value)
                    <div class="filters__input-wrap">
                        <input type="checkbox"
                               class="js-filters__checkbox filters__checkbox"
                               id="attribute_option_{{ $attribute->id . '_' . $loop->iteration }}"
                               name="attributes[]"
                               value="{{ $value }}"
                            {{ isset($filters['attributes']) && in_array($value, $filters['attributes']) ? 'checked' : '' }}
                        />
                        <label class="filters__label filters__label--checkbox" for="attribute_option_{{ $attribute->id . '_' . $loop->iteration }}">{{ $value }}</label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endforeach
