<div class="filters__item">
    <a class="js-filters__header filters__header" href="#">
        {{ __('catalog.filter_sections') }}
    </a>
    <div class="filters__body">
        <div class="filters__search-wrap">
            <input type="text" id="sections-search" class="filters__search" placeholder="{{ __('catalog.filter.search_sections') }}"/>
            <button class="filters__search-btn" type="button"></button>
        </div>
        <div class="js-filters__group filters__group" id="sections-filter-group">
            @foreach($sections as $section)
                <div class="filters__input-wrap">
                    @if($section->children)
                        <a class="filters__dropdown" href="#"></a>
                    @endif
                    <input type="checkbox"
                           class="js-filters__checkbox filters__checkbox"
                           id="section_{{ $section->id }}"
                           name="sections[]"
                           value="{{ $section->id }}"
                        {{ isset($filters['sections']) && in_array($section->id, $filters['sections']) ? 'checked' : '' }}
                    />
                    <label class="filters__label filters__label--checkbox"
                           for="section_{{ $section->id }}">
                        {{ $section->title }}
                    </label>
                    @if($section->children)
                        <div class="filters__children">
                            @foreach($section->children as $child)
                                <div class="filters__input-wrap">
                                    <input type="checkbox"
                                           class="js-filters__checkbox filters__checkbox"
                                           id="section_{{ $child->id }}"
                                           name="sections[]"
                                           value="{{ $child->id }}"
                                        {{ isset($filters['sections']) && in_array($child->id, $filters['sections']) ? 'checked' : '' }}
                                    />
                                    <label class="filters__label filters__label--checkbox"
                                           for="section_{{ $child->id }}">
                                        {{ $child->title }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
</div>
