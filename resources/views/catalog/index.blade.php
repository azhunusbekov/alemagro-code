@extends('layouts.app')

@section('content')
    <div class="product-page">
        <section class="main-production">
            <div class="main-production__item" style="background-image:url('/img/kz.jpg')">
                <div class="main-production__text">
                    <h4 class="main-production__title">{{ __('home.products_kz') }}</h4>
                    <div class="main-production__btn-wrap">
                        <a class="btn btn--green" href="{{ lang_route('catalog.main', 'kz') }}">{{ __('button.more') }}</a>
                    </div>
                </div>
            </div>
            <div class="main-production__item" style="background-image:url('/img/kg.jpg')">
                <div class="main-production__text">
                    <h4 class="main-production__title">{{ __('home.products_kg') }}</h4>
                    <div class="main-production__btn-wrap">
                        <a class="btn btn--green" href="{{ lang_route('catalog.main', 'kg') }}">{{ __('button.more') }}</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
