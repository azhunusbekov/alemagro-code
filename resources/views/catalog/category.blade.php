@extends('layouts.app')

@section('content')
    <section class="page-preview" style="background-image:url({{ $category->image_header ?? '/img/products-catalog.jpg' }})">
        <div class="container page-preview__container">
            <h1 class="page-preview__title">{{ $category->title }}</h1>
        </div>
    </section>
    <section class="products-catalog">
        <div class="container products-catalog__container">
            <aside class="filters products-catalog__filters">
                <form class="filters__form" action="{{ lang_route('catalog.category', ['country_code' => $countryCode, 'catalog_category' => $category->slug]) }}" method="GET">
                    <div class="filters__items">
                        @if (isset($categories) && $categories->count())
                            @include('catalog.partials._categories_nav', ['categories' => $categories])
                        @endif
                        @if (isset($sections) && $sections->count())
                            @include('catalog.partials._sections_nav', ['sections' => $sections])
                        @endif
                        @if (isset($cultures) && count($cultures))
                            @include('catalog.filters._cultures', ['cultures' => $cultures])
                        @endif
                        @if (isset($diseases) && $diseases->count())
                            @include('catalog.filters._diseases', ['diseases' => $diseases])
                        @endif
                        @if (isset($substances) && $substances->count())
                            @include('catalog.filters._substances', ['substances' => $substances])
                        @endif
                        @if (isset($producers) && $producers->count())
                            @include('catalog.filters._producers', ['producers' => $producers])
                        @endif
                    </div>
                    <div class="filters__btn-group">
                        <div class="filters__btn-wrap">
                            <button class="btn btn--blue btn--filters" type="submit">{{ __('catalog.filter.submit') }}</button>
                        </div>
                        <div class="filters__reset-wrap">
                            <button class="btn btn--reset" type="reset">{{ __('catalog.filter.reset') }}</button>
                        </div>
                    </div>
                </form>
            </aside>
            <div class="products-catalog__content">
                {{ Breadcrumbs::view('vendor.breadcrumbs.catalog_inner', 'catalog.category', $countryCode, $category) }}
                @if($products->isNotEmpty())
                    @include('catalog.partials._product_list_header')
                    @if($cultureProducts)
                        <div class="products-catalog__categories">
                            @foreach($cultureProducts as $item)
                                @if($item->productItems)
                                    <div class="products-catalog__category">
                                        <h3 class="products-catalog__category-title">{{ $item->title }}</h3>
                                        <div class="products-catalog__sub-categories">
                                            @include('catalog.partials._culture_products', ['cultureProducts' => $item->children])
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @else
                        <div class="products-catalog__items">
                            <div class="row products-catalog__row">
                                @foreach($products as $product)
                                    @include('catalog.partials._product_card')
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <a class="js-page-scroll-top page-scroll-top" href="#">{{ __('button.go_up') }}</a>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="pagination">
                                {{ $products->links('vendor.pagination.default') }}
                            </div>
                        </div>
                    </div>
                @else
                    <div class="alert">{!! __('catalog.empty_data') !!}</div>
                @endif
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script>
        $('#products-sort').change(function() {
            this.form.submit();
        });
    </script>
@endpush
