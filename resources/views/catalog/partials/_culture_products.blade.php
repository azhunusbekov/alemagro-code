@foreach($cultureProducts as $item)
    @if($item->productItems)
        <div class="products-catalog__sub-category">
            <h4 class="products-catalog__sub-category-title">{{ $item->title }}</h4>
            @if($item->children)
                @include('catalog.partials._culture_products', ['cultureProducts' => $item->children])
            @else
                <div class="products-catalog__items">
                    <div class="row products-catalog__row">
                        @foreach($item->productItems as $product)
                            <div class="col-xs-12 col-sm-4 products-catalog__col">
                                <a class="products-catalog__item" href="{{ $product->getLink($countryCode, $category) }}">
                                    <div class="products-catalog__item-category">{{ $product->producer->title }}</div>
                                    <div class="products-catalog__pic-wrap">
                                        <div class="products-catalog__pic"
                                             style="background-image:url('{{ $item->icon }}')">
                                        </div>
                                    </div>
                                    <div class="products-catalog__name">{{ $product->title }}</div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    @endif
@endforeach
