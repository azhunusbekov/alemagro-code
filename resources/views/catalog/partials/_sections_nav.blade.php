<div class="filters__item">
    <a class="js-filters__header filters__header" href="#">{{ __('catalog.sections') }}</a>
    <div class="filters__body">
        <div class="filters__search-wrap">
            <input type="text" id="sections-search" class="filters__search" placeholder="{{ __('catalog.filter.search_section') }}"/>
            <button class="filters__search-btn" type="button"></button>
        </div>
        <div class="js-filters__group filters__group" id="sections-filter-group">
            @foreach($sections as $sectionItem)
                <div class="filters__input-wrap">
                    @if($sectionItem->children->count())
                        <a class="filters__dropdown" href="#"></a>
                    @endif
                    <a href="{{ $sectionItem->getLink($countryCode, $category) }}"
                       class="filters__label filters__link filters__link--radio {{ isset($section) && $section->id == $sectionItem->id ? 'filters__link--active' : '' }}">
                        {{ $sectionItem->title }}
                    </a>
                    @if($sectionItem->children->count())
                        <div class="filters__children">
                            @foreach($sectionItem->children as $child)
                                <div class="filters__input-wrap">
                                    <a href="{{ $child->getLink($countryCode, $category, $sectionItem) }}"
                                       class="filters__label filters__link filters__link--radio {{ isset($section) && $section->id == $child->id ? 'filters__link--active' : '' }}">
                                        {{ $child->title }}
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
</div>
