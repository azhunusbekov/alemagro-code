<div class="row">
    <div class="col-xs-12 col-sm-3 products-catalog__col-filter">
        <div class="catalog-search js-catalog-search">
            <a class="js-catalog-search__icon catalog-search__icon" href="#"></a>
            <form class="catalog-search__form"
                  action="{{ lang_route('catalog.category', ['country_code' => $countryCode, 'catalog_category' => $category->slug]) }}"
                  method="GET">
                @if (count($filters))
                    @foreach($filters as $key => $value)
                        @if (is_array($value))
                            @foreach($value as $subValue)
                                <input type="hidden" name="{{ $key }}[]" value="{{ $subValue }}">
                            @endforeach
                        @else
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                        @endif
                    @endforeach
                @endif
                <input class="catalog-search__input" type="text" name="search"/>
                <button class="catalog-search__submit" type="submit"></button>
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-sm-2 products-catalog__col-filter">
        <div class="catalog-sorting">
{{--            <form action="{{ lang_route('catalog.category', ['country_code' => $countryCode, 'catalog_category' => $category->slug]) }}"--}}
{{--                  method="GET">--}}
{{--                @if (count($filters))--}}
{{--                    @foreach($filters as $key => $value)--}}
{{--                        @if (is_array($value))--}}
{{--                            @foreach($value as $subValue)--}}
{{--                                <input type="hidden" name="{{ $key }}[]" value="{{ $subValue }}">--}}
{{--                            @endforeach--}}
{{--                        @endif--}}
{{--                    @endforeach--}}
{{--                @endif--}}
{{--                <label class="catalog-sorting__name" for="products-sort">{{ __('catalog.sorting') }}</label>--}}
{{--                <select class="catalog-sorting__select" id="products-sort" name="sort">--}}
{{--                    <option value="alphabetically"--}}
{{--                            {{ isset($filters['sort']) && $filters['sort'] == 'alphabetically' ? 'selected' : ''}}>--}}
{{--                        {{ __('catalog.sorting_by_alphabetically') }}--}}
{{--                    </option>--}}
{{--                    <option value="id"--}}
{{--                            {{ isset($filters['sort']) && $filters['sort'] == 'id' ? 'selected' : ''}}>--}}
{{--                        ID--}}
{{--                    </option>--}}
{{--                </select>--}}
{{--            </form>--}}
        </div>
    </div>
    <div class="col-xs-12 col-sm-7 products-catalog__col-filter">
        <div class="pagination products-catalog__pagination">
            {{ $products->links('vendor.pagination.default') }}
        </div>
    </div>
</div>
