<div class="filters__item">
    <a class="js-filters__header filters__header" href="#">{{ __('catalog.categories') }}</a>
    <div class="filters__body">
        <div class="js-filters__group filters__group">
            @foreach($categories as $categoryItem)
                <div class="filters__input-wrap">
                    <a href="{{ lang_route('catalog.category', ['country_code' => $countryCode, 'catalog_category' => $categoryItem->slug]) }}"
                       class="filters__label filters__link filters__link--radio {{ $category->id == $categoryItem->id ? 'filters__link--active' : '' }}">
                        {{ $categoryItem->title }}
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>