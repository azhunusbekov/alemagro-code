<div class="col-xs-12 col-sm-4 products-catalog__col">
    <a class="products-catalog__item" href="{{ $product->getLink($countryCode, $category) }}">
        <div class="products-catalog__pic-wrap">
            <div class="products-catalog__pic" style="background-image:url({{ $product->preview }})"></div>
        </div>
        <div class="products-catalog__name">{{ $product->title }}</div>
    </a>
</div>
