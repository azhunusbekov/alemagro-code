@extends('layouts.app')

@section('content')
    <section class="product-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 product-page__col">
                    <div class="product-page__pic-wrap">
                        <img class="product-page__pic" src="{{ $product->preview }}" alt="{{ $product->title }}"/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 product-page__col">
                    <div class="product-page__content">
                        {{ Breadcrumbs::view('vendor.breadcrumbs.catalog_product', 'catalog.product', $countryCode, $category, $section, $product) }}
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <h4 class="product-page__subtitle">{{ $product->producer->title }}</h4>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <span class="product-page__vendor-code">{{ $product->vendor_code }}</span>
                            </div>
                        </div>
                        <h2 class="product-page__title">{{ $product->title }}</h2>
                        <div class="product-page__btn-wrap">
                            <a class="btn btn--green product-page__btn" href="#" data-toggle="modal" data-target="#modal-product-feedback">{{ __('button.order') }}</a>
                        </div>
                        @if($productCultures->isNotEmpty())
                            <div class="product-page__info">
                                <h4 class="product-page__info-title">{{ __('catalog.cultures') }}</h4>
                                <p class="product-page__info-desc">
                                    @foreach($productCultures as $culture)
                                        {{ $culture->title }} {{ !$loop->last ? ', ' : '' }}
                                    @endforeach
                                </p>
                            </div>
                        @endif
                        @if($productDiseases->isNotEmpty())
                            <div class="product-page__info">
                                <h4 class="product-page__info-title">{{ __('catalog.diseases') }}</h4>
                                <p class="product-page__info-desc">
                                    @foreach($productDiseases as $culture)
                                        {{ $culture->title }} {{ !$loop->last ? ', ' : '' }}
                                    @endforeach
                                </p>
                            </div>
                        @endif
                        @if($productSubstances->isNotEmpty())
                            <div class="product-page__info">
                                <h4 class="product-page__info-title">{{ __('catalog.substances') }}</h4>
                                <p class="product-page__info-desc">
                                    @foreach($productSubstances as $culture)
                                        {{ $culture->title }} {{ !$loop->last ? ', ' : '' }}
                                    @endforeach
                                </p>
                            </div>
                        @endif
                        @if ($categoryAttributes->isNotEmpty() && $productAttributes->isNotEmpty())
                            <div class="product-page__info">
                                <h4 class="product-page__info-title">{{ __('catalog.product_details') }}</h4>
                                @foreach($categoryAttributes as $attribute)
                                    @if ($value = $product->getValueOfAttribute($attribute->id))
                                        <div class="product-page__characteristic-item">
                                            <h5 class="product-page__characteristic-name">{{ $attribute->name }}:</h5>
                                            <p class="product-page__characteristic-desc">{{ $value }}</p>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif
                        @if ($product->composition)
                            <div class="product-page__info">
                                <h4 class="product-page__info-title">{{ __('catalog.product_composition') }}</h4>
                                <div class="product-page__info-desc">
                                    {!! $product->composition !!}
                                </div>
                            </div>
                        @endif
                        <div class="product-page__info">
                            <h4 class="product-page__info-title">{{ __('catalog.product_description') }}</h4>
                            <div class="product-page__info-desc">
                                {!! $product->description !!}
                            </div>
                        </div>
                        @if ($product->protection->first())
                            <div class="product-page__download-wrap">
                                <a href="{{ $product->protection->first()->src }}" class="product-page__download" target="_blank">{{ __('catalog.download_protection') }}</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal-product modal fade" id="modal-product-feedback" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button class="close" type="button" data-dismiss="modal">&times;</button>
                <div class="modal__pic" style="background-image:url('/img/modal.png')"></div>
                <div class="modal__content">
                    <h3 class="modal__title">Заказать продукт</h3>
                    <form class="feedback-request__form ajax-form" action="{{ lang_route('feedback.product') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div id="alert-error" class="alert alert-danger" style="display: none">
                            {{ __('general.feedback.error') }}
                        </div>
                        <div id="alert-success" class="alert alert-success" style="display: none">
                            {{ __('general.feedback.success') }}
                        </div>
                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                        <input type="hidden" name="product_title" value="{{ $product->title }}">
                        <input type="hidden" name="url" value="{{ request()->url() }}">
                        <input type="hidden" name="title" value="{{ $product->title }}">
                        <div class="modal__form-group">
                            <select class="modal__select selectpicker" name="region">
                                <option disabled selected>{{ __('general.feedback.input_region') }}</option>
                                @foreach($feedbackOffices as $office)
                                    <option value="{{ $office->alias }}">{{ $office->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="modal__form-group">
                            <input class="modal__input" type="text" name="name" placeholder="{{ __('catalog.feedback.input_name') }}"/>
                        </div>
                        <div class="modal__form-group">
                            <input class="modal__input" type="tel" name="phone" placeholder="{{ __('catalog.feedback.input_phone') }}"/>
                        </div>
                        <div class="modal__form-group">
                            <input class="modal__input" type="email" name="email" placeholder="{{ __('catalog.feedback.input_email') }}"/>
                        </div>
                        <div class="modal__btn-wrap">
                            <button class="btn btn--primary btn--blue" type="submit">{{ __('button.submit') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
