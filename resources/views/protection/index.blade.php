@extends('layouts.app')

@section('content')
    <section class="page-preview" style="background-image:url('/img/safe-program-bg.jpg')">
        <div class="container page-preview__container">
            <h1 class="page-preview__title">{{ __('catalog.protection_nutrition_title') }}</h1>
        </div>
    </section>
    <section class="safe-program">
        <div class="container safe-program__container">
            <aside class="filters safe-program__filters">
                <form class="filters__form" action="{{ lang_route('protection.index') }}" method="GET">
                    <div class="filters__items">
                        <div class="filters__item">
                            <a class="js-filters__header filters__header" href="#">{{ __('catalog.purpose') }} ({{ count($purposes) }})</a>
                            <div class="filters__body">
                                <div class="js-filters__group filters__group">
                                    @foreach($purposes as $key => $purpose)
                                        <div class="filters__input-wrap">
                                            <input type="checkbox"
                                                   class="js-filters__checkbox filters__checkbox"
                                                   id="{{ $key }}"
                                                   name="purposes[]"
                                                   value="{{ $key }}"
                                                    {{ isset($filters['purposes']) && in_array($key, $filters['purposes']) ? 'checked' : '' }}
                                            />
                                            <label class="filters__label filters__label--checkbox" for="{{ $key }}">{{ $purpose }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @if (isset($cultures) && count($cultures))
                            @include('catalog.filters._cultures', ['cultures' => $cultures])
                        @endif
                        @if (isset($producers) && $producers->count())
                            @include('catalog.filters._producers', ['producers' => $producers])
                        @endif
                    </div>
                    <div class="filters__btn-group">
                        <div class="filters__btn-wrap">
                            <button class="btn btn--blue btn--filters" type="submit">{{ __('catalog.filter.submit') }}</button>
                        </div>
                        <div class="filters__reset-wrap">
                            <button class="btn btn--reset" type="reset">{{ __('catalog.filter.reset') }}</button>
                        </div>
                    </div>
                </form>
            </aside>
            <div class="safe-program__content">
                @if($protections->isNotEmpty())
                    <div class="pagination safe-program__pagination">
                        {{ $protections->links('vendor.pagination.default') }}
                    </div>
                    <div class="safe-program__items">
                        <div class="row safe-program__row">
                            @foreach($protections as $protection)
                                <div class="col-xs-12 col-sm-4 safe-program__col">
                                    <a href="#"
                                       class="safe-program__item"
                                       data-toggle="modal"
                                       data-target="#modal-safe-program"
                                       data-link="{{ $protection->src }}"
                                       data-title="{{ $protection->title }}"
                                       data-link-document="{{ $protection->src }}">
                                        @if($protection->producer->image)
                                            <div class="safe-program__pic" style="background-image:url({{ $protection->producer->image }})"></div>
                                        @endif
                                        <div class="safe-program__name">{{ $protection->title }}</div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <a class="js-page-scroll-top page-scroll-top" href="#">{{ __('button.go_up') }}</a>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="pagination">
                                {{ $protections->links('vendor.pagination.default') }}
                            </div>
                        </div>
                    </div>
                @else
                    <div class="alert">{!! __('catalog.empty_data') !!}</div>
                @endif
            </div>
        </div>
    </section>
@endsection
