@extends('layouts.app')

@section('content')
    <section class="page-preview" style="background-image:url('/img/article/header_bg.jpg')">
        <div class="container page-preview__container">
            {{ Breadcrumbs::view('vendor.breadcrumbs.header', 'article') }}
            <h1 class="page-preview__title">{{ __('article.page_title') }}</h1>
        </div>
    </section>
    <section class="page-news">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    @include('article.partials._categories_nav', $categories)
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="pagination">
                        {{ $articles->links('vendor.pagination.default') }}
                    </div>
                </div>
            </div>
            <div class="page-news__list">
                <div class="row">
                    @foreach($articles as $article)
                        @include('article.partials._article_card', $article)
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <a class="js-page-scroll-top page-scroll-top" href="#">{{ __('button.go_up') }}</a>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="pagination">
                        {{ $articles->links('vendor.pagination.default') }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
