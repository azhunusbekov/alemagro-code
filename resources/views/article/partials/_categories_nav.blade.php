<div class="filter-news">
    @foreach($categories as $categoryItem)
        <div class="filter-news__item">
            <a class="filter-news__link {{ isset($category) && $category->id == $categoryItem->id ? 'filter-news__link--active' : '' }}" href="{{ lang_route('article.category', $categoryItem->slug) }}">{{ $categoryItem->title }}</a>
        </div>
    @endforeach
</div>