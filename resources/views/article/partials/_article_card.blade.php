<div class="col-xs-12 col-sm-4">
    <a class="page-news__item"
       href="{{ lang_route('article.article', ['category_slug' => $article->category->slug, 'article_slug' => $article->slug]) }}"
       style="background-image:url({{ $article->preview }})">
        <div class="page-news__text">
            <span class="page-news__date">{{ $article->created_at->format('d.m.Y') }}</span>
            <div class="page-news__title">{{ $article->title }}</div>
        </div>
    </a>
</div>