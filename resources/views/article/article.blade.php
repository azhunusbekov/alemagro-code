@extends('layouts.app')

@section('content')
    <section class="page-preview" style="background-image:url({{ $article->image_header ? $article->image_header : '/img/article/header_bg.jpg' }})">
        <div class="container page-preview__container">
            {{ Breadcrumbs::view('vendor.breadcrumbs.header', 'article.article', $category, $article) }}
            <h4 class="page-preview__date">{{ $article->created_at->format('m.d.Y') }}</h4>
            <h1 class="page-preview__title">{{ $article->title }}</h1>
        </div>
    </section>
    <section class="page-default-content">
        <div class="container">
            {!! $article->description !!}
        </div>
    </section>
@endsection
